!> @file STDLIB_CODATA_1973.f90
!! @author Robert Apthorpe
!! @copyright See LICENSE
!!
!! @brief Literal and derived fundamental physical constants consistent
!! with CODATA Bulletin No. 11, December, 1973.

!> @brief Literal and derived fundamental physical constants consistent
!! with CODATA Bulletin No. 11, "CODATA Recommended Values of the
!! Fundamental Physical Constants", December, 1973.
!!
!! Unless otherwise noted, all data has been taken from @cite Cohen1973
module STDLIB_CODATA_1973
use, intrinsic :: ISO_FORTRAN_ENV, only: R4 => REAL32, R8 => REAL64,    &
  R16 => REAL128
implicit none

public :: check

private

! Constants in REAL128 precision, selected_real_kind(33, 4931)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 33 significant digits.
real(kind=R16), parameter, public :: pi_R16 =                           &
  3.14159265358979323846264338327950_R16

! Constants in REAL64 precision, selected_real_kind(15, 307)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 15 significant digits.
real(kind=R8), parameter, public :: pi_R8 =                             &
  real(pi_R16, kind=R8)

! Constants in REAL32 precision, selected_real_kind(6, 37)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 6 significant digits.
real(kind=R4), parameter, public :: pi_R4 =                             &
  real(pi_R16, kind=R4)

!-----

!> Speed of light in vacuum, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  speed_of_light_in_vacuum_R16 =                                        &
  2.99792458e8_R16

!> Speed of light in vacuum, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  speed_of_light_in_vacuum_R8 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R8)

!> Speed of light in vacuum, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  speed_of_light_in_vacuum_R4 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R4)

!> Vacuum electric permittivity, F m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  vacuum_electric_permittivity_R16 =                                    &
  8.854187818e-12_R16

!> Vacuum electric permittivity, F m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  vacuum_electric_permittivity_R8 =                                     &
  real(vacuum_electric_permittivity_R16, kind=R8)

!> Vacuum electric permittivity, F m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  vacuum_electric_permittivity_R4 =                                     &
  real(vacuum_electric_permittivity_R16, kind=R4)

!> Fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  fine_structure_constant_R16 =                                         &
  7.2973506e-3_R16

!> Fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  fine_structure_constant_R8 =                                          &
  real(fine_structure_constant_R16, kind=R8)

!> Fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  fine_structure_constant_R4 =                                          &
  real(fine_structure_constant_R16, kind=R4)

!> Inverse fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_fine_structure_constant_R16 =                                 &
  137.03604_R16

!> Inverse fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_fine_structure_constant_R8 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R8)

!> Inverse fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_fine_structure_constant_R4 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R4)

!> Elementary charge, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  elementary_charge_R16 =                                               &
  1.6021892e-19_R16

!> Elementary charge, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  elementary_charge_R8 =                                                &
  real(elementary_charge_R16, kind=R8)

!> Elementary charge, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  elementary_charge_R4 =                                                &
  real(elementary_charge_R16, kind=R4)

!> Planck constant, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_constant_R16 =                                                 &
  6.626176e-34_R16

!> Planck constant, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_constant_R8 =                                                  &
  real(Planck_constant_R16, kind=R8)

!> Planck constant, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_constant_R4 =                                                  &
  real(Planck_constant_R16, kind=R4)

!> Planck constant over 2 pi, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_R16 =                                         &
  1.0545887e-34_R16

!> Planck constant over 2 pi, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_R8 =                                          &
  real(reduced_Planck_constant_R16, kind=R8)

!> Planck constant over 2 pi, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_R4 =                                          &
  real(reduced_Planck_constant_R16, kind=R4)

!> Avogadro constant, mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Avogadro_constant_R16 =                                               &
  6.022045e23_R16

!> Avogadro constant, mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Avogadro_constant_R8 =                                                &
  real(Avogadro_constant_R16, kind=R8)

!> Avogadro constant, mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Avogadro_constant_R4 =                                                &
  real(Avogadro_constant_R16, kind=R4)

!> Atomic mass constant, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_R16 =                                            &
  1.6605655e-27_R16

!> Atomic mass constant, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_R8 =                                             &
  real(atomic_mass_constant_R16, kind=R8)

!> Atomic mass constant, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_R4 =                                             &
  real(atomic_mass_constant_R16, kind=R4)

!> Electron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_R16 =                                                   &
  9.109534e-31_R16

!> Electron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_R8 =                                                    &
  real(electron_mass_R16, kind=R8)

!> Electron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_R4 =                                                    &
  real(electron_mass_R16, kind=R4)

!> Electron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_in_u_R16 =                                              &
  5.4858026e-4_R16

!> Electron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_in_u_R8 =                                               &
  real(electron_mass_in_u_R16, kind=R8)

!> Electron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_in_u_R4 =                                               &
  real(electron_mass_in_u_R16, kind=R4)

!> Proton mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_R16 =                                                     &
  1.6726485e-27_R16

!> Proton mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_R8 =                                                      &
  real(proton_mass_R16, kind=R8)

!> Proton mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_R4 =                                                      &
  real(proton_mass_R16, kind=R4)

!> Proton mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_in_u_R16 =                                                &
  1.007276470_R16

!> Proton mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_in_u_R8 =                                                 &
  real(proton_mass_in_u_R16, kind=R8)

!> Proton mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_in_u_R4 =                                                 &
  real(proton_mass_in_u_R16, kind=R4)

!> Proton-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_electron_mass_ratio_R16 =                                      &
  1836.15152_R16

!> Proton-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_electron_mass_ratio_R8 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R8)

!> Proton-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_electron_mass_ratio_R4 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R4)

!> Neutron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_R16 =                                                    &
  1.6749543e-27_R16

!> Neutron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_R8 =                                                     &
  real(neutron_mass_R16, kind=R8)

!> Neutron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_R4 =                                                     &
  real(neutron_mass_R16, kind=R4)

!> Neutron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_in_u_R16 =                                               &
  1.008665012_R16

!> Neutron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_in_u_R8 =                                                &
  real(neutron_mass_in_u_R16, kind=R8)

!> Neutron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_in_u_R4 =                                                &
  real(neutron_mass_in_u_R16, kind=R4)

!> Electron charge to mass quotient, C kg^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_charge_to_mass_quotient_R16 =                                &
  1.7588047e11_R16

!> Electron charge to mass quotient, C kg^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R8 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R8)

!> Electron charge to mass quotient, C kg^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R4 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R4)

!> Mag. flux quantum, Wb
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  magnetic_flux_quantum_R16 =                                               &
  2.0678506e-15_R16

!> Mag. flux quantum, Wb
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  magnetic_flux_quantum_R8 =                                                &
  real(magnetic_flux_quantum_R16, kind=R8)

!> Mag. flux quantum, Wb
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  magnetic_flux_quantum_R4 =                                                &
  real(magnetic_flux_quantum_R16, kind=R4)

!> Josephson constant, Hz V^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Josephson_constant_R16 =                                              &
  4.835939e14_R16

!> Josephson constant, Hz V^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Josephson_constant_R8 =                                               &
  real(Josephson_constant_R16, kind=R8)

!> Josephson constant, Hz V^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Josephson_constant_R4 =                                               &
  real(Josephson_constant_R16, kind=R4)

!> Quantum of circulation, m^2 s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  quantum_of_circulation_R16 =                                          &
  3.6369455e-4_R16

!> Quantum of circulation, m^2 s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  quantum_of_circulation_R8 =                                           &
  real(quantum_of_circulation_R16, kind=R8)

!> Quantum of circulation, m^2 s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  quantum_of_circulation_R4 =                                           &
  real(quantum_of_circulation_R16, kind=R4)

!> Faraday constant, C kmol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Faraday_constant_kmol_R16 =                                           &
  9.648456e7_R16

!> Faraday constant, C kmol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Faraday_constant_kmol_R8 =                                            &
  real(Faraday_constant_kmol_R16, kind=R8)

!> Faraday constant, C kmol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Faraday_constant_kmol_R4 =                                            &
  real(Faraday_constant_kmol_R16, kind=R4)

!> Rydberg constant, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_R16 =                                                &
  1.097373177e7_R16

!> Rydberg constant, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_R8 =                                                 &
  real(Rydberg_constant_R16, kind=R8)

!> Rydberg constant, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_R4 =                                                 &
  real(Rydberg_constant_R16, kind=R4)

!> Bohr radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_radius_R16 =                                                     &
  5.2917706e-11_R16

!> Bohr radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_radius_R8 =                                                      &
  real(Bohr_radius_R16, kind=R8)

!> Bohr radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_radius_R4 =                                                      &
  real(Bohr_radius_R16, kind=R4)

!> Classical electron radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  classical_electron_radius_R16 =                                       &
  2.8179380e-15_R16

!> Classical electron radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  classical_electron_radius_R8 =                                        &
  real(classical_electron_radius_R16, kind=R8)

!> Classical electron radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  classical_electron_radius_R4 =                                        &
  real(classical_electron_radius_R16, kind=R4)

!> Thomson cross section, m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Thomson_cross_section_R16 =                                           &
  6.652448e-29_R16

!> Thomson cross section, m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Thomson_cross_section_R8 =                                            &
  real(Thomson_cross_section_R16, kind=R8)

!> Thomson cross section, m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Thomson_cross_section_R4 =                                            &
  real(Thomson_cross_section_R16, kind=R4)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                       &
  1.0011596567_R16

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                        &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                        &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Muon g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_g_factor_R16 =                                                   &
  -2.0_R16 * 1.00116616_R16

!> Muon g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_g_factor_R8 =                                                    &
  real(muon_g_factor_R16, kind=R8)

!> Muon g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_g_factor_R4 =                                                    &
  real(muon_g_factor_R16, kind=R4)

!> Bohr magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_R16 =                                                   &
  9.274078e-24_R16

!> Bohr magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_R8 =                                                    &
  real(Bohr_magneton_R16, kind=R8)

!> Bohr magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_R4 =                                                    &
  real(Bohr_magneton_R16, kind=R4)

!> Electron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_R16 =                                              &
  -9.284832e-24_R16

!> Electron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_R8 =                                               &
  real(electron_magnetic_moment_R16, kind=R8)

!> Electron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_R4 =                                               &
  real(electron_magnetic_moment_R16, kind=R4)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_R16 =                                           &
  2.6751301e8_R16

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R8 =                                            &
  real(proton_gyromagnetic_ratio_R16, kind=R8)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R4 =                                            &
  real(proton_gyromagnetic_ratio_R16, kind=R4)

!> Proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_in_MHz_T_R16 =                                 &
  42.57602_R16

!> Proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R8 =                                  &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R4 =                                  &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R16 =                         &
  1.521032209e-3_R16

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R8 =                          &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R4 =                          &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_proton_magnetic_moment_ratio_R16 =                                 &
  -658.2106880_R16

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R8 =                                  &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R4 =                                  &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Proton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_R16 =                                                &
  1.4106171e-26_R16

!> Proton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_R8 =                                                 &
  real(proton_magnetic_moment_R16, kind=R8)

!> Proton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_R4 =                                                 &
  real(proton_magnetic_moment_R16, kind=R4)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R16 =                      &
  2.7927740_R16

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R8 =                       &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R4 =                       &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Nuclear magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_R16 =                                                &
  5.050824e-27_R16

!> Nuclear magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_R8 =                                                 &
  real(nuclear_magneton_R16, kind=R8)

!> Nuclear magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_R4 =                                                 &
  real(nuclear_magneton_R16, kind=R4)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_proton_magnetic_moment_ratio_R16 =                                     &
  -3.1833402_R16

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R8 =                                      &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R8)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R4 =                                      &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R4)

!> Muon mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_R16 =                                                  &
  -4.490474e-26_R16

!> Muon mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_R8 =                                                   &
  real(muon_magnetic_moment_R16, kind=R8)

!> Muon mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_R4 =                                                   &
  real(muon_magnetic_moment_R16, kind=R4)

!> Muon-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_electron_mass_ratio_R16 =                                        &
  206.76865_R16

!> Muon-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_electron_mass_ratio_R8 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R8)

!> Muon-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_electron_mass_ratio_R4 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R4)

!> Muon mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_R16 =                                                       &
  1.883566e-28_R16

!> Muon mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_R8 =                                                        &
  real(muon_mass_R16, kind=R8)

!> Muon mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_R4 =                                                        &
  real(muon_mass_R16, kind=R4)

!> Muon mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_in_u_R16 =                                                  &
  0.11342920_R16

!> Muon mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_in_u_R8 =                                                   &
  real(muon_mass_in_u_R16, kind=R8)

!> Muon mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_in_u_R4 =                                                   &
  real(muon_mass_in_u_R16, kind=R4)

!> Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Compton_wavelength_R16 =                                              &
  2.4263089e-12_R16

!> Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Compton_wavelength_R8 =                                               &
  real(Compton_wavelength_R16, kind=R8)

!> Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Compton_wavelength_R4 =                                               &
  real(Compton_wavelength_R16, kind=R4)

!> Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Compton_wavelength_over_2_pi_R16 =                                    &
  3.8615905e-13_R16

!> Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Compton_wavelength_over_2_pi_R8 =                                     &
  real(Compton_wavelength_over_2_pi_R16, kind=R8)

!> Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Compton_wavelength_over_2_pi_R4 =                                     &
  real(Compton_wavelength_over_2_pi_R16, kind=R4)

!> Proton Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_Compton_wavelength_R16 =                                       &
  1.3214099e-15_R16

!> Proton Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_Compton_wavelength_R8 =                                        &
  real(proton_Compton_wavelength_R16, kind=R8)

!> Proton Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_Compton_wavelength_R4 =                                        &
  real(proton_Compton_wavelength_R16, kind=R4)

!> Proton Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_Compton_wavelength_over_2_pi_R16 =                             &
  2.1030892e-16_R16

!> Proton Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_Compton_wavelength_over_2_pi_R8 =                              &
  real(proton_Compton_wavelength_over_2_pi_R16, kind=R8)

!> Proton Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_Compton_wavelength_over_2_pi_R4 =                              &
  real(proton_Compton_wavelength_over_2_pi_R16, kind=R4)

!> Neutron Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_Compton_wavelength_R16 =                                      &
  1.3195909e-15_R16

!> Neutron Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_Compton_wavelength_R8 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R8)

!> Neutron Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_Compton_wavelength_R4 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R4)

!> Neutron Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_Compton_wavelength_over_2_pi_R16 =                            &
  2.1001941e-16_R16

!> Neutron Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_Compton_wavelength_over_2_pi_R8 =                             &
  real(neutron_Compton_wavelength_over_2_pi_R16, kind=R8)

!> Neutron Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_Compton_wavelength_over_2_pi_R4 =                             &
  real(neutron_Compton_wavelength_over_2_pi_R16, kind=R4)

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16 =               &
  22.41383e-3_R16

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R8 =                &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R8)

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R4 =                &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R4)

!> Molar gas constant, J mol^-1 K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_gas_constant_R16 =                                              &
  8.31441_R16

!> Molar gas constant, J mol^-1 K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_gas_constant_R8 =                                               &
  real(molar_gas_constant_R16, kind=R8)

!> Molar gas constant, J mol^-1 K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_gas_constant_R4 =                                               &
  real(molar_gas_constant_R16, kind=R4)

!> Boltzmann constant, J K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_R16 =                                              &
  1.380662e-23_R16

!> Boltzmann constant, J K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_R8 =                                               &
  real(Boltzmann_constant_R16, kind=R8)

!> Boltzmann constant, J K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_R4 =                                               &
  real(Boltzmann_constant_R16, kind=R4)

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Stefan_Boltzmann_constant_R16 =                                       &
  5.67032e-8_R16

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R8 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R8)

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R4 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R4)

!> First radiation constant, J m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  first_radiation_constant_in_j_m_R16 =                                 &
  3.741832e-16_R16

!> First radiation constant, J m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  first_radiation_constant_in_j_m_R8 =                                  &
  real(first_radiation_constant_in_j_m_R16, kind=R8)

!> First radiation constant, J m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  first_radiation_constant_in_j_m_R4 =                                  &
  real(first_radiation_constant_in_j_m_R16, kind=R4)

!> Second radiation constant, m K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  second_radiation_constant_R16 =                                       &
  1.438786e-2_R16

!> Second radiation constant, m K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  second_radiation_constant_R8 =                                        &
  real(second_radiation_constant_R16, kind=R8)

!> Second radiation constant, m K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  second_radiation_constant_R4 =                                        &
  real(second_radiation_constant_R16, kind=R4)

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Newtonian_constant_of_gravitation_R16 =                               &
  6.6720e-11_R16

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R8 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R8)

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R4 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R4)

!-----
! Derived

!> Vacuum mag. permeability, N A^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  vacuum_magnetic_permeability_R16 = 4.0E-7_R16 * pi_R16

!> Vacuum mag. permeability, N A^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  vacuum_magnetic_permeability_R8 =                                         &
  real(vacuum_magnetic_permeability_R16, kind=R8)

!> Vacuum mag. permeability, N A^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  vacuum_magnetic_permeability_R4 =                                         &
  real(vacuum_magnetic_permeability_R16, kind=R4)

!> Faraday constant, C mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Faraday_constant_R16 =                                                &
  Faraday_constant_kmol_R16 * 1.0E-3_R16

!> Faraday constant, C mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Faraday_constant_R8 =                                                 &
  real(Faraday_constant_R16, kind=R8)

!> Faraday constant, C mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Faraday_constant_R4 =                                                 &
  real(Faraday_constant_R16, kind=R4)

!-----
! Conversions

!> Kilogram-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_electron_volt_relationship_R16 =                             &
  5.609545e35_R16

!> Kilogram-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R8 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R8)

!> Kilogram-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R4 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R4)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_energy_equivalent_in_MeV_R16 =                   &
  931.5016_R16

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R8 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R8)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R4 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R4)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_energy_equivalent_in_MeV_R16 =                          &
  0.5110034_R16

!> Electron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R8 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R4 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_energy_equivalent_in_MeV_R16 =                            &
  938.2796_R16

!> Proton mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R8 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R4 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_energy_equivalent_in_MeV_R16 =                           &
  939.5731_R16

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R8 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R4 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Electron volt-joule relationship, J
!! (at most 33 significant digits)
  real(kind=R16), parameter, public ::                                    &
  electron_volt_joule_relationship_R16 =                                &
  1.6021892e-19_R16

!> Electron volt-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_joule_relationship_R8 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R8)

!> Electron volt-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_joule_relationship_R4 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R4)

!> Electron volt-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_hertz_relationship_R16 =                                &
  2.4179696e14_R16

!> Electron volt-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_hertz_relationship_R8 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R8)

!> Electron volt-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_hertz_relationship_R4 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R4)

!> Electron volt-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_inverse_meter_relationship_R16 =                        &
  8.065479e5_R16

!> Electron volt-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R8 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R8)

!> Electron volt-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R4 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R4)

!> Electron volt-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_kelvin_relationship_R16 =                               &
  1.160450e4_R16

!> Electron volt-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R8 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R8)

!> Electron volt-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R4 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R4)

!> Inverse meter-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_electron_volt_relationship_R16 =                        &
  1.2398520e-6_R16

!> Inverse meter-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R8 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R8)

!> Inverse meter-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R4 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R4)

!> Rydberg constant times hc in J, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_J_R16 =                                  &
  2.179907e-18_R16

!> Rydberg constant times hc in J, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R8 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R8)

!> Rydberg constant times hc in J, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R4 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R4)

!> Rydberg constant times hc in eV, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_eV_R16 =                                 &
  13.605804_R16

!> Rydberg constant times hc in eV, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R8 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R8)

!> Rydberg constant times hc in eV, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R4 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R4)

!> Rydberg constant times c in Hz, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_c_in_Hz_R16 =                                  &
  3.28984200e15_R16

!> Rydberg constant times c in Hz, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R8 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R8)

!> Rydberg constant times c in Hz, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R4 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R4)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_eV_T_R16 =                                           &
  5.7883785e-5_R16

!> Bohr magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R8 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R8)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R4 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R4)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_Hz_T_R16 =                                           &
  1.3996123e10_R16

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R8 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R8)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R4 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R4)

!> Bohr magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_inverse_meter_per_tesla_R16 =                       &
  46.68604_R16

!> Bohr magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R8 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Bohr magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R4 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Bohr magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_K_T_R16 =                                            &
  0.671712_R16

!> Bohr magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R8 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R8)

!> Bohr magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R4 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R4)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_eV_T_R16 =                                        &
  3.1524515e-8_R16

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R8 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R8)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R4 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R4)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 33 significant digits)
  real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_MHz_T_R16 =                                       &
  7.622532_R16

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R8 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R8)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R4 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R4)

!> Nuclear magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_inverse_meter_per_tesla_R16 =                     &
  2.5426030e-2_R16

!> Nuclear magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R8 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Nuclear magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R4 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Nuclear magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_K_T_R16 =                                         &
  3.65826e-4_R16

!> Nuclear magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R8 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R8)

!> Nuclear magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R4 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R4)


contains

!> Derived unit checks
subroutine check()
  use, intrinsic :: ISO_FORTRAN_ENV, only: stdout => OUTPUT_UNIT
  implicit none

1 format(A)
! 2 format(ES44.35E4) ! +1.000000000000000000000000000000000000E+0000
3 format(A, ' ', ES44.35E4, ' =? ', A)

continue

write(unit=stdout, fmt=1)                                             &
  '! ---- STDLIB_CODATA_1973 Derived Unit Checks ----'

! write(unit=stdout, fmt=3)                                             &
!   'atomic_unit_of_permittivity_R16',                                  &
!   atomic_unit_of_permittivity_R16,                                    &
!   '1.112 650 056... e-10'

! write(unit=stdout, fmt=3)                                             &
!   'characteristic_impedance_of_vacuum_R16',                           &
!   characteristic_impedance_of_vacuum_R16,                             &
!   '376.730 313 461...'

! write(unit=stdout, fmt=3)                                             &
!   'electric_constant_R16',                                            &
!   electric_constant_R16,                                              &
!   '8.854 187 817... e-12'

! write(unit=stdout, fmt=3)                                             &
!   'hertz_inverse_meter_relationship_R16',                             &
!   hertz_inverse_meter_relationship_R16,                               &
!   '3.335 640 951... e-9'

! write(unit=stdout, fmt=3)                                             &
!   'joule_kilogram_relationship_R16',                                  &
!   joule_kilogram_relationship_R16,                                    &
!   '1.112 650 056... e-17'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_joule_relationship_R16',                                  &
!   kilogram_joule_relationship_R16,                                    &
!   '8.987 551 787... e16'

! write(unit=stdout, fmt=3)                                             &
!   'magnetic_constant_R16',                                                &
!   magnetic_constant_R16,                                                  &
!   '12.566 370 614... e-7'

  return
end subroutine check
end module STDLIB_CODATA_1973