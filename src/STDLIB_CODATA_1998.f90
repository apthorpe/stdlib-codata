!> @file STDLIB_CODATA_1998.f90
!! @author Robert Apthorpe
!! @copyright See LICENSE
!!
!! @brief Literal and derived fundamental physical constants consistent
!! with CODATA Bulletin No. ??

!> @brief Literal and derived fundamental physical constants consistent
!! with CODATA Bulletin No. ??
!!
!! Unless otherwise noted, all data has been taken from ?citation?
module STDLIB_CODATA_1998
use, intrinsic :: ISO_FORTRAN_ENV, only: R4 => REAL32, R8 => REAL64,    &
  R16 => REAL128
implicit none

public :: check

private

! Constants in REAL128 precision, selected_real_kind(33, 4931)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 33 significant digits.
real(kind=R16), parameter, public :: pi_R16 =                           &
  3.14159265358979323846264338327950_R16

! Constants in REAL64 precision, selected_real_kind(15, 307)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 15 significant digits.
real(kind=R8), parameter, public :: pi_R8 =                             &
  real(pi_R16, kind=R8)

! Constants in REAL32 precision, selected_real_kind(6, 37)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 6 significant digits.
real(kind=R4), parameter, public :: pi_R4 =                             &
  real(pi_R16, kind=R4)

!-----
! Universal constants

!> Speed of light in vacuum, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  speed_of_light_in_vacuum_R16 =                                        &
  299792458.0_R16

!> Speed of light in vacuum, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  speed_of_light_in_vacuum_R8 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R8)

!> Speed of light in vacuum, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  speed_of_light_in_vacuum_R4 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R4)

!> Mag. constant, N A^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  magnetic_constant_R16 = 4.0E-7_R16 * pi_R16
!   12.566 370 614... e-7_R16 ! derived

!> Mag. constant, N A^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  magnetic_constant_R8 =                                                    &
  real(magnetic_constant_R16, kind=R8) ! derived

!> Mag. constant, N A^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  magnetic_constant_R4 =                                                    &
  real(magnetic_constant_R16, kind=R4) ! derived

!> Electric constant, F m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electric_constant_R16 =                                               &
  1.0_R16 / ( magnetic_constant_R16                                         &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   8.854 187 817... e-12_R16 ! derived

!> Electric constant, F m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electric_constant_R8 =                                                &
  real(electric_constant_R16, kind=R8) ! derived

!> Electric constant, F m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electric_constant_R4 =                                                &
  real(electric_constant_R16, kind=R4) ! derived

!> Characteristic impedance of vacuum, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  characteristic_impedance_of_vacuum_R16 =                              &
  speed_of_light_in_vacuum_R16 * magnetic_constant_R16

!> Characteristic impedance of vacuum, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  characteristic_impedance_of_vacuum_R8 =                               &
  real(characteristic_impedance_of_vacuum_R16, kind=R8)

!> Characteristic impedance of vacuum, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  characteristic_impedance_of_vacuum_R4 =                               &
  real(characteristic_impedance_of_vacuum_R16, kind=R4)

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Newtonian_constant_of_gravitation_R16 =                               &
  6.673e-11_R16

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R8 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R8)

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R4 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R4)

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Newtonian_constant_of_gravitation_over_h_bar_c_R16 =                  &
  6.707e-39_R16

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_over_h_bar_c_R8 =                   &
  real(Newtonian_constant_of_gravitation_over_h_bar_c_R16, kind=R8)

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_over_h_bar_c_R4 =                   &
  real(Newtonian_constant_of_gravitation_over_h_bar_c_R16, kind=R4)

!> Planck constant, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_constant_R16 =                                                 &
  6.62606876e-34_R16

!> Planck constant, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_constant_R8 =                                                  &
  real(Planck_constant_R16, kind=R8)

!> Planck constant, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_constant_R4 =                                                  &
  real(Planck_constant_R16, kind=R4)

!> Planck constant in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_constant_in_eV_Hz_R16 =                                        &
  4.13566727e-15_R16

!> Planck constant in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_constant_in_eV_Hz_R8 =                                         &
  real(Planck_constant_in_eV_Hz_R16, kind=R8)

!> Planck constant in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_constant_in_eV_Hz_R4 =                                         &
  real(Planck_constant_in_eV_Hz_R16, kind=R4)

!> Planck constant over 2 pi, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_R16 =                                         &
  1.054571596e-34_R16

!> Planck constant over 2 pi, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_R8 =                                          &
  real(reduced_Planck_constant_R16, kind=R8)

!> Planck constant over 2 pi, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_R4 =                                          &
  real(reduced_Planck_constant_R16, kind=R4)

!> Planck constant over 2 pi in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_in_eV_s_R16 =                                 &
  6.58211889e-16_R16

!> Planck constant over 2 pi in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_in_eV_s_R8 =                                  &
  real(reduced_Planck_constant_in_eV_s_R16, kind=R8)

!> Planck constant over 2 pi in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_in_eV_s_R4 =                                  &
  real(reduced_Planck_constant_in_eV_s_R16, kind=R4)

!> Reduced Planck constant times c in MeV fm, MeV fm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_times_c_in_MeV_fm_R16 =                       &
  reduced_Planck_constant_in_eV_s_R16 * speed_of_light_in_vacuum_R16    &
  * 1.0e21_R16
  ! 197.326968_R16

!> Reduced Planck constant times c in MeV fm, MeV fm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_times_c_in_MeV_fm_R8 =                        &
  real(reduced_Planck_constant_times_c_in_MeV_fm_R16, kind=R8)

!> Reduced Planck constant times c in MeV fm, MeV fm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_times_c_in_MeV_fm_R4 =                        &
  real(reduced_Planck_constant_times_c_in_MeV_fm_R16, kind=R4)

!> Planck mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_mass_R16 =                                                     &
  2.1767e-8_R16

!> Planck mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_mass_R8 =                                                      &
  real(Planck_mass_R16, kind=R8)

!> Planck mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_mass_R4 =                                                      &
  real(Planck_mass_R16, kind=R4)

!> Planck length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_length_R16 =                                                   &
  1.6160e-35_R16

!> Planck length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_length_R8 =                                                    &
  real(Planck_length_R16, kind=R8)

!> Planck length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_length_R4 =                                                    &
  real(Planck_length_R16, kind=R4)

!> Planck time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_time_R16 =                                                     &
  5.3906e-44_R16

!> Planck time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_time_R8 =                                                      &
  real(Planck_time_R16, kind=R8)

!> Planck time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_time_R4 =                                                      &
  real(Planck_time_R16, kind=R4)

!-----
! Electromagnetic constants

!> Elementary charge, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  elementary_charge_R16 =                                               &
  1.602176462e-19_R16

!> Elementary charge, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  elementary_charge_R8 =                                                &
  real(elementary_charge_R16, kind=R8)

!> Elementary charge, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  elementary_charge_R4 =                                                &
  real(elementary_charge_R16, kind=R4)

!> Elementary charge over h, A J^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  elementary_charge_over_h_R16 =                                        &
  2.417989491e14_R16

!> Elementary charge over h, A J^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  elementary_charge_over_h_R8 =                                         &
  real(elementary_charge_over_h_R16, kind=R8)

!> Elementary charge over h, A J^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  elementary_charge_over_h_R4 =                                         &
  real(elementary_charge_over_h_R16, kind=R4)

!> Mag. flux quantum, Wb
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  magnetic_flux_quantum_R16 =                                           &
  2.067833636e-15_R16

!> Mag. flux quantum, Wb
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  magnetic_flux_quantum_R8 =                                            &
  real(magnetic_flux_quantum_R16, kind=R8)

!> Mag. flux quantum, Wb
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  magnetic_flux_quantum_R4 =                                            &
  real(magnetic_flux_quantum_R16, kind=R4)

!> Conductance quantum, S
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conductance_quantum_R16 =                                             &
  7.748091696e-5_R16

!> Conductance quantum, S
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conductance_quantum_R8 =                                              &
  real(conductance_quantum_R16, kind=R8)

!> Conductance quantum, S
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conductance_quantum_R4 =                                              &
  real(conductance_quantum_R16, kind=R4)

!> Inverse of conductance quantum, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_of_conductance_quantum_R16 =                                  &
  12906.403786_R16

!> Inverse of conductance quantum, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_of_conductance_quantum_R8 =                                   &
  real(inverse_of_conductance_quantum_R16, kind=R8)

!> Inverse of conductance quantum, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_of_conductance_quantum_R4 =                                   &
  real(inverse_of_conductance_quantum_R16, kind=R4)

!> Josephson constant, Hz V^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Josephson_constant_R16 =                                              &
  483597.898e9_R16

!> Josephson constant, Hz V^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Josephson_constant_R8 =                                               &
  real(Josephson_constant_R16, kind=R8)

!> Josephson constant, Hz V^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Josephson_constant_R4 =                                               &
  real(Josephson_constant_R16, kind=R4)

!> Von Klitzing constant, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  von_Klitzing_constant_R16 =                                           &
  25812.807572_R16

!> Von Klitzing constant, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  von_Klitzing_constant_R8 =                                            &
  real(von_Klitzing_constant_R16, kind=R8)

!> Von Klitzing constant, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  von_Klitzing_constant_R4 =                                            &
  real(von_Klitzing_constant_R16, kind=R4)

!> Bohr magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_R16 =                                                   &
  9.27400899e-24_R16

!> Bohr magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_R8 =                                                    &
  real(Bohr_magneton_R16, kind=R8)

!> Bohr magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_R4 =                                                    &
  real(Bohr_magneton_R16, kind=R4)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_eV_T_R16 =                                           &
  5.788381749e-5_R16

!> Bohr magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R8 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R8)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R4 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R4)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_Hz_T_R16 =                                           &
  13.99624624e9_R16

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R8 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R8)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R4 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R4)

!> Bohr magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_inverse_meter_per_tesla_R16 =                        &
  46.6864521_R16

!> Bohr magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R8 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Bohr magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R4 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Bohr magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_K_T_R16 =                                            &
  0.6717131_R16

!> Bohr magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R8 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R8)

!> Bohr magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R4 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R4)

!> Nuclear magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_R16 =                                                &
  5.05078317e-27_R16

!> Nuclear magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_R8 =                                                 &
  real(nuclear_magneton_R16, kind=R8)

!> Nuclear magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_R4 =                                                 &
  real(nuclear_magneton_R16, kind=R4)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_eV_T_R16 =                                        &
  3.152451238e-8_R16

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R8 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R8)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R4 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R4)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_MHz_T_R16 =                                       &
  7.62259396_R16

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R8 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R8)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R4 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R4)

!> Nuclear magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_inverse_meter_per_tesla_R16 =                     &
  2.54262366e-2_R16

!> Nuclear magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R8 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Nuclear magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R4 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Nuclear magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_K_T_R16 =                                         &
  3.6582638e-4_R16

!> Nuclear magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R8 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R8)

!> Nuclear magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R4 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R4)

!-----
! Atomic constants

!-----
! General

!> Fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  fine_structure_constant_R16 =                                         &
  7.297352533e-3_R16

!> Fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  fine_structure_constant_R8 =                                          &
  real(fine_structure_constant_R16, kind=R8)

!> Fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  fine_structure_constant_R4 =                                          &
  real(fine_structure_constant_R16, kind=R4)

!> Inverse fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_fine_structure_constant_R16 =                                 &
  137.03599976_R16

!> Inverse fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_fine_structure_constant_R8 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R8)

!> Inverse fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_fine_structure_constant_R4 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R4)

!> Rydberg constant, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_R16 =                                                &
  10973731.568549_R16

!> Rydberg constant, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_R8 =                                                 &
  real(Rydberg_constant_R16, kind=R8)

!> Rydberg constant, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_R4 =                                                 &
  real(Rydberg_constant_R16, kind=R4)

!> Rydberg constant times c in Hz, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_c_in_Hz_R16 =                                  &
  3.289841960368e15_R16

!> Rydberg constant times c in Hz, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R8 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R8)

!> Rydberg constant times c in Hz, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R4 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R4)

!> Rydberg constant times hc in J, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_J_R16 =                                  &
  2.17987190e-18_R16

!> Rydberg constant times hc in J, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R8 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R8)

!> Rydberg constant times hc in J, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R4 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R4)

!> Rydberg constant times hc in eV, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_eV_R16 =                                 &
  13.60569172_R16

!> Rydberg constant times hc in eV, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R8 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R8)

!> Rydberg constant times hc in eV, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R4 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R4)

!> Bohr radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_radius_R16 =                                                     &
  0.5291772083e-10_R16

!> Bohr radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_radius_R8 =                                                      &
  real(Bohr_radius_R16, kind=R8)

!> Bohr radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_radius_R4 =                                                      &
  real(Bohr_radius_R16, kind=R4)

!> Hartree energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Hartree_energy_R16 =                                                  &
  4.35974381e-18_R16

!> Hartree energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Hartree_energy_R8 =                                                   &
  real(Hartree_energy_R16, kind=R8)

!> Hartree energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Hartree_energy_R4 =                                                   &
  real(Hartree_energy_R16, kind=R4)

!> Hartree energy in eV, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Hartree_energy_in_eV_R16 =                                            &
  27.2113834_R16

!> Hartree energy in eV, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Hartree_energy_in_eV_R8 =                                             &
  real(Hartree_energy_in_eV_R16, kind=R8)

!> Hartree energy in eV, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Hartree_energy_in_eV_R4 =                                             &
  real(Hartree_energy_in_eV_R16, kind=R4)

!> Quantum of circulation, m^2 s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  quantum_of_circulation_R16 =                                          &
  3.636947516e-4_R16

!> Quantum of circulation, m^2 s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  quantum_of_circulation_R8 =                                           &
  real(quantum_of_circulation_R16, kind=R8)

!> Quantum of circulation, m^2 s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  quantum_of_circulation_R4 =                                           &
  real(quantum_of_circulation_R16, kind=R4)

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  quantum_of_circulation_times_2_R16 =                                  &
  7.273895032e-4_R16

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  quantum_of_circulation_times_2_R8 =                                   &
  real(quantum_of_circulation_times_2_R16, kind=R8)

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  quantum_of_circulation_times_2_R4 =                                   &
  real(quantum_of_circulation_times_2_R16, kind=R4)

!-----
! Electroweak

!> Fermi coupling constant, GeV^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Fermi_coupling_constant_R16 =                                         &
  1.16639e-5_R16

!> Fermi coupling constant, GeV^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Fermi_coupling_constant_R8 =                                          &
  real(Fermi_coupling_constant_R16, kind=R8)

!> Fermi coupling constant, GeV^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Fermi_coupling_constant_R4 =                                          &
  real(Fermi_coupling_constant_R16, kind=R4)

!> Weak mixing angle, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  weak_mixing_angle_R16 =                                               &
  0.2224_R16

!> Weak mixing angle, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  weak_mixing_angle_R8 =                                                &
  real(weak_mixing_angle_R16, kind=R8)

!> Weak mixing angle, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  weak_mixing_angle_R4 =                                                &
  real(weak_mixing_angle_R16, kind=R4)

!-----
! Electron

!> Electron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_R16 =                                                   &
  9.10938188e-31_R16

!> Electron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_R8 =                                                    &
  real(electron_mass_R16, kind=R8)

!> Electron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_R4 =                                                    &
  real(electron_mass_R16, kind=R4)

!> Electron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_in_u_R16 =                                              &
  5.485799110e-4_R16

!> Electron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_in_u_R8 =                                               &
  real(electron_mass_in_u_R16, kind=R8)

!> Electron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_in_u_R4 =                                               &
  real(electron_mass_in_u_R16, kind=R4)

!> Electron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_energy_equivalent_R16 =                                 &
  8.18710414e-14_R16

!> Electron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_energy_equivalent_R8 =                                  &
  real(electron_mass_energy_equivalent_R16, kind=R8)

!> Electron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_energy_equivalent_R4 =                                  &
  real(electron_mass_energy_equivalent_R16, kind=R4)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_energy_equivalent_in_MeV_R16 =                          &
  0.510998902_R16

!> Electron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R8 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R4 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Electron-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_muon_mass_ratio_R16 =                                        &
  4.83633210e-3_R16

!> Electron-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_muon_mass_ratio_R8 =                                         &
  real(electron_muon_mass_ratio_R16, kind=R8)

!> Electron-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_muon_mass_ratio_R4 =                                         &
  real(electron_muon_mass_ratio_R16, kind=R4)

!> Electron-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_tau_mass_ratio_R16 =                                         &
  2.87555e-4_R16

!> Electron-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_tau_mass_ratio_R8 =                                          &
  real(electron_tau_mass_ratio_R16, kind=R8)

!> Electron-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_tau_mass_ratio_R4 =                                          &
  real(electron_tau_mass_ratio_R16, kind=R4)

!> Electron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_proton_mass_ratio_R16 =                                      &
  5.446170232e-4_R16

!> Electron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_proton_mass_ratio_R8 =                                       &
  real(electron_proton_mass_ratio_R16, kind=R8)

!> Electron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_proton_mass_ratio_R4 =                                       &
  real(electron_proton_mass_ratio_R16, kind=R4)

!> Electron-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_neutron_mass_ratio_R16 =                                     &
  5.438673462e-4_R16

!> Electron-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_neutron_mass_ratio_R8 =                                      &
  real(electron_neutron_mass_ratio_R16, kind=R8)

!> Electron-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_neutron_mass_ratio_R4 =                                      &
  real(electron_neutron_mass_ratio_R16, kind=R4)

!> Electron-deuteron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_deuteron_mass_ratio_R16 =                                    &
  2.7244371170e-4_R16

!> Electron-deuteron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_deuteron_mass_ratio_R8 =                                     &
  real(electron_deuteron_mass_ratio_R16, kind=R8)

!> Electron-deuteron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_deuteron_mass_ratio_R4 =                                     &
  real(electron_deuteron_mass_ratio_R16, kind=R4)

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_alpha_particle_mass_ratio_R16 =                           &
  1.3709335611e-4_R16

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_alpha_particle_mass_ratio_R8 =                            &
  real(electron_to_alpha_particle_mass_ratio_R16, kind=R8)

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_alpha_particle_mass_ratio_R4 =                            &
  real(electron_to_alpha_particle_mass_ratio_R16, kind=R4)

!> Electron charge to mass quotient, C kg^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_charge_to_mass_quotient_R16 =                                &
  -1.758820174e11_R16

!> Electron charge to mass quotient, C kg^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R8 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R8)

!> Electron charge to mass quotient, C kg^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R4 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R4)

!> Electron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_molar_mass_R16 =                                             &
  5.485799110e-7_R16

!> Electron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_molar_mass_R8 =                                              &
  real(electron_molar_mass_R16, kind=R8)

!> Electron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_molar_mass_R4 =                                              &
  real(electron_molar_mass_R16, kind=R4)

!> Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Compton_wavelength_R16 =                                              &
  2.426310215e-12_R16

!> Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Compton_wavelength_R8 =                                               &
  real(Compton_wavelength_R16, kind=R8)

!> Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Compton_wavelength_R4 =                                               &
  real(Compton_wavelength_R16, kind=R4)

!> Reduced Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Compton_wavelength_R16 =                                      &
  3.861592642e-13_R16

!> Reduced Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Compton_wavelength_R8 =                                       &
  real(reduced_Compton_wavelength_R16, kind=R8)

!> Reduced Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Compton_wavelength_R4 =                                       &
  real(reduced_Compton_wavelength_R16, kind=R4)

!> Classical electron radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  classical_electron_radius_R16 =                                       &
  2.817940285e-15_R16

!> Classical electron radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  classical_electron_radius_R8 =                                        &
  real(classical_electron_radius_R16, kind=R8)

!> Classical electron radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  classical_electron_radius_R4 =                                        &
  real(classical_electron_radius_R16, kind=R4)

!> Thomson cross section, m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Thomson_cross_section_R16 =                                           &
  6.65245854e-29_R16

!> Thomson cross section, m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Thomson_cross_section_R8 =                                            &
  real(Thomson_cross_section_R16, kind=R8)

!> Thomson cross section, m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Thomson_cross_section_R4 =                                            &
  real(Thomson_cross_section_R16, kind=R4)

!> Electron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_R16 =                                        &
  -9.28476362e-24_R16

!> Electron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_R8 =                                         &
  real(electron_magnetic_moment_R16, kind=R8)

!> Electron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_R4 =                                         &
  real(electron_magnetic_moment_R16, kind=R4)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                 &
  -1.0011596521869_R16

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                  &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                  &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R16 =              &
  -1838.2819660_R16

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R8 =               &
  real(electron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R4 =               &
  real(electron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Electron mag. mom. anomaly, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_anomaly_R16 =                                &
  1.1596521869e-3_R16

!> Electron mag. mom. anomaly, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_anomaly_R8 =                                 &
  real(electron_magnetic_moment_anomaly_R16, kind=R8)

!> Electron mag. mom. anomaly, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_anomaly_R4 =                                 &
  real(electron_magnetic_moment_anomaly_R16, kind=R4)

!> Electron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_g_factor_R16 =                                               &
  -2.0023193043737_R16

!> Electron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_g_factor_R8 =                                                &
  real(electron_g_factor_R16, kind=R8)

!> Electron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_g_factor_R4 =                                                &
  real(electron_g_factor_R16, kind=R4)

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_muon_magnetic_moment_ratio_R16 =                             &
  206.7669720_R16

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_muon_magnetic_moment_ratio_R8 =                              &
  real(electron_muon_magnetic_moment_ratio_R16, kind=R8)

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_muon_magnetic_moment_ratio_R4 =                              &
  real(electron_muon_magnetic_moment_ratio_R16, kind=R4)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_proton_magnetic_moment_ratio_R16 =                           &
  -658.2106875_R16

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R8 =                            &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R4 =                            &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_shielded_proton_magnetic_moment_ratio_R16 =               &
  -658.2275954_R16

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_shielded_proton_magnetic_moment_ratio_R8 =                &
  real(electron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R8)

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_shielded_proton_magnetic_moment_ratio_R4 =                &
  real(electron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R4)

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_neutron_magnetic_moment_ratio_R16 =                          &
  960.92050_R16

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_neutron_magnetic_moment_ratio_R8 =                           &
  real(electron_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_neutron_magnetic_moment_ratio_R4 =                           &
  real(electron_neutron_magnetic_moment_ratio_R16, kind=R4)

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_deuteron_magnetic_moment_ratio_R16 =                         &
  -2143.923498_R16

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_deuteron_magnetic_moment_ratio_R8 =                          &
  real(electron_deuteron_magnetic_moment_ratio_R16, kind=R8)

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_deuteron_magnetic_moment_ratio_R4 =                          &
  real(electron_deuteron_magnetic_moment_ratio_R16, kind=R4)

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_gyromagnetic_ratio_R16 =                                     &
  1.760859794e11_R16

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_gyromagnetic_ratio_R8 =                                      &
  real(electron_gyromagnetic_ratio_R16, kind=R8)

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_gyromagnetic_ratio_R4 =                                      &
  real(electron_gyromagnetic_ratio_R16, kind=R4)

!> Electron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_gyromagnetic_ratio_in_MHz_T_R16 =                            &
  28024.9540_R16

!> Electron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_gyromagnetic_ratio_in_MHz_T_R8 =                             &
  real(electron_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Electron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_gyromagnetic_ratio_in_MHz_T_R4 =                             &
  real(electron_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_shielded_helion_magnetic_moment_ratio_R16 =               &
  864.058255_R16

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_shielded_helion_magnetic_moment_ratio_R8 =                &
  real(electron_to_shielded_helion_magnetic_moment_ratio_R16, kind=R8)

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_shielded_helion_magnetic_moment_ratio_R4 =                &
  real(electron_to_shielded_helion_magnetic_moment_ratio_R16, kind=R4)

!-----
! Muon

!> Muon mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_R16 =                                                       &
  1.88353109e-28_R16

!> Muon mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_R8 =                                                        &
  real(muon_mass_R16, kind=R8)

!> Muon mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_R4 =                                                        &
  real(muon_mass_R16, kind=R4)

!> Muon mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_in_u_R16 =                                                  &
  0.1134289168_R16

!> Muon mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_in_u_R8 =                                                   &
  real(muon_mass_in_u_R16, kind=R8)

!> Muon mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_in_u_R4 =                                                   &
  real(muon_mass_in_u_R16, kind=R4)

!> Muon mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_energy_equivalent_R16 =                                     &
  1.69283332e-11_R16

!> Muon mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_energy_equivalent_R8 =                                      &
  real(muon_mass_energy_equivalent_R16, kind=R8)

!> Muon mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_energy_equivalent_R4 =                                      &
  real(muon_mass_energy_equivalent_R16, kind=R4)

!> Muon mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_energy_equivalent_in_MeV_R16 =                              &
  105.6583568_R16

!> Muon mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_energy_equivalent_in_MeV_R8 =                               &
  real(muon_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Muon mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_energy_equivalent_in_MeV_R4 =                               &
  real(muon_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Muon-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_electron_mass_ratio_R16 =                                        &
  206.7682657_R16

!> Muon-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_electron_mass_ratio_R8 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R8)

!> Muon-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_electron_mass_ratio_R4 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R4)

!> Muon-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_tau_mass_ratio_R16 =                                             &
  5.94572e-2_R16

!> Muon-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_tau_mass_ratio_R8 =                                              &
  real(muon_tau_mass_ratio_R16, kind=R8)

!> Muon-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_tau_mass_ratio_R4 =                                              &
  real(muon_tau_mass_ratio_R16, kind=R4)

!> Muon-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_proton_mass_ratio_R16 =                                          &
  0.1126095173_R16

!> Muon-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_proton_mass_ratio_R8 =                                           &
  real(muon_proton_mass_ratio_R16, kind=R8)

!> Muon-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_proton_mass_ratio_R4 =                                           &
  real(muon_proton_mass_ratio_R16, kind=R4)

!> Muon-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_neutron_mass_ratio_R16 =                                         &
  0.1124545079_R16

!> Muon-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_neutron_mass_ratio_R8 =                                          &
  real(muon_neutron_mass_ratio_R16, kind=R8)

!> Muon-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_neutron_mass_ratio_R4 =                                          &
  real(muon_neutron_mass_ratio_R16, kind=R4)

!> Muon molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_molar_mass_R16 =                                                 &
  1.134289168e-4_R16

!> Muon molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_molar_mass_R8 =                                                  &
  real(muon_molar_mass_R16, kind=R8)

!> Muon molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_molar_mass_R4 =                                                  &
  real(muon_molar_mass_R16, kind=R4)

!> Muon Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_Compton_wavelength_R16 =                                         &
  1.173444197e-14_R16

!> Muon Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_Compton_wavelength_R8 =                                          &
  real(muon_Compton_wavelength_R16, kind=R8)

!> Muon Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_Compton_wavelength_R4 =                                          &
  real(muon_Compton_wavelength_R16, kind=R4)

!> Reduced muon Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_muon_Compton_wavelength_R16 =                                 &
  1.867594444e-15_R16

!> Reduced muon Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_muon_Compton_wavelength_R8 =                                  &
  real(reduced_muon_Compton_wavelength_R16, kind=R8)

!> Reduced muon Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_muon_Compton_wavelength_R4 =                                  &
  real(reduced_muon_Compton_wavelength_R16, kind=R4)

!> Muon mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_R16 =                                            &
  -4.49044813e-26_R16

!> Muon mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_R8 =                                             &
  real(muon_magnetic_moment_R16, kind=R8)

!> Muon mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_R4 =                                             &
  real(muon_magnetic_moment_R16, kind=R4)

!> Muon mag. mom. anomaly, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_anomaly_R16 =                                    &
  1.16591602e-3_R16

!> Muon mag. mom. anomaly, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_anomaly_R8 =                                     &
  real(muon_magnetic_moment_anomaly_R16, kind=R8)

!> Muon mag. mom. anomaly, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_anomaly_R4 =                                     &
  real(muon_magnetic_moment_anomaly_R16, kind=R4)

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R16 =                     &
  -4.84197085e-3_R16

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R8 =                      &
  real(muon_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R4 =                      &
  real(muon_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R16 =                  &
  -8.89059770_R16

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R8 =                   &
  real(muon_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R4 =                   &
  real(muon_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Muon g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_g_factor_R16 =                                                   &
  -2.0023318320_R16

!> Muon g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_g_factor_R8 =                                                    &
  real(muon_g_factor_R16, kind=R8)

!> Muon g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_g_factor_R4 =                                                    &
  real(muon_g_factor_R16, kind=R4)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_proton_magnetic_moment_ratio_R16 =                               &
  -3.18334539_R16

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R8 =                                &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R8)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R4 =                                &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R4)

!-----
! Tau

!> Tau mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_R16 =                                                        &
  3.16788e-27_R16

!> Tau mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_R8 =                                                         &
  real(tau_mass_R16, kind=R8)

!> Tau mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_R4 =                                                         &
  real(tau_mass_R16, kind=R4)

!> Tau mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_in_u_R16 =                                                   &
  1.90774_R16

!> Tau mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_in_u_R8 =                                                    &
  real(tau_mass_in_u_R16, kind=R8)

!> Tau mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_in_u_R4 =                                                    &
  real(tau_mass_in_u_R16, kind=R4)

!> Tau mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_energy_equivalent_R16 =                                      &
  2.84715e-10_R16

!> Tau mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_energy_equivalent_R8 =                                       &
  real(tau_mass_energy_equivalent_R16, kind=R8)

!> Tau mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_energy_equivalent_R4 =                                       &
  real(tau_mass_energy_equivalent_R16, kind=R4)

!> Tau energy equivalent, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_energy_equivalent_in_MeV_R16 =                               &
  1777.05_R16

!> Tau energy equivalent, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_energy_equivalent_in_MeV_R8 =                                &
  real(tau_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Tau energy equivalent, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_energy_equivalent_in_MeV_R4 =                                &
  real(tau_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Tau-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_electron_mass_ratio_R16 =                                         &
  3477.60_R16

!> Tau-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_electron_mass_ratio_R8 =                                          &
  real(tau_electron_mass_ratio_R16, kind=R8)

!> Tau-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_electron_mass_ratio_R4 =                                          &
  real(tau_electron_mass_ratio_R16, kind=R4)

!> Tau-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_muon_mass_ratio_R16 =                                             &
  16.8188_R16

!> Tau-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_muon_mass_ratio_R8 =                                              &
  real(tau_muon_mass_ratio_R16, kind=R8)

!> Tau-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_muon_mass_ratio_R4 =                                              &
  real(tau_muon_mass_ratio_R16, kind=R4)

!> Tau-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_proton_mass_ratio_R16 =                                           &
  1.89396_R16

!> Tau-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_proton_mass_ratio_R8 =                                            &
  real(tau_proton_mass_ratio_R16, kind=R8)

!> Tau-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_proton_mass_ratio_R4 =                                            &
  real(tau_proton_mass_ratio_R16, kind=R4)

!> Tau-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_neutron_mass_ratio_R16 =                                          &
  1.89135_R16

!> Tau-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_neutron_mass_ratio_R8 =                                           &
  real(tau_neutron_mass_ratio_R16, kind=R8)

!> Tau-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_neutron_mass_ratio_R4 =                                           &
  real(tau_neutron_mass_ratio_R16, kind=R4)

!> Tau molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_molar_mass_R16 =                                                  &
  1.90774e-3_R16

!> Tau molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_molar_mass_R8 =                                                   &
  real(tau_molar_mass_R16, kind=R8)

!> Tau molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_molar_mass_R4 =                                                   &
  real(tau_molar_mass_R16, kind=R4)

!> Tau Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_Compton_wavelength_R16 =                                          &
  0.69770e-16_R16

!> Tau Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_Compton_wavelength_R8 =                                           &
  real(tau_Compton_wavelength_R16, kind=R8)

!> Tau Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_Compton_wavelength_R4 =                                           &
  real(tau_Compton_wavelength_R16, kind=R4)

!> Reduced tau Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_tau_Compton_wavelength_R16 =                                  &
  1.11042e-16_R16

!> Reduced tau Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_tau_Compton_wavelength_R8 =                                   &
  real(reduced_tau_Compton_wavelength_R16, kind=R8)

!> Reduced tau Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_tau_Compton_wavelength_R4 =                                   &
  real(reduced_tau_Compton_wavelength_R16, kind=R4)

!-----
! Proton

!> Proton mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_R16 =                                                     &
  1.67262158e-27_R16

!> Proton mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_R8 =                                                      &
  real(proton_mass_R16, kind=R8)

!> Proton mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_R4 =                                                      &
  real(proton_mass_R16, kind=R4)

!> Proton mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_in_u_R16 =                                                &
  1.00727646688_R16

!> Proton mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_in_u_R8 =                                                 &
  real(proton_mass_in_u_R16, kind=R8)

!> Proton mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_in_u_R4 =                                                 &
  real(proton_mass_in_u_R16, kind=R4)

!> Proton mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_energy_equivalent_R16 =                                   &
  1.50327731e-10_R16

!> Proton mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_energy_equivalent_R8 =                                    &
  real(proton_mass_energy_equivalent_R16, kind=R8)

!> Proton mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_energy_equivalent_R4 =                                    &
  real(proton_mass_energy_equivalent_R16, kind=R4)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_energy_equivalent_in_MeV_R16 =                            &
  938.271998_R16

!> Proton mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R8 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R4 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Proton-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_electron_mass_ratio_R16 =                                      &
  1836.1526675_R16

!> Proton-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_electron_mass_ratio_R8 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R8)

!> Proton-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_electron_mass_ratio_R4 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R4)

!> Proton-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_muon_mass_ratio_R16 =                                          &
  8.88024408_R16

!> Proton-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_muon_mass_ratio_R8 =                                           &
  real(proton_muon_mass_ratio_R16, kind=R8)

!> Proton-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_muon_mass_ratio_R4 =                                           &
  real(proton_muon_mass_ratio_R16, kind=R4)

!> Proton-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_tau_mass_ratio_R16 =                                           &
  0.527994_R16

!> Proton-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_tau_mass_ratio_R8 =                                            &
  real(proton_tau_mass_ratio_R16, kind=R8)

!> Proton-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_tau_mass_ratio_R4 =                                            &
  real(proton_tau_mass_ratio_R16, kind=R4)

!> Proton-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_neutron_mass_ratio_R16 =                                       &
  0.99862347855_R16

!> Proton-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_neutron_mass_ratio_R8 =                                        &
  real(proton_neutron_mass_ratio_R16, kind=R8)

!> Proton-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_neutron_mass_ratio_R4 =                                        &
  real(proton_neutron_mass_ratio_R16, kind=R4)

!> Proton charge to mass quotient, C kg^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_charge_to_mass_quotient_R16 =                                  &
  9.57883408e7_R16

!> Proton charge to mass quotient, C kg^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_charge_to_mass_quotient_R8 =                                   &
  real(proton_charge_to_mass_quotient_R16, kind=R8)

!> Proton charge to mass quotient, C kg^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_charge_to_mass_quotient_R4 =                                   &
  real(proton_charge_to_mass_quotient_R16, kind=R4)

!> Proton molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_molar_mass_R16 =                                               &
  1.00727646688e-3_R16

!> Proton molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_molar_mass_R8 =                                                &
  real(proton_molar_mass_R16, kind=R8)

!> Proton molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_molar_mass_R4 =                                                &
  real(proton_molar_mass_R16, kind=R4)

!> Proton Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_Compton_wavelength_R16 =                                       &
  1.321409847e-15_R16

!> Proton Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_Compton_wavelength_R8 =                                        &
  real(proton_Compton_wavelength_R16, kind=R8)

!> Proton Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_Compton_wavelength_R4 =                                        &
  real(proton_Compton_wavelength_R16, kind=R4)

!> Reduced proton Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_proton_Compton_wavelength_R16 =                               &
  2.103089089e-16_R16

!> Reduced proton Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_proton_Compton_wavelength_R8 =                                &
  real(reduced_proton_Compton_wavelength_R16, kind=R8)

!> Reduced proton Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_proton_Compton_wavelength_R4 =                                &
  real(reduced_proton_Compton_wavelength_R16, kind=R4)


!> Proton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_R16 =                                          &
  1.410606633e-26_R16

!> Proton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_R8 =                                           &
  real(proton_magnetic_moment_R16, kind=R8)

!> Proton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_R4 =                                           &
  real(proton_magnetic_moment_R16, kind=R4)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R16 =                   &
  1.521032203e-3_R16

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R8 =                    &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R4 =                    &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R16 =                &
  2.792847337_R16

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R8 =                 &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R4 =                 &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Proton g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_g_factor_R16 =                                                 &
  5.585694675_R16

!> Proton g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_g_factor_R8 =                                                  &
  real(proton_g_factor_R16, kind=R8)

!> Proton g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_g_factor_R4 =                                                  &
  real(proton_g_factor_R16, kind=R4)

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_neutron_magnetic_moment_ratio_R16 =                            &
  -1.45989805_R16

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_neutron_magnetic_moment_ratio_R8 =                             &
  real(proton_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_neutron_magnetic_moment_ratio_R4 =                             &
  real(proton_neutron_magnetic_moment_ratio_R16, kind=R4)

!> Shielded proton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_R16 =                                 &
  1.410570399e-26_R16

!> Shielded proton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_R8 =                                  &
  real(shielded_proton_magnetic_moment_R16, kind=R8)

!> Shielded proton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_R4 =                                  &
  real(shielded_proton_magnetic_moment_R16, kind=R4)

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16 =          &
  1.520993132e-3_R16

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R8 =           &
  real(shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R8)

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R4 =           &
  real(shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R4)

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16 =       &
  2.792775597_R16

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R8 =        &
  real(shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R8)

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R4 =        &
  real(shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R4)

!> Proton mag. shielding correction, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_shielding_correction_R16 =                            &
  2.5687e-5_R16

!> Proton mag. shielding correction, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_shielding_correction_R8 =                             &
  real(proton_magnetic_shielding_correction_R16, kind=R8)

!> Proton mag. shielding correction, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_shielding_correction_R4 =                             &
  real(proton_magnetic_shielding_correction_R16, kind=R4)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_R16 =                                       &
  2.67522212e8_R16

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R8 =                                        &
  real(proton_gyromagnetic_ratio_R16, kind=R8)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R4 =                                        &
  real(proton_gyromagnetic_ratio_R16, kind=R4)

!> Proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_in_MHz_T_R16 =                              &
  42.5774825_R16

!> Proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R8 =                               &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R4 =                               &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_gyromagnetic_ratio_R16 =                              &
  2.67515341e8_R16

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_R8 =                               &
  real(shielded_proton_gyromagnetic_ratio_R16, kind=R8)

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_R4 =                               &
  real(shielded_proton_gyromagnetic_ratio_R16, kind=R4)

!> Shielded proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R16 =                     &
  42.5763888_R16

!> Shielded proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R8 =                      &
  real(shielded_proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Shielded proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R4 =                      &
  real(shielded_proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!-----
! Neutron

!> Neutron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_R16 =                                                    &
  1.67492716e-27_R16

!> Neutron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_R8 =                                                     &
  real(neutron_mass_R16, kind=R8)

!> Neutron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_R4 =                                                     &
  real(neutron_mass_R16, kind=R4)

!> Neutron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_in_u_R16 =                                               &
  1.00866491578_R16

!> Neutron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_in_u_R8 =                                                &
  real(neutron_mass_in_u_R16, kind=R8)

!> Neutron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_in_u_R4 =                                                &
  real(neutron_mass_in_u_R16, kind=R4)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_energy_equivalent_in_MeV_R16 =                           &
  939.565330_R16

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R8 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R4 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Neutron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_energy_equivalent_R16 =                                  &
  1.50534946e-10_R16

!> Neutron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_energy_equivalent_R8 =                                   &
  real(neutron_mass_energy_equivalent_R16, kind=R8)

!> Neutron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_energy_equivalent_R4 =                                   &
  real(neutron_mass_energy_equivalent_R16, kind=R4)

!> Neutron-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_electron_mass_ratio_R16 =                                     &
  1838.6836550_R16

!> Neutron-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_electron_mass_ratio_R8 =                                      &
  real(neutron_electron_mass_ratio_R16, kind=R8)

!> Neutron-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_electron_mass_ratio_R4 =                                      &
  real(neutron_electron_mass_ratio_R16, kind=R4)

!> Neutron-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_muon_mass_ratio_R16 =                                         &
  8.89248478_R16

!> Neutron-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_muon_mass_ratio_R8 =                                          &
  real(neutron_muon_mass_ratio_R16, kind=R8)

!> Neutron-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_muon_mass_ratio_R4 =                                          &
  real(neutron_muon_mass_ratio_R16, kind=R4)

!> Neutron-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_tau_mass_ratio_R16 =                                          &
  0.528722_R16

!> Neutron-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_tau_mass_ratio_R8 =                                           &
  real(neutron_tau_mass_ratio_R16, kind=R8)

!> Neutron-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_tau_mass_ratio_R4 =                                           &
  real(neutron_tau_mass_ratio_R16, kind=R4)

!> Neutron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_ratio_R16 =                                       &
  1.00137841887_R16

!> Neutron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_ratio_R8 =                                        &
  real(neutron_proton_mass_ratio_R16, kind=R8)

!> Neutron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_ratio_R4 =                                        &
  real(neutron_proton_mass_ratio_R16, kind=R4)

!> Neutron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_molar_mass_R16 =                                              &
  1.00866491578e-3_R16

!> Neutron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_molar_mass_R8 =                                               &
  real(neutron_molar_mass_R16, kind=R8)

!> Neutron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_molar_mass_R4 =                                               &
  real(neutron_molar_mass_R16, kind=R4)

!> Neutron Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_Compton_wavelength_R16 =                                      &
  1.319590898e-15_R16

!> Neutron Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_Compton_wavelength_R8 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R8)

!> Neutron Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_Compton_wavelength_R4 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R4)

!> Reduced neutron Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_neutron_Compton_wavelength_R16 =                              &
  2.100194142e-16_R16

!> Reduced neutron Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_neutron_Compton_wavelength_R8 =                               &
  real(reduced_neutron_Compton_wavelength_R16, kind=R8)

!> Reduced neutron Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_neutron_Compton_wavelength_R4 =                               &
  real(reduced_neutron_Compton_wavelength_R16, kind=R4)

!> Neutron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_R16 =                                         &
  -9.6623640e-27_R16

!> Neutron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_R8 =                                          &
  real(neutron_magnetic_moment_R16, kind=R8)

!> Neutron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_R4 =                                          &
  real(neutron_magnetic_moment_R16, kind=R4)

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                  &
  -1.04187563e-3_R16

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                   &
  real(neutron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                   &
  real(neutron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R16 =               &
  -1.91304272_R16

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R8 =                &
  real(neutron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R4 =                &
  real(neutron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Neutron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_g_factor_R16 =                                                &
  -3.82608545_R16

!> Neutron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_g_factor_R8 =                                                 &
  real(neutron_g_factor_R16, kind=R8)

!> Neutron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_g_factor_R4 =                                                 &
  real(neutron_g_factor_R16, kind=R4)

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_electron_magnetic_moment_ratio_R16 =                          &
  1.04066882e-3_R16

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_electron_magnetic_moment_ratio_R8 =                           &
  real(neutron_electron_magnetic_moment_ratio_R16, kind=R8)

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_electron_magnetic_moment_ratio_R4 =                           &
  real(neutron_electron_magnetic_moment_ratio_R16, kind=R4)

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_magnetic_moment_ratio_R16 =                            &
  -0.68497934_R16

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_magnetic_moment_ratio_R8 =                             &
  real(neutron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_magnetic_moment_ratio_R4 =                             &
  real(neutron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_to_shielded_proton_magnetic_moment_ratio_R16 =                &
  -0.68499694_R16

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_to_shielded_proton_magnetic_moment_ratio_R8 =                 &
  real(neutron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R8)

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_to_shielded_proton_magnetic_moment_ratio_R4 =                 &
  real(neutron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R4)

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_gyromagnetic_ratio_R16 =                                      &
  1.83247188e8_R16

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_R8 =                                       &
  real(neutron_gyromagnetic_ratio_R16, kind=R8)

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_R4 =                                       &
  real(neutron_gyromagnetic_ratio_R16, kind=R4)

!> Neutron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_gyromagnetic_ratio_in_MHz_T_R16 =                             &
  29.1646950_R16

!> Neutron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_in_MHz_T_R8 =                              &
  real(neutron_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Neutron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_in_MHz_T_R4 =                              &
  real(neutron_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!-----
! Deuteron

!> Deuteron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_R16 =                                                   &
  3.34358309e-27_R16

!> Deuteron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_R8 =                                                    &
  real(deuteron_mass_R16, kind=R8)

!> Deuteron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_R4 =                                                    &
  real(deuteron_mass_R16, kind=R4)

!> Deuteron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_in_u_R16 =                                              &
  2.01355321271_R16

!> Deuteron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_in_u_R8 =                                               &
  real(deuteron_mass_in_u_R16, kind=R8)

!> Deuteron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_in_u_R4 =                                               &
  real(deuteron_mass_in_u_R16, kind=R4)

!> Deuteron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_energy_equivalent_R16 =                                 &
  3.00506262e-10_R16

!> Deuteron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_R8 =                                  &
  real(deuteron_mass_energy_equivalent_R16, kind=R8)

!> Deuteron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_R4 =                                  &
  real(deuteron_mass_energy_equivalent_R16, kind=R4)

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_energy_equivalent_in_MeV_R16 =                          &
  1875.612762_R16

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_in_MeV_R8 =                           &
  real(deuteron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_in_MeV_R4 =                           &
  real(deuteron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Deuteron-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_electron_mass_ratio_R16 =                                    &
  3670.4829550_R16

!> Deuteron-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_electron_mass_ratio_R8 =                                     &
  real(deuteron_electron_mass_ratio_R16, kind=R8)

!> Deuteron-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_electron_mass_ratio_R4 =                                     &
  real(deuteron_electron_mass_ratio_R16, kind=R4)

!> Deuteron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_proton_mass_ratio_R16 =                                      &
  1.99900750083_R16

!> Deuteron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_proton_mass_ratio_R8 =                                       &
  real(deuteron_proton_mass_ratio_R16, kind=R8)

!> Deuteron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_proton_mass_ratio_R4 =                                       &
  real(deuteron_proton_mass_ratio_R16, kind=R4)

!> Deuteron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_molar_mass_R16 =                                             &
  2.01355321271e-3_R16

!> Deuteron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_molar_mass_R8 =                                              &
  real(deuteron_molar_mass_R16, kind=R8)

!> Deuteron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_molar_mass_R4 =                                              &
  real(deuteron_molar_mass_R16, kind=R4)

!> Deuteron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_R16 =                                        &
  4.33073457e-27_R16

!> Deuteron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_R8 =                                         &
  real(deuteron_magnetic_moment_R16, kind=R8)

!> Deuteron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_R4 =                                         &
  real(deuteron_magnetic_moment_R16, kind=R4)

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                 &
  4.669754556e-4_R16

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                  &
  real(deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                  &
  real(deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16 =              &
  0.8574382284_R16

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R8 =               &
  real(deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R4 =               &
  real(deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_electron_magnetic_moment_ratio_R16 =                         &
  -4.664345537e-4_R16

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_electron_magnetic_moment_ratio_R8 =                          &
  real(deuteron_electron_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_electron_magnetic_moment_ratio_R4 =                          &
  real(deuteron_electron_magnetic_moment_ratio_R16, kind=R4)

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_proton_magnetic_moment_ratio_R16 =                           &
  0.3070122083_R16

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_proton_magnetic_moment_ratio_R8 =                            &
  real(deuteron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_proton_magnetic_moment_ratio_R4 =                            &
  real(deuteron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_neutron_magnetic_moment_ratio_R16 =                          &
  -0.44820652_R16

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_neutron_magnetic_moment_ratio_R8 =                           &
  real(deuteron_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_neutron_magnetic_moment_ratio_R4 =                           &
  real(deuteron_neutron_magnetic_moment_ratio_R16, kind=R4)

!-----
! Helion

!> Helion mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_R16 =                                                     &
  5.00641174e-27_R16

!> Helion mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_R8 =                                                      &
  real(helion_mass_R16, kind=R8)

!> Helion mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_R4 =                                                      &
  real(helion_mass_R16, kind=R4)

!> Helion mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_in_u_R16 =                                                &
  3.01493223469_R16

!> Helion mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_in_u_R8 =                                                 &
  real(helion_mass_in_u_R16, kind=R8)

!> Helion mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_in_u_R4 =                                                 &
  real(helion_mass_in_u_R16, kind=R4)

!> Helion mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_energy_equivalent_R16 =                                   &
  4.49953848e-10_R16

!> Helion mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_energy_equivalent_R8 =                                    &
  real(helion_mass_energy_equivalent_R16, kind=R8)

!> Helion mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_energy_equivalent_R4 =                                    &
  real(helion_mass_energy_equivalent_R16, kind=R4)

!> Helion mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_energy_equivalent_in_MeV_R16 =                            &
  2808.39132_R16

!> Helion mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_energy_equivalent_in_MeV_R8 =                             &
  real(helion_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Helion mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_energy_equivalent_in_MeV_R4 =                             &
  real(helion_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Helion-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_electron_mass_ratio_R16 =                                      &
  5495.885238_R16

!> Helion-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_electron_mass_ratio_R8 =                                       &
  real(helion_electron_mass_ratio_R16, kind=R8)

!> Helion-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_electron_mass_ratio_R4 =                                       &
  real(helion_electron_mass_ratio_R16, kind=R4)

!> Helion-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_proton_mass_ratio_R16 =                                        &
  2.99315265850_R16

!> Helion-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_proton_mass_ratio_R8 =                                         &
  real(helion_proton_mass_ratio_R16, kind=R8)

!> Helion-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_proton_mass_ratio_R4 =                                         &
  real(helion_proton_mass_ratio_R16, kind=R4)

!> Helion molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_molar_mass_R16 =                                               &
  3.01493223469e-3_R16

!> Helion molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_molar_mass_R8 =                                                &
  real(helion_molar_mass_R16, kind=R8)

!> Helion molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_molar_mass_R4 =                                                &
  real(helion_molar_mass_R16, kind=R4)

!> Shielded helion mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_R16 =                                 &
  -1.074552967e-26_R16

!> Shielded helion mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_R8 =                                  &
  real(shielded_helion_magnetic_moment_R16, kind=R8)

!> Shielded helion mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_R4 =                                  &
  real(shielded_helion_magnetic_moment_R16, kind=R4)

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16 =          &
  -1.158671474e-3_R16

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R8 =           &
  real(shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R8)

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R4 =           &
  real(shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R4)

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16 =       &
  -2.127497718_R16

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R8 =        &
  real(shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R8)

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R4 =        &
  real(shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R4)

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_to_proton_magnetic_moment_ratio_R16 =                 &
  -0.761766563_R16

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_to_proton_magnetic_moment_ratio_R8 =                  &
  real(shielded_helion_to_proton_magnetic_moment_ratio_R16, kind=R8)

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_to_proton_magnetic_moment_ratio_R4 =                  &
  real(shielded_helion_to_proton_magnetic_moment_ratio_R16, kind=R4)

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16 =        &
  -0.7617861313_R16

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R8 =         &
  real(shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16,    &
  kind=R8)

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R4 =         &
  real(shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16,    &
  kind=R4)

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_gyromagnetic_ratio_R16 =                              &
  2.037894764e8_R16

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_R8 =                               &
  real(shielded_helion_gyromagnetic_ratio_R16, kind=R8)

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_R4 =                               &
  real(shielded_helion_gyromagnetic_ratio_R16, kind=R4)

!> Shielded helion gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R16 =                     &
  32.4341025_R16

!> Shielded helion gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R8 =                      &
  real(shielded_helion_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Shielded helion gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R4 =                      &
  real(shielded_helion_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!-----
! Alpha particle

!> Alpha particle mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_R16 =                                             &
  6.64465598e-27_R16

!> Alpha particle mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_R8 =                                              &
  real(alpha_particle_mass_R16, kind=R8)

!> Alpha particle mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_R4 =                                              &
  real(alpha_particle_mass_R16, kind=R4)

!> Alpha particle mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_in_u_R16 =                                        &
  4.0015061747_R16

!> Alpha particle mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_in_u_R8 =                                         &
  real(alpha_particle_mass_in_u_R16, kind=R8)

!> Alpha particle mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_in_u_R4 =                                         &
  real(alpha_particle_mass_in_u_R16, kind=R4)

!> Alpha particle mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_energy_equivalent_R16 =                           &
  5.97191897e-10_R16

!> Alpha particle mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_R8 =                            &
  real(alpha_particle_mass_energy_equivalent_R16, kind=R8)

!> Alpha particle mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_R4 =                            &
  real(alpha_particle_mass_energy_equivalent_R16, kind=R4)

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_energy_equivalent_in_MeV_R16 =                    &
  3727.37904_R16

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_in_MeV_R8 =                     &
  real(alpha_particle_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_in_MeV_R4 =                     &
  real(alpha_particle_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_electron_mass_ratio_R16 =                              &
  7294.299508_R16

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_electron_mass_ratio_R8 =                               &
  real(alpha_particle_electron_mass_ratio_R16, kind=R8)

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_electron_mass_ratio_R4 =                               &
  real(alpha_particle_electron_mass_ratio_R16, kind=R4)

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_proton_mass_ratio_R16 =                                &
  3.9725996846_R16

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_proton_mass_ratio_R8 =                                 &
  real(alpha_particle_proton_mass_ratio_R16, kind=R8)

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_proton_mass_ratio_R4 =                                 &
  real(alpha_particle_proton_mass_ratio_R16, kind=R4)

!> Alpha particle molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_molar_mass_R16 =                                       &
  4.0015061747e-3_R16

!> Alpha particle molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_molar_mass_R8 =                                        &
  real(alpha_particle_molar_mass_R16, kind=R8)

!> Alpha particle molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_molar_mass_R4 =                                        &
  real(alpha_particle_molar_mass_R16, kind=R4)

!-----
! Physico-Chemical Constants

!> Avogadro constant, mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Avogadro_constant_R16 =                                               &
  6.02214199e23_R16

!> Avogadro constant, mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Avogadro_constant_R8 =                                                &
  real(Avogadro_constant_R16, kind=R8)

!> Avogadro constant, mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Avogadro_constant_R4 =                                                &
  real(Avogadro_constant_R16, kind=R4)

!> Atomic mass constant, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_R16 =                                            &
  1.66053873e-27_R16

!> Atomic mass constant, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_R8 =                                             &
  real(atomic_mass_constant_R16, kind=R8)

!> Atomic mass constant, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_R4 =                                             &
  real(atomic_mass_constant_R16, kind=R4)

!> Atomic mass constant energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_energy_equivalent_R16 =                          &
  1.49241778e-10_R16

!> Atomic mass constant energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_R8 =                           &
  real(atomic_mass_constant_energy_equivalent_R16, kind=R8)

!> Atomic mass constant energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_R4 =                           &
  real(atomic_mass_constant_energy_equivalent_R16, kind=R4)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_energy_equivalent_in_MeV_R16 =                   &
  931.494013_R16

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R8 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R8)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R4 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R4)

!> Faraday constant, C mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Faraday_constant_R16 =                                                &
  96485.3415_R16

!> Faraday constant, C mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Faraday_constant_R8 =                                                 &
  real(Faraday_constant_R16, kind=R8)

!> Faraday constant, C mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Faraday_constant_R4 =                                                 &
  real(Faraday_constant_R16, kind=R4)

!> Molar Planck constant, J Hz^-1 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_Planck_constant_R16 =                                           &
  3.990312689e-10_R16

!> Molar Planck constant, J Hz^-1 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_Planck_constant_R8 =                                            &
  real(molar_Planck_constant_R16, kind=R8)

!> Molar Planck constant, J Hz^-1 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_Planck_constant_R4 =                                            &
  real(molar_Planck_constant_R16, kind=R4)

!> Molar Planck constant times c, J m mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_Planck_constant_times_c_R16 =                                   &
  0.11962656492_R16

!> Molar Planck constant times c, J m mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_Planck_constant_times_c_R8 =                                    &
  real(molar_Planck_constant_times_c_R16, kind=R8)

!> Molar Planck constant times c, J m mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_Planck_constant_times_c_R4 =                                    &
  real(molar_Planck_constant_times_c_R16, kind=R4)

!> Molar gas constant, J mol^-1 K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_gas_constant_R16 =                                              &
  8.314472_R16

!> Molar gas constant, J mol^-1 K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_gas_constant_R8 =                                               &
  real(molar_gas_constant_R16, kind=R8)

!> Molar gas constant, J mol^-1 K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_gas_constant_R4 =                                               &
  real(molar_gas_constant_R16, kind=R4)

!> Boltzmann constant, J K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_R16 =                                              &
  1.3806503e-23_R16

!> Boltzmann constant, J K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_R8 =                                               &
  real(Boltzmann_constant_R16, kind=R8)

!> Boltzmann constant, J K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_R4 =                                               &
  real(Boltzmann_constant_R16, kind=R4)

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_eV_K_R16 =                                      &
  8.617342e-5_R16

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_eV_K_R8 =                                       &
  real(Boltzmann_constant_in_eV_K_R16, kind=R8)

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_eV_K_R4 =                                       &
  real(Boltzmann_constant_in_eV_K_R16, kind=R4)

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_Hz_K_R16 =                                      &
  2.0836644e10_R16

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_Hz_K_R8 =                                       &
  real(Boltzmann_constant_in_Hz_K_R16, kind=R8)

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_Hz_K_R4 =                                       &
  real(Boltzmann_constant_in_Hz_K_R16, kind=R4)

!> Boltzmann constant in inverse meter per kelvin, m^-1 K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R16 =                  &
  69.50356_R16

!> Boltzmann constant in inverse meter per kelvin, m^-1 K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R8 =                   &
  real(Boltzmann_constant_in_inverse_meter_per_kelvin_R16, kind=R8)

!> Boltzmann constant in inverse meter per kelvin, m^-1 K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R4 =                   &
  real(Boltzmann_constant_in_inverse_meter_per_kelvin_R16, kind=R4)

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16 =                  &
  22.413996e-3_R16

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R8 =                   &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R8)

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R4 =                   &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R4)

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Loschmidt_constant_273_15_K_101_325_kPa_R16 =                         &
  2.6867773e25_R16

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_101_325_kPa_R8 =                          &
  real(Loschmidt_constant_273_15_K_101_325_kPa_R16, kind=R8)

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_101_325_kPa_R4 =                          &
  real(Loschmidt_constant_273_15_K_101_325_kPa_R16, kind=R4)

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R16 =                      &
  22.710981e-3_R16

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R8 =                       &
  real(molar_volume_of_ideal_gas_273_15_K_100_kPa_R16, kind=R8)

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R4 =                       &
  real(molar_volume_of_ideal_gas_273_15_K_100_kPa_R16, kind=R4)

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Sackur_Tetrode_constant_1_K_100_kPa_R16 =                             &
  -1.1517048_R16

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_100_kPa_R8 =                              &
  real(Sackur_Tetrode_constant_1_K_100_kPa_R16, kind=R8)

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_100_kPa_R4 =                              &
  real(Sackur_Tetrode_constant_1_K_100_kPa_R16, kind=R4)

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R16 =                         &
  -1.1648678_R16

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R8 =                          &
  real(Sackur_Tetrode_constant_1_K_101_325_kPa_R16, kind=R8)

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R4 =                          &
  real(Sackur_Tetrode_constant_1_K_101_325_kPa_R16, kind=R4)

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Stefan_Boltzmann_constant_R16 =                                       &
  5.670400e-8_R16

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R8 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R8)

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R4 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R4)

!> First radiation constant, W m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  first_radiation_constant_R16 =                                        &
  3.74177107e-16_R16

!> First radiation constant, W m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  first_radiation_constant_R8 =                                         &
  real(first_radiation_constant_R16, kind=R8)

!> First radiation constant, W m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  first_radiation_constant_R4 =                                         &
  real(first_radiation_constant_R16, kind=R4)

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  first_radiation_constant_for_spectral_radiance_R16 =                  &
  1.191042722e-16_R16

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  first_radiation_constant_for_spectral_radiance_R8 =                   &
  real(first_radiation_constant_for_spectral_radiance_R16, kind=R8)

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  first_radiation_constant_for_spectral_radiance_R4 =                   &
  real(first_radiation_constant_for_spectral_radiance_R16, kind=R4)

!> Second radiation constant, m K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  second_radiation_constant_R16 =                                       &
  1.4387752e-2_R16

!> Second radiation constant, m K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  second_radiation_constant_R8 =                                        &
  real(second_radiation_constant_R16, kind=R8)

!> Second radiation constant, m K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  second_radiation_constant_R4 =                                        &
  real(second_radiation_constant_R16, kind=R4)

!> Wien wavelength displacement law constant, m K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Wien_wavelength_displacement_law_constant_R16 =                       &
  2.8977686e-3_R16

!> Wien wavelength displacement law constant, m K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Wien_wavelength_displacement_law_constant_R8 =                        &
  real(Wien_wavelength_displacement_law_constant_R16, kind=R8)

!> Wien wavelength displacement law constant, m K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Wien_wavelength_displacement_law_constant_R4 =                        &
  real(Wien_wavelength_displacement_law_constant_R16, kind=R4)

!-----
! Non-SI units

!===== DEMARC =====>

!> Electron volt, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_R16 =                                                   &
  1.602176462e-19_R16

!> Electron volt, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_R8 =                                                    &
  real(electron_volt_R16, kind=R8)

!> Electron volt, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_R4 =                                                    &
  real(electron_volt_R16, kind=R4)

!> Unified atomic mass unit, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  unified_atomic_mass_unit_R16 =                                        &
  1.66053873e-27_R16

!> Unified atomic mass unit, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  unified_atomic_mass_unit_R8 =                                         &
  real(unified_atomic_mass_unit_R16, kind=R8)

!> Unified atomic mass unit, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  unified_atomic_mass_unit_R4 =                                         &
  real(unified_atomic_mass_unit_R16, kind=R4)

!-----
! Natural units

!> Natural unit of velocity, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_velocity_R16 =                                        &
  speed_of_light_in_vacuum_R16
  ! 299792458.0_R16 ! derived

!> Natural unit of velocity, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_velocity_R8 =                                         &
  real(natural_unit_of_velocity_R16, kind=R8) ! derived

!> Natural unit of velocity, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_velocity_R4 =                                         &
  real(natural_unit_of_velocity_R16, kind=R4) ! derived

!> Natural unit of action, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_action_R16 =                                          &
  reduced_Planck_constant_R16 ! derived

!> Natural unit of action, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_action_R8 =                                           &
  real(natural_unit_of_action_R16, kind=R8) ! derived

!> Natural unit of action, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_action_R4 =                                           &
  real(natural_unit_of_action_R16, kind=R4) ! derived

!> Natural unit of action in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_action_in_eV_s_R16 =                                  &
  reduced_Planck_constant_in_eV_s_R16 ! derived

!> Natural unit of action in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_action_in_eV_s_R8 =                                   &
  real(natural_unit_of_action_in_eV_s_R16, kind=R8) ! derived

!> Natural unit of action in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_action_in_eV_s_R4 =                                   &
  real(natural_unit_of_action_in_eV_s_R16, kind=R4) ! derived

!> Natural unit of mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_mass_R16 =                                            &
  electron_mass_R16 ! derived

!> Natural unit of mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_mass_R8 =                                             &
  real(natural_unit_of_mass_R16, kind=R8) ! derived

!> Natural unit of mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_mass_R4 =                                             &
  real(natural_unit_of_mass_R16, kind=R4) ! derived

!> Natural unit of energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_energy_R16 =                                          &
  electron_mass_energy_equivalent_R16 ! derived

!> Natural unit of energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_energy_R8 =                                           &
  real(natural_unit_of_energy_R16, kind=R8) ! derived

!> Natural unit of energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_energy_R4 =                                           &
  real(natural_unit_of_energy_R16, kind=R4) ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_energy_in_MeV_R16 =                                   &
  electron_mass_energy_equivalent_in_MeV_R16 ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_energy_in_MeV_R8 =                                    &
  real(natural_unit_of_energy_in_MeV_R16, kind=R8) ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_energy_in_MeV_R4 =                                    &
  real(natural_unit_of_energy_in_MeV_R16, kind=R4) ! derived

!> Natural unit of momentum, kg m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_momentum_R16 =                                        &
  electron_mass_R16 * speed_of_light_in_vacuum_R16
  ! 2.73092398e-22_R16

!> Natural unit of momentum, kg m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_momentum_R8 =                                         &
  real(natural_unit_of_momentum_R16, kind=R8) ! derived

!> Natural unit of momentum, kg m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_momentum_R4 =                                         &
  real(natural_unit_of_momentum_R16, kind=R4) ! derived

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_momentum_in_MeV_c_R16 =                               &
  electron_mass_energy_equivalent_in_MeV_R16 ! derived
  ! 0.510998902_R16

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_momentum_in_MeV_c_R8 =                                &
  real(natural_unit_of_momentum_in_MeV_c_R16, kind=R8) ! derived

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_momentum_in_MeV_c_R4 =                                &
  real(natural_unit_of_momentum_in_MeV_c_R16, kind=R4) ! derived

!> Natural unit of length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_length_R16 =                                          &
  reduced_Compton_wavelength_R16 ! derived

!> Natural unit of length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_length_R8 =                                           &
  real(natural_unit_of_length_R16, kind=R8) ! derived

!> Natural unit of length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_length_R4 =                                           &
  real(natural_unit_of_length_R16, kind=R4) ! derived

!> Natural unit of time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_time_R16 =                                            &
  reduced_Planck_constant_R16 / electron_mass_energy_equivalent_R16
  ! 1.2880886555e-21_R16

!> Natural unit of time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_time_R8 =                                             &
  real(natural_unit_of_time_R16, kind=R8) ! derived

!> Natural unit of time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_time_R4 =                                             &
  real(natural_unit_of_time_R16, kind=R4) ! derived

!-----
! Atomic units

!> Atomic unit of charge, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_charge_R16 =                                           &
  elementary_charge_R16 ! derived

!> Atomic unit of charge, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_charge_R8 =                                            &
  real(atomic_unit_of_charge_R16, kind=R8) ! derived

!> Atomic unit of charge, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_charge_R4 =                                            &
  real(atomic_unit_of_charge_R16, kind=R4) ! derived

!> Atomic unit of mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_mass_R16 =                                             &
  electron_mass_R16 ! derived

!> Atomic unit of mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_mass_R8 =                                              &
  real(atomic_unit_of_mass_R16, kind=R8) ! derived

!> Atomic unit of mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_mass_R4 =                                              &
  real(atomic_unit_of_mass_R16, kind=R4) ! derived

!> Atomic unit of action, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_action_R16 =                                           &
  reduced_Planck_constant_R16 ! derived

!> Atomic unit of action, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_action_R8 =                                            &
  real(atomic_unit_of_action_R16, kind=R8) ! derived

!> Atomic unit of action, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_action_R4 =                                            &
  real(atomic_unit_of_action_R16, kind=R4) ! derived

!> Atomic unit of length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_length_R16 =                                           &
  Bohr_radius_R16 ! derived

!> Atomic unit of length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_length_R8 =                                            &
  real(atomic_unit_of_length_R16, kind=R8) ! derived

!> Atomic unit of length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_length_R4 =                                            &
  real(atomic_unit_of_length_R16, kind=R4) ! derived

!> Atomic unit of energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_energy_R16 =                                           &
  Hartree_energy_R16 ! derived

!> Atomic unit of energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_energy_R8 =                                            &
  real(atomic_unit_of_energy_R16, kind=R8) ! derived

!> Atomic unit of energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_energy_R4 =                                            &
  real(atomic_unit_of_energy_R16, kind=R4) ! derived

!> Atomic unit of time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_time_R16 =                                             &
  reduced_Planck_constant_R16 / Hartree_energy_R16
  ! 2.418884326505e-17_R16

!> Atomic unit of time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_time_R8 =                                              &
  real(atomic_unit_of_time_R16, kind=R8)

!> Atomic unit of time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_time_R4 =                                              &
  real(atomic_unit_of_time_R16, kind=R4)

!> Atomic unit of force, N
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_force_R16 =                                            &
  Hartree_energy_R16 / Bohr_radius_R16
  ! 8.2387225e-8_R16

!> Atomic unit of force, N
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_force_R8 =                                             &
  real(atomic_unit_of_force_R16, kind=R8)

!> Atomic unit of force, N
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_force_R4 =                                             &
  real(atomic_unit_of_force_R16, kind=R4)

!> Atomic unit of velocity, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_velocity_R16 =                                         &
  Bohr_radius_R16 * Hartree_energy_R16 / reduced_Planck_constant_R16
  ! 2.1876912541e6_R16

!> Atomic unit of velocity, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_velocity_R8 =                                          &
  real(atomic_unit_of_velocity_R16, kind=R8)

!> Atomic unit of velocity, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_velocity_R4 =                                          &
  real(atomic_unit_of_velocity_R16, kind=R4)

!> Atomic unit of momentum, kg m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_momentum_R16 =                                         &
  reduced_Planck_constant_R16 / Bohr_radius_R16
  ! 1.992851565e-24_R16

!> Atomic unit of momentum, kg m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_momentum_R8 =                                          &
  real(atomic_unit_of_momentum_R16, kind=R8)

!> Atomic unit of momentum, kg m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_momentum_R4 =                                          &
  real(atomic_unit_of_momentum_R16, kind=R4)

!> Atomic unit of current, A
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_current_R16 =                                          &
  elementary_charge_R16 * Hartree_energy_R16                            &
  / reduced_Planck_constant_R16
  ! 6.62361763e-3_R16

!> Atomic unit of current, A
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_current_R8 =                                           &
  real(atomic_unit_of_current_R16, kind=R8)

!> Atomic unit of current, A
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_current_R4 =                                           &
  real(atomic_unit_of_current_R16, kind=R4)

!> Atomic unit of charge density, C m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_charge_density_R16 =                                   &
  elementary_charge_R16                                                 &
  / (Bohr_radius_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 1.081202300e12_R16

!> Atomic unit of charge density, C m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_charge_density_R8 =                                    &
  real(atomic_unit_of_charge_density_R16, kind=R8)

!> Atomic unit of charge density, C m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_charge_density_R4 =                                    &
  real(atomic_unit_of_charge_density_R16, kind=R4)

!> Atomic unit of electric potential, V
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_potential_R16 =                               &
  Hartree_energy_R16 / elementary_charge_R16
  ! 27.21138386_R16

!> Atomic unit of electric potential, V
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_potential_R8 =                                &
  real(atomic_unit_of_electric_potential_R16, kind=R8)

!> Atomic unit of electric potential, V
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_potential_R4 =                                &
  real(atomic_unit_of_electric_potential_R16, kind=R4)

!> Atomic unit of electric field, V m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_field_R16 =                                   &
  Hartree_energy_R16 / (elementary_charge_R16 * Bohr_radius_R16)
  ! 5.14220632e11_R16

!> Atomic unit of electric field, V m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_field_R8 =                                    &
  real(atomic_unit_of_electric_field_R16, kind=R8)

!> Atomic unit of electric field, V m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_field_R4 =                                    &
  real(atomic_unit_of_electric_field_R16, kind=R4)

!> Atomic unit of electric field gradient, V m^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_field_gradient_R16 =                          &
  Hartree_energy_R16                                                    &
  / (elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 9.71736166e21_R16

!> Atomic unit of electric field gradient, V m^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_field_gradient_R8 =                           &
  real(atomic_unit_of_electric_field_gradient_R16, kind=R8)

!> Atomic unit of electric field gradient, V m^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_field_gradient_R4 =                           &
  real(atomic_unit_of_electric_field_gradient_R16, kind=R4)

!> Atomic unit of electric dipole mom., C m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_dipole_moment_R16 =                           &
  elementary_charge_R16 * Bohr_radius_R16
  ! 8.47835281e-30_R16

!> Atomic unit of electric dipole mom., C m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_dipole_moment_R8 =                            &
  real(atomic_unit_of_electric_dipole_moment_R16, kind=R8)

!> Atomic unit of electric dipole mom., C m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_dipole_moment_R4 =                            &
  real(atomic_unit_of_electric_dipole_moment_R16, kind=R4)

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_quadrupole_moment_R16 =                       &
  elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16
  ! 4.48655107e-40_R16

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_quadrupole_moment_R8 =                        &
  real(atomic_unit_of_electric_quadrupole_moment_R16, kind=R8)

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_quadrupole_moment_R4 =                        &
  real(atomic_unit_of_electric_quadrupole_moment_R16, kind=R4)

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_polarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16                         &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / Hartree_energy_R16
  ! 1.6487772536e-41_R16

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_polarizability_R8 =                           &
  real(atomic_unit_of_electric_polarizability_R16, kind=R8)

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_polarizability_R4 =                           &
  real(atomic_unit_of_electric_polarizability_R16, kind=R4)

!> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_1st_hyperpolarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16 * elementary_charge_R16 &
  * Bohr_radius_R16 * Bohr_radius_R16 * Bohr_radius_R16                 &
  / (Hartree_energy_R16 * Hartree_energy_R16)
  ! 3.206361533e-53_R16

!> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_1st_hyperpolarizability_R8 =                           &
  real(atomic_unit_of_1st_hyperpolarizability_R16, kind=R8)

! UNDERFLOW
! !> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   atomic_unit_of_1st_hyperpolarizability_R4 =                           &
!   real(atomic_unit_of_1st_hyperpolarizability_R16, kind=R4)

!> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_2nd_hyperpolarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16                         &
  * elementary_charge_R16 * elementary_charge_R16                       &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / (Hartree_energy_R16 * Hartree_energy_R16 * Hartree_energy_R16)
  ! 6.23538095e-65_R16

!> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_2nd_hyperpolarizability_R8 =                           &
  real(atomic_unit_of_2nd_hyperpolarizability_R16, kind=R8)

! UNDERFLOW
! !> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   atomic_unit_of_2nd_hyperpolarizability_R4 =                           &
!   real(atomic_unit_of_2nd_hyperpolarizability_R16, kind=R4)

!> Atomic unit of mag. flux density, T
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetic_flux_density_R16 =                            &
  reduced_Planck_constant_R16                                           &
  / (elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 2.350517382e5_R16

!> Atomic unit of mag. flux density, T
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetic_flux_density_R8 =                             &
  real(atomic_unit_of_magnetic_flux_density_R16, kind=R8)

!> Atomic unit of mag. flux density, T
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetic_flux_density_R4 =                             &
  real(atomic_unit_of_magnetic_flux_density_R16, kind=R4)

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetic_dipole_moment_R16 =                           &
  reduced_Planck_constant_R16 * elementary_charge_R16                   &
  / electron_mass_R16
  ! 1.854801830e-23_R16

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetic_dipole_moment_R8 =                            &
  real(atomic_unit_of_magnetic_dipole_moment_R16, kind=R8)

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetic_dipole_moment_R4 =                            &
  real(atomic_unit_of_magnetic_dipole_moment_R16, kind=R4)

!> Atomic unit of magnetizability, J T^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetizability_R16 =                                  &
  elementary_charge_R16 * elementary_charge_R16                         &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / electron_mass_R16
  ! 7.891036433e-29_R16

!> Atomic unit of magnetizability, J T^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetizability_R8 =                                   &
  real(atomic_unit_of_magnetizability_R16, kind=R8)

!> Atomic unit of magnetizability, J T^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetizability_R4 =                                   &
  real(atomic_unit_of_magnetizability_R16, kind=R4)

!> Atomic unit of permittivity, F m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_permittivity_R16 =                                     &
  elementary_charge_R16 * elementary_charge_R16                         &
  / (Bohr_radius_R16 * Hartree_energy_R16)
  ! 1.112 650 056... e-10_R16 ! derived

!> Atomic unit of permittivity, F m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_permittivity_R8 =                                      &
  real(atomic_unit_of_permittivity_R16, kind=R8) ! derived

!> Atomic unit of permittivity, F m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_permittivity_R4 =                                      &
  real(atomic_unit_of_permittivity_R16, kind=R4) ! derived

!-----
! Adopted values

!> Molar mass constant, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_mass_constant_R16 =                                             &
  1.0e-3_R16

!> Molar mass constant, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_mass_constant_R8 =                                              &
  real(molar_mass_constant_R16, kind=R8)

!> Molar mass constant, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_mass_constant_R4 =                                              &
  real(molar_mass_constant_R16, kind=R4)

!> Molar mass of carbon-12, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_mass_of_carbon_12_R16 =                                         &
  12.0e-3_R16

!> Molar mass of carbon-12, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_mass_of_carbon_12_R8 =                                          &
  real(molar_mass_of_carbon_12_R16, kind=R8)

!> Molar mass of carbon-12, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_mass_of_carbon_12_R4 =                                          &
  real(molar_mass_of_carbon_12_R16, kind=R4)

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_Josephson_constant_R16 =                        &
  483597.9e9_R16

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_Josephson_constant_R8 =                         &
  real(conventional_value_of_Josephson_constant_R16, kind=R8)

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_Josephson_constant_R4 =                         &
  real(conventional_value_of_Josephson_constant_R16, kind=R4)

!> Conventional value of von Klitzing constant, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_von_Klitzing_constant_R16 =                     &
  25812.807_R16

!> Conventional value of von Klitzing constant, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_von_Klitzing_constant_R8 =                      &
  real(conventional_value_of_von_Klitzing_constant_R16, kind=R8)

!> Conventional value of von Klitzing constant, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_von_Klitzing_constant_R4 =                      &
  real(conventional_value_of_von_Klitzing_constant_R16, kind=R4)

!> Standard temperature, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_temperature_R16 =                                            &
  273.15_R16

!> Standard temperature, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_temperature_R8 =                                             &
  real(standard_temperature_R16, kind=R8)

!> Standard temperature, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_temperature_R4 =                                             &
  real(standard_temperature_R16, kind=R4)

!> Standard atmosphere, Pa
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_atmosphere_R16 =                                             &
  101325.0_R16

!> Standard atmosphere, Pa
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_atmosphere_R8 =                                              &
  real(standard_atmosphere_R16, kind=R8)

!> Standard atmosphere, Pa
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_atmosphere_R4 =                                              &
  real(standard_atmosphere_R16, kind=R4)

!> Standard acceleration of gravity, m s^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_acceleration_of_gravity_R16 =                                &
  9.80665_R16

!> Standard acceleration of gravity, m s^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_acceleration_of_gravity_R8 =                                 &
  real(standard_acceleration_of_gravity_R16, kind=R8)

!> Standard acceleration of gravity, m s^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_acceleration_of_gravity_R4 =                                 &
  real(standard_acceleration_of_gravity_R16, kind=R4)

!-----
! "As-maintained" electrical units

! BIPM maintained ohm-69BI -> ohm-BI85 = 0.999998437 ohm
! BIPM maintained volt-76BI = 0.99999241 V
! BIPM maintained ampere-BIPM = volt-76BI / ohm-69BI = 0.99999397 A

!-----
! X-ray standards

!> Copper x unit, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Copper_x_unit_R16 =                                                   &
  1.00207703e-13_R16

!> Copper x unit, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Copper_x_unit_R8 =                                                    &
  real(Copper_x_unit_R16, kind=R8)

!> Copper x unit, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Copper_x_unit_R4 =                                                    &
  real(Copper_x_unit_R16, kind=R4)

!> Molybdenum x unit, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Molybdenum_x_unit_R16 =                                               &
  1.00209959e-13_R16

!> Molybdenum x unit, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Molybdenum_x_unit_R8 =                                                &
  real(Molybdenum_x_unit_R16, kind=R8)

!> Molybdenum x unit, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Molybdenum_x_unit_R4 =                                                &
  real(Molybdenum_x_unit_R16, kind=R4)

!> Angstrom star, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Angstrom_star_R16 =                                                   &
  1.00001501e-10_R16

!> Angstrom star, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Angstrom_star_R8 =                                                    &
  real(Angstrom_star_R16, kind=R8)

!> Angstrom star, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Angstrom_star_R4 =                                                    &
  real(Angstrom_star_R16, kind=R4)

!> Lattice parameter of silicon, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  lattice_parameter_of_silicon_R16 =                                    &
  5.43102088e-10_R16

!> Lattice parameter of silicon, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  lattice_parameter_of_silicon_R8 =                                     &
  real(lattice_parameter_of_silicon_R16, kind=R8)

!> Lattice parameter of silicon, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  lattice_parameter_of_silicon_R4 =                                     &
  real(lattice_parameter_of_silicon_R16, kind=R4)

!> Lattice spacing of ideal Si (d220), m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  lattice_spacing_of_ideal_silicon_d220_R16 =                           &
  1.920155845e-10_R16

!> Lattice spacing of ideal Si (d220), m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  lattice_spacing_of_ideal_silicon_d220_R8 =                            &
  real(lattice_spacing_of_ideal_silicon_d220_R16, kind=R8)

!> Lattice spacing of ideal Si (d220), m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  lattice_spacing_of_ideal_silicon_d220_R4 =                            &
  real(lattice_spacing_of_ideal_silicon_d220_R16, kind=R4)

!> Molar volume of silicon, m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_silicon_R16 =                                         &
  1.20588369e-5_R16

!> Molar volume of silicon, m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_silicon_R8 =                                          &
  real(molar_volume_of_silicon_R16, kind=R8)

!> Molar volume of silicon, m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_silicon_R4 =                                          &
  real(molar_volume_of_silicon_R16, kind=R4)

!-----
! Energy conversion factors

!> Joule-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_kilogram_relationship_R16 = 1.0_R16                             &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   1.112 650 056... e-17_R16 ! derived

!> Joule-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_kilogram_relationship_R8 =                                      &
  real(joule_kilogram_relationship_R16, kind=R8) ! derived

!> Joule-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_kilogram_relationship_R4 =                                      &
  real(joule_kilogram_relationship_R16, kind=R4) ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_inverse_meter_relationship_R16 =                                &
  1.0_R16 / (speed_of_light_in_vacuum_R16 * Planck_constant_R16)
  ! 5.034 117 47... e24_R16 ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_inverse_meter_relationship_R8 =                                 &
  real(joule_inverse_meter_relationship_R16, kind=R8) ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_inverse_meter_relationship_R4 =                                 &
  real(joule_inverse_meter_relationship_R16, kind=R4) ! derived

!> Joule-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_hertz_relationship_R16 =                                        &
  1.0_R16 / Planck_constant_R16
  ! 1.509 190 450... e33_R16 ! derived

!> Joule-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_hertz_relationship_R8 =                                         &
  real(joule_hertz_relationship_R16, kind=R8) ! derived

!> Joule-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_hertz_relationship_R4 =                                         &
  real(joule_hertz_relationship_R16, kind=R4) ! derived

!> Joule-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_kelvin_relationship_R16 =                                       &
  1.0_R16 / Boltzmann_constant_R16
  ! 7.242 963... e22_R16 ! derived

!> Joule-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_kelvin_relationship_R8 =                                        &
  real(joule_kelvin_relationship_R16, kind=R8) ! derived

!> Joule-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_kelvin_relationship_R4 =                                        &
  real(joule_kelvin_relationship_R16, kind=R4) ! derived

!> Joule-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_electron_volt_relationship_R16 =                                &
  1.0_R16 / elementary_charge_R16
  !   6.241 509 65... e18_R16 ! derived

!> Joule-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_electron_volt_relationship_R8 =                                 &
  real(joule_electron_volt_relationship_R16, kind=R8) ! derived

!> Joule-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_electron_volt_relationship_R4 =                                 &
  real(joule_electron_volt_relationship_R16, kind=R4) ! derived

!> Joule-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_atomic_mass_unit_relationship_R16 =                             &
  1.0_R16 / atomic_mass_constant_energy_equivalent_R16
  ! 6.700 536 41... e9

!> Joule-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_atomic_mass_unit_relationship_R8 =                              &
  real(joule_atomic_mass_unit_relationship_R16, kind=R8)

!> Joule-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_atomic_mass_unit_relationship_R4 =                              &
  real(joule_atomic_mass_unit_relationship_R16, kind=R4)

!> Joule-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_hartree_relationship_R16 =                                      &
  1.0_R16 / Hartree_energy_R16
  ! 2.29371269...e17

!> Joule-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_hartree_relationship_R8 =                                       &
  real(joule_hartree_relationship_R16, kind=R8)

!> Joule-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_hartree_relationship_R4 =                                       &
  real(joule_hartree_relationship_R16, kind=R4)


!> Kilogram-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_joule_relationship_R16 =                                     &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16
!   8.987 551 787... e16_R16 ! derived

!> Kilogram-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_joule_relationship_R8 =                                      &
  real(kilogram_joule_relationship_R16, kind=R8) ! derived

!> Kilogram-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_joule_relationship_R4 =                                      &
  real(kilogram_joule_relationship_R16, kind=R4) ! derived

!> Kilogram-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_inverse_meter_relationship_R16 =                             &
  speed_of_light_in_vacuum_R16 / Planck_constant_R16
  ! 4.524 439 15... e41_R16 ! derived

!> Kilogram-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_inverse_meter_relationship_R8 =                              &
  real(kilogram_inverse_meter_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-inverse meter relationship, m^-1
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_inverse_meter_relationship_R4 =                              &
!   real(kilogram_inverse_meter_relationship_R16, kind=R4) ! derived

!> Kilogram-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_hertz_relationship_R16 =                                     &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Planck_constant_R16
  ! 1.356 392 489... e50_R16 ! derived

!> Kilogram-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_hertz_relationship_R8 =                                      &
  real(kilogram_hertz_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-hertz relationship, Hz
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_hertz_relationship_R4 =                                      &
!   real(kilogram_hertz_relationship_R16, kind=R4) ! derived

!> Kilogram-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_kelvin_relationship_R16 =                                    &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Boltzmann_constant_R16
  ! 6.509 657 260... e39_R16 ! derived

!> Kilogram-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_kelvin_relationship_R8 =                                     &
  real(kilogram_kelvin_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-kelvin relationship, K
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_kelvin_relationship_R4 =                                     &
!   real(kilogram_kelvin_relationship_R16, kind=R4) ! derived

!> Kilogram-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_electron_volt_relationship_R16 =                             &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / electron_volt_R16
  ! 5.609 588 603... e35_R16 ! derived

!> Kilogram-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R8 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R8) ! derived

!> Kilogram-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R4 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R4) ! derived

!> Kilogram-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_atomic_mass_unit_relationship_R16 =                          &
  1.0_R16 / unified_atomic_mass_unit_R16
  ! 6.0221407621e26_R16

!> Kilogram-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_atomic_mass_unit_relationship_R8 =                           &
  real(kilogram_atomic_mass_unit_relationship_R16, kind=R8)

!> Kilogram-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_atomic_mass_unit_relationship_R4 =                           &
  real(kilogram_atomic_mass_unit_relationship_R16, kind=R4)

!> Kilogram-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_hartree_relationship_R16 =                                   &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Hartree_energy_R16
  ! 2.0614857887409e34_R16

!> Kilogram-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_hartree_relationship_R8 =                                    &
  real(kilogram_hartree_relationship_R16, kind=R8)

!> Kilogram-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_hartree_relationship_R4 =                                    &
  real(kilogram_hartree_relationship_R16, kind=R4)


!> Inverse meter-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_joule_relationship_R16 =                                &
  speed_of_light_in_vacuum_R16 * Planck_constant_R16
  ! 1.986 445 857... e-25_R16 ! derived

!> Inverse meter-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_joule_relationship_R8 =                                 &
  real(inverse_meter_joule_relationship_R16, kind=R8) ! derived

!> Inverse meter-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_joule_relationship_R4 =                                 &
  real(inverse_meter_joule_relationship_R16, kind=R4) ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_kilogram_relationship_R16 =                             &
  Planck_constant_R16 / speed_of_light_in_vacuum_R16
!   2.210 219 094... e-42_R16 ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_kilogram_relationship_R8 =                              &
  real(inverse_meter_kilogram_relationship_R16, kind=R8) ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_kilogram_relationship_R4 =                              &
  real(inverse_meter_kilogram_relationship_R16, kind=R4) ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_hertz_relationship_R16 =                                &
  speed_of_light_in_vacuum_R16
  ! 299792458.0_R16 ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_hertz_relationship_R8 =                                 &
  real(inverse_meter_hertz_relationship_R16, kind=R8) ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_hertz_relationship_R4 =                                 &
  real(inverse_meter_hertz_relationship_R16, kind=R4) ! derived

!> Inverse meter-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_kelvin_relationship_R16 =                               &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / Boltzmann_constant_R16
  ! 1.438 776 877... e-2_R16 ! derived

!> Inverse meter-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_kelvin_relationship_R8 =                                &
  real(inverse_meter_kelvin_relationship_R16, kind=R8) ! derived

!> Inverse meter-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_kelvin_relationship_R4 =                                &
  real(inverse_meter_kelvin_relationship_R16, kind=R4) ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_electron_volt_relationship_R16 =                        &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / elementary_charge_R16
  ! 1.239 841 984... e-6_R16 ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R8 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R8) ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R4 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R4) ! derived

!> Inverse meter-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_atomic_mass_unit_relationship_R16 =                     &
  Planck_constant_R16                                                   &
  / (speed_of_light_in_vacuum_R16 * unified_atomic_mass_unit_R16)
  ! 1.33102505010e-15_R16

!> Inverse meter-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_atomic_mass_unit_relationship_R8 =                      &
  real(inverse_meter_atomic_mass_unit_relationship_R16, kind=R8)

!> Inverse meter-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_atomic_mass_unit_relationship_R4 =                      &
  real(inverse_meter_atomic_mass_unit_relationship_R16, kind=R4)

!> Inverse meter-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_hartree_relationship_R16 =                              &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / Hartree_energy_R16
  ! 4.5563352529120e-8_R16

!> Inverse meter-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_hartree_relationship_R8 =                               &
  real(inverse_meter_hartree_relationship_R16, kind=R8)

!> Inverse meter-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_hartree_relationship_R4 =                               &
  real(inverse_meter_hartree_relationship_R16, kind=R4)


!> Hertz-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_joule_relationship_R16 =                                        &
  Planck_constant_R16
  ! 6.62607015e-34_R16 - derived

!> Hertz-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_joule_relationship_R8 =                                         &
  real(hertz_joule_relationship_R16, kind=R8)

!> Hertz-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_joule_relationship_R4 =                                         &
  real(hertz_joule_relationship_R16, kind=R4)

!> Hertz-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_kilogram_relationship_R16 =   Planck_constant_R16               &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   7.372 497 323... e-51_R16 ! derived

!> Hertz-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_kilogram_relationship_R8 =                                      &
  real(hertz_kilogram_relationship_R16, kind=R8) ! derived

! UNDERFLOW
! !> Hertz-kilogram relationship, kg
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   hertz_kilogram_relationship_R4 =                                      &
!   real(hertz_kilogram_relationship_R16, kind=R4) ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_inverse_meter_relationship_R16 =                                &
  1.0_R16 / speed_of_light_in_vacuum_R16
!   3.335 640 951... e-9_R16 ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_inverse_meter_relationship_R8 =                                 &
  real(hertz_inverse_meter_relationship_R16, kind=R8) ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_inverse_meter_relationship_R4 =                                 &
  real(hertz_inverse_meter_relationship_R16, kind=R4) ! derived

!> Hertz-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_kelvin_relationship_R16 =                                       &
  Planck_constant_R16 / Boltzmann_constant_R16
!   4.799 243 073... e-11_R16 ! derived

!> Hertz-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_kelvin_relationship_R8 =                                        &
  real(hertz_kelvin_relationship_R16, kind=R8) ! derived

!> Hertz-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_kelvin_relationship_R4 =                                        &
  real(hertz_kelvin_relationship_R16, kind=R4) ! derived

!> Hertz-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_electron_volt_relationship_R16 =                                &
  Planck_constant_R16 / electron_volt_R16
!   4.135 667 696... e-15_R16 ! derived

!> Hertz-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_electron_volt_relationship_R8 =                                 &
  real(hertz_electron_volt_relationship_R16, kind=R8) ! derived

!> Hertz-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_electron_volt_relationship_R4 =                                 &
  real(hertz_electron_volt_relationship_R16, kind=R4) ! derived

!> Hertz-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_atomic_mass_unit_relationship_R16 =                             &
  Planck_constant_R16                                                   &
  / atomic_mass_constant_energy_equivalent_R16
  ! 4.4398216652e-24_R16

!> Hertz-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_atomic_mass_unit_relationship_R8 =                              &
  real(hertz_atomic_mass_unit_relationship_R16, kind=R8)

!> Hertz-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_atomic_mass_unit_relationship_R4 =                              &
  real(hertz_atomic_mass_unit_relationship_R16, kind=R4)

!> Hertz-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_hartree_relationship_R16 =                                      &
  Planck_constant_R16 / Hartree_energy_R16
  !  1.5198298460570e-16_R16

!> Hertz-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_hartree_relationship_R8 =                                       &
  real(hertz_hartree_relationship_R16, kind=R8)

!> Hertz-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_hartree_relationship_R4 =                                       &
  real(hertz_hartree_relationship_R16, kind=R4)


!> Kelvin-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_joule_relationship_R16 =                                       &
  Boltzmann_constant_R16
!  1.380649e-23_R16

!> Kelvin-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_joule_relationship_R8 =                                        &
  real(kelvin_joule_relationship_R16, kind=R8)

!> Kelvin-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_joule_relationship_R4 =                                        &
  real(kelvin_joule_relationship_R16, kind=R4)

!> Kelvin-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_kilogram_relationship_R16 =                                    &
  Boltzmann_constant_R16                                                &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 1.536 179 187... e-40_R16 ! derived

!> Kelvin-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_kilogram_relationship_R8 =                                     &
  real(kelvin_kilogram_relationship_R16, kind=R8) ! derived

!> Kelvin-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_kilogram_relationship_R4 =                                     &
  real(kelvin_kilogram_relationship_R16, kind=R4) ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_inverse_meter_relationship_R16 =                               &
  Boltzmann_constant_R16                                                &
  / (Planck_constant_R16 * speed_of_light_in_vacuum_R16)
!   69.503 480 04..._R16 ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_inverse_meter_relationship_R8 =                                &
  real(kelvin_inverse_meter_relationship_R16, kind=R8) ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_inverse_meter_relationship_R4 =                                &
  real(kelvin_inverse_meter_relationship_R16, kind=R4) ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_hertz_relationship_R16 =                                       &
  Boltzmann_constant_R16 / Planck_constant_R16
!   2.083 661 912... e10_R16 ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_hertz_relationship_R8 =                                        &
  real(kelvin_hertz_relationship_R16, kind=R8) ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_hertz_relationship_R4 =                                        &
  real(kelvin_hertz_relationship_R16, kind=R4) ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_electron_volt_relationship_R16 =                               &
  Boltzmann_constant_R16 / electron_volt_R16
  ! 8.617 333 262... e-5_R16 ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_electron_volt_relationship_R8 =                                &
  real(kelvin_electron_volt_relationship_R16, kind=R8) ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_electron_volt_relationship_R4 =                                &
  real(kelvin_electron_volt_relationship_R16, kind=R4) ! derived

!> Kelvin-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_atomic_mass_unit_relationship_R16 =                            &
  Boltzmann_constant_R16                                                &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16        &
  * unified_atomic_mass_unit_R16)
  ! 9.2510873014e-14_R16

!> Kelvin-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_atomic_mass_unit_relationship_R8 =                             &
  real(kelvin_atomic_mass_unit_relationship_R16, kind=R8)

!> Kelvin-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_atomic_mass_unit_relationship_R4 =                             &
  real(kelvin_atomic_mass_unit_relationship_R16, kind=R4)

!> Kelvin-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_hartree_relationship_R16 =                                     &
  Boltzmann_constant_R16 / Hartree_energy_R16
  ! 3.1668115634556e-6_R16

!> Kelvin-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_hartree_relationship_R8 =                                      &
  real(kelvin_hartree_relationship_R16, kind=R8)

!> Kelvin-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_hartree_relationship_R4 =                                      &
  real(kelvin_hartree_relationship_R16, kind=R4)


!> Electron volt-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_joule_relationship_R16 =                                &
  electron_volt_R16
  ! 1.602176634e-19_R16

!> Electron volt-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_joule_relationship_R8 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R8)

!> Electron volt-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_joule_relationship_R4 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R4)

!> Electron volt-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_kilogram_relationship_R16 =                             &
  electron_volt_R16                                                     &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 1.782 661 921... e-36_R16 ! derived

!> Electron volt-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_kilogram_relationship_R8 =                              &
  real(electron_volt_kilogram_relationship_R16, kind=R8) ! derived

!> Electron volt-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_kilogram_relationship_R4 =                              &
  real(electron_volt_kilogram_relationship_R16, kind=R4) ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_inverse_meter_relationship_R16 =                        &
  electron_volt_R16                                                     &
  / (Planck_constant_R16 * speed_of_light_in_vacuum_R16)
  ! 8.065 543 937... e5_R16 ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R8 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R8) ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R4 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R4) ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_hertz_relationship_R16 =                                &
  electron_volt_R16 / Planck_constant_R16
  ! 2.417 989 242... e14_R16 ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_hertz_relationship_R8 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R8) ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_hertz_relationship_R4 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R4) ! derived

!> Electron volt-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_kelvin_relationship_R16 =                               &
  electron_volt_R16 / Boltzmann_constant_R16
  ! 1.160 451 812... e4_R16 ! derived

!> Electron volt-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R8 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R8) ! derived

!> Electron volt-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R4 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R4) ! derived

!> Electron volt-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_atomic_mass_unit_relationship_R16 =                     &
  electron_volt_R16                                                     &
  / (unified_atomic_mass_unit_R16                                       &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 1.07354410233e-9_R16

!> Electron volt-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_atomic_mass_unit_relationship_R8 =                      &
  real(electron_volt_atomic_mass_unit_relationship_R16, kind=R8)

!> Electron volt-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_atomic_mass_unit_relationship_R4 =                      &
  real(electron_volt_atomic_mass_unit_relationship_R16, kind=R4)

!> Electron volt-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_hartree_relationship_R16 =                              &
  electron_volt_R16 / Hartree_energy_R16
  ! 3.6749322175655e-2_R16

!> Electron volt-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_hartree_relationship_R8 =                               &
  real(electron_volt_hartree_relationship_R16, kind=R8)

!> Electron volt-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_hartree_relationship_R4 =                               &
  real(electron_volt_hartree_relationship_R16, kind=R4)


!> Atomic mass unit-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_joule_relationship_R16 =                             &
  atomic_mass_constant_energy_equivalent_R16
  ! 1.49241808560e-10_R16

!> Atomic mass unit-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_joule_relationship_R8 =                              &
  real(atomic_mass_unit_joule_relationship_R16, kind=R8)

!> Atomic mass unit-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_joule_relationship_R4 =                              &
  real(atomic_mass_unit_joule_relationship_R16, kind=R4)

!> Atomic mass unit-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_kilogram_relationship_R16 =                          &
  atomic_mass_constant_R16
  ! 1.66053906660e-27_R16

!> Atomic mass unit-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_kilogram_relationship_R8 =                           &
  real(atomic_mass_unit_kilogram_relationship_R16, kind=R8)

!> Atomic mass unit-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_kilogram_relationship_R4 =                           &
  real(atomic_mass_unit_kilogram_relationship_R16, kind=R4)

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_inverse_meter_relationship_R16 =                     &
  unified_atomic_mass_unit_R16 * speed_of_light_in_vacuum_R16           &
  / Planck_constant_R16
  ! 7.5130066104e14_R16

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_inverse_meter_relationship_R8 =                      &
  real(atomic_mass_unit_inverse_meter_relationship_R16, kind=R8)

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_inverse_meter_relationship_R4 =                      &
  real(atomic_mass_unit_inverse_meter_relationship_R16, kind=R4)

!> Atomic mass unit-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_hertz_relationship_R16 =                             &
  atomic_mass_constant_energy_equivalent_R16                            &
  / Planck_constant_R16
  ! 2.25234271871e23_R16

!> Atomic mass unit-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_hertz_relationship_R8 =                              &
  real(atomic_mass_unit_hertz_relationship_R16, kind=R8)

!> Atomic mass unit-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_hertz_relationship_R4 =                              &
  real(atomic_mass_unit_hertz_relationship_R16, kind=R4)

!> Atomic mass unit-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_kelvin_relationship_R16 =                            &
  atomic_mass_constant_energy_equivalent_R16                            &
  / Boltzmann_constant_R16
  ! 1.08095401916e13_R16

!> Atomic mass unit-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_kelvin_relationship_R8 =                             &
  real(atomic_mass_unit_kelvin_relationship_R16, kind=R8)

!> Atomic mass unit-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_kelvin_relationship_R4 =                             &
  real(atomic_mass_unit_kelvin_relationship_R16, kind=R4)

!> Atomic mass unit-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_electron_volt_relationship_R16 =                     &
  1.0E6_R16 * atomic_mass_constant_energy_equivalent_in_MeV_R16
  ! 9.3149410242e8_R16

!> Atomic mass unit-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_electron_volt_relationship_R8 =                      &
  real(atomic_mass_unit_electron_volt_relationship_R16, kind=R8)

!> Atomic mass unit-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_electron_volt_relationship_R4 =                      &
  real(atomic_mass_unit_electron_volt_relationship_R16, kind=R4)

!> Atomic mass unit-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_hartree_relationship_R16 =                           &
  atomic_mass_constant_energy_equivalent_R16                            &
  / Hartree_energy_R16
  ! 3.4231776874e7_R16

!> Atomic mass unit-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_hartree_relationship_R8 =                            &
  real(atomic_mass_unit_hartree_relationship_R16, kind=R8)

!> Atomic mass unit-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_hartree_relationship_R4 =                            &
  real(atomic_mass_unit_hartree_relationship_R16, kind=R4)


!> Hartree-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_joule_relationship_R16 =                                      &
  Hartree_energy_R16
  ! 4.3597447222071e-18_R16

!> Hartree-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_joule_relationship_R8 =                                       &
  real(hartree_joule_relationship_R16, kind=R8)

!> Hartree-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_joule_relationship_R4 =                                       &
  real(hartree_joule_relationship_R16, kind=R4)

!> Hartree-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_kilogram_relationship_R16 =                                   &
  Hartree_energy_R16                                                    &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 4.8508702095432e-35_R16

!> Hartree-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_kilogram_relationship_R8 =                                    &
  real(hartree_kilogram_relationship_R16, kind=R8)

!> Hartree-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_kilogram_relationship_R4 =                                    &
  real(hartree_kilogram_relationship_R16, kind=R4)

!> Hartree-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_inverse_meter_relationship_R16 =                              &
  Hartree_energy_R16                                                    &
  / (Planck_constant_R16 * speed_of_light_in_vacuum_R16)
  ! 2.1947463136320e7_R16

!> Hartree-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_inverse_meter_relationship_R8 =                               &
  real(hartree_inverse_meter_relationship_R16, kind=R8)

!> Hartree-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_inverse_meter_relationship_R4 =                               &
  real(hartree_inverse_meter_relationship_R16, kind=R4)

!> Hartree-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_hertz_relationship_R16 =                                      &
  Hartree_energy_R16 / Planck_constant_R16
  ! 6.579683920502e15_R16

!> Hartree-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_hertz_relationship_R8 =                                       &
  real(hartree_hertz_relationship_R16, kind=R8)

!> Hartree-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_hertz_relationship_R4 =                                       &
  real(hartree_hertz_relationship_R16, kind=R4)

!> Hartree-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_kelvin_relationship_R16 =                                     &
  Hartree_energy_R16 / Boltzmann_constant_R16
  ! 3.1577502480407e5_R16

!> Hartree-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_kelvin_relationship_R8 =                                      &
  real(hartree_kelvin_relationship_R16, kind=R8)

!> Hartree-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_kelvin_relationship_R4 =                                      &
  real(hartree_kelvin_relationship_R16, kind=R4)

!> Hartree-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_electron_volt_relationship_R16 =                              &
  Hartree_energy_R16 / electron_volt_R16
  ! 27.211386245988_R16

!> Hartree-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_electron_volt_relationship_R8 =                               &
  real(hartree_electron_volt_relationship_R16, kind=R8)

!> Hartree-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_electron_volt_relationship_R4 =                               &
  real(hartree_electron_volt_relationship_R16, kind=R4)

!> Hartree-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_atomic_mass_unit_relationship_R16 =                           &
  Hartree_energy_R16                                                    &
  / (unified_atomic_mass_unit_R16                                       &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 2.92126232205e-8_R16

!> Hartree-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_atomic_mass_unit_relationship_R8 =                            &
  real(hartree_atomic_mass_unit_relationship_R16, kind=R8)

!> Hartree-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_atomic_mass_unit_relationship_R4 =                            &
  real(hartree_atomic_mass_unit_relationship_R16, kind=R4)

contains

!> Derived unit checks
subroutine check()
  use, intrinsic :: ISO_FORTRAN_ENV, only: stdout => OUTPUT_UNIT
  implicit none

1 format(A)
! 2 format(ES44.35E4) ! +1.000000000000000000000000000000000000E+0000
3 format(A, T62, ES44.35E4, ' =? ', A)

continue

write(unit=stdout, fmt=1)                                             &
  '! ---- STDLIB_CODATA_1998 Derived Unit Checks ----'

!   write(unit=stdout, fmt=3)                                             &
!   'characteristic_impedance_of_vacuum_R16',                           &
!   characteristic_impedance_of_vacuum_R16,                             &
!   '376.730 313 461...'

! ! write(unit=stdout, fmt=3)                                             &
! !   'electric_constant_R16',                                            &
! !   electric_constant_R16,                                              &
! !   '8.854 187 817... e-12'

! write(unit=stdout, fmt=3)                                             &
!   'joule_kilogram_relationship_R16',                                  &
!   joule_kilogram_relationship_R16,                                    &
!   '1.112 650 056... e-17'

! write(unit=stdout, fmt=3)                                             &
!   'joule_inverse_meter_relationship_R16',                             &
!   joule_inverse_meter_relationship_R16,                               &
!   '5.034 117 47... e24'

! write(unit=stdout, fmt=3)                                             &
!   'joule_hertz_relationship_R16',                                     &
!   joule_hertz_relationship_R16,                                       &
!   '1.509 190 450... e33'

! write(unit=stdout, fmt=3)                                             &
!   'joule_kelvin_relationship_R16',                                    &
!   joule_kelvin_relationship_R16,                                      &
!   '7.242 963... e22'

! write(unit=stdout, fmt=3)                                             &
!   'joule_electron_volt_relationship_R16',                             &
!   joule_electron_volt_relationship_R16,                               &
!   '6.241 509 65... e18'

! write(unit=stdout, fmt=3)                                             &
!   'joule_atomic_mass_unit_relationship_R16',                          &
!   joule_atomic_mass_unit_relationship_R16,                            &
!   '6.700 536 41... e9'

! write(unit=stdout, fmt=3)                                             &
!   'joule_hartree_relationship_R16',                                   &
!   joule_hartree_relationship_R16,                                     &
!   '2.293 712 69... e17'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_joule_relationship_R16',                                  &
!   kilogram_joule_relationship_R16,                                    &
!   '8.987 551 787... e16'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_inverse_meter_relationship_R16',                          &
!   kilogram_inverse_meter_relationship_R16,                            &
!   '4.524 439 15... e41'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_hertz_relationship_R16',                                  &
!   kilogram_hertz_relationship_R16,                                    &
!   '1.356 392 733... e50'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_kelvin_relationship_R16',                                 &
!   kilogram_kelvin_relationship_R16,                                   &
!   '6.509 651... e39'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_electron_volt_relationship_R16',                          &
!   kilogram_electron_volt_relationship_R16,                            &
!   '5.609 589 12... e35'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_atomic_mass_unit_relationship_R16',                       &
!   kilogram_atomic_mass_unit_relationship_R16,                         &
!   '6.022 141 79... e26'

! write(unit=stdout, fmt=3)                                             &
!   'kilogram_hartree_relationship_R16',                                &
!   kilogram_hartree_relationship_R16,                                  &
!   '2.061 486 16... e34'


! write(unit=stdout, fmt=3)                                             &
!   'inverse_meter_joule_relationship_R16',                             &
!   inverse_meter_joule_relationship_R16,                               &
!   '1.986 445 501... e-25'

! write(unit=stdout, fmt=3)                                             &
!   'inverse_meter_kilogram_relationship_R16',                          &
!   inverse_meter_kilogram_relationship_R16,                            &
!   '2.210 218 70... e-42'

! write(unit=stdout, fmt=3)                                             &
!   'inverse_meter_hertz_relationship_R16',                             &
!   inverse_meter_hertz_relationship_R16,                               &
!   '299 792 458'

! write(unit=stdout, fmt=3)                                             &
!   'inverse_meter_kelvin_relationship_R16',                            &
!   inverse_meter_kelvin_relationship_R16,                              &
!   '1.438 7752... e-2'

! write(unit=stdout, fmt=3)                                             &
!   'inverse_meter_electron_volt_relationship_R16',                     &
!   inverse_meter_electron_volt_relationship_R16,                       &
!   '1.239 841 875... e-6'

! write(unit=stdout, fmt=3)                                             &
!   'inverse_meter_atomic_mass_unit_relationship_R16',                  &
!   inverse_meter_atomic_mass_unit_relationship_R16,                    &
!   '1.331 025 0394... e-15'

! write(unit=stdout, fmt=3)                                             &
!   'inverse_meter_hartree_relationship_R16',                           &
!   inverse_meter_hartree_relationship_R16,                             &
!   '4.556 335 252 760... e-8'


! write(unit=stdout, fmt=3)                                             &
!   'hertz_joule_relationship_R16',                                     &
!   hertz_joule_relationship_R16,                                       &
!   '6.626 068 96... e−34'

! write(unit=stdout, fmt=3)                                             &
!   'hertz_kilogram_relationship_R16',                                  &
!   hertz_kilogram_relationship_R16,                                    &
!   '7.372 496 00... e-51'

! write(unit=stdout, fmt=3)                                             &
!   'hertz_inverse_meter_relationship_R16',                             &
!   hertz_inverse_meter_relationship_R16,                               &
!   '3.335 640 951 ... e-9'

! write(unit=stdout, fmt=3)                                             &
!   'hertz_kelvin_relationship_R16',                                    &
!   hertz_kelvin_relationship_R16,                                      &
!   '4.799 2374... e-11'

! write(unit=stdout, fmt=3)                                             &
!   'hertz_electron_volt_relationship_R16',                             &
!   hertz_electron_volt_relationship_R16,                               &
!   '4.135 667 33... e-15'

! write(unit=stdout, fmt=3)                                             &
!   'hertz_atomic_mass_unit_relationship_R16',                          &
!   hertz_atomic_mass_unit_relationship_R16,                            &
!   '4.439 821 6294... e-24'

! write(unit=stdout, fmt=3)                                             &
!   'hertz_hartree_relationship_R16',                                   &
!   hertz_hartree_relationship_R16,                                     &
!   '1.519 829 846 006... e-16'


! write(unit=stdout, fmt=3)                                             &
!   'kelvin_joule_relationship_R16',                                    &
!   kelvin_joule_relationship_R16,                                      &
!   '1.380 6504... e−23'

! write(unit=stdout, fmt=3)                                             &
!   'kelvin_kilogram_relationship_R16',                                 &
!   kelvin_kilogram_relationship_R16,                                   &
!   '1.536 1807... e-40'

! write(unit=stdout, fmt=3)                                             &
!   'kelvin_inverse_meter_relationship_R16',                            &
!   kelvin_inverse_meter_relationship_R16,                              &
!   '69.503 56 ...'

! write(unit=stdout, fmt=3)                                             &
!   'kelvin_hertz_relationship_R16',                                    &
!   kelvin_hertz_relationship_R16,                                      &
!   '2.083 6644... e10'

! write(unit=stdout, fmt=3)                                             &
!   'kelvin_electron_volt_relationship_R16',                            &
!   kelvin_electron_volt_relationship_R16,                              &
!   '8.617 343... e-5'

! write(unit=stdout, fmt=3)                                             &
!   'kelvin_atomic_mass_unit_relationship_R16',                         &
!   kelvin_atomic_mass_unit_relationship_R16,                           &
!   '9.251 098... e-14'

! write(unit=stdout, fmt=3)                                             &
!   'kelvin_hartree_relationship_R16',                                  &
!   kelvin_hartree_relationship_R16,                                    &
!   '3.166 8153... e-6'


! write(unit=stdout, fmt=3)                                             &
!   'electron_volt_joule_relationship_R16',                             &
!   electron_volt_joule_relationship_R16,                               &
!   '1.602 176 487... e-19'

! write(unit=stdout, fmt=3)                                             &
!   'electron_volt_kilogram_relationship_R16',                          &
!   electron_volt_kilogram_relationship_R16,                            &
!   '1.782 661 758... e-36'

! write(unit=stdout, fmt=3)                                             &
!   'electron_volt_inverse_meter_relationship_R16',                     &
!   electron_volt_inverse_meter_relationship_R16,                       &
!   '8.065 544 65... e5'

! write(unit=stdout, fmt=3)                                             &
!   'electron_volt_hertz_relationship_R16',                             &
!   electron_volt_hertz_relationship_R16,                               &
!   '2.417 989 454... e14'

! write(unit=stdout, fmt=3)                                             &
!   'electron_volt_kelvin_relationship_R16',                            &
!   electron_volt_kelvin_relationship_R16,                              &
!   '1.160 4505... e4'

! write(unit=stdout, fmt=3)                                             &
!   'electron_volt_atomic_mass_unit_relationship_R16',                  &
!   electron_volt_atomic_mass_unit_relationship_R16,                    &
!   '1.073 544 188... e-9'

! write(unit=stdout, fmt=3)                                             &
!   'electron_volt_hartree_relationship_R16',                           &
!   electron_volt_hartree_relationship_R16,                             &
!   '3.674 932 540... e-2'


! write(unit=stdout, fmt=3)                                               &
!   'atomic_mass_unit_joule_relationship_R16',                            &
!   atomic_mass_unit_joule_relationship_R16,                              &
!   '1.492 417 830... e-10'

! write(unit=stdout, fmt=3)                                               &
!   'atomic_mass_unit_kilogram_relationship_R16',                         &
!   atomic_mass_unit_kilogram_relationship_R16,                           &
!   '1.660 538 782... e-27'

! write(unit=stdout, fmt=3)                                               &
!   'atomic_mass_unit_inverse_meter_relationship_R16',                    &
!   atomic_mass_unit_inverse_meter_relationship_R16,                      &
!   '7.513 006 671... e14'

! write(unit=stdout, fmt=3)                                               &
!   'atomic_mass_unit_hertz_relationship_R16',                            &
!   atomic_mass_unit_hertz_relationship_R16,                              &
!   '2.252 342 7369... e14'

! write(unit=stdout, fmt=3)                                               &
!   'atomic_mass_unit_kelvin_relationship_R16',                           &
!   atomic_mass_unit_kelvin_relationship_R16,                             &
!   '1.080 9527... e13'

! write(unit=stdout, fmt=3)                                               &
!   'atomic_mass_unit_electron_volt_relationship_R16',                    &
!   atomic_mass_unit_electron_volt_relationship_R16,                      &
!   '931.494 028... e6'

! write(unit=stdout, fmt=3)                                               &
!   'atomic_mass_unit_hartree_relationship_R16',                          &
!   atomic_mass_unit_hartree_relationship_R16,                            &
!   '3.423 177 7149... e7'


! write(unit=stdout, fmt=3)                                               &
!   'hartree_joule_relationship_R16',                                     &
!   hartree_joule_relationship_R16,                                       &
!   '4.359 743 94... e-18'

! write(unit=stdout, fmt=3)                                               &
!   'hartree_kilogram_relationship_R16',                                  &
!   hartree_kilogram_relationship_R16,                                    &
!   '4.850 869 34... e-35'

! write(unit=stdout, fmt=3)                                               &
!   'hartree_inverse_meter_relationship_R16',                             &
!   hartree_inverse_meter_relationship_R16,                               &
!   '2.194 746 313 705... e7'

! write(unit=stdout, fmt=3)                                               &
!   'hartree_hertz_relationship_R16',                                     &
!   hartree_hertz_relationship_R16,                                       &
!   '6.579 683 920 722... e15'

! write(unit=stdout, fmt=3)                                               &
!   'hartree_kelvin_relationship_R16',                                    &
!   hartree_kelvin_relationship_R16,                                      &
!   '3.157 7465... e5'

! write(unit=stdout, fmt=3)                                               &
!   'hartree_electron_volt_relationship_R16',                             &
!   hartree_electron_volt_relationship_R16,                               &
!   '27.211 383 86...'

! write(unit=stdout, fmt=3)                                               &
!   'hartree_atomic_mass_unit_relationship_R16',                          &
!   hartree_atomic_mass_unit_relationship_R16,                            &
!   '2.921 262 2986... e-8'

  return
end subroutine check
end module STDLIB_CODATA_1998
