!> @file STDLIB_CODATA_2014.f90
!! @author Robert Apthorpe
!! @copyright See LICENSE
!!
!! @brief Literal and derived fundamental physical constants consistent
!! with CODATA Bulletin No. ??

!> @brief Literal and derived fundamental physical constants consistent
!! with CODATA Bulletin No. ??
!!
!! Unless otherwise noted, all data has been taken from @cite Mohr2016
module STDLIB_CODATA_2014
use, intrinsic :: ISO_FORTRAN_ENV, only: R4 => REAL32, R8 => REAL64,    &
  R16 => REAL128
implicit none

public :: check

private

! Constants in REAL128 precision, selected_real_kind(33, 4931)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 33 significant digits.
real(kind=R16), parameter, public :: pi_R16 =                           &
  3.14159265358979323846264338327950_R16

! Constants in REAL64 precision, selected_real_kind(15, 307)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 15 significant digits.
real(kind=R8), parameter, public :: pi_R8 =                             &
  real(pi_R16, kind=R8)

! Constants in REAL32 precision, selected_real_kind(6, 37)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 6 significant digits.
real(kind=R4), parameter, public :: pi_R4 =                             &
  real(pi_R16, kind=R4)

!-----
! Universal constants

!> Speed of light in vacuum, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  speed_of_light_in_vacuum_R16 =                                        &
  299792458.0_R16

!> Speed of light in vacuum, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  speed_of_light_in_vacuum_R8 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R8)

!> Speed of light in vacuum, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  speed_of_light_in_vacuum_R4 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R4)

!> Mag. constant, N A^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  magnetic_constant_R16 = 4.0E-7_R16 * pi_R16
!   12.566 370 614... e-7_R16 ! derived

!> Mag. constant, N A^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  magnetic_constant_R8 =                                                &
  real(magnetic_constant_R16, kind=R8) ! derived

!> Mag. constant, N A^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  magnetic_constant_R4 =                                                &
  real(magnetic_constant_R16, kind=R4) ! derived

!> Electric constant, F m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electric_constant_R16 =                                               &
  1.0_R16 / (magnetic_constant_R16                                      &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   8.854 187 817... e-12_R16 ! derived

!> Electric constant, F m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electric_constant_R8 =                                                &
  real(electric_constant_R16, kind=R8) ! derived

!> Electric constant, F m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electric_constant_R4 =                                                &
  real(electric_constant_R16, kind=R4) ! derived

!> Characteristic impedance of vacuum, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  characteristic_impedance_of_vacuum_R16 =                              &
  magnetic_constant_R16 * speed_of_light_in_vacuum_R16
!   376.730 313 461..._R16 ! derived

!> Characteristic impedance of vacuum, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  characteristic_impedance_of_vacuum_R8 =                               &
  real(characteristic_impedance_of_vacuum_R16, kind=R8) ! derived

!> Characteristic impedance of vacuum, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  characteristic_impedance_of_vacuum_R4 =                               &
  real(characteristic_impedance_of_vacuum_R16, kind=R4) ! derived

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Newtonian_constant_of_gravitation_R16 =                               &
  6.67408e-11_R16

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R8 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R8)

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R4 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R4)

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Newtonian_constant_of_gravitation_over_h_bar_c_R16 =                  &
  6.70861e-39_R16

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_over_h_bar_c_R8 =                   &
  real(Newtonian_constant_of_gravitation_over_h_bar_c_R16, kind=R8)

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_over_h_bar_c_R4 =                   &
  real(Newtonian_constant_of_gravitation_over_h_bar_c_R16, kind=R4)

!> Planck constant, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_constant_R16 =                                                 &
  6.626070040e-34_R16

!> Planck constant, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_constant_R8 =                                                  &
  real(Planck_constant_R16, kind=R8)

!> Planck constant, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_constant_R4 =                                                  &
  real(Planck_constant_R16, kind=R4)

!> Planck constant in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_constant_in_eV_Hz_R16 =                                        &
  4.135667662e-15_R16

!> Planck constant in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_constant_in_eV_Hz_R8 =                                         &
  real(Planck_constant_in_eV_Hz_R16, kind=R8)

!> Planck constant in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_constant_in_eV_Hz_R4 =                                         &
  real(Planck_constant_in_eV_Hz_R16, kind=R4)

!> Planck constant over 2 pi, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_R16 =                                         &
  1.054571800e-34_R16

!> Planck constant over 2 pi, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_R8 =                                          &
  real(reduced_Planck_constant_R16, kind=R8)

!> Planck constant over 2 pi, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_R4 =                                          &
  real(reduced_Planck_constant_R16, kind=R4)

!> Planck constant over 2 pi in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_in_eV_s_R16 =                                 &
  6.582119514e-16_R16

!> Planck constant over 2 pi in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_in_eV_s_R8 =                                  &
  real(reduced_Planck_constant_in_eV_s_R16, kind=R8)

!> Planck constant over 2 pi in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_in_eV_s_R4 =                                  &
  real(reduced_Planck_constant_in_eV_s_R16, kind=R4)

!> Planck constant over 2 pi times c in MeV fm, MeV fm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_times_c_in_MeV_fm_R16 =                       &
  197.3269788_R16

!> Planck constant over 2 pi times c in MeV fm, MeV fm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_times_c_in_MeV_fm_R8 =                        &
  real(reduced_Planck_constant_times_c_in_MeV_fm_R16, kind=R8)

!> Planck constant over 2 pi times c in MeV fm, MeV fm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_times_c_in_MeV_fm_R4 =                        &
  real(reduced_Planck_constant_times_c_in_MeV_fm_R16, kind=R4)

!> Planck mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_mass_R16 =                                                     &
  2.176470e-8_R16

!> Planck mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_mass_R8 =                                                      &
  real(Planck_mass_R16, kind=R8)

!> Planck mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_mass_R4 =                                                      &
  real(Planck_mass_R16, kind=R4)

!> Planck mass energy equivalent in GeV, GeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_mass_energy_equivalent_in_GeV_R16 =                            &
  1.220910e19_R16

!> Planck mass energy equivalent in GeV, GeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_mass_energy_equivalent_in_GeV_R8 =                             &
  real(Planck_mass_energy_equivalent_in_GeV_R16, kind=R8)

!> Planck mass energy equivalent in GeV, GeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_mass_energy_equivalent_in_GeV_R4 =                             &
  real(Planck_mass_energy_equivalent_in_GeV_R16, kind=R4)

!> Planck temperature, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_temperature_R16 =                                              &
  1.416808e32_R16

!> Planck temperature, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_temperature_R8 =                                               &
  real(Planck_temperature_R16, kind=R8)

!> Planck temperature, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_temperature_R4 =                                               &
  real(Planck_temperature_R16, kind=R4)

!> Planck length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_length_R16 =                                                   &
  1.616229e-35_R16

!> Planck length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_length_R8 =                                                    &
  real(Planck_length_R16, kind=R8)

!> Planck length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_length_R4 =                                                    &
  real(Planck_length_R16, kind=R4)

!> Planck time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_time_R16 =                                                     &
  5.39116e-44_R16

!> Planck time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_time_R8 =                                                      &
  real(Planck_time_R16, kind=R8)

!> Planck time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_time_R4 =                                                      &
  real(Planck_time_R16, kind=R4)

!-----
! Electromagnetic constants

!> Elementary charge, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  elementary_charge_R16 =                                               &
  1.6021766208e-19_R16

!> Elementary charge, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  elementary_charge_R8 =                                                &
  real(elementary_charge_R16, kind=R8)

!> Elementary charge, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  elementary_charge_R4 =                                                &
  real(elementary_charge_R16, kind=R4)

!> Elementary charge over h, A J^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  elementary_charge_over_h_R16 =                                        &
  2.417989262e14_R16

!> Elementary charge over h, A J^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  elementary_charge_over_h_R8 =                                         &
  real(elementary_charge_over_h_R16, kind=R8)

!> Elementary charge over h, A J^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  elementary_charge_over_h_R4 =                                         &
  real(elementary_charge_over_h_R16, kind=R4)

!> Mag. flux quantum, Wb
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  magnetic_flux_quantum_R16 =                                           &
  2.067833831e-15_R16

!> Mag. flux quantum, Wb
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  magnetic_flux_quantum_R8 =                                            &
  real(magnetic_flux_quantum_R16, kind=R8)

!> Mag. flux quantum, Wb
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  magnetic_flux_quantum_R4 =                                            &
  real(magnetic_flux_quantum_R16, kind=R4)

!> Conductance quantum, S
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conductance_quantum_R16 =                                             &
  7.7480917310e-5_R16

!> Conductance quantum, S
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conductance_quantum_R8 =                                              &
  real(conductance_quantum_R16, kind=R8)

!> Conductance quantum, S
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conductance_quantum_R4 =                                              &
  real(conductance_quantum_R16, kind=R4)

!> Inverse of conductance quantum, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_of_conductance_quantum_R16 =                                  &
  12906.4037278_R16

!> Inverse of conductance quantum, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_of_conductance_quantum_R8 =                                   &
  real(inverse_of_conductance_quantum_R16, kind=R8)

!> Inverse of conductance quantum, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_of_conductance_quantum_R4 =                                   &
  real(inverse_of_conductance_quantum_R16, kind=R4)

!> Josephson constant, Hz V^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Josephson_constant_R16 =                                              &
  483597.8525e9_R16

!> Josephson constant, Hz V^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Josephson_constant_R8 =                                               &
  real(Josephson_constant_R16, kind=R8)

!> Josephson constant, Hz V^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Josephson_constant_R4 =                                               &
  real(Josephson_constant_R16, kind=R4)

!> Von Klitzing constant, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  von_Klitzing_constant_R16 =                                           &
  25812.8074555_R16

!> Von Klitzing constant, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  von_Klitzing_constant_R8 =                                            &
  real(von_Klitzing_constant_R16, kind=R8)

!> Von Klitzing constant, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  von_Klitzing_constant_R4 =                                            &
  real(von_Klitzing_constant_R16, kind=R4)

! !> Quantized Hall conductance, S
! !! (at most 33 significant digits)
!   real(kind=R16), parameter, public ::                                  &
!   quantized_hall_conductance_R16 =                                      &
!   3.87404614E-5_R16
! !  elementary_charge_R16 * elementary_charge_R16 /Planck_constant_R16

! !> Quantized Hall conductance, S
! !! (at most 15 significant digits)
! real(kind=R8), parameter, public ::                                     &
!   quantized_hall_conductance_R8 =                                       &
!   real(quantized_hall_conductance_R16, kind=R8)

! !> Quantized Hall conductance, S
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   quantized_hall_conductance_R4 =                                       &
!   real(quantized_hall_conductance_R16, kind=R4)

! !> Quantized Hall resistance, ohm
! !! (at most 33 significant digits)
!   real(kind=R16), parameter, public ::                                  &
!   quantized_hall_resistance_R16 =                                       &
!   25812.8056_R16
! !  elementary_charge_R16 * elementary_charge_R16 /Planck_constant_R16

! !> Quantized Hall resistance, ohm
! !! (at most 15 significant digits)
! real(kind=R8), parameter, public ::                                     &
!   quantized_hall_resistance_R8 =                                        &
!   real(quantized_hall_resistance_R16, kind=R8)

! !> Quantized Hall resistance, ohm
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   quantized_hall_resistance_R4 =                                        &
!   real(quantized_hall_resistance_R16, kind=R4)

!> Bohr magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_R16 =                                                   &
  927.4009994e-26_R16

!> Bohr magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_R8 =                                                    &
  real(Bohr_magneton_R16, kind=R8)

!> Bohr magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_R4 =                                                    &
  real(Bohr_magneton_R16, kind=R4)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_eV_T_R16 =                                           &
  5.7883818012e-5_R16

!> Bohr magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R8 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R8)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R4 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R4)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_Hz_T_R16 =                                           &
  13.996245042e9_R16

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R8 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R8)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R4 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R4)

!> Bohr magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_inverse_meter_per_tesla_R16 =                        &
  46.68644814_R16

!> Bohr magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R8 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Bohr magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R4 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Bohr magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_K_T_R16 =                                            &
  0.67171405_R16

!> Bohr magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R8 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R8)

!> Bohr magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R4 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R4)

!> Nuclear magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_R16 =                                                &
  5.050783699e-27_R16

!> Nuclear magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_R8 =                                                 &
  real(nuclear_magneton_R16, kind=R8)

!> Nuclear magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_R4 =                                                 &
  real(nuclear_magneton_R16, kind=R4)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_eV_T_R16 =                                        &
  3.1524512550e-8_R16

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R8 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R8)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R4 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R4)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_MHz_T_R16 =                                       &
  7.622593285_R16

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R8 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R8)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R4 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R4)

!> Nuclear magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_inverse_meter_per_tesla_R16 =                     &
  2.542623432e-2_R16

!> Nuclear magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R8 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Nuclear magneton in inverse meters per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R4 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Nuclear magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_K_T_R16 =                                         &
  3.6582690e-4_R16

!> Nuclear magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R8 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R8)

!> Nuclear magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R4 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R4)

!-----
! Atomic constants

!-----
! General

!> Fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  fine_structure_constant_R16 =                                         &
  7.2973525664e-3_R16

!> Fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  fine_structure_constant_R8 =                                          &
  real(fine_structure_constant_R16, kind=R8)

!> Fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  fine_structure_constant_R4 =                                          &
  real(fine_structure_constant_R16, kind=R4)

!> Inverse fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_fine_structure_constant_R16 =                                 &
  137.035999139_R16

!> Inverse fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_fine_structure_constant_R8 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R8)

!> Inverse fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_fine_structure_constant_R4 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R4)

!> Rydberg constant, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_R16 =                                                &
  10973731.568508_R16

!> Rydberg constant, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_R8 =                                                 &
  real(Rydberg_constant_R16, kind=R8)

!> Rydberg constant, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_R4 =                                                 &
  real(Rydberg_constant_R16, kind=R4)

!> Rydberg constant times c in Hz, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_c_in_Hz_R16 =                                  &
  3.289841960355e15_R16

!> Rydberg constant times c in Hz, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R8 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R8)

!> Rydberg constant times c in Hz, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R4 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R4)

!> Rydberg constant times hc in J, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_J_R16 =                                  &
  2.179872325e-18_R16

!> Rydberg constant times hc in J, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R8 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R8)

!> Rydberg constant times hc in J, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R4 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R4)

!> Rydberg constant times hc in eV, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_eV_R16 =                                 &
  13.605693009_R16

!> Rydberg constant times hc in eV, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R8 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R8)

!> Rydberg constant times hc in eV, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R4 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R4)

!> Bohr radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_radius_R16 =                                                     &
  0.52917721067e-10_R16

!> Bohr radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_radius_R8 =                                                      &
  real(Bohr_radius_R16, kind=R8)

!> Bohr radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_radius_R4 =                                                      &
  real(Bohr_radius_R16, kind=R4)

!> Hartree energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Hartree_energy_R16 =                                                  &
  4.359744650e-18_R16

!> Hartree energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Hartree_energy_R8 =                                                   &
  real(Hartree_energy_R16, kind=R8)

!> Hartree energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Hartree_energy_R4 =                                                   &
  real(Hartree_energy_R16, kind=R4)

!> Hartree energy in eV, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Hartree_energy_in_eV_R16 =                                            &
  27.21138602_R16

!> Hartree energy in eV, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Hartree_energy_in_eV_R8 =                                             &
  real(Hartree_energy_in_eV_R16, kind=R8)

!> Hartree energy in eV, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Hartree_energy_in_eV_R4 =                                             &
  real(Hartree_energy_in_eV_R16, kind=R4)

!> Quantum of circulation, m^2 s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  quantum_of_circulation_R16 =                                          &
  3.6369475486e-4_R16

!> Quantum of circulation, m^2 s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  quantum_of_circulation_R8 =                                           &
  real(quantum_of_circulation_R16, kind=R8)

!> Quantum of circulation, m^2 s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  quantum_of_circulation_R4 =                                           &
  real(quantum_of_circulation_R16, kind=R4)

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  quantum_of_circulation_times_2_R16 =                                  &
  7.2738950972e-4_R16

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  quantum_of_circulation_times_2_R8 =                                   &
  real(quantum_of_circulation_times_2_R16, kind=R8)

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  quantum_of_circulation_times_2_R4 =                                   &
  real(quantum_of_circulation_times_2_R16, kind=R4)

!-----
! Electroweak

!> Fermi coupling constant, GeV^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Fermi_coupling_constant_R16 =                                         &
  1.1663787e-5_R16

!> Fermi coupling constant, GeV^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Fermi_coupling_constant_R8 =                                          &
  real(Fermi_coupling_constant_R16, kind=R8)

!> Fermi coupling constant, GeV^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Fermi_coupling_constant_R4 =                                          &
  real(Fermi_coupling_constant_R16, kind=R4)

!> Weak mixing angle, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  weak_mixing_angle_R16 =                                               &
  0.2223_R16

!> Weak mixing angle, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  weak_mixing_angle_R8 =                                                &
  real(weak_mixing_angle_R16, kind=R8)

!> Weak mixing angle, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  weak_mixing_angle_R4 =                                                &
  real(weak_mixing_angle_R16, kind=R4)

!-----
! Electron

!> Electron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_R16 =                                                   &
  9.10938356e-31_R16

!> Electron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_R8 =                                                    &
  real(electron_mass_R16, kind=R8)

!> Electron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_R4 =                                                    &
  real(electron_mass_R16, kind=R4)

!> Electron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_in_u_R16 =                                              &
  5.48579909070e-4_R16

!> Electron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_in_u_R8 =                                               &
  real(electron_mass_in_u_R16, kind=R8)

!> Electron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_in_u_R4 =                                               &
  real(electron_mass_in_u_R16, kind=R4)

!> Electron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_energy_equivalent_R16 =                                 &
  8.18710565e-14_R16

!> Electron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_energy_equivalent_R8 =                                  &
  real(electron_mass_energy_equivalent_R16, kind=R8)

!> Electron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_energy_equivalent_R4 =                                  &
  real(electron_mass_energy_equivalent_R16, kind=R4)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_energy_equivalent_in_MeV_R16 =                          &
  0.5109989461_R16

!> Electron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R8 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R4 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Electron-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_muon_mass_ratio_R16 =                                        &
  4.83633170e-3_R16

!> Electron-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_muon_mass_ratio_R8 =                                         &
  real(electron_muon_mass_ratio_R16, kind=R8)

!> Electron-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_muon_mass_ratio_R4 =                                         &
  real(electron_muon_mass_ratio_R16, kind=R4)

!> Electron-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_tau_mass_ratio_R16 =                                         &
  2.87592e-4_R16

!> Electron-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_tau_mass_ratio_R8 =                                          &
  real(electron_tau_mass_ratio_R16, kind=R8)

!> Electron-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_tau_mass_ratio_R4 =                                          &
  real(electron_tau_mass_ratio_R16, kind=R4)

!> Electron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_proton_mass_ratio_R16 =                                      &
  5.44617021352e-4_R16

!> Electron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_proton_mass_ratio_R8 =                                       &
  real(electron_proton_mass_ratio_R16, kind=R8)

!> Electron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_proton_mass_ratio_R4 =                                       &
  real(electron_proton_mass_ratio_R16, kind=R4)

!> Electron-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_neutron_mass_ratio_R16 =                                     &
  5.4386734428e-4_R16

!> Electron-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_neutron_mass_ratio_R8 =                                      &
  real(electron_neutron_mass_ratio_R16, kind=R8)

!> Electron-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_neutron_mass_ratio_R4 =                                      &
  real(electron_neutron_mass_ratio_R16, kind=R4)

!> Electron-deuteron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_deuteron_mass_ratio_R16 =                                    &
  2.724437107484e-4_R16

!> Electron-deuteron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_deuteron_mass_ratio_R8 =                                     &
  real(electron_deuteron_mass_ratio_R16, kind=R8)

!> Electron-deuteron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_deuteron_mass_ratio_R4 =                                     &
  real(electron_deuteron_mass_ratio_R16, kind=R4)

!> Electron-helion mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_helion_mass_ratio_R16 =                                      &
  1.819543074854e-4_R16

!> Electron-helion mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_helion_mass_ratio_R8 =                                       &
  real(electron_helion_mass_ratio_R16, kind=R8)

!> Electron-helion mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_helion_mass_ratio_R4 =                                       &
  real(electron_helion_mass_ratio_R16, kind=R4)

!> Electron-triton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_triton_mass_ratio_R16 =                                      &
  1.819200062203e-4_R16

!> Electron-triton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_triton_mass_ratio_R8 =                                       &
  real(electron_triton_mass_ratio_R16, kind=R8)

!> Electron-triton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_triton_mass_ratio_R4 =                                       &
  real(electron_triton_mass_ratio_R16, kind=R4)

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_alpha_particle_mass_ratio_R16 =                           &
  1.370933554798e-4_R16

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_alpha_particle_mass_ratio_R8 =                            &
  real(electron_to_alpha_particle_mass_ratio_R16, kind=R8)

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_alpha_particle_mass_ratio_R4 =                            &
  real(electron_to_alpha_particle_mass_ratio_R16, kind=R4)

!> Electron charge to mass quotient, C kg^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_charge_to_mass_quotient_R16 =                                &
  -1.758820024e11_R16

!> Electron charge to mass quotient, C kg^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R8 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R8)

!> Electron charge to mass quotient, C kg^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R4 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R4)

!> Electron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_molar_mass_R16 =                                             &
  5.48579909070e-7_R16

!> Electron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_molar_mass_R8 =                                              &
  real(electron_molar_mass_R16, kind=R8)

!> Electron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_molar_mass_R4 =                                              &
  real(electron_molar_mass_R16, kind=R4)

!> Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Compton_wavelength_R16 =                                              &
  2.4263102367e-12_R16

!> Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Compton_wavelength_R8 =                                               &
  real(Compton_wavelength_R16, kind=R8)

!> Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Compton_wavelength_R4 =                                               &
  real(Compton_wavelength_R16, kind=R4)

!> Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Compton_wavelength_R16 =                                      &
  386.15926764e-15_R16

!> Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Compton_wavelength_R8 =                                       &
  real(reduced_Compton_wavelength_R16, kind=R8)

!> Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Compton_wavelength_R4 =                                       &
  real(reduced_Compton_wavelength_R16, kind=R4)

!> Classical electron radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  classical_electron_radius_R16 =                                       &
  2.8179403227e-15_R16

!> Classical electron radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  classical_electron_radius_R8 =                                        &
  real(classical_electron_radius_R16, kind=R8)

!> Classical electron radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  classical_electron_radius_R4 =                                        &
  real(classical_electron_radius_R16, kind=R4)

!> Thomson cross section, m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Thomson_cross_section_R16 =                                           &
  0.66524587158e-28_R16

!> Thomson cross section, m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Thomson_cross_section_R8 =                                            &
  real(Thomson_cross_section_R16, kind=R8)

!> Thomson cross section, m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Thomson_cross_section_R4 =                                            &
  real(Thomson_cross_section_R16, kind=R4)

!> Electron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_R16 =                                        &
  -928.4764620e-26_R16

!> Electron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_R8 =                                         &
  real(electron_magnetic_moment_R16, kind=R8)

!> Electron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_R4 =                                         &
  real(electron_magnetic_moment_R16, kind=R4)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                 &
  -1.00115965218091_R16

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                  &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                  &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R16 =              &
  -1838.28197234_R16

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R8 =               &
  real(electron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R4 =               &
  real(electron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Electron mag. mom. anomaly, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_anomaly_R16 =                                &
  1.15965218091e-3_R16

!> Electron mag. mom. anomaly, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_anomaly_R8 =                                 &
  real(electron_magnetic_moment_anomaly_R16, kind=R8)

!> Electron mag. mom. anomaly, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_anomaly_R4 =                                 &
  real(electron_magnetic_moment_anomaly_R16, kind=R4)

!> Electron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_g_factor_R16 =                                               &
  -2.00231930436182_R16

!> Electron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_g_factor_R8 =                                                &
  real(electron_g_factor_R16, kind=R8)

!> Electron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_g_factor_R4 =                                                &
  real(electron_g_factor_R16, kind=R4)

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_muon_magnetic_moment_ratio_R16 =                             &
  206.7669880_R16

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_muon_magnetic_moment_ratio_R8 =                              &
  real(electron_muon_magnetic_moment_ratio_R16, kind=R8)

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_muon_magnetic_moment_ratio_R4 =                              &
  real(electron_muon_magnetic_moment_ratio_R16, kind=R4)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_proton_magnetic_moment_ratio_R16 =                           &
  -658.2106866_R16

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R8 =                            &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R4 =                            &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_shielded_proton_magnetic_moment_ratio_R16 =               &
  -658.2275971_R16

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_shielded_proton_magnetic_moment_ratio_R8 =                &
  real(electron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R8)

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_shielded_proton_magnetic_moment_ratio_R4 =                &
  real(electron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R4)

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_neutron_magnetic_moment_ratio_R16 =                          &
  960.92050_R16

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_neutron_magnetic_moment_ratio_R8 =                           &
  real(electron_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_neutron_magnetic_moment_ratio_R4 =                           &
  real(electron_neutron_magnetic_moment_ratio_R16, kind=R4)

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_deuteron_magnetic_moment_ratio_R16 =                         &
  -2143.923499_R16

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_deuteron_magnetic_moment_ratio_R8 =                          &
  real(electron_deuteron_magnetic_moment_ratio_R16, kind=R8)

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_deuteron_magnetic_moment_ratio_R4 =                          &
  real(electron_deuteron_magnetic_moment_ratio_R16, kind=R4)

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_shielded_helion_magnetic_moment_ratio_R16 =               &
  864.058257_R16

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_shielded_helion_magnetic_moment_ratio_R8 =                &
  real(electron_to_shielded_helion_magnetic_moment_ratio_R16, kind=R8)

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_shielded_helion_magnetic_moment_ratio_R4 =                &
  real(electron_to_shielded_helion_magnetic_moment_ratio_R16, kind=R4)

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_gyromagnetic_ratio_R16 =                                     &
  1.760859644e11_R16

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_gyromagnetic_ratio_R8 =                                      &
  real(electron_gyromagnetic_ratio_R16, kind=R8)

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_gyromagnetic_ratio_R4 =                                      &
  real(electron_gyromagnetic_ratio_R16, kind=R4)

!> Electron gyromag. ratio over 2 pi, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_gyromagnetic_ratio_in_MHz_T_R16 =                            &
  28024.95164_R16

!> Electron gyromag. ratio over 2 pi, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_gyromagnetic_ratio_in_MHz_T_R8 =                             &
  real(electron_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Electron gyromag. ratio over 2 pi, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_gyromagnetic_ratio_in_MHz_T_R4 =                             &
  real(electron_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!-----
! Muon

!> Muon mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_R16 =                                                       &
  1.883531594e-28_R16

!> Muon mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_R8 =                                                        &
  real(muon_mass_R16, kind=R8)

!> Muon mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_R4 =                                                        &
  real(muon_mass_R16, kind=R4)

!> Muon mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_in_u_R16 =                                                  &
  0.1134289257_R16

!> Muon mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_in_u_R8 =                                                   &
  real(muon_mass_in_u_R16, kind=R8)

!> Muon mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_in_u_R4 =                                                   &
  real(muon_mass_in_u_R16, kind=R4)

!> Muon mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_energy_equivalent_R16 =                                     &
  1.692833774e-11_R16

!> Muon mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_energy_equivalent_R8 =                                      &
  real(muon_mass_energy_equivalent_R16, kind=R8)

!> Muon mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_energy_equivalent_R4 =                                      &
  real(muon_mass_energy_equivalent_R16, kind=R4)

!> Muon mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_energy_equivalent_in_MeV_R16 =                              &
  105.6583745_R16

!> Muon mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_energy_equivalent_in_MeV_R8 =                               &
  real(muon_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Muon mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_energy_equivalent_in_MeV_R4 =                               &
  real(muon_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Muon-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_electron_mass_ratio_R16 =                                        &
  206.7682826_R16

!> Muon-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_electron_mass_ratio_R8 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R8)

!> Muon-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_electron_mass_ratio_R4 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R4)

!> Muon-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_tau_mass_ratio_R16 =                                             &
  5.94649e-2_R16

!> Muon-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_tau_mass_ratio_R8 =                                              &
  real(muon_tau_mass_ratio_R16, kind=R8)

!> Muon-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_tau_mass_ratio_R4 =                                              &
  real(muon_tau_mass_ratio_R16, kind=R4)

!> Muon-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_proton_mass_ratio_R16 =                                          &
  0.1126095262_R16

!> Muon-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_proton_mass_ratio_R8 =                                           &
  real(muon_proton_mass_ratio_R16, kind=R8)

!> Muon-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_proton_mass_ratio_R4 =                                           &
  real(muon_proton_mass_ratio_R16, kind=R4)

!> Muon-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_neutron_mass_ratio_R16 =                                         &
  0.1124545167_R16

!> Muon-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_neutron_mass_ratio_R8 =                                          &
  real(muon_neutron_mass_ratio_R16, kind=R8)

!> Muon-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_neutron_mass_ratio_R4 =                                          &
  real(muon_neutron_mass_ratio_R16, kind=R4)

!> Muon molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_molar_mass_R16 =                                                 &
  0.1134289257e-3_R16

!> Muon molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_molar_mass_R8 =                                                  &
  real(muon_molar_mass_R16, kind=R8)

!> Muon molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_molar_mass_R4 =                                                  &
  real(muon_molar_mass_R16, kind=R4)

!> Muon Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_Compton_wavelength_R16 =                                         &
  11.73444111e-15_R16

!> Muon Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_Compton_wavelength_R8 =                                          &
  real(muon_Compton_wavelength_R16, kind=R8)

!> Muon Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_Compton_wavelength_R4 =                                          &
  real(muon_Compton_wavelength_R16, kind=R4)

!> Muon Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_muon_Compton_wavelength_R16 =                                 &
  1.867594308e-15_R16

!> Muon Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_muon_Compton_wavelength_R8 =                                  &
  real(reduced_muon_Compton_wavelength_R16, kind=R8)

!> Muon Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_muon_Compton_wavelength_R4 =                                  &
  real(reduced_muon_Compton_wavelength_R16, kind=R4)

!> Muon mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_R16 =                                            &
  -4.49044826e-26_R16

!> Muon mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_R8 =                                             &
  real(muon_magnetic_moment_R16, kind=R8)

!> Muon mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_R4 =                                             &
  real(muon_magnetic_moment_R16, kind=R4)

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R16 =                     &
  -4.84197048e-3_R16

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R8 =                      &
  real(muon_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R4 =                      &
  real(muon_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R16 =                  &
  -8.89059705_R16

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R8 =                   &
  real(muon_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R4 =                   &
  real(muon_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Muon mag. mom. anomaly, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_anomaly_R16 =                                    &
  1.16592089e-3_R16

!> Muon mag. mom. anomaly, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_anomaly_R8 =                                     &
  real(muon_magnetic_moment_anomaly_R16, kind=R8)

!> Muon mag. mom. anomaly, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_anomaly_R4 =                                     &
  real(muon_magnetic_moment_anomaly_R16, kind=R4)

!> Muon g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_g_factor_R16 =                                                   &
  -2.0023318418_R16

!> Muon g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_g_factor_R8 =                                                    &
  real(muon_g_factor_R16, kind=R8)

!> Muon g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_g_factor_R4 =                                                    &
  real(muon_g_factor_R16, kind=R4)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_proton_magnetic_moment_ratio_R16 =                               &
  -3.183345142_R16

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R8 =                                &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R8)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R4 =                                &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R4)

!-----
! Tau

!> Tau mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_R16 =                                                        &
  3.16747e-27_R16

!> Tau mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_R8 =                                                         &
  real(tau_mass_R16, kind=R8)

!> Tau mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_R4 =                                                         &
  real(tau_mass_R16, kind=R4)

!> Tau mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_in_u_R16 =                                                   &
  1.90749_R16

!> Tau mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_in_u_R8 =                                                    &
  real(tau_mass_in_u_R16, kind=R8)

!> Tau mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_in_u_R4 =                                                    &
  real(tau_mass_in_u_R16, kind=R4)

!> Tau mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_energy_equivalent_R16 =                                      &
  2.84678e-10_R16

!> Tau mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_energy_equivalent_R8 =                                       &
  real(tau_mass_energy_equivalent_R16, kind=R8)

!> Tau mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_energy_equivalent_R4 =                                       &
  real(tau_mass_energy_equivalent_R16, kind=R4)

!> Tau mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_energy_equivalent_in_MeV_R16 =                               &
  1776.82_R16

!> Tau mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_energy_equivalent_in_MeV_R8 =                                &
  real(tau_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Tau mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_energy_equivalent_in_MeV_R4 =                                &
  real(tau_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Tau-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_electron_mass_ratio_R16 =                                         &
  3477.15_R16

!> Tau-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_electron_mass_ratio_R8 =                                          &
  real(tau_electron_mass_ratio_R16, kind=R8)

!> Tau-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_electron_mass_ratio_R4 =                                          &
  real(tau_electron_mass_ratio_R16, kind=R4)

!> Tau-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_muon_mass_ratio_R16 =                                             &
  16.8167_R16

!> Tau-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_muon_mass_ratio_R8 =                                              &
  real(tau_muon_mass_ratio_R16, kind=R8)

!> Tau-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_muon_mass_ratio_R4 =                                              &
  real(tau_muon_mass_ratio_R16, kind=R4)

!> Tau-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_proton_mass_ratio_R16 =                                           &
  1.89372_R16

!> Tau-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_proton_mass_ratio_R8 =                                            &
  real(tau_proton_mass_ratio_R16, kind=R8)

!> Tau-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_proton_mass_ratio_R4 =                                            &
  real(tau_proton_mass_ratio_R16, kind=R4)

!> Tau-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_neutron_mass_ratio_R16 =                                          &
  1.89111_R16

!> Tau-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_neutron_mass_ratio_R8 =                                           &
  real(tau_neutron_mass_ratio_R16, kind=R8)

!> Tau-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_neutron_mass_ratio_R4 =                                           &
  real(tau_neutron_mass_ratio_R16, kind=R4)

!> Tau molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_molar_mass_R16 =                                                  &
  1.90749e-3_R16

!> Tau molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_molar_mass_R8 =                                                   &
  real(tau_molar_mass_R16, kind=R8)

!> Tau molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_molar_mass_R4 =                                                   &
  real(tau_molar_mass_R16, kind=R4)

!> Tau Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_Compton_wavelength_R16 =                                          &
  0.697787e-15_R16

!> Tau Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_Compton_wavelength_R8 =                                           &
  real(tau_Compton_wavelength_R16, kind=R8)

!> Tau Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_Compton_wavelength_R4 =                                           &
  real(tau_Compton_wavelength_R16, kind=R4)

!> Tau Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_tau_Compton_wavelength_R16 =                                  &
  0.111056e-15_R16

!> Tau Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_tau_Compton_wavelength_R8 =                                   &
  real(reduced_tau_Compton_wavelength_R16, kind=R8)

!> Tau Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_tau_Compton_wavelength_R4 =                                   &
  real(reduced_tau_Compton_wavelength_R16, kind=R4)

!-----
! Proton

!> Proton mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_R16 =                                                     &
  1.672621898e-27_R16

!> Proton mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_R8 =                                                      &
  real(proton_mass_R16, kind=R8)

!> Proton mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_R4 =                                                      &
  real(proton_mass_R16, kind=R4)

!> Proton mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_in_u_R16 =                                                &
  1.007276466879_R16

!> Proton mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_in_u_R8 =                                                 &
  real(proton_mass_in_u_R16, kind=R8)

!> Proton mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_in_u_R4 =                                                 &
  real(proton_mass_in_u_R16, kind=R4)

!> Proton mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_energy_equivalent_R16 =                                   &
  1.503277593e-10_R16

!> Proton mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_energy_equivalent_R8 =                                    &
  real(proton_mass_energy_equivalent_R16, kind=R8)

!> Proton mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_energy_equivalent_R4 =                                    &
  real(proton_mass_energy_equivalent_R16, kind=R4)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_energy_equivalent_in_MeV_R16 =                            &
  938.2720813_R16

!> Proton mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R8 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R4 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Proton-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_electron_mass_ratio_R16 =                                      &
  1836.15267389_R16

!> Proton-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_electron_mass_ratio_R8 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R8)

!> Proton-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_electron_mass_ratio_R4 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R4)

!> Proton-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_muon_mass_ratio_R16 =                                          &
  8.88024338_R16

!> Proton-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_muon_mass_ratio_R8 =                                           &
  real(proton_muon_mass_ratio_R16, kind=R8)

!> Proton-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_muon_mass_ratio_R4 =                                           &
  real(proton_muon_mass_ratio_R16, kind=R4)

!> Proton-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_tau_mass_ratio_R16 =                                           &
  0.528063_R16

!> Proton-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_tau_mass_ratio_R8 =                                            &
  real(proton_tau_mass_ratio_R16, kind=R8)

!> Proton-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_tau_mass_ratio_R4 =                                            &
  real(proton_tau_mass_ratio_R16, kind=R4)

!> Proton-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_neutron_mass_ratio_R16 =                                       &
  0.99862347844_R16

!> Proton-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_neutron_mass_ratio_R8 =                                        &
  real(proton_neutron_mass_ratio_R16, kind=R8)

!> Proton-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_neutron_mass_ratio_R4 =                                        &
  real(proton_neutron_mass_ratio_R16, kind=R4)

!> Proton charge to mass quotient, C kg^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_charge_to_mass_quotient_R16 =                                  &
  9.578833226e7_R16

!> Proton charge to mass quotient, C kg^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_charge_to_mass_quotient_R8 =                                   &
  real(proton_charge_to_mass_quotient_R16, kind=R8)

!> Proton charge to mass quotient, C kg^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_charge_to_mass_quotient_R4 =                                   &
  real(proton_charge_to_mass_quotient_R16, kind=R4)

!> Proton molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_molar_mass_R16 =                                               &
  1.007276466879e-3_R16

!> Proton molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_molar_mass_R8 =                                                &
  real(proton_molar_mass_R16, kind=R8)

!> Proton molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_molar_mass_R4 =                                                &
  real(proton_molar_mass_R16, kind=R4)

!> Proton Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_Compton_wavelength_R16 =                                       &
  1.32140985396e-15_R16

!> Proton Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_Compton_wavelength_R8 =                                        &
  real(proton_Compton_wavelength_R16, kind=R8)

!> Proton Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_Compton_wavelength_R4 =                                        &
  real(proton_Compton_wavelength_R16, kind=R4)

!> Proton Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_proton_Compton_wavelength_R16 =                               &
  0.210308910109e-15_R16

!> Proton Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_proton_Compton_wavelength_R8 =                                &
  real(reduced_proton_Compton_wavelength_R16, kind=R8)

!> Proton Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_proton_Compton_wavelength_R4 =                                &
  real(reduced_proton_Compton_wavelength_R16, kind=R4)

!> Proton rms charge radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_rms_charge_radius_R16 =                                        &
  0.8751e-15_R16

!> Proton rms charge radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_rms_charge_radius_R8 =                                         &
  real(proton_rms_charge_radius_R16, kind=R8)

!> Proton rms charge radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_rms_charge_radius_R4 =                                         &
  real(proton_rms_charge_radius_R16, kind=R4)

!> Proton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_R16 =                                          &
  1.4106067873e-26_R16

!> Proton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_R8 =                                           &
  real(proton_magnetic_moment_R16, kind=R8)

!> Proton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_R4 =                                           &
  real(proton_magnetic_moment_R16, kind=R4)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R16 =                   &
  1.5210322053e-3_R16

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R8 =                    &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R4 =                    &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R16 =                &
  2.7928473508_R16

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R8 =                 &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R4 =                 &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Proton g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_g_factor_R16 =                                                 &
  5.585694702_R16

!> Proton g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_g_factor_R8 =                                                  &
  real(proton_g_factor_R16, kind=R8)

!> Proton g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_g_factor_R4 =                                                  &
  real(proton_g_factor_R16, kind=R4)

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_neutron_magnetic_moment_ratio_R16 =                            &
  -1.45989805_R16

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_neutron_magnetic_moment_ratio_R8 =                             &
  real(proton_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_neutron_magnetic_moment_ratio_R4 =                             &
  real(proton_neutron_magnetic_moment_ratio_R16, kind=R4)

!> Shielded proton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_R16 =                                 &
  1.410570547e-26_R16

!> Shielded proton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_R8 =                                  &
  real(shielded_proton_magnetic_moment_R16, kind=R8)

!> Shielded proton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_R4 =                                  &
  real(shielded_proton_magnetic_moment_R16, kind=R4)

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16 =          &
  1.520993128e-3_R16

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R8 =           &
  real(shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R8)

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R4 =           &
  real(shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R4)

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16 =       &
  2.792775600_R16

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R8 =        &
  real(shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R8)

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R4 =        &
  real(shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R4)

!> Proton mag. shielding correction, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_shielding_correction_R16 =                            &
  25.691e-6_R16

!> Proton mag. shielding correction, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_shielding_correction_R8 =                             &
  real(proton_magnetic_shielding_correction_R16, kind=R8)

!> Proton mag. shielding correction, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_shielding_correction_R4 =                             &
  real(proton_magnetic_shielding_correction_R16, kind=R4)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_R16 =                                       &
  2.675221900e8_R16

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R8 =                                        &
  real(proton_gyromagnetic_ratio_R16, kind=R8)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R4 =                                        &
  real(proton_gyromagnetic_ratio_R16, kind=R4)

!> Proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_in_MHz_T_R16 =                              &
  42.57747892_R16

!> Proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R8 =                               &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R4 =                               &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_gyromagnetic_ratio_R16 =                              &
  2.675153171e8_R16

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_R8 =                               &
  real(shielded_proton_gyromagnetic_ratio_R16, kind=R8)

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_R4 =                               &
  real(shielded_proton_gyromagnetic_ratio_R16, kind=R4)

!> Shielded proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R16 =                     &
  42.57638507_R16

!> Shielded proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R8 =                      &
  real(shielded_proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Shielded proton gyromag. ratio over 2 pi, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R4 =                      &
  real(shielded_proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!-----
! Neutron

!> Neutron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_R16 =                                                    &
  1.674927471e-27_R16

!> Neutron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_R8 =                                                     &
  real(neutron_mass_R16, kind=R8)

!> Neutron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_R4 =                                                     &
  real(neutron_mass_R16, kind=R4)

!> Neutron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_in_u_R16 =                                               &
  1.00866491588_R16

!> Neutron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_in_u_R8 =                                                &
  real(neutron_mass_in_u_R16, kind=R8)

!> Neutron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_in_u_R4 =                                                &
  real(neutron_mass_in_u_R16, kind=R4)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_energy_equivalent_in_MeV_R16 =                           &
  939.5654133_R16

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R8 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R4 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Neutron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_energy_equivalent_R16 =                                  &
  1.505349739e-10_R16

!> Neutron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_energy_equivalent_R8 =                                   &
  real(neutron_mass_energy_equivalent_R16, kind=R8)

!> Neutron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_energy_equivalent_R4 =                                   &
  real(neutron_mass_energy_equivalent_R16, kind=R4)

!> Neutron-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_electron_mass_ratio_R16 =                                     &
  1838.68366158_R16

!> Neutron-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_electron_mass_ratio_R8 =                                      &
  real(neutron_electron_mass_ratio_R16, kind=R8)

!> Neutron-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_electron_mass_ratio_R4 =                                      &
  real(neutron_electron_mass_ratio_R16, kind=R4)

!> Neutron-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_muon_mass_ratio_R16 =                                         &
  8.89248408_R16

!> Neutron-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_muon_mass_ratio_R8 =                                          &
  real(neutron_muon_mass_ratio_R16, kind=R8)

!> Neutron-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_muon_mass_ratio_R4 =                                          &
  real(neutron_muon_mass_ratio_R16, kind=R4)

!> Neutron-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_tau_mass_ratio_R16 =                                          &
  0.528790_R16

!> Neutron-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_tau_mass_ratio_R8 =                                           &
  real(neutron_tau_mass_ratio_R16, kind=R8)

!> Neutron-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_tau_mass_ratio_R4 =                                           &
  real(neutron_tau_mass_ratio_R16, kind=R4)

!> Neutron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_ratio_R16 =                                       &
  1.00137841898_R16

!> Neutron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_ratio_R8 =                                        &
  real(neutron_proton_mass_ratio_R16, kind=R8)

!> Neutron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_ratio_R4 =                                        &
  real(neutron_proton_mass_ratio_R16, kind=R4)

!> Neutron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_molar_mass_R16 =                                              &
  1.00866491588e-3_R16

!> Neutron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_molar_mass_R8 =                                               &
  real(neutron_molar_mass_R16, kind=R8)

!> Neutron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_molar_mass_R4 =                                               &
  real(neutron_molar_mass_R16, kind=R4)

!> Neutron Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_Compton_wavelength_R16 =                                      &
  1.31959090481e-15_R16

!> Neutron Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_Compton_wavelength_R8 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R8)

!> Neutron Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_Compton_wavelength_R4 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R4)

!> Neutron Compton wavelength over 2 pi, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_neutron_Compton_wavelength_R16 =                              &
  0.21001941536e-15_R16

!> Neutron Compton wavelength over 2 pi, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_neutron_Compton_wavelength_R8 =                               &
  real(reduced_neutron_Compton_wavelength_R16, kind=R8)

!> Neutron Compton wavelength over 2 pi, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_neutron_Compton_wavelength_R4 =                               &
  real(reduced_neutron_Compton_wavelength_R16, kind=R4)

!> Neutron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_R16 =                                         &
  -0.96623650e-26_R16

!> Neutron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_R8 =                                          &
  real(neutron_magnetic_moment_R16, kind=R8)

!> Neutron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_R4 =                                          &
  real(neutron_magnetic_moment_R16, kind=R4)

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                  &
  -1.04187563e-3_R16

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                   &
  real(neutron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                   &
  real(neutron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R16 =               &
  -1.91304273_R16

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R8 =                &
  real(neutron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R4 =                &
  real(neutron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Neutron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_g_factor_R16 =                                                &
  -3.82608545_R16

!> Neutron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_g_factor_R8 =                                                 &
  real(neutron_g_factor_R16, kind=R8)

!> Neutron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_g_factor_R4 =                                                 &
  real(neutron_g_factor_R16, kind=R4)

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_electron_magnetic_moment_ratio_R16 =                          &
  1.04066882e-3_R16

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_electron_magnetic_moment_ratio_R8 =                           &
  real(neutron_electron_magnetic_moment_ratio_R16, kind=R8)

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_electron_magnetic_moment_ratio_R4 =                           &
  real(neutron_electron_magnetic_moment_ratio_R16, kind=R4)

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_magnetic_moment_ratio_R16 =                            &
  -0.68497934_R16

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_magnetic_moment_ratio_R8 =                             &
  real(neutron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_magnetic_moment_ratio_R4 =                             &
  real(neutron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_to_shielded_proton_magnetic_moment_ratio_R16 =                &
  -0.68499694_R16

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_to_shielded_proton_magnetic_moment_ratio_R8 =                 &
  real(neutron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R8)

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_to_shielded_proton_magnetic_moment_ratio_R4 =                 &
  real(neutron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R4)

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_gyromagnetic_ratio_R16 =                                      &
  1.83247172e8_R16

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_R8 =                                       &
  real(neutron_gyromagnetic_ratio_R16, kind=R8)

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_R4 =                                       &
  real(neutron_gyromagnetic_ratio_R16, kind=R4)

!> Neutron gyromag. ratio over 2 pi, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_gyromagnetic_ratio_in_MHz_T_R16 =                             &
  29.1646933_R16

!> Neutron gyromag. ratio over 2 pi, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_in_MHz_T_R8 =                              &
  real(neutron_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Neutron gyromag. ratio over 2 pi, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_in_MHz_T_R4 =                              &
  real(neutron_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Neutron-proton mass difference, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_R16 =                                  &
  2.30557377e-30_R16

!> Neutron-proton mass difference, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_R8 =                                   &
  real(neutron_proton_mass_difference_R16, kind=R8)

!> Neutron-proton mass difference, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_R4 =                                   &
  real(neutron_proton_mass_difference_R16, kind=R4)

!> Neutron-proton mass difference energy equivalent, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_energy_equivalent_R16 =                &
  2.07214637e-13_R16

!> Neutron-proton mass difference energy equivalent, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_R8 =                 &
  real(neutron_proton_mass_difference_energy_equivalent_R16, kind=R8)

!> Neutron-proton mass difference energy equivalent, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_R4 =                 &
  real(neutron_proton_mass_difference_energy_equivalent_R16, kind=R4)

!> Neutron-proton mass difference energy equivalent in MeV, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_energy_equivalent_in_MeV_R16 =         &
  1.29333205_R16

!> Neutron-proton mass difference energy equivalent in MeV, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_in_MeV_R8 =          &
  real(neutron_proton_mass_difference_energy_equivalent_in_MeV_R16,     &
  kind=R8)

!> Neutron-proton mass difference energy equivalent in MeV, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_in_MeV_R4 =          &
  real(neutron_proton_mass_difference_energy_equivalent_in_MeV_R16,     &
  kind=R4)

!> Neutron-proton mass difference in u, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_in_u_R16 =                             &
  0.00138844900_R16

!> Neutron-proton mass difference in u, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_in_u_R8 =                              &
  real(neutron_proton_mass_difference_in_u_R16, kind=R8)

!> Neutron-proton mass difference in u, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_in_u_R4 =                              &
  real(neutron_proton_mass_difference_in_u_R16, kind=R4)

!-----
! Deuteron

!> Deuteron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_R16 =                                                   &
  3.343583719e-27_R16

!> Deuteron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_R8 =                                                    &
  real(deuteron_mass_R16, kind=R8)

!> Deuteron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_R4 =                                                    &
  real(deuteron_mass_R16, kind=R4)

!> Deuteron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_in_u_R16 =                                              &
  2.013553212745_R16

!> Deuteron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_in_u_R8 =                                               &
  real(deuteron_mass_in_u_R16, kind=R8)

!> Deuteron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_in_u_R4 =                                               &
  real(deuteron_mass_in_u_R16, kind=R4)

!> Deuteron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_energy_equivalent_R16 =                                 &
  3.005063183e-10_R16

!> Deuteron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_R8 =                                  &
  real(deuteron_mass_energy_equivalent_R16, kind=R8)

!> Deuteron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_R4 =                                  &
  real(deuteron_mass_energy_equivalent_R16, kind=R4)

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_energy_equivalent_in_MeV_R16 =                          &
  1875.612928_R16

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_in_MeV_R8 =                           &
  real(deuteron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_in_MeV_R4 =                           &
  real(deuteron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Deuteron-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_electron_mass_ratio_R16 =                                    &
  3670.48296785_R16

!> Deuteron-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_electron_mass_ratio_R8 =                                     &
  real(deuteron_electron_mass_ratio_R16, kind=R8)

!> Deuteron-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_electron_mass_ratio_R4 =                                     &
  real(deuteron_electron_mass_ratio_R16, kind=R4)

!> Deuteron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_proton_mass_ratio_R16 =                                      &
  1.99900750087_R16

!> Deuteron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_proton_mass_ratio_R8 =                                       &
  real(deuteron_proton_mass_ratio_R16, kind=R8)

!> Deuteron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_proton_mass_ratio_R4 =                                       &
  real(deuteron_proton_mass_ratio_R16, kind=R4)

!> Deuteron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_molar_mass_R16 =                                             &
  2.013553212745e-3_R16

!> Deuteron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_molar_mass_R8 =                                              &
  real(deuteron_molar_mass_R16, kind=R8)

!> Deuteron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_molar_mass_R4 =                                              &
  real(deuteron_molar_mass_R16, kind=R4)

!> Deuteron rms charge radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_rms_charge_radius_R16 =                                      &
  2.1413e-15_R16

!> Deuteron rms charge radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_rms_charge_radius_R8 =                                       &
  real(deuteron_rms_charge_radius_R16, kind=R8)

!> Deuteron rms charge radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_rms_charge_radius_R4 =                                       &
  real(deuteron_rms_charge_radius_R16, kind=R4)

!> Deuteron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_R16 =                                        &
  0.4330735040e-26_R16

!> Deuteron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_R8 =                                         &
  real(deuteron_magnetic_moment_R16, kind=R8)

!> Deuteron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_R4 =                                         &
  real(deuteron_magnetic_moment_R16, kind=R4)

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                 &
  0.4669754554e-3_R16

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                  &
  real(deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                  &
  real(deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16 =              &
  0.8574382311_R16

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R8 =               &
  real(deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R4 =               &
  real(deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Deuteron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_g_factor_R16 =                                               &
  0.8574382311_R16

!> Deuteron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_g_factor_R8 =                                                &
  real(deuteron_g_factor_R16, kind=R8)

!> Deuteron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_g_factor_R4 =                                                &
  real(deuteron_g_factor_R16, kind=R4)

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_electron_magnetic_moment_ratio_R16 =                         &
  -4.664345535e-4_R16

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_electron_magnetic_moment_ratio_R8 =                          &
  real(deuteron_electron_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_electron_magnetic_moment_ratio_R4 =                          &
  real(deuteron_electron_magnetic_moment_ratio_R16, kind=R4)

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_proton_magnetic_moment_ratio_R16 =                           &
  0.3070122077_R16

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_proton_magnetic_moment_ratio_R8 =                            &
  real(deuteron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_proton_magnetic_moment_ratio_R4 =                            &
  real(deuteron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_neutron_magnetic_moment_ratio_R16 =                          &
  -0.44820652_R16

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_neutron_magnetic_moment_ratio_R8 =                           &
  real(deuteron_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_neutron_magnetic_moment_ratio_R4 =                           &
  real(deuteron_neutron_magnetic_moment_ratio_R16, kind=R4)

!-----
! Triton

!> Triton mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_R16 =                                                     &
  5.007356665e-27_R16

!> Triton mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_R8 =                                                      &
  real(triton_mass_R16, kind=R8)

!> Triton mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_R4 =                                                      &
  real(triton_mass_R16, kind=R4)

!> Triton mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_in_u_R16 =                                                &
  3.01550071632_R16

!> Triton mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_in_u_R8 =                                                 &
  real(triton_mass_in_u_R16, kind=R8)

!> Triton mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_in_u_R4 =                                                 &
  real(triton_mass_in_u_R16, kind=R4)

!> Triton mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_energy_equivalent_R16 =                                   &
  4.500387735e-10_R16

!> Triton mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_energy_equivalent_R8 =                                    &
  real(triton_mass_energy_equivalent_R16, kind=R8)

!> Triton mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_energy_equivalent_R4 =                                    &
  real(triton_mass_energy_equivalent_R16, kind=R4)

!> Triton mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_energy_equivalent_in_MeV_R16 =                            &
  2808.921112_R16

!> Triton mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_energy_equivalent_in_MeV_R8 =                             &
  real(triton_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Triton mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_energy_equivalent_in_MeV_R4 =                             &
  real(triton_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Triton-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_electron_mass_ratio_R16 =                                      &
  5496.92153588_R16

!> Triton-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_electron_mass_ratio_R8 =                                       &
  real(triton_electron_mass_ratio_R16, kind=R8)

!> Triton-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_electron_mass_ratio_R4 =                                       &
  real(triton_electron_mass_ratio_R16, kind=R4)

!> Triton-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_proton_mass_ratio_R16 =                                        &
  2.99371703348_R16

!> Triton-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_proton_mass_ratio_R8 =                                         &
  real(triton_proton_mass_ratio_R16, kind=R8)

!> Triton-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_proton_mass_ratio_R4 =                                         &
  real(triton_proton_mass_ratio_R16, kind=R4)

!> Triton molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_molar_mass_R16 =                                               &
  3.01550071632e-3_R16

!> Triton molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_molar_mass_R8 =                                                &
  real(triton_molar_mass_R16, kind=R8)

!> Triton molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_molar_mass_R4 =                                                &
  real(triton_molar_mass_R16, kind=R4)

!> Triton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_magnetic_moment_R16 =                                          &
  1.504609503e-26_R16

!> Triton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_magnetic_moment_R8 =                                           &
  real(triton_magnetic_moment_R16, kind=R8)

!> Triton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_magnetic_moment_R4 =                                           &
  real(triton_magnetic_moment_R16, kind=R4)

!> Triton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_magnetic_moment_to_Bohr_magneton_ratio_R16 =                   &
  1.6223936616e-3_R16

!> Triton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_magnetic_moment_to_Bohr_magneton_ratio_R8 =                    &
  real(triton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Triton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_magnetic_moment_to_Bohr_magneton_ratio_R4 =                    &
  real(triton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Triton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_magnetic_moment_to_nuclear_magneton_ratio_R16 =                &
  2.978962460_R16

!> Triton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_magnetic_moment_to_nuclear_magneton_ratio_R8 =                 &
  real(triton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Triton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_magnetic_moment_to_nuclear_magneton_ratio_R4 =                 &
  real(triton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Triton g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_g_factor_R16 =                                                 &
  5.957924920_R16

!> Triton g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_g_factor_R8 =                                                  &
  real(triton_g_factor_R16, kind=R8)

!> Triton g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_g_factor_R4 =                                                  &
  real(triton_g_factor_R16, kind=R4)

!-----
! Helion

!> Helion mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_R16 =                                                     &
  5.006412700e-27_R16

!> Helion mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_R8 =                                                      &
  real(helion_mass_R16, kind=R8)

!> Helion mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_R4 =                                                      &
  real(helion_mass_R16, kind=R4)

!> Helion mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_in_u_R16 =                                                &
  3.01493224673_R16

!> Helion mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_in_u_R8 =                                                 &
  real(helion_mass_in_u_R16, kind=R8)

!> Helion mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_in_u_R4 =                                                 &
  real(helion_mass_in_u_R16, kind=R4)

!> Helion mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_energy_equivalent_R16 =                                   &
  4.499539341e-10_R16

!> Helion mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_energy_equivalent_R8 =                                    &
  real(helion_mass_energy_equivalent_R16, kind=R8)

!> Helion mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_energy_equivalent_R4 =                                    &
  real(helion_mass_energy_equivalent_R16, kind=R4)

!> Helion mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_energy_equivalent_in_MeV_R16 =                            &
  2808.391586_R16

!> Helion mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_energy_equivalent_in_MeV_R8 =                             &
  real(helion_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Helion mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_energy_equivalent_in_MeV_R4 =                             &
  real(helion_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Helion-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_electron_mass_ratio_R16 =                                      &
  5495.88527922_R16

!> Helion-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_electron_mass_ratio_R8 =                                       &
  real(helion_electron_mass_ratio_R16, kind=R8)

!> Helion-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_electron_mass_ratio_R4 =                                       &
  real(helion_electron_mass_ratio_R16, kind=R4)

!> Helion-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_proton_mass_ratio_R16 =                                        &
  2.99315267046_R16

!> Helion-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_proton_mass_ratio_R8 =                                         &
  real(helion_proton_mass_ratio_R16, kind=R8)

!> Helion-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_proton_mass_ratio_R4 =                                         &
  real(helion_proton_mass_ratio_R16, kind=R4)

!> Helion molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_molar_mass_R16 =                                               &
  3.01493224673e-3_R16

!> Helion molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_molar_mass_R8 =                                                &
  real(helion_molar_mass_R16, kind=R8)

!> Helion molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_molar_mass_R4 =                                                &
  real(helion_molar_mass_R16, kind=R4)

!> Shielded helion mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_R16 =                                 &
  -1.074553080e-26_R16

!> Shielded helion mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_R8 =                                  &
  real(shielded_helion_magnetic_moment_R16, kind=R8)

!> Shielded helion mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_R4 =                                  &
  real(shielded_helion_magnetic_moment_R16, kind=R4)

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16 =          &
  -1.158671471e-3_R16

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R8 =           &
  real(shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R8)

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R4 =           &
  real(shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R4)

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16 =       &
  -2.127497720_R16

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R8 =        &
  real(shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R8)

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R4 =        &
  real(shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R4)

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_to_proton_magnetic_moment_ratio_R16 =                 &
  -0.7617665603_R16

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_to_proton_magnetic_moment_ratio_R8 =                  &
  real(shielded_helion_to_proton_magnetic_moment_ratio_R16, kind=R8)

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_to_proton_magnetic_moment_ratio_R4 =                  &
  real(shielded_helion_to_proton_magnetic_moment_ratio_R16, kind=R4)

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16 =        &
  -0.7617861313_R16

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R8 =         &
  real(shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16,    &
  kind=R8)

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R4 =         &
  real(shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16,    &
  kind=R4)

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_gyromagnetic_ratio_R16 =                              &
  2.037894585e8_R16

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_R8 =                               &
  real(shielded_helion_gyromagnetic_ratio_R16, kind=R8)

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_R4 =                               &
  real(shielded_helion_gyromagnetic_ratio_R16, kind=R4)

!> Shielded helion gyromag. ratio over 2 pi, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R16 =                        &
  32.43409966_R16

!> Shielded helion gyromag. ratio over 2 pi, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R8 =                         &
  real(shielded_helion_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Shielded helion gyromag. ratio over 2 pi, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R4 =                         &
  real(shielded_helion_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Helion mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_magnetic_moment_R16 =                                          &
  -1.074617522e-26_R16

!> Helion mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_magnetic_moment_R8 =                                           &
  real(helion_magnetic_moment_R16, kind=R8)

!> Helion mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_magnetic_moment_R4 =                                           &
  real(helion_magnetic_moment_R16, kind=R4)

!> Helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_magnetic_moment_to_Bohr_magneton_ratio_R16 =                   &
  -1.158740958e-3_R16

!> Helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_magnetic_moment_to_Bohr_magneton_ratio_R8 =                    &
  real(helion_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_magnetic_moment_to_Bohr_magneton_ratio_R4 =                    &
  real(helion_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_magnetic_moment_to_nuclear_magneton_ratio_R16 =                &
  -2.127625308_R16

!> Helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_magnetic_moment_to_nuclear_magneton_ratio_R8 =                 &
  real(helion_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_magnetic_moment_to_nuclear_magneton_ratio_R4 =                 &
  real(helion_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Helion g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_g_factor_R16 =                                                 &
  -4.255250616_R16

!> Helion g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_g_factor_R8 =                                                  &
  real(helion_g_factor_R16, kind=R8)

!> Helion g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_g_factor_R4 =                                                  &
  real(helion_g_factor_R16, kind=R4)

!-----
! Alpha particle

!> Alpha particle mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_R16 =                                             &
  6.644657230e-27_R16

!> Alpha particle mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_R8 =                                              &
  real(alpha_particle_mass_R16, kind=R8)

!> Alpha particle mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_R4 =                                              &
  real(alpha_particle_mass_R16, kind=R4)

!> Alpha particle mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_in_u_R16 =                                        &
  4.001506179127_R16

!> Alpha particle mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_in_u_R8 =                                         &
  real(alpha_particle_mass_in_u_R16, kind=R8)

!> Alpha particle mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_in_u_R4 =                                         &
  real(alpha_particle_mass_in_u_R16, kind=R4)

!> Alpha particle mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_energy_equivalent_R16 =                           &
  5.971920097e-10_R16

!> Alpha particle mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_R8 =                            &
  real(alpha_particle_mass_energy_equivalent_R16, kind=R8)

!> Alpha particle mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_R4 =                            &
  real(alpha_particle_mass_energy_equivalent_R16, kind=R4)

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_energy_equivalent_in_MeV_R16 =                    &
  3727.379378_R16

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_in_MeV_R8 =                     &
  real(alpha_particle_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_in_MeV_R4 =                     &
  real(alpha_particle_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_electron_mass_ratio_R16 =                              &
  7294.29954136_R16

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_electron_mass_ratio_R8 =                               &
  real(alpha_particle_electron_mass_ratio_R16, kind=R8)

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_electron_mass_ratio_R4 =                               &
  real(alpha_particle_electron_mass_ratio_R16, kind=R4)

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_proton_mass_ratio_R16 =                                &
  3.97259968907_R16

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_proton_mass_ratio_R8 =                                 &
  real(alpha_particle_proton_mass_ratio_R16, kind=R8)

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_proton_mass_ratio_R4 =                                 &
  real(alpha_particle_proton_mass_ratio_R16, kind=R4)

!> Alpha particle molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_molar_mass_R16 =                                       &
  4.001506179127e-3_R16

!> Alpha particle molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_molar_mass_R8 =                                        &
  real(alpha_particle_molar_mass_R16, kind=R8)

!> Alpha particle molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_molar_mass_R4 =                                        &
  real(alpha_particle_molar_mass_R16, kind=R4)

!-----
! Physico-Chemical Constants

!> Avogadro constant, mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Avogadro_constant_R16 =                                               &
  6.022140857e23_R16

!> Avogadro constant, mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Avogadro_constant_R8 =                                                &
  real(Avogadro_constant_R16, kind=R8)

!> Avogadro constant, mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Avogadro_constant_R4 =                                                &
  real(Avogadro_constant_R16, kind=R4)

!> Atomic mass constant, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_R16 =                                            &
  1.660539040e-27_R16

!> Atomic mass constant, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_R8 =                                             &
  real(atomic_mass_constant_R16, kind=R8)

!> Atomic mass constant, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_R4 =                                             &
  real(atomic_mass_constant_R16, kind=R4)

!> Atomic mass constant energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_energy_equivalent_R16 =                          &
  1.492418062e-10_R16

!> Atomic mass constant energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_R8 =                           &
  real(atomic_mass_constant_energy_equivalent_R16, kind=R8)

!> Atomic mass constant energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_R4 =                           &
  real(atomic_mass_constant_energy_equivalent_R16, kind=R4)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_energy_equivalent_in_MeV_R16 =                   &
  931.4940954_R16

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R8 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R8)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R4 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R4)

!> Faraday constant, C mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Faraday_constant_R16 =                                                &
  96485.33289_R16

!> Faraday constant, C mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Faraday_constant_R8 =                                                 &
  real(Faraday_constant_R16, kind=R8)

!> Faraday constant, C mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Faraday_constant_R4 =                                                 &
  real(Faraday_constant_R16, kind=R4)

!> Molar Planck constant, J s mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_Planck_constant_R16 =                                           &
  3.9903127110e-10_R16

!> Molar Planck constant, J s mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_Planck_constant_R8 =                                            &
  real(molar_Planck_constant_R16, kind=R8)

!> Molar Planck constant, J s mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_Planck_constant_R4 =                                            &
  real(molar_Planck_constant_R16, kind=R4)

!> Molar Planck constant times c, J m mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_Planck_constant_times_c_R16 =                                   &
  0.119626565582_R16

!> Molar Planck constant times c, J m mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_Planck_constant_times_c_R8 =                                    &
  real(molar_Planck_constant_times_c_R16, kind=R8)

!> Molar Planck constant times c, J m mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_Planck_constant_times_c_R4 =                                    &
  real(molar_Planck_constant_times_c_R16, kind=R4)

!> Molar gas constant, J mol^-1 K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_gas_constant_R16 =                                              &
  8.3144598_R16

!> Molar gas constant, J mol^-1 K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_gas_constant_R8 =                                               &
  real(molar_gas_constant_R16, kind=R8)

!> Molar gas constant, J mol^-1 K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_gas_constant_R4 =                                               &
  real(molar_gas_constant_R16, kind=R4)

!> Boltzmann constant, J K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_R16 =                                              &
  1.38064852e-23_R16

!> Boltzmann constant, J K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_R8 =                                               &
  real(Boltzmann_constant_R16, kind=R8)

!> Boltzmann constant, J K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_R4 =                                               &
  real(Boltzmann_constant_R16, kind=R4)

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_eV_K_R16 =                                      &
  8.6173303e-5_R16

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_eV_K_R8 =                                       &
  real(Boltzmann_constant_in_eV_K_R16, kind=R8)

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_eV_K_R4 =                                       &
  real(Boltzmann_constant_in_eV_K_R16, kind=R4)

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_Hz_K_R16 =                                      &
  2.0836612e10_R16

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_Hz_K_R8 =                                       &
  real(Boltzmann_constant_in_Hz_K_R16, kind=R8)

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_Hz_K_R4 =                                       &
  real(Boltzmann_constant_in_Hz_K_R16, kind=R4)

!> Boltzmann constant in inverse meters per kelvin, m^-1 K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R16 =                  &
  69.503457_R16

!> Boltzmann constant in inverse meters per kelvin, m^-1 K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R8 =                   &
  real(Boltzmann_constant_in_inverse_meter_per_kelvin_R16, kind=R8)

!> Boltzmann constant in inverse meters per kelvin, m^-1 K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R4 =                   &
  real(Boltzmann_constant_in_inverse_meter_per_kelvin_R16, kind=R4)

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16 =                  &
  22.413962e-3_R16

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R8 =                   &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R8)

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R4 =                   &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R4)

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Loschmidt_constant_273_15_K_101_325_kPa_R16 =                         &
  2.6867811e25_R16

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_101_325_kPa_R8 =                          &
  real(Loschmidt_constant_273_15_K_101_325_kPa_R16, kind=R8)

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_101_325_kPa_R4 =                          &
  real(Loschmidt_constant_273_15_K_101_325_kPa_R16, kind=R4)

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R16 =                      &
  22.710947e-3_R16

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R8 =                       &
  real(molar_volume_of_ideal_gas_273_15_K_100_kPa_R16, kind=R8)

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R4 =                       &
  real(molar_volume_of_ideal_gas_273_15_K_100_kPa_R16, kind=R4)

!> Loschmidt constant (273.15 K, 100 kPa), m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Loschmidt_constant_273_15_K_100_kPa_R16 =                             &
  2.6516467e25_R16

!> Loschmidt constant (273.15 K, 100 kPa), m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_100_kPa_R8 =                              &
  real(Loschmidt_constant_273_15_K_100_kPa_R16, kind=R8)

!> Loschmidt constant (273.15 K, 100 kPa), m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_100_kPa_R4 =                              &
  real(Loschmidt_constant_273_15_K_100_kPa_R16, kind=R4)

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Sackur_Tetrode_constant_1_K_100_kPa_R16 =                             &
  -1.1517084_R16

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_100_kPa_R8 =                              &
  real(Sackur_Tetrode_constant_1_K_100_kPa_R16, kind=R8)

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_100_kPa_R4 =                              &
  real(Sackur_Tetrode_constant_1_K_100_kPa_R16, kind=R4)

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R16 =                         &
  -1.1648714_R16

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R8 =                          &
  real(Sackur_Tetrode_constant_1_K_101_325_kPa_R16, kind=R8)

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R4 =                          &
  real(Sackur_Tetrode_constant_1_K_101_325_kPa_R16, kind=R4)

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Stefan_Boltzmann_constant_R16 =                                       &
  5.670367e-8_R16

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R8 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R8)

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R4 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R4)

!> First radiation constant, W m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  first_radiation_constant_R16 =                                        &
  3.741771790e-16_R16

!> First radiation constant, W m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  first_radiation_constant_R8 =                                         &
  real(first_radiation_constant_R16, kind=R8)

!> First radiation constant, W m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  first_radiation_constant_R4 =                                         &
  real(first_radiation_constant_R16, kind=R4)

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  first_radiation_constant_for_spectral_radiance_R16 =                  &
  1.191042953e-16_R16

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  first_radiation_constant_for_spectral_radiance_R8 =                   &
  real(first_radiation_constant_for_spectral_radiance_R16, kind=R8)

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  first_radiation_constant_for_spectral_radiance_R4 =                   &
  real(first_radiation_constant_for_spectral_radiance_R16, kind=R4)

!> Second radiation constant, m K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  second_radiation_constant_R16 =                                       &
  1.43877736e-2_R16

!> Second radiation constant, m K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  second_radiation_constant_R8 =                                        &
  real(second_radiation_constant_R16, kind=R8)

!> Second radiation constant, m K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  second_radiation_constant_R4 =                                        &
  real(second_radiation_constant_R16, kind=R4)

!> Wien wavelength displacement law constant, m K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Wien_wavelength_displacement_law_constant_R16 =                       &
  2.8977729e-3_R16

!> Wien wavelength displacement law constant, m K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Wien_wavelength_displacement_law_constant_R8 =                        &
  real(Wien_wavelength_displacement_law_constant_R16, kind=R8)

!> Wien wavelength displacement law constant, m K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Wien_wavelength_displacement_law_constant_R4 =                        &
  real(Wien_wavelength_displacement_law_constant_R16, kind=R4)

!> Wien frequency displacement law constant, Hz K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Wien_frequency_displacement_law_constant_R16 =                        &
  5.8789238e10_R16

!> Wien frequency displacement law constant, Hz K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Wien_frequency_displacement_law_constant_R8 =                         &
  real(Wien_frequency_displacement_law_constant_R16, kind=R8)

!> Wien frequency displacement law constant, Hz K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Wien_frequency_displacement_law_constant_R4 =                         &
  real(Wien_frequency_displacement_law_constant_R16, kind=R4)

!-----
! Non-SI units

!> Electron volt, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_R16 =                                                   &
  1.6021766208e-19_R16

!> Electron volt, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_R8 =                                                    &
  real(electron_volt_R16, kind=R8)

!> Electron volt, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_R4 =                                                    &
  real(electron_volt_R16, kind=R4)

!> Unified atomic mass unit, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  unified_atomic_mass_unit_R16 =                                        &
  1.660539040e-27_R16

!> Unified atomic mass unit, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  unified_atomic_mass_unit_R8 =                                         &
  real(unified_atomic_mass_unit_R16, kind=R8)

!> Unified atomic mass unit, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  unified_atomic_mass_unit_R4 =                                         &
  real(unified_atomic_mass_unit_R16, kind=R4)

!-----
! Natural units

!> Natural unit of velocity, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_velocity_R16 =                                        &
  299792458.0_R16

!> Natural unit of velocity, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_velocity_R8 =                                         &
  real(natural_unit_of_velocity_R16, kind=R8)

!> Natural unit of velocity, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_velocity_R4 =                                         &
  real(natural_unit_of_velocity_R16, kind=R4)

!> Natural unit of action, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_action_R16 =                                          &
  reduced_Planck_constant_R16 ! derived

!> Natural unit of action, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_action_R8 =                                           &
  real(natural_unit_of_action_R16, kind=R8) ! derived

!> Natural unit of action, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_action_R4 =                                           &
  real(natural_unit_of_action_R16, kind=R4) ! derived

!> Natural unit of action in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_action_in_eV_s_R16 =                                  &
  reduced_Planck_constant_in_eV_s_R16 ! derived

!> Natural unit of action in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_action_in_eV_s_R8 =                                   &
  real(natural_unit_of_action_in_eV_s_R16, kind=R8) ! derived

!> Natural unit of action in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_action_in_eV_s_R4 =                                   &
  real(natural_unit_of_action_in_eV_s_R16, kind=R4) ! derived

!> Natural unit of mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_mass_R16 =                                            &
  electron_mass_R16 ! derived

!> Natural unit of mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_mass_R8 =                                             &
  real(natural_unit_of_mass_R16, kind=R8) ! derived

!> Natural unit of mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_mass_R4 =                                             &
  real(natural_unit_of_mass_R16, kind=R4) ! derived

!> Natural unit of energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_energy_R16 =                                          &
  electron_mass_energy_equivalent_R16 ! derived

!> Natural unit of energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_energy_R8 =                                           &
  real(natural_unit_of_energy_R16, kind=R8) ! derived

!> Natural unit of energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_energy_R4 =                                           &
  real(natural_unit_of_energy_R16, kind=R4) ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_energy_in_MeV_R16 =                                   &
  electron_mass_energy_equivalent_in_MeV_R16 ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_energy_in_MeV_R8 =                                    &
  real(natural_unit_of_energy_in_MeV_R16, kind=R8) ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_energy_in_MeV_R4 =                                    &
  real(natural_unit_of_energy_in_MeV_R16, kind=R4) ! derived

!> Natural unit of momentum, kg m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_momentum_R16 =                                        &
  2.730924488e-22_R16

!> Natural unit of momentum, kg m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_momentum_R8 =                                         &
  real(natural_unit_of_momentum_R16, kind=R8)

!> Natural unit of momentum, kg m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_momentum_R4 =                                         &
  real(natural_unit_of_momentum_R16, kind=R4)

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_momentum_in_MeV_c_R16 =                               &
  electron_mass_energy_equivalent_in_MeV_R16 ! derived

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_momentum_in_MeV_c_R8 =                                &
  real(natural_unit_of_momentum_in_MeV_c_R16, kind=R8) ! derived

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_momentum_in_MeV_c_R4 =                                &
  real(natural_unit_of_momentum_in_MeV_c_R16, kind=R4) ! derived

!> Natural unit of length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_length_R16 =                                          &
  reduced_Compton_wavelength_R16 ! derived

!> Natural unit of length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_length_R8 =                                           &
  real(natural_unit_of_length_R16, kind=R8) ! derived

!> Natural unit of length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_length_R4 =                                           &
  real(natural_unit_of_length_R16, kind=R4) ! derived

!> Natural unit of time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_time_R16 =                                            &
  1.28808866712e-21_R16

!> Natural unit of time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_time_R8 =                                             &
  real(natural_unit_of_time_R16, kind=R8)

!> Natural unit of time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_time_R4 =                                             &
  real(natural_unit_of_time_R16, kind=R4)

!-----
! Atomic units

!> Atomic unit of charge, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_charge_R16 =                                           &
  elementary_charge_R16 ! derived

!> Atomic unit of charge, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_charge_R8 =                                            &
  real(atomic_unit_of_charge_R16, kind=R8) ! derived

!> Atomic unit of charge, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_charge_R4 =                                            &
  real(atomic_unit_of_charge_R16, kind=R4) ! derived

!> Atomic unit of mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_mass_R16 =                                             &
  electron_mass_R16 ! derived

!> Atomic unit of mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_mass_R8 =                                              &
  real(atomic_unit_of_mass_R16, kind=R8) ! derived

!> Atomic unit of mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_mass_R4 =                                              &
  real(atomic_unit_of_mass_R16, kind=R4) ! derived

!> Atomic unit of action, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_action_R16 =                                           &
  reduced_Planck_constant_R16 ! derived

!> Atomic unit of action, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_action_R8 =                                            &
  real(atomic_unit_of_action_R16, kind=R8) ! derived

!> Atomic unit of action, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_action_R4 =                                            &
  real(atomic_unit_of_action_R16, kind=R4) ! derived

!> Atomic unit of length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_length_R16 =                                           &
  Bohr_radius_R16 ! derived

!> Atomic unit of length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_length_R8 =                                            &
  real(atomic_unit_of_length_R16, kind=R8) ! derived

!> Atomic unit of length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_length_R4 =                                            &
  real(atomic_unit_of_length_R16, kind=R4) ! derived

!> Atomic unit of energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_energy_R16 =                                           &
  Hartree_energy_R16 ! derived

!> Atomic unit of energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_energy_R8 =                                            &
  real(atomic_unit_of_energy_R16, kind=R8) ! derived

!> Atomic unit of energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_energy_R4 =                                            &
  real(atomic_unit_of_energy_R16, kind=R4) ! derived

!> Atomic unit of time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_time_R16 =                                             &
  reduced_Planck_constant_R16 / Hartree_energy_R16
  ! 2.418884326509e-17_R16

!> Atomic unit of time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_time_R8 =                                              &
  real(atomic_unit_of_time_R16, kind=R8)

!> Atomic unit of time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_time_R4 =                                              &
  real(atomic_unit_of_time_R16, kind=R4)

!> Atomic unit of force, N
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_force_R16 =                                            &
  Hartree_energy_R16 / Bohr_radius_R16
  ! 8.23872336e-8_R16

!> Atomic unit of force, N
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_force_R8 =                                             &
  real(atomic_unit_of_force_R16, kind=R8)

!> Atomic unit of force, N
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_force_R4 =                                             &
  real(atomic_unit_of_force_R16, kind=R4)

!> Atomic unit of velocity, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_velocity_R16 =                                         &
  Bohr_radius_R16 * Hartree_energy_R16 / reduced_Planck_constant_R16
  ! 2.18769126277e6_R16

!> Atomic unit of velocity, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_velocity_R8 =                                          &
  real(atomic_unit_of_velocity_R16, kind=R8)

!> Atomic unit of velocity, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_velocity_R4 =                                          &
  real(atomic_unit_of_velocity_R16, kind=R4)

!> Atomic unit of momentum, kg m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_momentum_R16 =                                         &
  reduced_Planck_constant_R16 / Bohr_radius_R16
  ! 1.992851882e-24_R16

!> Atomic unit of momentum, kg m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_momentum_R8 =                                          &
  real(atomic_unit_of_momentum_R16, kind=R8)

!> Atomic unit of momentum, kg m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_momentum_R4 =                                          &
  real(atomic_unit_of_momentum_R16, kind=R4)

!> Atomic unit of current, A
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_current_R16 =                                          &
  elementary_charge_R16 * Hartree_energy_R16                            &
  / reduced_Planck_constant_R16
  ! 6.623618183e-3_R16

!> Atomic unit of current, A
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_current_R8 =                                           &
  real(atomic_unit_of_current_R16, kind=R8)

!> Atomic unit of current, A
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_current_R4 =                                           &
  real(atomic_unit_of_current_R16, kind=R4)

!> Atomic unit of charge density, C m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_charge_density_R16 =                                   &
  elementary_charge_R16                                                 &
  / (Bohr_radius_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 1.0812023770e12_R16

!> Atomic unit of charge density, C m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_charge_density_R8 =                                    &
  real(atomic_unit_of_charge_density_R16, kind=R8)

!> Atomic unit of charge density, C m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_charge_density_R4 =                                    &
  real(atomic_unit_of_charge_density_R16, kind=R4)

!> Atomic unit of electric potential, V
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_potential_R16 =                               &
  Hartree_energy_R16 / elementary_charge_R16
!  27.21138602_R16

!> Atomic unit of electric potential, V
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_potential_R8 =                                &
  real(atomic_unit_of_electric_potential_R16, kind=R8)

!> Atomic unit of electric potential, V
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_potential_R4 =                                &
  real(atomic_unit_of_electric_potential_R16, kind=R4)

!> Atomic unit of electric field, V m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_field_R16 =                                   &
  Hartree_energy_R16 / (elementary_charge_R16 * Bohr_radius_R16)
  ! 5.142206707e11_R16

!> Atomic unit of electric field, V m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_field_R8 =                                    &
  real(atomic_unit_of_electric_field_R16, kind=R8)

!> Atomic unit of electric field, V m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_field_R4 =                                    &
  real(atomic_unit_of_electric_field_R16, kind=R4)

!> Atomic unit of electric field gradient, V m^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_field_gradient_R16 =                          &
  Hartree_energy_R16                                                    &
  / (elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 9.717362356e21_R16

!> Atomic unit of electric field gradient, V m^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_field_gradient_R8 =                           &
  real(atomic_unit_of_electric_field_gradient_R16, kind=R8)

!> Atomic unit of electric field gradient, V m^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_field_gradient_R4 =                           &
  real(atomic_unit_of_electric_field_gradient_R16, kind=R4)

!> Atomic unit of electric dipole mom., C m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_dipole_moment_R16 =                           &
  elementary_charge_R16 * Bohr_radius_R16
  ! 8.478353552e-30_R16

!> Atomic unit of electric dipole mom., C m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_dipole_moment_R8 =                            &
  real(atomic_unit_of_electric_dipole_moment_R16, kind=R8)

!> Atomic unit of electric dipole mom., C m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_dipole_moment_R4 =                            &
  real(atomic_unit_of_electric_dipole_moment_R16, kind=R4)

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_quadrupole_moment_R16 =                       &
  elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16
  ! 4.486551484e-40_R16

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_quadrupole_moment_R8 =                        &
  real(atomic_unit_of_electric_quadrupole_moment_R16, kind=R8)

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_quadrupole_moment_R4 =                        &
  real(atomic_unit_of_electric_quadrupole_moment_R16, kind=R4)

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_polarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16                         &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / Hartree_energy_R16
!  1.6487772731e-41_R16

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_polarizability_R8 =                           &
  real(atomic_unit_of_electric_polarizability_R16, kind=R8)

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_polarizability_R4 =                           &
  real(atomic_unit_of_electric_polarizability_R16, kind=R4)

!> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_1st_hyperpolarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16 * elementary_charge_R16 &
  * Bohr_radius_R16 * Bohr_radius_R16 * Bohr_radius_R16                 &
  / (Hartree_energy_R16 * Hartree_energy_R16)
  ! 3.206361329e-53_R16

!> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_1st_hyperpolarizability_R8 =                           &
  real(atomic_unit_of_1st_hyperpolarizability_R16, kind=R8)

! UNDERFLOW
! !> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   atomic_unit_of_1st_hyperpolarizability_R4 =                           &
!   real(atomic_unit_of_1st_hyperpolarizability_R16, kind=R4)

!> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_2nd_hyperpolarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16                         &
  * elementary_charge_R16 * elementary_charge_R16                       &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / (Hartree_energy_R16 * Hartree_energy_R16 * Hartree_energy_R16)
  ! 6.235380085e-65_R16

!> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_2nd_hyperpolarizability_R8 =                           &
  real(atomic_unit_of_2nd_hyperpolarizability_R16, kind=R8)

! UNDERFLOW
! !> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   atomic_unit_of_2nd_hyperpolarizability_R4 =                           &
!   real(atomic_unit_of_2nd_hyperpolarizability_R16, kind=R4)

!> Atomic unit of mag. flux density, T
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetic_flux_density_R16 =                            &
  reduced_Planck_constant_R16                                           &
  / (elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16)
!  2.350517550e5_R16

!> Atomic unit of mag. flux density, T
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetic_flux_density_R8 =                             &
  real(atomic_unit_of_magnetic_flux_density_R16, kind=R8)

!> Atomic unit of mag. flux density, T
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetic_flux_density_R4 =                             &
  real(atomic_unit_of_magnetic_flux_density_R16, kind=R4)

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetic_dipole_moment_R16 =                           &
  reduced_Planck_constant_R16 * elementary_charge_R16                   &
  / electron_mass_R16
  ! 1.854801999e-23_R16

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetic_dipole_moment_R8 =                            &
  real(atomic_unit_of_magnetic_dipole_moment_R16, kind=R8)

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetic_dipole_moment_R4 =                            &
  real(atomic_unit_of_magnetic_dipole_moment_R16, kind=R4)

!> Atomic unit of magnetizability, J T^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetizability_R16 =                                  &
  elementary_charge_R16 * elementary_charge_R16                         &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / electron_mass_R16
  ! 7.8910365886e-29_R16

!> Atomic unit of magnetizability, J T^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetizability_R8 =                                   &
  real(atomic_unit_of_magnetizability_R16, kind=R8)

!> Atomic unit of magnetizability, J T^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetizability_R4 =                                   &
  real(atomic_unit_of_magnetizability_R16, kind=R4)

!> Atomic unit of permittivity, F m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_permittivity_R16 =                                     &
  elementary_charge_R16 * elementary_charge_R16                         &
  / (Bohr_radius_R16 * Hartree_energy_R16)
!   1.112 650 056... e-10_R16 ! derived

!> Atomic unit of permittivity, F m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_permittivity_R8 =                                      &
  real(atomic_unit_of_permittivity_R16, kind=R8) ! derived

!> Atomic unit of permittivity, F m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_permittivity_R4 =                                      &
  real(atomic_unit_of_permittivity_R16, kind=R4) ! derived

!-----
! Adopted values

!> Molar mass constant, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_mass_constant_R16 =                                             &
  1e-3_R16

!> Molar mass constant, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_mass_constant_R8 =                                              &
  real(molar_mass_constant_R16, kind=R8)

!> Molar mass constant, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_mass_constant_R4 =                                              &
  real(molar_mass_constant_R16, kind=R4)

!> Molar mass of carbon-12, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_mass_of_carbon_12_R16 =                                         &
  12e-3_R16

!> Molar mass of carbon-12, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_mass_of_carbon_12_R8 =                                          &
  real(molar_mass_of_carbon_12_R16, kind=R8)

!> Molar mass of carbon-12, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_mass_of_carbon_12_R4 =                                          &
  real(molar_mass_of_carbon_12_R16, kind=R4)

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_Josephson_constant_R16 =                        &
  483597.9e9_R16

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_Josephson_constant_R8 =                         &
  real(conventional_value_of_Josephson_constant_R16, kind=R8)

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_Josephson_constant_R4 =                         &
  real(conventional_value_of_Josephson_constant_R16, kind=R4)

!> Conventional value of von Klitzing constant, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_von_Klitzing_constant_R16 =                     &
  25812.807_R16

!> Conventional value of von Klitzing constant, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_von_Klitzing_constant_R8 =                      &
  real(conventional_value_of_von_Klitzing_constant_R16, kind=R8)

!> Conventional value of von Klitzing constant, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_von_Klitzing_constant_R4 =                      &
  real(conventional_value_of_von_Klitzing_constant_R16, kind=R4)

!> Standard atmosphere, Pa
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_atmosphere_R16 =                                             &
  101325.0_R16

!> Standard atmosphere, Pa
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_atmosphere_R8 =                                              &
  real(standard_atmosphere_R16, kind=R8)

!> Standard atmosphere, Pa
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_atmosphere_R4 =                                              &
  real(standard_atmosphere_R16, kind=R4)

!> Standard acceleration of gravity, m s^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_acceleration_of_gravity_R16 =                                &
  9.80665_R16

!> Standard acceleration of gravity, m s^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_acceleration_of_gravity_R8 =                                 &
  real(standard_acceleration_of_gravity_R16, kind=R8)

!> Standard acceleration of gravity, m s^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_acceleration_of_gravity_R4 =                                 &
  real(standard_acceleration_of_gravity_R16, kind=R4)

!> Standard-state pressure, Pa
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_state_pressure_R16 =                                         &
  100000.0_R16

!> Standard-state pressure, Pa
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_state_pressure_R8 =                                          &
  real(standard_state_pressure_R16, kind=R8)

!> Standard-state pressure, Pa
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_state_pressure_R4 =                                          &
  real(standard_state_pressure_R16, kind=R4)

!-----
! "As-maintained" electrical units

! BIPM maintained ohm-69BI -> ohm-BI85 = 0.999998437 ohm
! BIPM maintained volt-76BI = 0.99999241 V
! BIPM maintained ampere-BIPM = volt-76BI / ohm-69BI = 0.99999397 A

!> Faraday constant for conventional electric current, C_90 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Faraday_constant_for_conventional_electric_current_R16 =              &
  96485.3251_R16

!> Faraday constant for conventional electric current, C_90 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Faraday_constant_for_conventional_electric_current_R8 =               &
  real(Faraday_constant_for_conventional_electric_current_R16, kind=R8)

!> Faraday constant for conventional electric current, C_90 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Faraday_constant_for_conventional_electric_current_R4 =               &
  real(Faraday_constant_for_conventional_electric_current_R16, kind=R4)

!-----
! X-ray standards

!> Copper x unit, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Copper_x_unit_R16 =                                                   &
  1.00207697e-13_R16

!> Copper x unit, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Copper_x_unit_R8 =                                                    &
  real(Copper_x_unit_R16, kind=R8)

!> Copper x unit, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Copper_x_unit_R4 =                                                    &
  real(Copper_x_unit_R16, kind=R4)

!> Molybdenum x unit, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Molybdenum_x_unit_R16 =                                               &
  1.00209952e-13_R16

!> Molybdenum x unit, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Molybdenum_x_unit_R8 =                                                &
  real(Molybdenum_x_unit_R16, kind=R8)

!> Molybdenum x unit, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Molybdenum_x_unit_R4 =                                                &
  real(Molybdenum_x_unit_R16, kind=R4)

!> Angstrom star, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Angstrom_star_R16 =                                                   &
  1.00001495e-10_R16

!> Angstrom star, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Angstrom_star_R8 =                                                    &
  real(Angstrom_star_R16, kind=R8)

!> Angstrom star, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Angstrom_star_R4 =                                                    &
  real(Angstrom_star_R16, kind=R4)

!> Lattice parameter of silicon, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  lattice_parameter_of_silicon_R16 =                                    &
  543.1020504e-12_R16

!> Lattice parameter of silicon, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  lattice_parameter_of_silicon_R8 =                                     &
  real(lattice_parameter_of_silicon_R16, kind=R8)

!> Lattice parameter of silicon, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  lattice_parameter_of_silicon_R4 =                                     &
  real(lattice_parameter_of_silicon_R16, kind=R4)

!> Lattice spacing of silicon, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  lattice_spacing_of_ideal_silicon_d220_R16 =                           &
  192.0155714e-12_R16

!> Lattice spacing of silicon, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  lattice_spacing_of_ideal_silicon_d220_R8 =                            &
  real(lattice_spacing_of_ideal_silicon_d220_R16, kind=R8)

!> Lattice spacing of silicon, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  lattice_spacing_of_ideal_silicon_d220_R4 =                            &
  real(lattice_spacing_of_ideal_silicon_d220_R16, kind=R4)

!> Molar volume of silicon, m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_silicon_R16 =                                         &
  12.05883214e-6_R16

!> Molar volume of silicon, m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_silicon_R8 =                                          &
  real(molar_volume_of_silicon_R16, kind=R8)

!> Molar volume of silicon, m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_silicon_R4 =                                          &
  real(molar_volume_of_silicon_R16, kind=R4)

!-----
! Energy conversion factors

!> Joule-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_kilogram_relationship_R16 = 1.0_R16                             &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   1.112 650 056... e-17_R16 ! derived

!> Joule-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_kilogram_relationship_R8 =                                      &
  real(joule_kilogram_relationship_R16, kind=R8) ! derived

!> Joule-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_kilogram_relationship_R4 =                                      &
  real(joule_kilogram_relationship_R16, kind=R4) ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_inverse_meter_relationship_R16 =                                &
  1.0_R16 / (speed_of_light_in_vacuum_R16 * Planck_constant_R16)
  ! 5.034116651e24_R16

!> Joule-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_inverse_meter_relationship_R8 =                                 &
  real(joule_inverse_meter_relationship_R16, kind=R8) ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_inverse_meter_relationship_R4 =                                 &
  real(joule_inverse_meter_relationship_R16, kind=R4) ! derived

!> Joule-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_hertz_relationship_R16 =                                        &
  1.0_R16 / Planck_constant_R16
  ! 1.509190205e33_R16

!> Joule-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_hertz_relationship_R8 =                                         &
  real(joule_hertz_relationship_R16, kind=R8) ! derived

!> Joule-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_hertz_relationship_R4 =                                         &
  real(joule_hertz_relationship_R16, kind=R4) ! derived

!> Joule-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_kelvin_relationship_R16 =                                       &
  1.0_R16 / Boltzmann_constant_R16
  ! 7.2429731e22_R16

!> Joule-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_kelvin_relationship_R8 =                                        &
  real(joule_kelvin_relationship_R16, kind=R8) ! derived

!> Joule-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_kelvin_relationship_R4 =                                        &
  real(joule_kelvin_relationship_R16, kind=R4) ! derived

!> Joule-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_electron_volt_relationship_R16 =                                &
  1.0_R16 / elementary_charge_R16
  ! 6.241509126e18_R16

!> Joule-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_electron_volt_relationship_R8 =                                 &
  real(joule_electron_volt_relationship_R16, kind=R8) ! derived

!> Joule-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_electron_volt_relationship_R4 =                                 &
  real(joule_electron_volt_relationship_R16, kind=R4) ! derived

!> Joule-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_atomic_mass_unit_relationship_R16 =                             &
  1.0_R16 / (unified_atomic_mass_unit_R16                               &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 6.700535363e9_R16

!> Joule-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_atomic_mass_unit_relationship_R8 =                              &
  real(joule_atomic_mass_unit_relationship_R16, kind=R8)

!> Joule-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_atomic_mass_unit_relationship_R4 =                              &
  real(joule_atomic_mass_unit_relationship_R16, kind=R4)

!> Joule-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_hartree_relationship_R16 =                                      &
  1.0_R16 / Hartree_energy_R16
  ! 2.293712317e17_R16

!> Joule-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_hartree_relationship_R8 =                                       &
  real(joule_hartree_relationship_R16, kind=R8)

!> Joule-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_hartree_relationship_R4 =                                       &
  real(joule_hartree_relationship_R16, kind=R4)


!> Kilogram-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_joule_relationship_R16 =                                     &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16
!   8.987 551 787... e16_R16 ! derived

!> Kilogram-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_joule_relationship_R8 =                                      &
  real(kilogram_joule_relationship_R16, kind=R8) ! derived

!> Kilogram-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_joule_relationship_R4 =                                      &
  real(kilogram_joule_relationship_R16, kind=R4) ! derived

!> Kilogram-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_inverse_meter_relationship_R16 =                             &
  speed_of_light_in_vacuum_R16 / Planck_constant_R16
  ! 4.524438411e41_R16

!> Kilogram-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_inverse_meter_relationship_R8 =                              &
  real(kilogram_inverse_meter_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-inverse meter relationship, m^-1
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_inverse_meter_relationship_R4 =                              &
!   real(kilogram_inverse_meter_relationship_R16, kind=R4) ! derived

!> Kilogram-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_hertz_relationship_R16 =                                     &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Planck_constant_R16
  !  1.356392512e50_R16

!> Kilogram-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_hertz_relationship_R8 =                                      &
  real(kilogram_hertz_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-hertz relationship, Hz
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_hertz_relationship_R4 =                                      &
!   real(kilogram_hertz_relationship_R16, kind=R4) ! derived

!> Kilogram-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_kelvin_relationship_R16 =                                    &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Boltzmann_constant_R16
  ! 6.5096595e39_R16

!> Kilogram-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_kelvin_relationship_R8 =                                     &
  real(kilogram_kelvin_relationship_R16, kind=R8)

! ERROR: OVERFLOW
! !> Kilogram-kelvin relationship, K
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_kelvin_relationship_R4 =                                     &
!   real(kilogram_kelvin_relationship_R16, kind=R4)

!> Kilogram-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_electron_volt_relationship_R16 =                             &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / electron_volt_R16
  ! 5.609588650e35_R16

!> Kilogram-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R8 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R8) ! derived

!> Kilogram-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R4 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R4) ! derived

!> Kilogram-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_atomic_mass_unit_relationship_R16 =                          &
  1.0_R16 / unified_atomic_mass_unit_R16
  ! 6.022140857e26_R16

!> Kilogram-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_atomic_mass_unit_relationship_R8 =                           &
  real(kilogram_atomic_mass_unit_relationship_R16, kind=R8)

!> Kilogram-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_atomic_mass_unit_relationship_R4 =                           &
  real(kilogram_atomic_mass_unit_relationship_R16, kind=R4)

!> Kilogram-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_hartree_relationship_R16 =                                   &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Hartree_energy_R16
  ! 2.061485823e34_R16

!> Kilogram-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_hartree_relationship_R8 =                                    &
  real(kilogram_hartree_relationship_R16, kind=R8)

!> Kilogram-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_hartree_relationship_R4 =                                    &
  real(kilogram_hartree_relationship_R16, kind=R4)


!> Inverse meter-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_joule_relationship_R16 =                                &
  1.986445824e-25_R16

!> Inverse meter-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_joule_relationship_R8 =                                 &
  real(inverse_meter_joule_relationship_R16, kind=R8) ! derived

!> Inverse meter-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_joule_relationship_R4 =                                 &
  real(inverse_meter_joule_relationship_R16, kind=R4) ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_kilogram_relationship_R16 =                             &
  2.210219057e-42_R16

!> Inverse meter-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_kilogram_relationship_R8 =                              &
  real(inverse_meter_kilogram_relationship_R16, kind=R8) ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_kilogram_relationship_R4 =                              &
  real(inverse_meter_kilogram_relationship_R16, kind=R4) ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_hertz_relationship_R16 =                                &
  speed_of_light_in_vacuum_R16
  ! 299792458.0_R16 ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_hertz_relationship_R8 =                                 &
  real(inverse_meter_hertz_relationship_R16, kind=R8) ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_hertz_relationship_R4 =                                 &
  real(inverse_meter_hertz_relationship_R16, kind=R4)

!> Inverse meter-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_kelvin_relationship_R16 =                               &
  1.43877736e-2_R16

!> Inverse meter-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_kelvin_relationship_R8 =                                &
  real(inverse_meter_kelvin_relationship_R16, kind=R8) ! derived

!> Inverse meter-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_kelvin_relationship_R4 =                                &
  real(inverse_meter_kelvin_relationship_R16, kind=R4) ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_electron_volt_relationship_R16 =                        &
  1.2398419739e-6_R16

!> Inverse meter-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R8 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R8) ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R4 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R4) ! derived

!> Inverse meter-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_atomic_mass_unit_relationship_R16 =                     &
  1.33102504900e-15_R16

!> Inverse meter-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_atomic_mass_unit_relationship_R8 =                      &
  real(inverse_meter_atomic_mass_unit_relationship_R16, kind=R8)

!> Inverse meter-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_atomic_mass_unit_relationship_R4 =                      &
  real(inverse_meter_atomic_mass_unit_relationship_R16, kind=R4)

!> Inverse meter-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_hartree_relationship_R16 =                              &
  4.556335252767e-8_R16

!> Inverse meter-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_hartree_relationship_R8 =                               &
  real(inverse_meter_hartree_relationship_R16, kind=R8)

!> Inverse meter-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_hartree_relationship_R4 =                               &
  real(inverse_meter_hartree_relationship_R16, kind=R4)


!> Hertz-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_joule_relationship_R16 =                                        &
  6.626070040e-34_R16

!> Hertz-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_joule_relationship_R8 =                                         &
  real(hertz_joule_relationship_R16, kind=R8)

!> Hertz-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_joule_relationship_R4 =                                         &
  real(hertz_joule_relationship_R16, kind=R4)

!> Hertz-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_kilogram_relationship_R16 =   Planck_constant_R16               &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 7.372497201e-51_R16

!> Hertz-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_kilogram_relationship_R8 =                                      &
  real(hertz_kilogram_relationship_R16, kind=R8) ! derived

! UNDERFLOW
! !> Hertz-kilogram relationship, kg
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   hertz_kilogram_relationship_R4 =                                      &
!   real(hertz_kilogram_relationship_R16, kind=R4) ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_inverse_meter_relationship_R16 =                                &
  1.0_R16 / speed_of_light_in_vacuum_R16
!   3.335 640 951... e-9_R16 ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_inverse_meter_relationship_R8 =                                 &
  real(hertz_inverse_meter_relationship_R16, kind=R8) ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_inverse_meter_relationship_R4 =                                 &
  real(hertz_inverse_meter_relationship_R16, kind=R4) ! derived

!> Hertz-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_kelvin_relationship_R16 =                                       &
  4.7992447e-11_R16

!> Hertz-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_kelvin_relationship_R8 =                                        &
  real(hertz_kelvin_relationship_R16, kind=R8) ! derived

!> Hertz-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_kelvin_relationship_R4 =                                        &
  real(hertz_kelvin_relationship_R16, kind=R4) ! derived

!> Hertz-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_electron_volt_relationship_R16 =                                &
  4.135667662e-15_R16

!> Hertz-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_electron_volt_relationship_R8 =                                 &
  real(hertz_electron_volt_relationship_R16, kind=R8) ! derived

!> Hertz-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_electron_volt_relationship_R4 =                                 &
  real(hertz_electron_volt_relationship_R16, kind=R4) ! derived

!> Hertz-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_atomic_mass_unit_relationship_R16 =                             &
  4.4398216616e-24_R16

!> Hertz-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_atomic_mass_unit_relationship_R8 =                              &
  real(hertz_atomic_mass_unit_relationship_R16, kind=R8)

!> Hertz-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_atomic_mass_unit_relationship_R4 =                              &
  real(hertz_atomic_mass_unit_relationship_R16, kind=R4)

!> Hertz-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_hartree_relationship_R16 =                                      &
  1.5198298460088e-16_R16

!> Hertz-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_hartree_relationship_R8 =                                       &
  real(hertz_hartree_relationship_R16, kind=R8)

!> Hertz-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_hartree_relationship_R4 =                                       &
  real(hertz_hartree_relationship_R16, kind=R4)


!> Kelvin-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_joule_relationship_R16 =                                       &
  1.38064852e-23_R16

!> Kelvin-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_joule_relationship_R8 =                                        &
  real(kelvin_joule_relationship_R16, kind=R8)

!> Kelvin-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_joule_relationship_R4 =                                        &
  real(kelvin_joule_relationship_R16, kind=R4)

!> Kelvin-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_kilogram_relationship_R16 =                                    &
  1.53617865e-40_R16

!> Kelvin-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_kilogram_relationship_R8 =                                     &
  real(kelvin_kilogram_relationship_R16, kind=R8) ! derived

!> Kelvin-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_kilogram_relationship_R4 =                                     &
  real(kelvin_kilogram_relationship_R16, kind=R4) ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_inverse_meter_relationship_R16 =                               &
  69.503457_R16

!> Kelvin-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_inverse_meter_relationship_R8 =                                &
  real(kelvin_inverse_meter_relationship_R16, kind=R8) ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_inverse_meter_relationship_R4 =                                &
  real(kelvin_inverse_meter_relationship_R16, kind=R4) ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_hertz_relationship_R16 =                                       &
  2.0836612e10_R16

!> Kelvin-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_hertz_relationship_R8 =                                        &
  real(kelvin_hertz_relationship_R16, kind=R8) ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_hertz_relationship_R4 =                                        &
  real(kelvin_hertz_relationship_R16, kind=R4) ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_electron_volt_relationship_R16 =                               &
  8.6173303e-5_R16

!> Kelvin-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_electron_volt_relationship_R8 =                                &
  real(kelvin_electron_volt_relationship_R16, kind=R8) ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_electron_volt_relationship_R4 =                                &
  real(kelvin_electron_volt_relationship_R16, kind=R4) ! derived

!> Kelvin-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_atomic_mass_unit_relationship_R16 =                            &
  9.2510842e-14_R16

!> Kelvin-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_atomic_mass_unit_relationship_R8 =                             &
  real(kelvin_atomic_mass_unit_relationship_R16, kind=R8)

!> Kelvin-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_atomic_mass_unit_relationship_R4 =                             &
  real(kelvin_atomic_mass_unit_relationship_R16, kind=R4)

!> Kelvin-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_hartree_relationship_R16 =                                     &
  3.1668105e-6_R16

!> Kelvin-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_hartree_relationship_R8 =                                      &
  real(kelvin_hartree_relationship_R16, kind=R8)

!> Kelvin-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_hartree_relationship_R4 =                                      &
  real(kelvin_hartree_relationship_R16, kind=R4)


!> Electron volt-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_joule_relationship_R16 =                                &
  1.6021766208e-19_R16

!> Electron volt-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_joule_relationship_R8 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R8)

!> Electron volt-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_joule_relationship_R4 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R4)

!> Electron volt-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_kilogram_relationship_R16 =                             &
  1.782661907e-36_R16

!> Electron volt-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_kilogram_relationship_R8 =                              &
  real(electron_volt_kilogram_relationship_R16, kind=R8) ! derived

!> Electron volt-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_kilogram_relationship_R4 =                              &
  real(electron_volt_kilogram_relationship_R16, kind=R4) ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_inverse_meter_relationship_R16 =                        &
  8.065544005e5_R16

!> Electron volt-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R8 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R8) ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R4 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R4) ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_hertz_relationship_R16 =                                &
  2.417989262e14_R16

!> Electron volt-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_hertz_relationship_R8 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R8) ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_hertz_relationship_R4 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R4) ! derived

!> Electron volt-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_kelvin_relationship_R16 =                               &
  1.16045221e4_R16

!> Electron volt-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R8 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R8) ! derived

!> Electron volt-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R4 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R4) ! derived

!> Electron volt-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_atomic_mass_unit_relationship_R16 =                     &
  1.0735441105e-9_R16

!> Electron volt-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_atomic_mass_unit_relationship_R8 =                      &
  real(electron_volt_atomic_mass_unit_relationship_R16, kind=R8)

!> Electron volt-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_atomic_mass_unit_relationship_R4 =                      &
  real(electron_volt_atomic_mass_unit_relationship_R16, kind=R4)

!> Electron volt-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_hartree_relationship_R16 =                              &
  3.674932248e-2_R16

!> Electron volt-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_hartree_relationship_R8 =                               &
  real(electron_volt_hartree_relationship_R16, kind=R8)

!> Electron volt-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_hartree_relationship_R4 =                               &
  real(electron_volt_hartree_relationship_R16, kind=R4)


!> Atomic mass unit-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_joule_relationship_R16 =                             &
  1.492418062e-10_R16

!> Atomic mass unit-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_joule_relationship_R8 =                              &
  real(atomic_mass_unit_joule_relationship_R16, kind=R8)

!> Atomic mass unit-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_joule_relationship_R4 =                              &
  real(atomic_mass_unit_joule_relationship_R16, kind=R4)

!> Atomic mass unit-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_kilogram_relationship_R16 =                          &
  1.660539040e-27_R16

!> Atomic mass unit-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_kilogram_relationship_R8 =                           &
  real(atomic_mass_unit_kilogram_relationship_R16, kind=R8)

!> Atomic mass unit-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_kilogram_relationship_R4 =                           &
  real(atomic_mass_unit_kilogram_relationship_R16, kind=R4)

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_inverse_meter_relationship_R16 =                     &
  7.5130066166e14_R16

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_inverse_meter_relationship_R8 =                      &
  real(atomic_mass_unit_inverse_meter_relationship_R16, kind=R8)

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_inverse_meter_relationship_R4 =                      &
  real(atomic_mass_unit_inverse_meter_relationship_R16, kind=R4)

!> Atomic mass unit-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_hertz_relationship_R16 =                             &
  2.2523427206e23_R16

!> Atomic mass unit-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_hertz_relationship_R8 =                              &
  real(atomic_mass_unit_hertz_relationship_R16, kind=R8)

!> Atomic mass unit-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_hertz_relationship_R4 =                              &
  real(atomic_mass_unit_hertz_relationship_R16, kind=R4)

!> Atomic mass unit-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_kelvin_relationship_R16 =                            &
  1.08095438e13_R16

!> Atomic mass unit-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_kelvin_relationship_R8 =                             &
  real(atomic_mass_unit_kelvin_relationship_R16, kind=R8)

!> Atomic mass unit-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_kelvin_relationship_R4 =                             &
  real(atomic_mass_unit_kelvin_relationship_R16, kind=R4)

!> Atomic mass unit-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_electron_volt_relationship_R16 =                     &
  931.4940954e6_R16

!> Atomic mass unit-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_electron_volt_relationship_R8 =                      &
  real(atomic_mass_unit_electron_volt_relationship_R16, kind=R8)

!> Atomic mass unit-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_electron_volt_relationship_R4 =                      &
  real(atomic_mass_unit_electron_volt_relationship_R16, kind=R4)

!> Atomic mass unit-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_hartree_relationship_R16 =                           &
  3.4231776902e7_R16

!> Atomic mass unit-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_hartree_relationship_R8 =                            &
  real(atomic_mass_unit_hartree_relationship_R16, kind=R8)

!> Atomic mass unit-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_hartree_relationship_R4 =                            &
  real(atomic_mass_unit_hartree_relationship_R16, kind=R4)


!> Hartree-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_joule_relationship_R16 =                                      &
  4.359744650e-18_R16

!> Hartree-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_joule_relationship_R8 =                                       &
  real(hartree_joule_relationship_R16, kind=R8)

!> Hartree-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_joule_relationship_R4 =                                       &
  real(hartree_joule_relationship_R16, kind=R4)

!> Hartree-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_kilogram_relationship_R16 =                                   &
  4.850870129e-35_R16

!> Hartree-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_kilogram_relationship_R8 =                                    &
  real(hartree_kilogram_relationship_R16, kind=R8)

!> Hartree-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_kilogram_relationship_R4 =                                    &
  real(hartree_kilogram_relationship_R16, kind=R4)

!> Hartree-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_inverse_meter_relationship_R16 =                              &
  2.194746313702e7_R16

!> Hartree-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_inverse_meter_relationship_R8 =                               &
  real(hartree_inverse_meter_relationship_R16, kind=R8)

!> Hartree-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_inverse_meter_relationship_R4 =                               &
  real(hartree_inverse_meter_relationship_R16, kind=R4)

!> Hartree-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_hertz_relationship_R16 =                                      &
  6.579683920711e15_R16

!> Hartree-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_hertz_relationship_R8 =                                       &
  real(hartree_hertz_relationship_R16, kind=R8)

!> Hartree-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_hertz_relationship_R4 =                                       &
  real(hartree_hertz_relationship_R16, kind=R4)

!> Hartree-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_kelvin_relationship_R16 =                                     &
  3.1577513e5_R16

!> Hartree-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_kelvin_relationship_R8 =                                      &
  real(hartree_kelvin_relationship_R16, kind=R8)

!> Hartree-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_kelvin_relationship_R4 =                                      &
  real(hartree_kelvin_relationship_R16, kind=R4)

!> Hartree-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_electron_volt_relationship_R16 =                              &
  27.21138602_R16

!> Hartree-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_electron_volt_relationship_R8 =                               &
  real(hartree_electron_volt_relationship_R16, kind=R8)

!> Hartree-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_electron_volt_relationship_R4 =                               &
  real(hartree_electron_volt_relationship_R16, kind=R4)

!> Hartree-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_atomic_mass_unit_relationship_R16 =                           &
  2.9212623197e-8_R16

!> Hartree-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_atomic_mass_unit_relationship_R8 =                            &
  real(hartree_atomic_mass_unit_relationship_R16, kind=R8)

!> Hartree-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_atomic_mass_unit_relationship_R4 =                            &
  real(hartree_atomic_mass_unit_relationship_R16, kind=R4)

! File statistics:
! Maximum variable length =     55
! Literal parameters      =    328
! Derived parameters      =      7
! Total parameters        =    335

contains

!> Derived unit checks
subroutine check()
  use, intrinsic :: ISO_FORTRAN_ENV, only: stdout => OUTPUT_UNIT
  implicit none

1 format(A)
! 2 format(ES44.35E4) ! +1.000000000000000000000000000000000000E+0000
3 format(A, T62, ES44.35E4, ' =? ', A)

continue

write(unit=stdout, fmt=1)                                             &
  '! ---- STDLIB_CODATA_2014 Derived Unit Checks ----'

write(unit=stdout, fmt=3)                                             &
  'atomic_unit_of_permittivity_R16',                                  &
  atomic_unit_of_permittivity_R16,                                    &
  '1.112 650 056... e-10'

write(unit=stdout, fmt=3)                                             &
  'characteristic_impedance_of_vacuum_R16',                           &
  characteristic_impedance_of_vacuum_R16,                             &
  '376.730 313 461...'

write(unit=stdout, fmt=3)                                             &
  'electric_constant_R16',                                            &
  electric_constant_R16,                                              &
  '8.854 187 817... e-12'

write(unit=stdout, fmt=3)                                             &
  'magnetic_constant_R16',                                            &
  magnetic_constant_R16,                                              &
  '12.566 370 614... e-7'

write(unit=stdout, fmt=3)                                             &
  'joule_kilogram_relationship_R16',                                  &
  joule_kilogram_relationship_R16,                                    &
  '1.112 650 056... e-17'

write(unit=stdout, fmt=3)                                             &
  'joule_inverse_meter_relationship_R16',                             &
  joule_inverse_meter_relationship_R16,                               &
  '5.034 117 47... e24'

write(unit=stdout, fmt=3)                                             &
  'joule_hertz_relationship_R16',                                     &
  joule_hertz_relationship_R16,                                       &
  '1.509 190 450... e33'

write(unit=stdout, fmt=3)                                             &
  'joule_kelvin_relationship_R16',                                    &
  joule_kelvin_relationship_R16,                                      &
  '7.242 963... e22'

write(unit=stdout, fmt=3)                                             &
  'joule_electron_volt_relationship_R16',                             &
  joule_electron_volt_relationship_R16,                               &
  '6.241 509 65... e18'

write(unit=stdout, fmt=3)                                             &
  'joule_atomic_mass_unit_relationship_R16',                          &
  joule_atomic_mass_unit_relationship_R16,                            &
  '6.700 536 41... e9'

write(unit=stdout, fmt=3)                                             &
  'joule_hartree_relationship_R16',                                   &
  joule_hartree_relationship_R16,                                     &
  '2.293 712 69... e17'

write(unit=stdout, fmt=3)                                             &
  'kilogram_joule_relationship_R16',                                  &
  kilogram_joule_relationship_R16,                                    &
  '8.987 551 787... e16'

write(unit=stdout, fmt=3)                                             &
  'kilogram_inverse_meter_relationship_R16',                          &
  kilogram_inverse_meter_relationship_R16,                            &
  '4.524 439 15... e41'

write(unit=stdout, fmt=3)                                             &
  'kilogram_hertz_relationship_R16',                                  &
  kilogram_hertz_relationship_R16,                                    &
  '1.356 392 733... e50'

write(unit=stdout, fmt=3)                                             &
  'kilogram_kelvin_relationship_R16',                                 &
  kilogram_kelvin_relationship_R16,                                   &
  '6.509 651... e39'

write(unit=stdout, fmt=3)                                             &
  'kilogram_electron_volt_relationship_R16',                          &
  kilogram_electron_volt_relationship_R16,                            &
  '5.609 589 12... e35'

write(unit=stdout, fmt=3)                                             &
  'kilogram_atomic_mass_unit_relationship_R16',                       &
  kilogram_atomic_mass_unit_relationship_R16,                         &
  '6.022 141 79... e26'

write(unit=stdout, fmt=3)                                             &
  'kilogram_hartree_relationship_R16',                                &
  kilogram_hartree_relationship_R16,                                  &
  '2.061 486 16... e34'


write(unit=stdout, fmt=3)                                             &
  'inverse_meter_joule_relationship_R16',                             &
  inverse_meter_joule_relationship_R16,                               &
  '1.986 445 501... e-25'

write(unit=stdout, fmt=3)                                             &
  'inverse_meter_kilogram_relationship_R16',                          &
  inverse_meter_kilogram_relationship_R16,                            &
  '2.210 218 70... e-42'

write(unit=stdout, fmt=3)                                             &
  'inverse_meter_hertz_relationship_R16',                             &
  inverse_meter_hertz_relationship_R16,                               &
  '299 792 458'

write(unit=stdout, fmt=3)                                             &
  'inverse_meter_kelvin_relationship_R16',                            &
  inverse_meter_kelvin_relationship_R16,                              &
  '1.438 7752... e-2'

write(unit=stdout, fmt=3)                                             &
  'inverse_meter_electron_volt_relationship_R16',                     &
  inverse_meter_electron_volt_relationship_R16,                       &
  '1.239 841 875... e-6'

write(unit=stdout, fmt=3)                                             &
  'inverse_meter_atomic_mass_unit_relationship_R16',                  &
  inverse_meter_atomic_mass_unit_relationship_R16,                    &
  '1.331 025 0394... e-15'

write(unit=stdout, fmt=3)                                             &
  'inverse_meter_hartree_relationship_R16',                           &
  inverse_meter_hartree_relationship_R16,                             &
  '4.556 335 252 760... e-8'


write(unit=stdout, fmt=3)                                             &
  'hertz_joule_relationship_R16',                                     &
  hertz_joule_relationship_R16,                                       &
  '6.626 068 96... e−34'

write(unit=stdout, fmt=3)                                             &
  'hertz_kilogram_relationship_R16',                                  &
  hertz_kilogram_relationship_R16,                                    &
  '7.372 496 00... e-51'

write(unit=stdout, fmt=3)                                             &
  'hertz_hertz_relationship_R16',                                     &
  hertz_inverse_meter_relationship_R16,                               &
  '3.335 640 951 ... e-9'

write(unit=stdout, fmt=3)                                             &
  'hertz_kelvin_relationship_R16',                                    &
  hertz_kelvin_relationship_R16,                                      &
  '4.799 2374... e-11'

write(unit=stdout, fmt=3)                                             &
  'hertz_electron_volt_relationship_R16',                             &
  hertz_electron_volt_relationship_R16,                               &
  '4.135 667 33... e-15'

write(unit=stdout, fmt=3)                                             &
  'hertz_atomic_mass_unit_relationship_R16',                          &
  hertz_atomic_mass_unit_relationship_R16,                            &
  '4.439 821 6294... e-24'

write(unit=stdout, fmt=3)                                             &
  'hertz_hartree_relationship_R16',                                   &
  hertz_hartree_relationship_R16,                                     &
  '1.519 829 846 006... e-16'


write(unit=stdout, fmt=3)                                             &
  'kelvin_joule_relationship_R16',                                    &
  kelvin_joule_relationship_R16,                                      &
  '1.380 6504... e−23'

write(unit=stdout, fmt=3)                                             &
  'kelvin_kilogram_relationship_R16',                                 &
  kelvin_kilogram_relationship_R16,                                   &
  '1.536 1807... e-40'

write(unit=stdout, fmt=3)                                             &
  'kelvin_inverse_meter_relationship_R16',                            &
  kelvin_inverse_meter_relationship_R16,                              &
  '69.503 56 ...'

write(unit=stdout, fmt=3)                                             &
  'kelvin_hertz_relationship_R16',                                    &
  kelvin_hertz_relationship_R16,                                      &
  '2.083 6644... e10'

write(unit=stdout, fmt=3)                                             &
  'kelvin_electron_volt_relationship_R16',                            &
  kelvin_electron_volt_relationship_R16,                              &
  '8.617 343... e-5'

write(unit=stdout, fmt=3)                                             &
  'kelvin_atomic_mass_unit_relationship_R16',                         &
  kelvin_atomic_mass_unit_relationship_R16,                           &
  '9.251 098... e-14'

write(unit=stdout, fmt=3)                                             &
  'kelvin_hartree_relationship_R16',                                  &
  kelvin_hartree_relationship_R16,                                    &
  '3.166 8153... e-6'


write(unit=stdout, fmt=3)                                             &
  'electron_volt_joule_relationship_R16',                             &
  electron_volt_joule_relationship_R16,                               &
  '1.602 176 487... e-19'

write(unit=stdout, fmt=3)                                             &
  'electron_volt_kilogram_relationship_R16',                          &
  electron_volt_kilogram_relationship_R16,                            &
  '1.782 661 758... e-36'

write(unit=stdout, fmt=3)                                             &
  'electron_volt_inverse_meter_relationship_R16',                     &
  electron_volt_inverse_meter_relationship_R16,                       &
  '8.065 544 65... e5'

write(unit=stdout, fmt=3)                                             &
  'electron_volt_hertz_relationship_R16',                             &
  electron_volt_hertz_relationship_R16,                               &
  '2.417 989 454... e14'

write(unit=stdout, fmt=3)                                             &
  'electron_volt_kelvin_relationship_R16',                            &
  electron_volt_kelvin_relationship_R16,                              &
  '1.160 4505... e4'

write(unit=stdout, fmt=3)                                             &
  'electron_volt_atomic_mass_unit_relationship_R16',                  &
  electron_volt_atomic_mass_unit_relationship_R16,                    &
  '1.073 544 188... e-9'

write(unit=stdout, fmt=3)                                             &
  'electron_volt_hartree_relationship_R16',                           &
  electron_volt_hartree_relationship_R16,                             &
  '3.674 932 540... e-2'


write(unit=stdout, fmt=3)                                               &
  'atomic_mass_unit_joule_relationship_R16',                            &
  atomic_mass_unit_joule_relationship_R16,                              &
  '1.492 417 830... e-10'

write(unit=stdout, fmt=3)                                               &
  'atomic_mass_unit_kilogram_relationship_R16',                         &
  atomic_mass_unit_kilogram_relationship_R16,                           &
  '1.660 538 782... e-27'

write(unit=stdout, fmt=3)                                               &
  'atomic_mass_unit_inverse_meter_relationship_R16',                    &
  atomic_mass_unit_inverse_meter_relationship_R16,                      &
  '7.513 006 671... e14'

write(unit=stdout, fmt=3)                                               &
  'atomic_mass_unit_hertz_relationship_R16',                            &
  atomic_mass_unit_hertz_relationship_R16,                              &
  '2.252 342 7369... e14'

write(unit=stdout, fmt=3)                                               &
  'atomic_mass_unit_kelvin_relationship_R16',                           &
  atomic_mass_unit_kelvin_relationship_R16,                             &
  '1.080 9527... e13'

write(unit=stdout, fmt=3)                                               &
  'atomic_mass_unit_electron_volt_relationship_R16',                    &
  atomic_mass_unit_electron_volt_relationship_R16,                      &
  '931.494 028... e6'

write(unit=stdout, fmt=3)                                               &
  'atomic_mass_unit_hartree_relationship_R16',                          &
  atomic_mass_unit_hartree_relationship_R16,                            &
  '3.423 177 7149... e7'


write(unit=stdout, fmt=3)                                               &
  'hartree_joule_relationship_R16',                                     &
  hartree_joule_relationship_R16,                                       &
  '4.359 743 94... e-18'

write(unit=stdout, fmt=3)                                               &
  'hartree_kilogram_relationship_R16',                                  &
  hartree_kilogram_relationship_R16,                                    &
  '4.850 869 34... e-35'

write(unit=stdout, fmt=3)                                               &
  'hartree_inverse_meter_relationship_R16',                             &
  hartree_inverse_meter_relationship_R16,                               &
  '2.194 746 313 705... e7'

write(unit=stdout, fmt=3)                                               &
  'hartree_hertz_relationship_R16',                                     &
  hartree_hertz_relationship_R16,                                       &
  '6.579 683 920 722... e15'

write(unit=stdout, fmt=3)                                               &
  'hartree_kelvin_relationship_R16',                                    &
  hartree_kelvin_relationship_R16,                                      &
  '3.157 7465... e5'

write(unit=stdout, fmt=3)                                               &
  'hartree_electron_volt_relationship_R16',                             &
  hartree_electron_volt_relationship_R16,                               &
  '27.211 383 86...'

write(unit=stdout, fmt=3)                                               &
  'hartree_atomic_mass_unit_relationship_R16',                          &
  hartree_atomic_mass_unit_relationship_R16,                            &
  '2.921 262 2986... e-8'

  return
end subroutine check
end module STDLIB_CODATA_2014
