!> @file STDLIB_CODATA_2018.f90
!! @author Robert Apthorpe
!! @copyright See LICENSE
!!
!! @brief Literal and derived fundamental physical constants consistent
!! with @cite CODATA_2018

!> @brief Literal and derived fundamental physical constants consistent
!! with @cite CODATA_2018
!!
!! Unless otherwise noted, all data has been taken from @cite CODATA_2018
module STDLIB_CODATA_2018
use, intrinsic :: ISO_FORTRAN_ENV, only: R4 => REAL32, R8 => REAL64,    &
  R16 => REAL128
implicit none

public :: check

private

! Constants in REAL128 precision, selected_real_kind(33, 4931)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 33 significant digits.
real(kind=R16), parameter, public :: pi_R16 =                           &
  3.14159265358979323846264338327950_R16

! Constants in REAL64 precision, selected_real_kind(15, 307)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 15 significant digits.
real(kind=R8), parameter, public :: pi_R8 =                             &
  real(pi_R16, kind=R8)

! Constants in REAL32 precision, selected_real_kind(6, 37)

!> Ratio of circle circumference to diameter, \f$\pi\f$, dimensionless.
!! From @cite Shanks1962. Truncated to 6 significant digits.
real(kind=R4), parameter, public :: pi_R4 =                             &
  real(pi_R16, kind=R4)

!> Wien wavelength root; solution of (x - 5) * exp(x) + 5 = 0
!! Truncated to 35 significant digits.
  real(kind=R16), parameter, public :: wlroot_R16 =                     &
  4.96511423174427630369875913132289352_R16

!> Wien frequency root; solution of (x - 3) * exp(x) + 3 = 0
!! Truncated to 35 significant digits.
  real(kind=R16), parameter, public :: froot_R16 =                      &
  2.82143937212207889340319133029448507_R16

! Reordered constants (work around forward-referencing by NIST)

!> Electron volt, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_R16 =                                                   &
  1.602176634e-19_R16

!> Boltzmann constant, J K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_R16 =                                              &
  1.380649e-23_R16

!> Standard temperature, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_temperature_R16 =                                            &
  273.15_R16

!> Standard atmosphere, Pa
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_atmosphere_R16 =                                             &
  101325.0_R16

!> Standard-state pressure, Pa
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_state_pressure_R16 =                                         &
  100000.0_R16


!-----
! Universal constants

!> Speed of light in vacuum, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  speed_of_light_in_vacuum_R16 =                                        &
  299792458.0_R16

!> Speed of light in vacuum, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  speed_of_light_in_vacuum_R8 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R8)

!> Speed of light in vacuum, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  speed_of_light_in_vacuum_R4 =                                         &
  real(speed_of_light_in_vacuum_R16, kind=R4)

!> Vacuum mag. permeability, N A^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  vacuum_magnetic_permeability_R16 =                                    &
  1.25663706212e-6_R16

!> Vacuum mag. permeability, N A^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  vacuum_magnetic_permeability_R8 =                                     &
  real(vacuum_magnetic_permeability_R16, kind=R8)

!> Vacuum mag. permeability, N A^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  vacuum_magnetic_permeability_R4 =                                     &
  real(vacuum_magnetic_permeability_R16, kind=R4)

!> Vacuum electric permittivity, F m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  vacuum_electric_permittivity_R16 =                                    &
  8.8541878128e-12_R16

!> Vacuum electric permittivity, F m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  vacuum_electric_permittivity_R8 =                                     &
  real(vacuum_electric_permittivity_R16, kind=R8)

!> Vacuum electric permittivity, F m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  vacuum_electric_permittivity_R4 =                                     &
  real(vacuum_electric_permittivity_R16, kind=R4)

!> Characteristic impedance of vacuum, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  characteristic_impedance_of_vacuum_R16 =                              &
  376.730313668_R16

!> Characteristic impedance of vacuum, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  characteristic_impedance_of_vacuum_R8 =                               &
  real(characteristic_impedance_of_vacuum_R16, kind=R8)

!> Characteristic impedance of vacuum, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  characteristic_impedance_of_vacuum_R4 =                               &
  real(characteristic_impedance_of_vacuum_R16, kind=R4)

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Newtonian_constant_of_gravitation_R16 =                               &
  6.67430e-11_R16

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R8 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R8)

!> Newtonian constant of gravitation, m^3 kg^-1 s^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_R4 =                                &
  real(Newtonian_constant_of_gravitation_R16, kind=R4)

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Newtonian_constant_of_gravitation_over_h_bar_c_R16 =                  &
  6.70883e-39_R16

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_over_h_bar_c_R8 =                   &
  real(Newtonian_constant_of_gravitation_over_h_bar_c_R16, kind=R8)

!> Newtonian constant of gravitation over h-bar c, (GeV/c^2)^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Newtonian_constant_of_gravitation_over_h_bar_c_R4 =                   &
  real(Newtonian_constant_of_gravitation_over_h_bar_c_R16, kind=R4)

!> Planck constant, J Hz^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_constant_R16 =                                                 &
  6.62607015e-34_R16

!> Planck constant, J Hz^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_constant_R8 =                                                  &
  real(Planck_constant_R16, kind=R8)

!> Planck constant, J Hz^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_constant_R4 =                                                  &
  real(Planck_constant_R16, kind=R4)

!> Planck constant in eV/Hz, eV Hz^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_constant_in_eV_Hz_R16 =                                        &
  Planck_constant_R16 / electron_volt_R16
!   4.135 667 696... e-15_R16 ! derived

!> Planck constant in eV/Hz, eV Hz^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_constant_in_eV_Hz_R8 =                                         &
  real(Planck_constant_in_eV_Hz_R16, kind=R8) ! derived

!> Planck constant in eV/Hz, eV Hz^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_constant_in_eV_Hz_R4 =                                         &
  real(Planck_constant_in_eV_Hz_R16, kind=R4) ! derived

!> Reduced Planck constant, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_R16 =                                         &
  Planck_constant_R16 / (2.0_R16 * pi_R16)
!   1.054 571 817... e-34_R16 ! derived

!> Reduced Planck constant, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_R8 =                                          &
  real(reduced_Planck_constant_R16, kind=R8) ! derived

!> Reduced Planck constant, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_R4 =                                          &
  real(reduced_Planck_constant_R16, kind=R4) ! derived

!> Reduced Planck constant in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_in_eV_s_R16 =                                 &
  Planck_constant_R16 / (2.0_R16 * pi_R16 * electron_volt_R16)
!   6.582 119 569... e-16_R16 ! derived

!> Reduced Planck constant in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_in_eV_s_R8 =                                  &
  real(reduced_Planck_constant_in_eV_s_R16, kind=R8) ! derived

!> Reduced Planck constant in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_in_eV_s_R4 =                                  &
  real(reduced_Planck_constant_in_eV_s_R16, kind=R4) ! derived

!> Reduced Planck constant times c in MeV fm, MeV fm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Planck_constant_times_c_in_MeV_fm_R16 =                       &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / (2.0_R16 * pi_R16 * electron_volt_R16)
!   197.326 980 4..._R16 ! derived

!> Reduced Planck constant times c in MeV fm, MeV fm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Planck_constant_times_c_in_MeV_fm_R8 =                        &
  real(reduced_Planck_constant_times_c_in_MeV_fm_R16, kind=R8) ! derived

!> Reduced Planck constant times c in MeV fm, MeV fm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Planck_constant_times_c_in_MeV_fm_R4 =                        &
  real(reduced_Planck_constant_times_c_in_MeV_fm_R16, kind=R4) ! derived

!> Planck mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_mass_R16 =                                                     &
  2.176434e-8_R16

!> Planck mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_mass_R8 =                                                      &
  real(Planck_mass_R16, kind=R8)

!> Planck mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_mass_R4 =                                                      &
  real(Planck_mass_R16, kind=R4)

!> Planck mass energy equivalent in GeV, GeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_mass_energy_equivalent_in_GeV_R16 =                            &
  1.220890e19_R16

!> Planck mass energy equivalent in GeV, GeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_mass_energy_equivalent_in_GeV_R8 =                             &
  real(Planck_mass_energy_equivalent_in_GeV_R16, kind=R8)

!> Planck mass energy equivalent in GeV, GeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_mass_energy_equivalent_in_GeV_R4 =                             &
  real(Planck_mass_energy_equivalent_in_GeV_R16, kind=R4)

!> Planck temperature, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_temperature_R16 =                                              &
  1.416784e32_R16

!> Planck temperature, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_temperature_R8 =                                               &
  real(Planck_temperature_R16, kind=R8)

!> Planck temperature, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_temperature_R4 =                                               &
  real(Planck_temperature_R16, kind=R4)

!> Planck length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_length_R16 =                                                   &
  1.616255e-35_R16

!> Planck length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_length_R8 =                                                    &
  real(Planck_length_R16, kind=R8)

!> Planck length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_length_R4 =                                                    &
  real(Planck_length_R16, kind=R4)

!> Planck time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Planck_time_R16 =                                                     &
  5.391247e-44_R16

!> Planck time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Planck_time_R8 =                                                      &
  real(Planck_time_R16, kind=R8)

!> Planck time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Planck_time_R4 =                                                      &
  real(Planck_time_R16, kind=R4)

!-----
! Electromagnetic constants

!> Elementary charge, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  elementary_charge_R16 =                                               &
  1.602176634e-19_R16

!> Elementary charge, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  elementary_charge_R8 =                                                &
  real(elementary_charge_R16, kind=R8)

!> Elementary charge, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  elementary_charge_R4 =                                                &
  real(elementary_charge_R16, kind=R4)

!> Elementary charge over h-bar, A J^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  elementary_charge_over_h_bar_R16 =                                    &
  elementary_charge_R16 / reduced_Planck_constant_R16
  ! 1.519 267 447... e15_R16 ! derived

!> Elementary charge over h-bar, A J^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  elementary_charge_over_h_bar_R8 =                                     &
  real(elementary_charge_over_h_bar_R16, kind=R8) ! derived

!> Elementary charge over h-bar, A J^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  elementary_charge_over_h_bar_R4 =                                     &
  real(elementary_charge_over_h_bar_R16, kind=R4) ! derived

!> Mag. flux quantum, Wb
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  magnetic_flux_quantum_R16 =                                           &
  Planck_constant_R16 / (2.0_R16 * elementary_charge_R16)
  ! 2.067 833 848... e-15_R16 ! derived

!> Mag. flux quantum, Wb
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  magnetic_flux_quantum_R8 =                                            &
  real(magnetic_flux_quantum_R16, kind=R8) ! derived

!> Mag. flux quantum, Wb
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  magnetic_flux_quantum_R4 =                                            &
  real(magnetic_flux_quantum_R16, kind=R4) ! derived

!> Conductance quantum, S
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conductance_quantum_R16 =                                             &
  2.0_R16 * elementary_charge_R16 * elementary_charge_R16               &
  / Planck_constant_R16
!   7.748 091 729... e-5_R16 ! derived

!> Conductance quantum, S
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conductance_quantum_R8 =                                              &
  real(conductance_quantum_R16, kind=R8) ! derived

!> Conductance quantum, S
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conductance_quantum_R4 =                                              &
  real(conductance_quantum_R16, kind=R4) ! derived

!> Inverse of conductance quantum, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_of_conductance_quantum_R16 =                                  &
  Planck_constant_R16                                                   &
  / (2.0_R16 * elementary_charge_R16 * elementary_charge_R16)
!   12 906.403 72..._R16 ! derived

!> Inverse of conductance quantum, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_of_conductance_quantum_R8 =                                   &
  real(inverse_of_conductance_quantum_R16, kind=R8) ! derived

!> Inverse of conductance quantum, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_of_conductance_quantum_R4 =                                   &
  real(inverse_of_conductance_quantum_R16, kind=R4) ! derived

!> Josephson constant, Hz V^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Josephson_constant_R16 =                                              &
  2.0_R16 * elementary_charge_R16 / Planck_constant_R16
!   483 597.848 4... e9_R16 ! derived

!> Josephson constant, Hz V^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Josephson_constant_R8 =                                               &
  real(Josephson_constant_R16, kind=R8) ! derived

!> Josephson constant, Hz V^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Josephson_constant_R4 =                                               &
  real(Josephson_constant_R16, kind=R4) ! derived

!> Von Klitzing constant, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  von_Klitzing_constant_R16 =                                           &
  Planck_constant_R16                                                   &
  / (elementary_charge_R16 * elementary_charge_R16)
!   25 812.807 45..._R16 ! derived

!> Von Klitzing constant, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  von_Klitzing_constant_R8 =                                            &
  real(von_Klitzing_constant_R16, kind=R8) ! derived

!> Von Klitzing constant, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  von_Klitzing_constant_R4 =                                            &
  real(von_Klitzing_constant_R16, kind=R4) ! derived

!> Bohr magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_R16 =                                                   &
  9.2740100783e-24_R16

!> Bohr magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_R8 =                                                    &
  real(Bohr_magneton_R16, kind=R8)

!> Bohr magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_R4 =                                                    &
  real(Bohr_magneton_R16, kind=R4)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_eV_T_R16 =                                           &
  5.7883818060e-5_R16

!> Bohr magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R8 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R8)

!> Bohr magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_eV_T_R4 =                                            &
  real(Bohr_magneton_in_eV_T_R16, kind=R4)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_Hz_T_R16 =                                           &
  1.39962449361e10_R16

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R8 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R8)

!> Bohr magneton in Hz/T, Hz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_Hz_T_R4 =                                            &
  real(Bohr_magneton_in_Hz_T_R16, kind=R4)

!> Bohr magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_inverse_meter_per_tesla_R16 =                        &
  46.686447783_R16

!> Bohr magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R8 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Bohr magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_inverse_meter_per_tesla_R4 =                         &
  real(Bohr_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Bohr magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_magneton_in_K_T_R16 =                                            &
  0.67171381563_R16

!> Bohr magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R8 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R8)

!> Bohr magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_magneton_in_K_T_R4 =                                             &
  real(Bohr_magneton_in_K_T_R16, kind=R4)

!> Nuclear magneton, J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_R16 =                                                &
  5.0507837461e-27_R16

!> Nuclear magneton, J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_R8 =                                                 &
  real(nuclear_magneton_R16, kind=R8)

!> Nuclear magneton, J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_R4 =                                                 &
  real(nuclear_magneton_R16, kind=R4)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_eV_T_R16 =                                        &
  3.15245125844e-8_R16

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R8 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R8)

!> Nuclear magneton in eV/T, eV T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_eV_T_R4 =                                         &
  real(nuclear_magneton_in_eV_T_R16, kind=R4)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_MHz_T_R16 =                                       &
  7.6225932291_R16

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R8 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R8)

!> Nuclear magneton in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_MHz_T_R4 =                                        &
  real(nuclear_magneton_in_MHz_T_R16, kind=R4)

!> Nuclear magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_inverse_meter_per_tesla_R16 =                     &
  2.54262341353e-2_R16

!> Nuclear magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R8 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R8)

!> Nuclear magneton in inverse meter per tesla, m^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_inverse_meter_per_tesla_R4 =                      &
  real(nuclear_magneton_in_inverse_meter_per_tesla_R16, kind=R4)

!> Nuclear magneton in K/T, K T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  nuclear_magneton_in_K_T_R16 =                                         &
  3.6582677756e-4_R16

!> Nuclear magneton in K/T, K T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R8 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R8)

!> Nuclear magneton in K/T, K T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  nuclear_magneton_in_K_T_R4 =                                          &
  real(nuclear_magneton_in_K_T_R16, kind=R4)

!-----
! Atomic constants

!-----
! General

!> Fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  fine_structure_constant_R16 =                                         &
  7.2973525693e-3_R16

!> Fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  fine_structure_constant_R8 =                                          &
  real(fine_structure_constant_R16, kind=R8)

!> Fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  fine_structure_constant_R4 =                                          &
  real(fine_structure_constant_R16, kind=R4)

!> Inverse fine-structure constant, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_fine_structure_constant_R16 =                                 &
  137.035999084_R16

!> Inverse fine-structure constant, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_fine_structure_constant_R8 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R8)

!> Inverse fine-structure constant, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_fine_structure_constant_R4 =                                  &
  real(inverse_fine_structure_constant_R16, kind=R4)

!> Rydberg constant, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_R16 =                                                &
  10973731.568160_R16

!> Rydberg constant, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_R8 =                                                 &
  real(Rydberg_constant_R16, kind=R8)

!> Rydberg constant, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_R4 =                                                 &
  real(Rydberg_constant_R16, kind=R4)

!> Rydberg constant times c in Hz, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_c_in_Hz_R16 =                                  &
  3.2898419602508e15_R16

!> Rydberg constant times c in Hz, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R8 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R8)

!> Rydberg constant times c in Hz, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_c_in_Hz_R4 =                                   &
  real(Rydberg_constant_times_c_in_Hz_R16, kind=R4)

!> Rydberg constant times hc in J, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_J_R16 =                                  &
  2.1798723611035e-18_R16

!> Rydberg constant times hc in J, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R8 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R8)

!> Rydberg constant times hc in J, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_J_R4 =                                   &
  real(Rydberg_constant_times_hc_in_J_R16, kind=R4)

!> Rydberg constant times hc in eV, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Rydberg_constant_times_hc_in_eV_R16 =                                 &
  13.605693122994_R16

!> Rydberg constant times hc in eV, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R8 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R8)

!> Rydberg constant times hc in eV, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Rydberg_constant_times_hc_in_eV_R4 =                                  &
  real(Rydberg_constant_times_hc_in_eV_R16, kind=R4)

!> Bohr radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Bohr_radius_R16 =                                                     &
  5.29177210903e-11_R16

!> Bohr radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Bohr_radius_R8 =                                                      &
  real(Bohr_radius_R16, kind=R8)

!> Bohr radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Bohr_radius_R4 =                                                      &
  real(Bohr_radius_R16, kind=R4)

!> Hartree energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Hartree_energy_R16 =                                                  &
  4.3597447222071e-18_R16

!> Hartree energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Hartree_energy_R8 =                                                   &
  real(Hartree_energy_R16, kind=R8)

!> Hartree energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Hartree_energy_R4 =                                                   &
  real(Hartree_energy_R16, kind=R4)

!> Hartree energy in eV, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Hartree_energy_in_eV_R16 =                                            &
  27.211386245988_R16

!> Hartree energy in eV, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Hartree_energy_in_eV_R8 =                                             &
  real(Hartree_energy_in_eV_R16, kind=R8)

!> Hartree energy in eV, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Hartree_energy_in_eV_R4 =                                             &
  real(Hartree_energy_in_eV_R16, kind=R4)

!> Quantum of circulation, m^2 s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  quantum_of_circulation_R16 =                                          &
  3.6369475516e-4_R16

!> Quantum of circulation, m^2 s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  quantum_of_circulation_R8 =                                           &
  real(quantum_of_circulation_R16, kind=R8)

!> Quantum of circulation, m^2 s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  quantum_of_circulation_R4 =                                           &
  real(quantum_of_circulation_R16, kind=R4)

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  quantum_of_circulation_times_2_R16 =                                  &
  7.2738951032e-4_R16

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  quantum_of_circulation_times_2_R8 =                                   &
  real(quantum_of_circulation_times_2_R16, kind=R8)

!> Quantum of circulation times 2, m^2 s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  quantum_of_circulation_times_2_R4 =                                   &
  real(quantum_of_circulation_times_2_R16, kind=R4)

!-----
! Electroweak

!> Fermi coupling constant, GeV^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Fermi_coupling_constant_R16 =                                         &
  1.1663787e-5_R16

!> Fermi coupling constant, GeV^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Fermi_coupling_constant_R8 =                                          &
  real(Fermi_coupling_constant_R16, kind=R8)

!> Fermi coupling constant, GeV^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Fermi_coupling_constant_R4 =                                          &
  real(Fermi_coupling_constant_R16, kind=R4)

!> Weak mixing angle, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  weak_mixing_angle_R16 =                                               &
  0.22290_R16

!> Weak mixing angle, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  weak_mixing_angle_R8 =                                                &
  real(weak_mixing_angle_R16, kind=R8)

!> Weak mixing angle, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  weak_mixing_angle_R4 =                                                &
  real(weak_mixing_angle_R16, kind=R4)

!-----
! Electron

!> Electron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_R16 =                                                   &
  9.1093837015e-31_R16

!> Electron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_R8 =                                                    &
  real(electron_mass_R16, kind=R8)

!> Electron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_R4 =                                                    &
  real(electron_mass_R16, kind=R4)

!> Electron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_in_u_R16 =                                              &
  5.48579909065e-4_R16

!> Electron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_in_u_R8 =                                               &
  real(electron_mass_in_u_R16, kind=R8)

!> Electron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_in_u_R4 =                                               &
  real(electron_mass_in_u_R16, kind=R4)

!> Electron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_energy_equivalent_R16 =                                 &
  8.1871057769e-14_R16

!> Electron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_energy_equivalent_R8 =                                  &
  real(electron_mass_energy_equivalent_R16, kind=R8)

!> Electron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_energy_equivalent_R4 =                                  &
  real(electron_mass_energy_equivalent_R16, kind=R4)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_mass_energy_equivalent_in_MeV_R16 =                          &
  0.51099895000_R16

!> Electron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R8 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Electron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_mass_energy_equivalent_in_MeV_R4 =                           &
  real(electron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Electron-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_muon_mass_ratio_R16 =                                        &
  4.83633169e-3_R16

!> Electron-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_muon_mass_ratio_R8 =                                         &
  real(electron_muon_mass_ratio_R16, kind=R8)

!> Electron-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_muon_mass_ratio_R4 =                                         &
  real(electron_muon_mass_ratio_R16, kind=R4)

!> Electron-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_tau_mass_ratio_R16 =                                         &
  2.87585e-4_R16

!> Electron-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_tau_mass_ratio_R8 =                                          &
  real(electron_tau_mass_ratio_R16, kind=R8)

!> Electron-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_tau_mass_ratio_R4 =                                          &
  real(electron_tau_mass_ratio_R16, kind=R4)

!> Electron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_proton_mass_ratio_R16 =                                      &
  5.44617021487e-4_R16

!> Electron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_proton_mass_ratio_R8 =                                       &
  real(electron_proton_mass_ratio_R16, kind=R8)

!> Electron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_proton_mass_ratio_R4 =                                       &
  real(electron_proton_mass_ratio_R16, kind=R4)

!> Electron-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_neutron_mass_ratio_R16 =                                     &
  5.4386734424e-4_R16

!> Electron-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_neutron_mass_ratio_R8 =                                      &
  real(electron_neutron_mass_ratio_R16, kind=R8)

!> Electron-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_neutron_mass_ratio_R4 =                                      &
  real(electron_neutron_mass_ratio_R16, kind=R4)

!> Electron-deuteron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_deuteron_mass_ratio_R16 =                                    &
  2.724437107462e-4_R16

!> Electron-deuteron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_deuteron_mass_ratio_R8 =                                     &
  real(electron_deuteron_mass_ratio_R16, kind=R8)

!> Electron-deuteron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_deuteron_mass_ratio_R4 =                                     &
  real(electron_deuteron_mass_ratio_R16, kind=R4)

!> Electron-helion mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_helion_mass_ratio_R16 =                                      &
  1.819543074573e-4_R16

!> Electron-helion mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_helion_mass_ratio_R8 =                                       &
  real(electron_helion_mass_ratio_R16, kind=R8)

!> Electron-helion mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_helion_mass_ratio_R4 =                                       &
  real(electron_helion_mass_ratio_R16, kind=R4)

!> Electron-triton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_triton_mass_ratio_R16 =                                      &
  1.819200062251e-4_R16

!> Electron-triton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_triton_mass_ratio_R8 =                                       &
  real(electron_triton_mass_ratio_R16, kind=R8)

!> Electron-triton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_triton_mass_ratio_R4 =                                       &
  real(electron_triton_mass_ratio_R16, kind=R4)

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_alpha_particle_mass_ratio_R16 =                           &
  1.370933554787e-4_R16

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_alpha_particle_mass_ratio_R8 =                            &
  real(electron_to_alpha_particle_mass_ratio_R16, kind=R8)

!> Electron to alpha particle mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_alpha_particle_mass_ratio_R4 =                            &
  real(electron_to_alpha_particle_mass_ratio_R16, kind=R4)

!> Electron charge to mass quotient, C kg^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_charge_to_mass_quotient_R16 =                                &
  -1.75882001076e11_R16

!> Electron charge to mass quotient, C kg^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R8 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R8)

!> Electron charge to mass quotient, C kg^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_charge_to_mass_quotient_R4 =                                 &
  real(electron_charge_to_mass_quotient_R16, kind=R4)

!> Electron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_molar_mass_R16 =                                             &
  5.4857990888e-7_R16

!> Electron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_molar_mass_R8 =                                              &
  real(electron_molar_mass_R16, kind=R8)

!> Electron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_molar_mass_R4 =                                              &
  real(electron_molar_mass_R16, kind=R4)

!> Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Compton_wavelength_R16 =                                              &
  2.42631023867e-12_R16

!> Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Compton_wavelength_R8 =                                               &
  real(Compton_wavelength_R16, kind=R8)

!> Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Compton_wavelength_R4 =                                               &
  real(Compton_wavelength_R16, kind=R4)

!> Reduced Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_Compton_wavelength_R16 =                                      &
  3.8615926796e-13_R16

!> Reduced Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_Compton_wavelength_R8 =                                       &
  real(reduced_Compton_wavelength_R16, kind=R8)

!> Reduced Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_Compton_wavelength_R4 =                                       &
  real(reduced_Compton_wavelength_R16, kind=R4)

!> Classical electron radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  classical_electron_radius_R16 =                                       &
  2.8179403262e-15_R16

!> Classical electron radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  classical_electron_radius_R8 =                                        &
  real(classical_electron_radius_R16, kind=R8)

!> Classical electron radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  classical_electron_radius_R4 =                                        &
  real(classical_electron_radius_R16, kind=R4)

!> Thomson cross section, m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Thomson_cross_section_R16 =                                           &
  6.6524587321e-29_R16

!> Thomson cross section, m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Thomson_cross_section_R8 =                                            &
  real(Thomson_cross_section_R16, kind=R8)

!> Thomson cross section, m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Thomson_cross_section_R4 =                                            &
  real(Thomson_cross_section_R16, kind=R4)

!> Electron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_R16 =                                        &
  -9.2847647043e-24_R16

!> Electron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_R8 =                                         &
  real(electron_magnetic_moment_R16, kind=R8)

!> Electron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_R4 =                                         &
  real(electron_magnetic_moment_R16, kind=R4)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                 &
  -1.00115965218128_R16

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                  &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Electron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                  &
  real(electron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R16 =              &
  -1838.28197188_R16

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R8 =               &
  real(electron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Electron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_to_nuclear_magneton_ratio_R4 =               &
  real(electron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Electron mag. mom. anomaly, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_magnetic_moment_anomaly_R16 =                                &
  1.15965218128e-3_R16

!> Electron mag. mom. anomaly, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_magnetic_moment_anomaly_R8 =                                 &
  real(electron_magnetic_moment_anomaly_R16, kind=R8)

!> Electron mag. mom. anomaly, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_magnetic_moment_anomaly_R4 =                                 &
  real(electron_magnetic_moment_anomaly_R16, kind=R4)

!> Electron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_g_factor_R16 =                                               &
  -2.00231930436256_R16

!> Electron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_g_factor_R8 =                                                &
  real(electron_g_factor_R16, kind=R8)

!> Electron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_g_factor_R4 =                                                &
  real(electron_g_factor_R16, kind=R4)

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_muon_magnetic_moment_ratio_R16 =                             &
  206.7669883_R16

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_muon_magnetic_moment_ratio_R8 =                              &
  real(electron_muon_magnetic_moment_ratio_R16, kind=R8)

!> Electron-muon mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_muon_magnetic_moment_ratio_R4 =                              &
  real(electron_muon_magnetic_moment_ratio_R16, kind=R4)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_proton_magnetic_moment_ratio_R16 =                           &
  -658.21068789_R16

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R8 =                            &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Electron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_proton_magnetic_moment_ratio_R4 =                            &
  real(electron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_shielded_proton_magnetic_moment_ratio_R16 =               &
  -658.2275971_R16

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_shielded_proton_magnetic_moment_ratio_R8 =                &
  real(electron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R8)

!> Electron to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_shielded_proton_magnetic_moment_ratio_R4 =                &
  real(electron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R4)

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_neutron_magnetic_moment_ratio_R16 =                          &
  960.92050_R16

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_neutron_magnetic_moment_ratio_R8 =                           &
  real(electron_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Electron-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_neutron_magnetic_moment_ratio_R4 =                           &
  real(electron_neutron_magnetic_moment_ratio_R16, kind=R4)

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_deuteron_magnetic_moment_ratio_R16 =                         &
  -2143.9234915_R16

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_deuteron_magnetic_moment_ratio_R8 =                          &
  real(electron_deuteron_magnetic_moment_ratio_R16, kind=R8)

!> Electron-deuteron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_deuteron_magnetic_moment_ratio_R4 =                          &
  real(electron_deuteron_magnetic_moment_ratio_R16, kind=R4)

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_gyromagnetic_ratio_R16 =                                     &
  1.76085963023e11_R16

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_gyromagnetic_ratio_R8 =                                      &
  real(electron_gyromagnetic_ratio_R16, kind=R8)

!> Electron gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_gyromagnetic_ratio_R4 =                                      &
  real(electron_gyromagnetic_ratio_R16, kind=R4)

!> Electron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_gyromagnetic_ratio_in_MHz_T_R16 =                            &
  28024.9514242_R16

!> Electron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_gyromagnetic_ratio_in_MHz_T_R8 =                             &
  real(electron_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Electron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_gyromagnetic_ratio_in_MHz_T_R4 =                             &
  real(electron_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Electron relative atomic mass, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_relative_atomic_mass_R16 =                                   &
  5.48579909065e-4_R16

!> Electron relative atomic mass, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_relative_atomic_mass_R8 =                                    &
  real(electron_relative_atomic_mass_R16, kind=R8)

!> Electron relative atomic mass, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_relative_atomic_mass_R4 =                                    &
  real(electron_relative_atomic_mass_R16, kind=R4)

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_to_shielded_helion_magnetic_moment_ratio_R16 =               &
  864.058257_R16

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_to_shielded_helion_magnetic_moment_ratio_R8 =                &
  real(electron_to_shielded_helion_magnetic_moment_ratio_R16, kind=R8)

!> Electron to shielded helion mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_to_shielded_helion_magnetic_moment_ratio_R4 =                &
  real(electron_to_shielded_helion_magnetic_moment_ratio_R16, kind=R4)

!-----
! Muon

!> Muon mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_R16 =                                                       &
  1.883531627e-28_R16

!> Muon mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_R8 =                                                        &
  real(muon_mass_R16, kind=R8)

!> Muon mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_R4 =                                                        &
  real(muon_mass_R16, kind=R4)

!> Muon mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_in_u_R16 =                                                  &
  0.1134289259_R16

!> Muon mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_in_u_R8 =                                                   &
  real(muon_mass_in_u_R16, kind=R8)

!> Muon mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_in_u_R4 =                                                   &
  real(muon_mass_in_u_R16, kind=R4)

!> Muon mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_energy_equivalent_R16 =                                     &
  1.692833804e-11_R16

!> Muon mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_energy_equivalent_R8 =                                      &
  real(muon_mass_energy_equivalent_R16, kind=R8)

!> Muon mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_energy_equivalent_R4 =                                      &
  real(muon_mass_energy_equivalent_R16, kind=R4)

!> Muon mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_mass_energy_equivalent_in_MeV_R16 =                              &
  105.6583755_R16

!> Muon mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_mass_energy_equivalent_in_MeV_R8 =                               &
  real(muon_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Muon mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_mass_energy_equivalent_in_MeV_R4 =                               &
  real(muon_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Muon-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_electron_mass_ratio_R16 =                                        &
  206.7682830_R16

!> Muon-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_electron_mass_ratio_R8 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R8)

!> Muon-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_electron_mass_ratio_R4 =                                         &
  real(muon_electron_mass_ratio_R16, kind=R4)

!> Muon-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_tau_mass_ratio_R16 =                                             &
  5.94635e-2_R16

!> Muon-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_tau_mass_ratio_R8 =                                              &
  real(muon_tau_mass_ratio_R16, kind=R8)

!> Muon-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_tau_mass_ratio_R4 =                                              &
  real(muon_tau_mass_ratio_R16, kind=R4)

!> Muon-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_proton_mass_ratio_R16 =                                          &
  0.1126095264_R16

!> Muon-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_proton_mass_ratio_R8 =                                           &
  real(muon_proton_mass_ratio_R16, kind=R8)

!> Muon-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_proton_mass_ratio_R4 =                                           &
  real(muon_proton_mass_ratio_R16, kind=R4)

!> Muon-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_neutron_mass_ratio_R16 =                                         &
  0.1124545170_R16

!> Muon-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_neutron_mass_ratio_R8 =                                          &
  real(muon_neutron_mass_ratio_R16, kind=R8)

!> Muon-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_neutron_mass_ratio_R4 =                                          &
  real(muon_neutron_mass_ratio_R16, kind=R4)

!> Muon molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_molar_mass_R16 =                                                 &
  1.134289259e-4_R16

!> Muon molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_molar_mass_R8 =                                                  &
  real(muon_molar_mass_R16, kind=R8)

!> Muon molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_molar_mass_R4 =                                                  &
  real(muon_molar_mass_R16, kind=R4)

!> Muon Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_Compton_wavelength_R16 =                                         &
  1.173444110e-14_R16

!> Muon Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_Compton_wavelength_R8 =                                          &
  real(muon_Compton_wavelength_R16, kind=R8)

!> Muon Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_Compton_wavelength_R4 =                                          &
  real(muon_Compton_wavelength_R16, kind=R4)

!> Reduced muon Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_muon_Compton_wavelength_R16 =                                 &
  1.867594306e-15_R16

!> Reduced muon Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_muon_Compton_wavelength_R8 =                                  &
  real(reduced_muon_Compton_wavelength_R16, kind=R8)

!> Reduced muon Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_muon_Compton_wavelength_R4 =                                  &
  real(reduced_muon_Compton_wavelength_R16, kind=R4)

!> Muon mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_R16 =                                            &
  -4.49044830e-26_R16

!> Muon mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_R8 =                                             &
  real(muon_magnetic_moment_R16, kind=R8)

!> Muon mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_R4 =                                             &
  real(muon_magnetic_moment_R16, kind=R4)

!> Muon mag. mom. anomaly, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_anomaly_R16 =                                    &
  1.16592089e-3_R16

!> Muon mag. mom. anomaly, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_anomaly_R8 =                                     &
  real(muon_magnetic_moment_anomaly_R16, kind=R8)

!> Muon mag. mom. anomaly, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_anomaly_R4 =                                     &
  real(muon_magnetic_moment_anomaly_R16, kind=R4)

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R16 =                     &
  -4.84197047e-3_R16

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R8 =                      &
  real(muon_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Muon mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_to_Bohr_magneton_ratio_R4 =                      &
  real(muon_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R16 =                  &
  -8.89059703_R16

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R8 =                   &
  real(muon_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Muon mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_magnetic_moment_to_nuclear_magneton_ratio_R4 =                   &
  real(muon_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Muon g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_g_factor_R16 =                                                   &
  -2.0023318418_R16

!> Muon g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_g_factor_R8 =                                                    &
  real(muon_g_factor_R16, kind=R8)

!> Muon g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_g_factor_R4 =                                                    &
  real(muon_g_factor_R16, kind=R4)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  muon_proton_magnetic_moment_ratio_R16 =                               &
  -3.183345142_R16

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R8 =                                &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R8)

!> Muon-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  muon_proton_magnetic_moment_ratio_R4 =                                &
  real(muon_proton_magnetic_moment_ratio_R16, kind=R4)

!-----
! Tau

!> Tau mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_R16 =                                                        &
  3.16754e-27_R16

!> Tau mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_R8 =                                                         &
  real(tau_mass_R16, kind=R8)

!> Tau mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_R4 =                                                         &
  real(tau_mass_R16, kind=R4)

!> Tau mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_in_u_R16 =                                                   &
  1.90754_R16

!> Tau mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_in_u_R8 =                                                    &
  real(tau_mass_in_u_R16, kind=R8)

!> Tau mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_in_u_R4 =                                                    &
  real(tau_mass_in_u_R16, kind=R4)

!> Tau mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_energy_equivalent_R16 =                                      &
  2.84684e-10_R16

!> Tau mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_energy_equivalent_R8 =                                       &
  real(tau_mass_energy_equivalent_R16, kind=R8)

!> Tau mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_energy_equivalent_R4 =                                       &
  real(tau_mass_energy_equivalent_R16, kind=R4)

!> Tau energy equivalent, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_mass_energy_equivalent_in_MeV_R16 =                               &
  1776.86_R16

!> Tau energy equivalent, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_mass_energy_equivalent_in_MeV_R8 =                                &
  real(tau_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Tau energy equivalent, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_mass_energy_equivalent_in_MeV_R4 =                                &
  real(tau_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Tau-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_electron_mass_ratio_R16 =                                         &
  3477.23_R16

!> Tau-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_electron_mass_ratio_R8 =                                          &
  real(tau_electron_mass_ratio_R16, kind=R8)

!> Tau-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_electron_mass_ratio_R4 =                                          &
  real(tau_electron_mass_ratio_R16, kind=R4)

!> Tau-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_muon_mass_ratio_R16 =                                             &
  16.8170_R16

!> Tau-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_muon_mass_ratio_R8 =                                              &
  real(tau_muon_mass_ratio_R16, kind=R8)

!> Tau-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_muon_mass_ratio_R4 =                                              &
  real(tau_muon_mass_ratio_R16, kind=R4)

!> Tau-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_proton_mass_ratio_R16 =                                           &
  1.89376_R16

!> Tau-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_proton_mass_ratio_R8 =                                            &
  real(tau_proton_mass_ratio_R16, kind=R8)

!> Tau-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_proton_mass_ratio_R4 =                                            &
  real(tau_proton_mass_ratio_R16, kind=R4)

!> Tau-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_neutron_mass_ratio_R16 =                                          &
  1.89115_R16

!> Tau-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_neutron_mass_ratio_R8 =                                           &
  real(tau_neutron_mass_ratio_R16, kind=R8)

!> Tau-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_neutron_mass_ratio_R4 =                                           &
  real(tau_neutron_mass_ratio_R16, kind=R4)

!> Tau molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_molar_mass_R16 =                                                  &
  1.90754e-3_R16

!> Tau molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_molar_mass_R8 =                                                   &
  real(tau_molar_mass_R16, kind=R8)

!> Tau molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_molar_mass_R4 =                                                   &
  real(tau_molar_mass_R16, kind=R4)

!> Tau Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  tau_Compton_wavelength_R16 =                                          &
  6.97771e-16_R16

!> Tau Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  tau_Compton_wavelength_R8 =                                           &
  real(tau_Compton_wavelength_R16, kind=R8)

!> Tau Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  tau_Compton_wavelength_R4 =                                           &
  real(tau_Compton_wavelength_R16, kind=R4)

!> Reduced tau Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_tau_Compton_wavelength_R16 =                                  &
  1.110538e-16_R16

!> Reduced tau Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_tau_Compton_wavelength_R8 =                                   &
  real(reduced_tau_Compton_wavelength_R16, kind=R8)

!> Reduced tau Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_tau_Compton_wavelength_R4 =                                   &
  real(reduced_tau_Compton_wavelength_R16, kind=R4)

!-----
! Proton

!> Proton mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_R16 =                                                     &
  1.67262192369e-27_R16

!> Proton mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_R8 =                                                      &
  real(proton_mass_R16, kind=R8)

!> Proton mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_R4 =                                                      &
  real(proton_mass_R16, kind=R4)

!> Proton mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_in_u_R16 =                                                &
  1.007276466621_R16

!> Proton mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_in_u_R8 =                                                 &
  real(proton_mass_in_u_R16, kind=R8)

!> Proton mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_in_u_R4 =                                                 &
  real(proton_mass_in_u_R16, kind=R4)

!> Proton mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_energy_equivalent_R16 =                                   &
  1.50327761598e-10_R16

!> Proton mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_energy_equivalent_R8 =                                    &
  real(proton_mass_energy_equivalent_R16, kind=R8)

!> Proton mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_energy_equivalent_R4 =                                    &
  real(proton_mass_energy_equivalent_R16, kind=R4)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_mass_energy_equivalent_in_MeV_R16 =                            &
  938.27208816_R16

!> Proton mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R8 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Proton mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_mass_energy_equivalent_in_MeV_R4 =                             &
  real(proton_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Proton-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_electron_mass_ratio_R16 =                                      &
  1836.15267343_R16

!> Proton-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_electron_mass_ratio_R8 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R8)

!> Proton-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_electron_mass_ratio_R4 =                                       &
  real(proton_electron_mass_ratio_R16, kind=R4)

!> Proton-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_muon_mass_ratio_R16 =                                          &
  8.88024337_R16

!> Proton-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_muon_mass_ratio_R8 =                                           &
  real(proton_muon_mass_ratio_R16, kind=R8)

!> Proton-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_muon_mass_ratio_R4 =                                           &
  real(proton_muon_mass_ratio_R16, kind=R4)

!> Proton-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_tau_mass_ratio_R16 =                                           &
  0.528051_R16

!> Proton-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_tau_mass_ratio_R8 =                                            &
  real(proton_tau_mass_ratio_R16, kind=R8)

!> Proton-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_tau_mass_ratio_R4 =                                            &
  real(proton_tau_mass_ratio_R16, kind=R4)

!> Proton-neutron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_neutron_mass_ratio_R16 =                                       &
  0.99862347812_R16

!> Proton-neutron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_neutron_mass_ratio_R8 =                                        &
  real(proton_neutron_mass_ratio_R16, kind=R8)

!> Proton-neutron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_neutron_mass_ratio_R4 =                                        &
  real(proton_neutron_mass_ratio_R16, kind=R4)

!> Proton charge to mass quotient, C kg^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_charge_to_mass_quotient_R16 =                                  &
  9.5788331560e7_R16

!> Proton charge to mass quotient, C kg^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_charge_to_mass_quotient_R8 =                                   &
  real(proton_charge_to_mass_quotient_R16, kind=R8)

!> Proton charge to mass quotient, C kg^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_charge_to_mass_quotient_R4 =                                   &
  real(proton_charge_to_mass_quotient_R16, kind=R4)

!> Proton molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_molar_mass_R16 =                                               &
  1.00727646627e-3_R16

!> Proton molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_molar_mass_R8 =                                                &
  real(proton_molar_mass_R16, kind=R8)

!> Proton molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_molar_mass_R4 =                                                &
  real(proton_molar_mass_R16, kind=R4)

!> Proton Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_Compton_wavelength_R16 =                                       &
  1.32140985539e-15_R16

!> Proton Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_Compton_wavelength_R8 =                                        &
  real(proton_Compton_wavelength_R16, kind=R8)

!> Proton Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_Compton_wavelength_R4 =                                        &
  real(proton_Compton_wavelength_R16, kind=R4)

!> Reduced proton Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_proton_Compton_wavelength_R16 =                               &
  2.10308910336e-16_R16

!> Reduced proton Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_proton_Compton_wavelength_R8 =                                &
  real(reduced_proton_Compton_wavelength_R16, kind=R8)

!> Reduced proton Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_proton_Compton_wavelength_R4 =                                &
  real(reduced_proton_Compton_wavelength_R16, kind=R4)

!> Proton rms charge radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_rms_charge_radius_R16 =                                        &
  8.414e-16_R16

!> Proton rms charge radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_rms_charge_radius_R8 =                                         &
  real(proton_rms_charge_radius_R16, kind=R8)

!> Proton rms charge radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_rms_charge_radius_R4 =                                         &
  real(proton_rms_charge_radius_R16, kind=R4)

!> Proton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_R16 =                                          &
  1.41060679736e-26_R16

!> Proton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_R8 =                                           &
  real(proton_magnetic_moment_R16, kind=R8)

!> Proton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_R4 =                                           &
  real(proton_magnetic_moment_R16, kind=R4)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R16 =                   &
  1.52103220230e-3_R16

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R8 =                    &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_Bohr_magneton_ratio_R4 =                    &
  real(proton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R16 =                &
  2.79284734463_R16

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R8 =                 &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_moment_to_nuclear_magneton_ratio_R4 =                 &
  real(proton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Proton g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_g_factor_R16 =                                                 &
  5.5856946893_R16

!> Proton g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_g_factor_R8 =                                                  &
  real(proton_g_factor_R16, kind=R8)

!> Proton g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_g_factor_R4 =                                                  &
  real(proton_g_factor_R16, kind=R4)

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_neutron_magnetic_moment_ratio_R16 =                            &
  -1.45989805_R16

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_neutron_magnetic_moment_ratio_R8 =                             &
  real(proton_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Proton-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_neutron_magnetic_moment_ratio_R4 =                             &
  real(proton_neutron_magnetic_moment_ratio_R16, kind=R4)

!> Shielded proton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_R16 =                                 &
  1.410570560e-26_R16

!> Shielded proton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_R8 =                                  &
  real(shielded_proton_magnetic_moment_R16, kind=R8)

!> Shielded proton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_R4 =                                  &
  real(shielded_proton_magnetic_moment_R16, kind=R4)

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16 =          &
  1.520993128e-3_R16

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R8 =           &
  real(shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R8)

!> Shielded proton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R4 =           &
  real(shielded_proton_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R4)

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16 =       &
  2.792775599_R16

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R8 =        &
  real(shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R8)

!> Shielded proton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R4 =        &
  real(shielded_proton_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R4)

!> Proton mag. shielding correction, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_magnetic_shielding_correction_R16 =                            &
  2.5689e-5_R16

!> Proton mag. shielding correction, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_magnetic_shielding_correction_R8 =                             &
  real(proton_magnetic_shielding_correction_R16, kind=R8)

!> Proton mag. shielding correction, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_magnetic_shielding_correction_R4 =                             &
  real(proton_magnetic_shielding_correction_R16, kind=R4)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_R16 =                                       &
  2.6752218744e8_R16

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R8 =                                        &
  real(proton_gyromagnetic_ratio_R16, kind=R8)

!> Proton gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_R4 =                                        &
  real(proton_gyromagnetic_ratio_R16, kind=R4)

!> Proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_gyromagnetic_ratio_in_MHz_T_R16 =                              &
  42.577478518_R16

!> Proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R8 =                               &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_gyromagnetic_ratio_in_MHz_T_R4 =                               &
  real(proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_gyromagnetic_ratio_R16 =                              &
  2.675153151e8_R16

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_R8 =                               &
  real(shielded_proton_gyromagnetic_ratio_R16, kind=R8)

!> Shielded proton gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_R4 =                               &
  real(shielded_proton_gyromagnetic_ratio_R16, kind=R4)

!> Shielded proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R16 =                     &
  42.57638474_R16

!> Shielded proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R8 =                      &
  real(shielded_proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Shielded proton gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_proton_gyromagnetic_ratio_in_MHz_T_R4 =                      &
  real(shielded_proton_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Proton relative atomic mass, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  proton_relative_atomic_mass_R16 =                                     &
  1.007276466621_R16

!> Proton relative atomic mass, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  proton_relative_atomic_mass_R8 =                                      &
  real(proton_relative_atomic_mass_R16, kind=R8)

!> Proton relative atomic mass, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  proton_relative_atomic_mass_R4 =                                      &
  real(proton_relative_atomic_mass_R16, kind=R4)

!-----
! Neutron

!> Neutron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_R16 =                                                    &
  1.67492749804e-27_R16

!> Neutron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_R8 =                                                     &
  real(neutron_mass_R16, kind=R8)

!> Neutron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_R4 =                                                     &
  real(neutron_mass_R16, kind=R4)

!> Neutron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_in_u_R16 =                                               &
  1.00866491595_R16

!> Neutron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_in_u_R8 =                                                &
  real(neutron_mass_in_u_R16, kind=R8)

!> Neutron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_in_u_R4 =                                                &
  real(neutron_mass_in_u_R16, kind=R4)

!> Neutron relative atomic mass, dimensionless
!! (at most 33 significant digits)
  real(kind=R16), parameter, public ::                                    &
  neutron_relative_atomic_mass_R16 =                                    &
  1.00866491595_R16

!> Neutron relative atomic mass, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_relative_atomic_mass_R8 =                                     &
  real(neutron_relative_atomic_mass_R16, kind=R8)

!> Neutron relative atomic mass, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_relative_atomic_mass_R4 =                                     &
  real(neutron_relative_atomic_mass_R16, kind=R4)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_energy_equivalent_in_MeV_R16 =                           &
  939.56542052_R16

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R8 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Neutron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_energy_equivalent_in_MeV_R4 =                            &
  real(neutron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Neutron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_mass_energy_equivalent_R16 =                                  &
  1.50534976287e-10_R16

!> Neutron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_mass_energy_equivalent_R8 =                                   &
  real(neutron_mass_energy_equivalent_R16, kind=R8)

!> Neutron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_mass_energy_equivalent_R4 =                                   &
  real(neutron_mass_energy_equivalent_R16, kind=R4)

!> Neutron-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_electron_mass_ratio_R16 =                                     &
  1838.68366173_R16

!> Neutron-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_electron_mass_ratio_R8 =                                      &
  real(neutron_electron_mass_ratio_R16, kind=R8)

!> Neutron-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_electron_mass_ratio_R4 =                                      &
  real(neutron_electron_mass_ratio_R16, kind=R4)

!> Neutron-muon mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_muon_mass_ratio_R16 =                                         &
  8.89248406_R16

!> Neutron-muon mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_muon_mass_ratio_R8 =                                          &
  real(neutron_muon_mass_ratio_R16, kind=R8)

!> Neutron-muon mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_muon_mass_ratio_R4 =                                          &
  real(neutron_muon_mass_ratio_R16, kind=R4)

!> Neutron-tau mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_tau_mass_ratio_R16 =                                          &
  0.528779_R16

!> Neutron-tau mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_tau_mass_ratio_R8 =                                           &
  real(neutron_tau_mass_ratio_R16, kind=R8)

!> Neutron-tau mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_tau_mass_ratio_R4 =                                           &
  real(neutron_tau_mass_ratio_R16, kind=R4)

!> Neutron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_ratio_R16 =                                       &
  1.00137841931_R16

!> Neutron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_ratio_R8 =                                        &
  real(neutron_proton_mass_ratio_R16, kind=R8)

!> Neutron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_ratio_R4 =                                        &
  real(neutron_proton_mass_ratio_R16, kind=R4)

!> Neutron-proton mass difference, kg
!! (at most 33 significant digits)
  real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_R16 =                                  &
  2.30557435e-30_R16

!> Neutron-proton mass difference, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_R8 =                                   &
  real(neutron_proton_mass_difference_R16, kind=R8)

!> Neutron-proton mass difference, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_R4 =                                   &
  real(neutron_proton_mass_difference_R16, kind=R4)

!> Neutron-proton mass difference in u, u
!! (at most 33 significant digits)
  real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_in_u_R16 =                             &
  1.38844933e-3_R16

!> Neutron-proton mass difference in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_in_u_R8 =                              &
  real(neutron_proton_mass_difference_in_u_R16, kind=R8)

!> Neutron-proton mass difference in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_in_u_R4 =                              &
  real(neutron_proton_mass_difference_in_u_R16, kind=R4)

!> Neutron-proton mass difference energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_energy_equivalent_R16 =                &
  2.07214689e-13_R16

!> Neutron-proton mass difference energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_R8 =                 &
  real(neutron_proton_mass_difference_energy_equivalent_R16, kind=R8)

!> Neutron-proton mass difference energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_R4 =                 &
  real(neutron_proton_mass_difference_energy_equivalent_R16, kind=R4)

!> Neutron-proton mass difference energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_mass_difference_energy_equivalent_in_MeV_R16 =         &
  1.29333236_R16

!> Neutron-proton mass difference energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_in_MeV_R8 =          &
  real(neutron_proton_mass_difference_energy_equivalent_in_MeV_R16, kind=R8)

!> Neutron-proton mass difference energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_mass_difference_energy_equivalent_in_MeV_R4 =          &
  real(neutron_proton_mass_difference_energy_equivalent_in_MeV_R16, kind=R4)

!> Neutron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_molar_mass_R16 =                                              &
  1.00866491560e-3_R16

!> Neutron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_molar_mass_R8 =                                               &
  real(neutron_molar_mass_R16, kind=R8)

!> Neutron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_molar_mass_R4 =                                               &
  real(neutron_molar_mass_R16, kind=R4)

!> Neutron Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_Compton_wavelength_R16 =                                      &
  1.31959090581e-15_R16

!> Neutron Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_Compton_wavelength_R8 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R8)

!> Neutron Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_Compton_wavelength_R4 =                                       &
  real(neutron_Compton_wavelength_R16, kind=R4)

!> Reduced neutron Compton wavelength, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  reduced_neutron_Compton_wavelength_R16 =                              &
  2.1001941552e-16_R16

!> Reduced neutron Compton wavelength, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  reduced_neutron_Compton_wavelength_R8 =                               &
  real(reduced_neutron_Compton_wavelength_R16, kind=R8)

!> Reduced neutron Compton wavelength, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  reduced_neutron_Compton_wavelength_R4 =                               &
  real(reduced_neutron_Compton_wavelength_R16, kind=R4)

!> Neutron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_R16 =                                         &
  -9.6623651e-27_R16

!> Neutron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_R8 =                                          &
  real(neutron_magnetic_moment_R16, kind=R8)

!> Neutron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_R4 =                                          &
  real(neutron_magnetic_moment_R16, kind=R4)

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                  &
  -1.04187563e-3_R16

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                   &
  real(neutron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Neutron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                   &
  real(neutron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R16 =               &
  -1.91304273_R16

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R8 =                &
  real(neutron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Neutron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_magnetic_moment_to_nuclear_magneton_ratio_R4 =                &
  real(neutron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Neutron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_g_factor_R16 =                                                &
  -3.82608545_R16

!> Neutron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_g_factor_R8 =                                                 &
  real(neutron_g_factor_R16, kind=R8)

!> Neutron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_g_factor_R4 =                                                 &
  real(neutron_g_factor_R16, kind=R4)

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_electron_magnetic_moment_ratio_R16 =                          &
  1.04066882e-3_R16

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_electron_magnetic_moment_ratio_R8 =                           &
  real(neutron_electron_magnetic_moment_ratio_R16, kind=R8)

!> Neutron-electron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_electron_magnetic_moment_ratio_R4 =                           &
  real(neutron_electron_magnetic_moment_ratio_R16, kind=R4)

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_proton_magnetic_moment_ratio_R16 =                            &
  -0.68497934_R16

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_proton_magnetic_moment_ratio_R8 =                             &
  real(neutron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Neutron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_proton_magnetic_moment_ratio_R4 =                             &
  real(neutron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_to_shielded_proton_magnetic_moment_ratio_R16 =                &
  -0.68499694_R16

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_to_shielded_proton_magnetic_moment_ratio_R8 =                 &
  real(neutron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R8)

!> Neutron to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_to_shielded_proton_magnetic_moment_ratio_R4 =                 &
  real(neutron_to_shielded_proton_magnetic_moment_ratio_R16, kind=R4)

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_gyromagnetic_ratio_R16 =                                      &
  1.83247171e8_R16

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_R8 =                                       &
  real(neutron_gyromagnetic_ratio_R16, kind=R8)

!> Neutron gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_R4 =                                       &
  real(neutron_gyromagnetic_ratio_R16, kind=R4)

!> Neutron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  neutron_gyromagnetic_ratio_in_MHz_T_R16 =                             &
  29.1646931_R16

!> Neutron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_in_MHz_T_R8 =                              &
  real(neutron_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Neutron gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  neutron_gyromagnetic_ratio_in_MHz_T_R4 =                              &
  real(neutron_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!-----
! Deuteron

!> Deuteron mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_R16 =                                                   &
  3.3435837724e-27_R16

!> Deuteron mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_R8 =                                                    &
  real(deuteron_mass_R16, kind=R8)

!> Deuteron mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_R4 =                                                    &
  real(deuteron_mass_R16, kind=R4)

!> Deuteron mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_in_u_R16 =                                              &
  2.013553212745_R16

!> Deuteron mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_in_u_R8 =                                               &
  real(deuteron_mass_in_u_R16, kind=R8)

!> Deuteron mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_in_u_R4 =                                               &
  real(deuteron_mass_in_u_R16, kind=R4)

!> Deuteron relative atomic mass, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_relative_atomic_mass_R16 =                                   &
  2.013553212745_R16

!> Deuteron relative atomic mass, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_relative_atomic_mass_R8 =                                    &
  real(deuteron_relative_atomic_mass_R16, kind=R8)

!> Deuteron relative atomic mass, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_relative_atomic_mass_R4 =                                    &
  real(deuteron_relative_atomic_mass_R16, kind=R4)

!> Deuteron mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_energy_equivalent_R16 =                                 &
  3.00506323102e-10_R16

!> Deuteron mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_R8 =                                  &
  real(deuteron_mass_energy_equivalent_R16, kind=R8)

!> Deuteron mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_R4 =                                  &
  real(deuteron_mass_energy_equivalent_R16, kind=R4)

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_mass_energy_equivalent_in_MeV_R16 =                          &
  1875.61294257_R16

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_in_MeV_R8 =                           &
  real(deuteron_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Deuteron mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_mass_energy_equivalent_in_MeV_R4 =                           &
  real(deuteron_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Deuteron-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_electron_mass_ratio_R16 =                                    &
  3670.48296788_R16

!> Deuteron-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_electron_mass_ratio_R8 =                                     &
  real(deuteron_electron_mass_ratio_R16, kind=R8)

!> Deuteron-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_electron_mass_ratio_R4 =                                     &
  real(deuteron_electron_mass_ratio_R16, kind=R4)

!> Deuteron-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_proton_mass_ratio_R16 =                                      &
  1.99900750139_R16

!> Deuteron-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_proton_mass_ratio_R8 =                                       &
  real(deuteron_proton_mass_ratio_R16, kind=R8)

!> Deuteron-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_proton_mass_ratio_R4 =                                       &
  real(deuteron_proton_mass_ratio_R16, kind=R4)

!> Deuteron molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_molar_mass_R16 =                                             &
  2.01355321205e-3_R16

!> Deuteron molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_molar_mass_R8 =                                              &
  real(deuteron_molar_mass_R16, kind=R8)

!> Deuteron molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_molar_mass_R4 =                                              &
  real(deuteron_molar_mass_R16, kind=R4)

!> Deuteron rms charge radius, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_rms_charge_radius_R16 =                                      &
  2.12799e-15_R16

!> Deuteron rms charge radius, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_rms_charge_radius_R8 =                                       &
  real(deuteron_rms_charge_radius_R16, kind=R8)

!> Deuteron rms charge radius, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_rms_charge_radius_R4 =                                       &
  real(deuteron_rms_charge_radius_R16, kind=R4)

!> Deuteron mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_R16 =                                              &
  4.330735094e-27_R16

!> Deuteron mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_R8 =                                               &
  real(deuteron_magnetic_moment_R16, kind=R8)

!> Deuteron mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_R4 =                                               &
  real(deuteron_magnetic_moment_R16, kind=R4)

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16 =                       &
  4.669754570e-4_R16

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R8 =                        &
  real(deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Deuteron mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_to_Bohr_magneton_ratio_R4 =                        &
  real(deuteron_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16 =                    &
  0.8574382338_R16

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R8 =                     &
  real(deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Deuteron mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_magnetic_moment_to_nuclear_magneton_ratio_R4 =                     &
  real(deuteron_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Deuteron g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_g_factor_R16 =                                               &
  0.8574382338_R16

!> Deuteron g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_g_factor_R8 =                                                &
  real(deuteron_g_factor_R16, kind=R8)

!> Deuteron g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_g_factor_R4 =                                                &
  real(deuteron_g_factor_R16, kind=R4)

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_electron_magnetic_moment_ratio_R16 =                         &
  -4.664345551e-4_R16

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_electron_magnetic_moment_ratio_R8 =                          &
  real(deuteron_electron_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-electron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_electron_magnetic_moment_ratio_R4 =                          &
  real(deuteron_electron_magnetic_moment_ratio_R16, kind=R4)

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_proton_magnetic_moment_ratio_R16 =                           &
  0.30701220939_R16

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_proton_magnetic_moment_ratio_R8 =                            &
  real(deuteron_proton_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_proton_magnetic_moment_ratio_R4 =                            &
  real(deuteron_proton_magnetic_moment_ratio_R16, kind=R4)

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  deuteron_neutron_magnetic_moment_ratio_R16 =                          &
  -0.44820653_R16

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  deuteron_neutron_magnetic_moment_ratio_R8 =                           &
  real(deuteron_neutron_magnetic_moment_ratio_R16, kind=R8)

!> Deuteron-neutron mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  deuteron_neutron_magnetic_moment_ratio_R4 =                           &
  real(deuteron_neutron_magnetic_moment_ratio_R16, kind=R4)

!-----
! Triton

!> Triton mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_R16 =                                                     &
  5.0073567446e-27_R16

!> Triton mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_R8 =                                                      &
  real(triton_mass_R16, kind=R8)

!> Triton mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_R4 =                                                      &
  real(triton_mass_R16, kind=R4)

!> Triton mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_in_u_R16 =                                                &
  3.01550071621_R16

!> Triton mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_in_u_R8 =                                                 &
  real(triton_mass_in_u_R16, kind=R8)

!> Triton mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_in_u_R4 =                                                 &
  real(triton_mass_in_u_R16, kind=R4)

!> Triton relative atomic mass, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_relative_atomic_mass_R16 =                                     &
  3.01550071621_R16

!> Triton relative atomic mass, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_relative_atomic_mass_R8 =                                      &
  real(triton_relative_atomic_mass_R16, kind=R8)

!> Triton relative atomic mass, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_relative_atomic_mass_R4 =                                      &
  real(triton_relative_atomic_mass_R16, kind=R4)

!> Triton mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_energy_equivalent_R16 =                                   &
  4.5003878060e-10_R16

!> Triton mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_energy_equivalent_R8 =                                    &
  real(triton_mass_energy_equivalent_R16, kind=R8)

!> Triton mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_energy_equivalent_R4 =                                    &
  real(triton_mass_energy_equivalent_R16, kind=R4)

!> Triton mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_mass_energy_equivalent_in_MeV_R16 =                            &
  2808.92113298_R16

!> Triton mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_mass_energy_equivalent_in_MeV_R8 =                             &
  real(triton_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Triton mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_mass_energy_equivalent_in_MeV_R4 =                             &
  real(triton_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Triton-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_electron_mass_ratio_R16 =                                      &
  5496.92153573_R16

!> Triton-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_electron_mass_ratio_R8 =                                       &
  real(triton_electron_mass_ratio_R16, kind=R8)

!> Triton-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_electron_mass_ratio_R4 =                                       &
  real(triton_electron_mass_ratio_R16, kind=R4)

!> Triton-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_proton_mass_ratio_R16 =                                        &
  2.99371703414_R16

!> Triton-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_proton_mass_ratio_R8 =                                         &
  real(triton_proton_mass_ratio_R16, kind=R8)

!> Triton-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_proton_mass_ratio_R4 =                                         &
  real(triton_proton_mass_ratio_R16, kind=R4)

!> Triton molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_molar_mass_R16 =                                               &
  3.01550071517e-3_R16

!> Triton molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_molar_mass_R8 =                                                &
  real(triton_molar_mass_R16, kind=R8)

!> Triton molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_molar_mass_R4 =                                                &
  real(triton_molar_mass_R16, kind=R4)

!> Triton mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_magnetic_moment_R16 =                                          &
  1.5046095202e-26_R16

!> Triton mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_magnetic_moment_R8 =                                           &
  real(triton_magnetic_moment_R16, kind=R8)

!> Triton mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_magnetic_moment_R4 =                                           &
  real(triton_magnetic_moment_R16, kind=R4)

!> Triton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_magnetic_moment_to_Bohr_magneton_ratio_R16 =                   &
  1.6223936651e-3_R16

!> Triton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_magnetic_moment_to_Bohr_magneton_ratio_R8 =                    &
  real(triton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Triton mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_magnetic_moment_to_Bohr_magneton_ratio_R4 =                    &
  real(triton_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Triton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_magnetic_moment_to_nuclear_magneton_ratio_R16 =                &
  2.9789624656_R16

!> Triton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_magnetic_moment_to_nuclear_magneton_ratio_R8 =                 &
  real(triton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Triton mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_magnetic_moment_to_nuclear_magneton_ratio_R4 =                 &
  real(triton_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Triton g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_g_factor_R16 =                                                 &
  5.957924931_R16

!> Triton g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_g_factor_R8 =                                                  &
  real(triton_g_factor_R16, kind=R8)

!> Triton g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_g_factor_R4 =                                                  &
  real(triton_g_factor_R16, kind=R4)

!> Triton to proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  triton_to_proton_magnetic_moment_ratio_R16 =                          &
  1.0666399191_R16

!> Triton to proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  triton_to_proton_magnetic_moment_ratio_R8 =                           &
  real(triton_to_proton_magnetic_moment_ratio_R16, kind=R8)

!> Triton to proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  triton_to_proton_magnetic_moment_ratio_R4 =                           &
  real(triton_to_proton_magnetic_moment_ratio_R16, kind=R4)

!-----
! Helion

!> Helion mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_R16 =                                                     &
  5.0064127796e-27_R16

!> Helion mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_R8 =                                                      &
  real(helion_mass_R16, kind=R8)

!> Helion mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_R4 =                                                      &
  real(helion_mass_R16, kind=R4)

!> Helion mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_in_u_R16 =                                                &
  3.014932247175_R16

!> Helion mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_in_u_R8 =                                                 &
  real(helion_mass_in_u_R16, kind=R8)

!> Helion mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_in_u_R4 =                                                 &
  real(helion_mass_in_u_R16, kind=R4)

!> Helion relative atomic mass, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_relative_atomic_mass_R16 =                                     &
  3.014932247175_R16

!> Helion relative atomic mass, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_relative_atomic_mass_R8 =                                      &
  real(helion_relative_atomic_mass_R16, kind=R8)

!> Helion relative atomic mass, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_relative_atomic_mass_R4 =                                      &
  real(helion_relative_atomic_mass_R16, kind=R4)

!> Helion mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_energy_equivalent_R16 =                                   &
  4.4995394125e-10_R16

!> Helion mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_energy_equivalent_R8 =                                    &
  real(helion_mass_energy_equivalent_R16, kind=R8)

!> Helion mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_energy_equivalent_R4 =                                    &
  real(helion_mass_energy_equivalent_R16, kind=R4)

!> Helion mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_mass_energy_equivalent_in_MeV_R16 =                            &
  2808.39160743_R16

!> Helion mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_mass_energy_equivalent_in_MeV_R8 =                             &
  real(helion_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Helion mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_mass_energy_equivalent_in_MeV_R4 =                             &
  real(helion_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Helion-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_electron_mass_ratio_R16 =                                      &
  5495.88528007_R16

!> Helion-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_electron_mass_ratio_R8 =                                       &
  real(helion_electron_mass_ratio_R16, kind=R8)

!> Helion-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_electron_mass_ratio_R4 =                                       &
  real(helion_electron_mass_ratio_R16, kind=R4)

!> Helion-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_proton_mass_ratio_R16 =                                        &
  2.99315267167_R16

!> Helion-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_proton_mass_ratio_R8 =                                         &
  real(helion_proton_mass_ratio_R16, kind=R8)

!> Helion-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_proton_mass_ratio_R4 =                                         &
  real(helion_proton_mass_ratio_R16, kind=R4)

!> Helion molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_molar_mass_R16 =                                               &
  3.01493224613e-3_R16

!> Helion molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_molar_mass_R8 =                                                &
  real(helion_molar_mass_R16, kind=R8)

!> Helion molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_molar_mass_R4 =                                                &
  real(helion_molar_mass_R16, kind=R4)

!> Shielded helion mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_R16 =                                 &
  -1.074553090e-26_R16

!> Shielded helion mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_R8 =                                  &
  real(shielded_helion_magnetic_moment_R16, kind=R8)

!> Shielded helion mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_R4 =                                  &
  real(shielded_helion_magnetic_moment_R16, kind=R4)

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16 =          &
  -1.158671471e-3_R16

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R8 =           &
  real(shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R8)

!> Shielded helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R4 =           &
  real(shielded_helion_magnetic_moment_to_Bohr_magneton_ratio_R16,      &
  kind=R4)

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16 =       &
  -2.127497719_R16

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R8 =        &
  real(shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R8)

!> Shielded helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R4 =        &
  real(shielded_helion_magnetic_moment_to_nuclear_magneton_ratio_R16,   &
  kind=R4)

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_to_proton_magnetic_moment_ratio_R16 =                 &
  -0.7617665618_R16

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_to_proton_magnetic_moment_ratio_R8 =                  &
  real(shielded_helion_to_proton_magnetic_moment_ratio_R16, kind=R8)

!> Shielded helion to proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_to_proton_magnetic_moment_ratio_R4 =                  &
  real(shielded_helion_to_proton_magnetic_moment_ratio_R16, kind=R4)

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16 =        &
  -0.7617861313_R16

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R8 =         &
  real(shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16,    &
  kind=R8)

!> Shielded helion to shielded proton mag. mom. ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_to_shielded_proton_magnetic_moment_ratio_R4 =         &
  real(shielded_helion_to_shielded_proton_magnetic_moment_ratio_R16,    &
  kind=R4)

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_gyromagnetic_ratio_R16 =                              &
  2.037894569e8_R16

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_R8 =                               &
  real(shielded_helion_gyromagnetic_ratio_R16, kind=R8)

!> Shielded helion gyromag. ratio, s^-1 T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_R4 =                               &
  real(shielded_helion_gyromagnetic_ratio_R16, kind=R4)

!> Shielded helion gyromag. ratio in MHz/T, MHz T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R16 =                     &
  32.43409942_R16

!> Shielded helion gyromag. ratio in MHz/T, MHz T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R8 =                      &
  real(shielded_helion_gyromagnetic_ratio_in_MHz_T_R16, kind=R8)

!> Shielded helion gyromag. ratio in MHz/T, MHz T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielded_helion_gyromagnetic_ratio_in_MHz_T_R4 =                      &
  real(shielded_helion_gyromagnetic_ratio_in_MHz_T_R16, kind=R4)

!> Helion mag. mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_magnetic_moment_R16 =                                          &
  -1.074617532e-26_R16

!> Helion mag. mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_magnetic_moment_R8 =                                           &
  real(helion_magnetic_moment_R16, kind=R8)

!> Helion mag. mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_magnetic_moment_R4 =                                           &
  real(helion_magnetic_moment_R16, kind=R4)

!> Helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_magnetic_moment_to_Bohr_magneton_ratio_R16 =                   &
  -1.158740958e-3_R16

!> Helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_magnetic_moment_to_Bohr_magneton_ratio_R8 =                    &
  real(helion_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R8)

!> Helion mag. mom. to Bohr magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_magnetic_moment_to_Bohr_magneton_ratio_R4 =                    &
  real(helion_magnetic_moment_to_Bohr_magneton_ratio_R16, kind=R4)

!> Helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_magnetic_moment_to_nuclear_magneton_ratio_R16 =                &
  -2.127625307_R16

!> Helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_magnetic_moment_to_nuclear_magneton_ratio_R8 =                 &
  real(helion_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R8)

!> Helion mag. mom. to nuclear magneton ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_magnetic_moment_to_nuclear_magneton_ratio_R4 =                 &
  real(helion_magnetic_moment_to_nuclear_magneton_ratio_R16, kind=R4)

!> Helion g factor, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_g_factor_R16 =                                                 &
  -4.255250615_R16

!> Helion g factor, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_g_factor_R8 =                                                  &
  real(helion_g_factor_R16, kind=R8)

!> Helion g factor, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_g_factor_R4 =                                                  &
  real(helion_g_factor_R16, kind=R4)

!> Helion shielding shift, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  helion_shielding_shift_R16 =                                          &
  5.996743e-5_R16

!> Helion shielding shift, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  helion_shielding_shift_R8 =                                           &
  real(helion_shielding_shift_R16, kind=R8)

!> Helion shielding shift, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  helion_shielding_shift_R4 =                                           &
  real(helion_shielding_shift_R16, kind=R4)

!-----
! Alpha particle

!> Alpha particle mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_R16 =                                             &
  6.6446573357e-27_R16

!> Alpha particle mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_R8 =                                              &
  real(alpha_particle_mass_R16, kind=R8)

!> Alpha particle mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_R4 =                                              &
  real(alpha_particle_mass_R16, kind=R4)

!> Alpha particle mass in u, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_in_u_R16 =                                        &
  4.001506179127_R16

!> Alpha particle mass in u, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_in_u_R8 =                                         &
  real(alpha_particle_mass_in_u_R16, kind=R8)

!> Alpha particle mass in u, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_in_u_R4 =                                         &
  real(alpha_particle_mass_in_u_R16, kind=R4)

!> Alpha particle relative atomic mass, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_relative_atomic_mass_R16 =                             &
  4.001506179127_R16

!> Alpha particle relative atomic mass, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_relative_atomic_mass_R8 =                              &
  real(alpha_particle_relative_atomic_mass_R16, kind=R8)

!> Alpha particle relative atomic mass, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_relative_atomic_mass_R4 =                              &
  real(alpha_particle_relative_atomic_mass_R16, kind=R4)

!> Alpha particle mass energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_energy_equivalent_R16 =                           &
  5.9719201914e-10_R16

!> Alpha particle mass energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_R8 =                            &
  real(alpha_particle_mass_energy_equivalent_R16, kind=R8)

!> Alpha particle mass energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_R4 =                            &
  real(alpha_particle_mass_energy_equivalent_R16, kind=R4)

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_mass_energy_equivalent_in_MeV_R16 =                    &
  3727.3794066_R16

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_in_MeV_R8 =                     &
  real(alpha_particle_mass_energy_equivalent_in_MeV_R16, kind=R8)

!> Alpha particle mass energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_mass_energy_equivalent_in_MeV_R4 =                     &
  real(alpha_particle_mass_energy_equivalent_in_MeV_R16, kind=R4)

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_electron_mass_ratio_R16 =                              &
  7294.29954142_R16

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_electron_mass_ratio_R8 =                               &
  real(alpha_particle_electron_mass_ratio_R16, kind=R8)

!> Alpha particle-electron mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_electron_mass_ratio_R4 =                               &
  real(alpha_particle_electron_mass_ratio_R16, kind=R4)

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_proton_mass_ratio_R16 =                                &
  3.97259969009_R16

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_proton_mass_ratio_R8 =                                 &
  real(alpha_particle_proton_mass_ratio_R16, kind=R8)

!> Alpha particle-proton mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_proton_mass_ratio_R4 =                                 &
  real(alpha_particle_proton_mass_ratio_R16, kind=R4)

!> Alpha particle molar mass, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  alpha_particle_molar_mass_R16 =                                       &
  4.0015061777e-3_R16

!> Alpha particle molar mass, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  alpha_particle_molar_mass_R8 =                                        &
  real(alpha_particle_molar_mass_R16, kind=R8)

!> Alpha particle molar mass, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  alpha_particle_molar_mass_R4 =                                        &
  real(alpha_particle_molar_mass_R16, kind=R4)

!-----
! Physico-Chemical Constants

!> Avogadro constant, mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Avogadro_constant_R16 =                                               &
  6.02214076e23_R16

!> Avogadro constant, mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Avogadro_constant_R8 =                                                &
  real(Avogadro_constant_R16, kind=R8)

!> Avogadro constant, mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Avogadro_constant_R4 =                                                &
  real(Avogadro_constant_R16, kind=R4)

!> Atomic mass constant, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_R16 =                                            &
  1.66053906660e-27_R16

!> Atomic mass constant, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_R8 =                                             &
  real(atomic_mass_constant_R16, kind=R8)

!> Atomic mass constant, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_R4 =                                             &
  real(atomic_mass_constant_R16, kind=R4)

!> Atomic mass constant energy equivalent, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_energy_equivalent_R16 =                          &
  1.49241808560e-10_R16

!> Atomic mass constant energy equivalent, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_R8 =                           &
  real(atomic_mass_constant_energy_equivalent_R16, kind=R8)

!> Atomic mass constant energy equivalent, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_R4 =                           &
  real(atomic_mass_constant_energy_equivalent_R16, kind=R4)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_constant_energy_equivalent_in_MeV_R16 =                   &
  931.49410242_R16

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R8 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R8)

!> Atomic mass constant energy equivalent in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_constant_energy_equivalent_in_MeV_R4 =                    &
  real(atomic_mass_constant_energy_equivalent_in_MeV_R16, kind=R4)

!> Faraday constant, C mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Faraday_constant_R16 =                                                &
  Avogadro_constant_R16 * elementary_charge_R16
  ! 96 485.332 12..._R16 ! derived

!> Faraday constant, C mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Faraday_constant_R8 =                                                 &
  real(Faraday_constant_R16, kind=R8) ! derived

!> Faraday constant, C mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Faraday_constant_R4 =                                                 &
  real(Faraday_constant_R16, kind=R4) ! derived

!> Molar Planck constant, J Hz^-1 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_Planck_constant_R16 =                                           &
  Avogadro_constant_R16 * Planck_constant_R16
  ! 3.990 312 712... e-10_R16 ! derived

!> Molar Planck constant, J Hz^-1 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_Planck_constant_R8 =                                            &
  real(molar_Planck_constant_R16, kind=R8) ! derived

!> Molar Planck constant, J Hz^-1 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_Planck_constant_R4 =                                            &
  real(molar_Planck_constant_R16, kind=R4) ! derived

!> Molar gas constant, J mol^-1 K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_gas_constant_R16 =                                              &
  Avogadro_constant_R16 * Boltzmann_constant_R16
  ! 8.314 462 618..._R16 ! derived

!> Molar gas constant, J mol^-1 K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_gas_constant_R8 =                                               &
  real(molar_gas_constant_R16, kind=R8) ! derived

!> Molar gas constant, J mol^-1 K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_gas_constant_R4 =                                               &
  real(molar_gas_constant_R16, kind=R4) ! derived

! !> Boltzmann constant, J K^-1
! !! (at most 33 significant digits)
! real(kind=R16), parameter, public ::                                    &
!   Boltzmann_constant_R16 =                                              &
!   1.380649e-23_R16

!> Boltzmann constant, J K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_R8 =                                               &
  real(Boltzmann_constant_R16, kind=R8)

!> Boltzmann constant, J K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_R4 =                                               &
  real(Boltzmann_constant_R16, kind=R4)

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_eV_K_R16 =                                      &
  Boltzmann_constant_R16 / elementary_charge_R16
!   8.617 333 262... e-5_R16 ! derived

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_eV_K_R8 =                                       &
  real(Boltzmann_constant_in_eV_K_R16, kind=R8) ! derived

!> Boltzmann constant in eV/K, eV K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_eV_K_R4 =                                       &
  real(Boltzmann_constant_in_eV_K_R16, kind=R4) ! derived

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_Hz_K_R16 =                                      &
  Boltzmann_constant_R16 / Planck_constant_R16
!   2.083 661 912... e10_R16 ! derived

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_Hz_K_R8 =                                       &
  real(Boltzmann_constant_in_Hz_K_R16, kind=R8) ! derived

!> Boltzmann constant in Hz/K, Hz K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_Hz_K_R4 =                                       &
  real(Boltzmann_constant_in_Hz_K_R16, kind=R4) ! derived

!> Boltzmann constant in inverse meter per kelvin, m^-1 K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R16 =                  &
  Boltzmann_constant_R16                                                &
  / (Planck_constant_R16 * speed_of_light_in_vacuum_R16)
!   69.503 480 04..._R16 ! derived

!> Boltzmann constant in inverse meter per kelvin, m^-1 K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R8 =                   &
  real(Boltzmann_constant_in_inverse_meter_per_kelvin_R16, kind=R8) ! derived

!> Boltzmann constant in inverse meter per kelvin, m^-1 K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Boltzmann_constant_in_inverse_meter_per_kelvin_R4 =                   &
  real(Boltzmann_constant_in_inverse_meter_per_kelvin_R16, kind=R4) ! derived

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16 =                  &
  molar_gas_constant_R16 * standard_temperature_R16                     &
  / standard_atmosphere_R16
!   22.413 969 54... e-3_R16 ! derived

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R8 =                   &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R8) ! derived

!> Molar volume of ideal gas (273.15 K, 101.325 kPa), m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R4 =                   &
  real(molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16, kind=R4) ! derived

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Loschmidt_constant_273_15_K_101_325_kPa_R16 =                         &
  standard_atmosphere_R16 * Avogadro_constant_R16                       &
  / (molar_gas_constant_R16 * standard_temperature_R16)
!   2.686 780 111... e25_R16 ! derived

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_101_325_kPa_R8 =                          &
  real(Loschmidt_constant_273_15_K_101_325_kPa_R16, kind=R8) ! derived

!> Loschmidt constant (273.15 K, 101.325 kPa), m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_101_325_kPa_R4 =                          &
  real(Loschmidt_constant_273_15_K_101_325_kPa_R16, kind=R4) ! derived

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R16 =                      &
  molar_gas_constant_R16 * standard_temperature_R16                     &
  / standard_state_pressure_R16
!   22.710 954 64... e-3_R16 ! derived

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R8 =                       &
  real(molar_volume_of_ideal_gas_273_15_K_100_kPa_R16, kind=R8) ! derived

!> Molar volume of ideal gas (273.15 K, 100 kPa), m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_ideal_gas_273_15_K_100_kPa_R4 =                       &
  real(molar_volume_of_ideal_gas_273_15_K_100_kPa_R16, kind=R4) ! derived

!> Loschmidt constant (273.15 K, 100 kPa), m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Loschmidt_constant_273_15_K_100_kPa_R16 =                             &
  standard_state_pressure_R16 * Avogadro_constant_R16                   &
  / (molar_gas_constant_R16 * standard_temperature_R16)
!   2.651 645 804... e25_R16 ! derived

!> Loschmidt constant (273.15 K, 100 kPa), m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_100_kPa_R8 =                              &
  real(Loschmidt_constant_273_15_K_100_kPa_R16, kind=R8) ! derived

!> Loschmidt constant (273.15 K, 100 kPa), m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Loschmidt_constant_273_15_K_100_kPa_R4 =                              &
  real(Loschmidt_constant_273_15_K_100_kPa_R16, kind=R4) ! derived

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Sackur_Tetrode_constant_1_K_100_kPa_R16 =                             &
  -1.15170753706_R16

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_100_kPa_R8 =                              &
  real(Sackur_Tetrode_constant_1_K_100_kPa_R16, kind=R8)

!> Sackur-Tetrode constant (1 K, 100 kPa), dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_100_kPa_R4 =                              &
  real(Sackur_Tetrode_constant_1_K_100_kPa_R16, kind=R4)

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R16 =                         &
  -1.16487052358_R16

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R8 =                          &
  real(Sackur_Tetrode_constant_1_K_101_325_kPa_R16, kind=R8)

!> Sackur-Tetrode constant (1 K, 101.325 kPa), dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Sackur_Tetrode_constant_1_K_101_325_kPa_R4 =                          &
  real(Sackur_Tetrode_constant_1_K_101_325_kPa_R16, kind=R4)

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Stefan_Boltzmann_constant_R16 =                                       &
  pi_R16 * pi_R16                                                       &
  * Boltzmann_constant_R16 * Boltzmann_constant_R16                     &
  * Boltzmann_constant_R16 * Boltzmann_constant_R16                     &
  / (60_R16                                                             &
  * reduced_Planck_constant_R16 * reduced_Planck_constant_R16           &
  * reduced_Planck_constant_R16                                         &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   5.670 374 419... e-8_R16 ! derived

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R8 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R8) ! derived

!> Stefan-Boltzmann constant, W m^-2 K^-4
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Stefan_Boltzmann_constant_R4 =                                        &
  real(Stefan_Boltzmann_constant_R16, kind=R4) ! derived

!> First radiation constant, W m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  first_radiation_constant_R16 =                                        &
  2.0_R16 * pi_R16 * Planck_constant_R16                                &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16
!   3.741 771 852... e-16_R16 ! derived

!> First radiation constant, W m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  first_radiation_constant_R8 =                                         &
  real(first_radiation_constant_R16, kind=R8) ! derived

!> First radiation constant, W m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  first_radiation_constant_R4 =                                         &
  real(first_radiation_constant_R16, kind=R4) ! derived

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  first_radiation_constant_for_spectral_radiance_R16 =                  &
  2.0_R16 * Planck_constant_R16                                         &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16
  ! 1.191 042 972... e-16_R16 ! derived

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  first_radiation_constant_for_spectral_radiance_R8 =                   &
  real(first_radiation_constant_for_spectral_radiance_R16, kind=R8) ! derived

!> First radiation constant for spectral radiance, W m^2 sr^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  first_radiation_constant_for_spectral_radiance_R4 =                   &
  real(first_radiation_constant_for_spectral_radiance_R16, kind=R4) ! derived

!> Second radiation constant, m K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  second_radiation_constant_R16 =                                       &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / Boltzmann_constant_R16
  ! 1.438 776 877... e-2_R16 ! derived

!> Second radiation constant, m K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  second_radiation_constant_R8 =                                        &
  real(second_radiation_constant_R16, kind=R8) ! derived

!> Second radiation constant, m K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  second_radiation_constant_R4 =                                        &
  real(second_radiation_constant_R16, kind=R4) ! derived

!> Wien wavelength displacement law constant, m K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Wien_wavelength_displacement_law_constant_R16 =                       &
  second_radiation_constant_R16 / wlroot_R16
! !   2.897 771 955... e-3_R16 ! derived

!> Wien wavelength displacement law constant, m K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Wien_wavelength_displacement_law_constant_R8 =                        &
  real(Wien_wavelength_displacement_law_constant_R16, kind=R8) ! derived

!> Wien wavelength displacement law constant, m K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Wien_wavelength_displacement_law_constant_R4 =                        &
  real(Wien_wavelength_displacement_law_constant_R16, kind=R4) ! derived

!> Wien frequency displacement law constant, Hz K^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Wien_frequency_displacement_law_constant_R16 =                        &
  froot_R16 * speed_of_light_in_vacuum_R16                              &
  / second_radiation_constant_R16
! !   5.878 925 757... e10_R16 ! derived

!> Wien frequency displacement law constant, Hz K^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Wien_frequency_displacement_law_constant_R8 =                         &
  real(Wien_frequency_displacement_law_constant_R16, kind=R8) ! derived

!> Wien frequency displacement law constant, Hz K^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Wien_frequency_displacement_law_constant_R4 =                         &
  real(Wien_frequency_displacement_law_constant_R16, kind=R4) ! derived

!> W to Z mass ratio, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  W_to_Z_mass_ratio_R16 =                                               &
  0.88153_R16

!> W to Z mass ratio, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  W_to_Z_mass_ratio_R8 =                                                &
  real(W_to_Z_mass_ratio_R16, kind=R8)

!> W to Z mass ratio, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  W_to_Z_mass_ratio_R4 =                                                &
  real(W_to_Z_mass_ratio_R16, kind=R4)

!-----
! Non-SI units

! Migrated to top of file for dependency resolution; see above
! !> Electron volt, J
! !! (at most 33 significant digits)
! real(kind=R16), parameter, public ::                                    &
!   electron_volt_R16 =                                                   &
!   1.602176634e-19_R16

!> Electron volt, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_R8 =                                                    &
  real(electron_volt_R16, kind=R8)

!> Electron volt, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_R4 =                                                    &
  real(electron_volt_R16, kind=R4)

!> Unified atomic mass unit, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  unified_atomic_mass_unit_R16 =                                        &
  1.66053906660e-27_R16

!> Unified atomic mass unit, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  unified_atomic_mass_unit_R8 =                                         &
  real(unified_atomic_mass_unit_R16, kind=R8)

!> Unified atomic mass unit, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  unified_atomic_mass_unit_R4 =                                         &
  real(unified_atomic_mass_unit_R16, kind=R4)

!-----
! Natural units

!> Natural unit of velocity, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_velocity_R16 =                                        &
  speed_of_light_in_vacuum_R16
  ! 299792458.0_R16 ! derived

!> Natural unit of velocity, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_velocity_R8 =                                         &
  real(natural_unit_of_velocity_R16, kind=R8) ! derived

!> Natural unit of velocity, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_velocity_R4 =                                         &
  real(natural_unit_of_velocity_R16, kind=R4) ! derived

!> Natural unit of action, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_action_R16 =                                          &
  reduced_Planck_constant_R16
  ! 1.054 571 817... e-34_R16 ! derived

!> Natural unit of action, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_action_R8 =                                           &
  real(natural_unit_of_action_R16, kind=R8) ! derived

!> Natural unit of action, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_action_R4 =                                           &
  real(natural_unit_of_action_R16, kind=R4) ! derived

!> Natural unit of action in eV s, eV s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_action_in_eV_s_R16 =                                  &
  reduced_Planck_constant_in_eV_s_R16
  ! 6.582 119 569... e-16_R16 ! derived

!> Natural unit of action in eV s, eV s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_action_in_eV_s_R8 =                                   &
  real(natural_unit_of_action_in_eV_s_R16, kind=R8) ! derived

!> Natural unit of action in eV s, eV s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_action_in_eV_s_R4 =                                   &
  real(natural_unit_of_action_in_eV_s_R16, kind=R4) ! derived

!> Natural unit of mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_mass_R16 =                                            &
  electron_mass_R16
  ! 9.1093837015e-31_R16 ! derived

!> Natural unit of mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_mass_R8 =                                             &
  real(natural_unit_of_mass_R16, kind=R8) ! derived

!> Natural unit of mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_mass_R4 =                                             &
  real(natural_unit_of_mass_R16, kind=R4) ! derived

!> Natural unit of energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_energy_R16 =                                          &
  electron_mass_energy_equivalent_R16
  ! 8.1871057769e-14_R16 ! derived

!> Natural unit of energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_energy_R8 =                                           &
  real(natural_unit_of_energy_R16, kind=R8) ! derived

!> Natural unit of energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_energy_R4 =                                           &
  real(natural_unit_of_energy_R16, kind=R4) ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_energy_in_MeV_R16 =                                   &
  electron_mass_energy_equivalent_in_MeV_R16
  ! 0.51099895000_R16 ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_energy_in_MeV_R8 =                                    &
  real(natural_unit_of_energy_in_MeV_R16, kind=R8) ! derived

!> Natural unit of energy in MeV, MeV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_energy_in_MeV_R4 =                                    &
  real(natural_unit_of_energy_in_MeV_R16, kind=R4) ! derived

!> Natural unit of momentum, kg m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_momentum_R16 =                                        &
  electron_mass_R16 * speed_of_light_in_vacuum_R16
  ! 2.73092453075e-22_R16 ! derived

!> Natural unit of momentum, kg m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_momentum_R8 =                                         &
  real(natural_unit_of_momentum_R16, kind=R8) ! derived

!> Natural unit of momentum, kg m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_momentum_R4 =                                         &
  real(natural_unit_of_momentum_R16, kind=R4) ! derived

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_momentum_in_MeV_c_R16 =                               &
  electron_mass_energy_equivalent_in_MeV_R16
  ! 0.51099895000_R16 ! derived

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_momentum_in_MeV_c_R8 =                                &
  real(natural_unit_of_momentum_in_MeV_c_R16, kind=R8) ! derived

!> Natural unit of momentum in MeV/c, MeV/c
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_momentum_in_MeV_c_R4 =                                &
  real(natural_unit_of_momentum_in_MeV_c_R16, kind=R4) ! derived

!> Natural unit of length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_length_R16 =                                          &
  reduced_Compton_wavelength_R16
  ! 3.8615926796e-13_R16 ! derived

!> Natural unit of length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_length_R8 =                                           &
  real(natural_unit_of_length_R16, kind=R8) ! derived

!> Natural unit of length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_length_R4 =                                           &
  real(natural_unit_of_length_R16, kind=R4) ! derived

!> Natural unit of time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  natural_unit_of_time_R16 =                                            &
  reduced_Planck_constant_R16 / electron_mass_energy_equivalent_R16
  ! 1.28808866819e-21_R16

!> Natural unit of time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  natural_unit_of_time_R8 =                                             &
  real(natural_unit_of_time_R16, kind=R8) ! derived

!> Natural unit of time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  natural_unit_of_time_R4 =                                             &
  real(natural_unit_of_time_R16, kind=R4) ! derived

!-----
! Atomic units

!> Atomic unit of charge, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_charge_R16 =                                           &
  elementary_charge_R16
  ! 1.602176634e-19_R16

!> Atomic unit of charge, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_charge_R8 =                                            &
  real(atomic_unit_of_charge_R16, kind=R8)

!> Atomic unit of charge, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_charge_R4 =                                            &
  real(atomic_unit_of_charge_R16, kind=R4)

!> Atomic unit of mass, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_mass_R16 =                                             &
  electron_mass_R16
  ! 9.1093837015e-31_R16

!> Atomic unit of mass, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_mass_R8 =                                              &
  real(atomic_unit_of_mass_R16, kind=R8)

!> Atomic unit of mass, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_mass_R4 =                                              &
  real(atomic_unit_of_mass_R16, kind=R4)

!> Atomic unit of action, J s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_action_R16 =                                           &
  reduced_Planck_constant_R16
! ! atomic_unit_of_action = 1.054 571 817... e-34 ! derived

!> Atomic unit of action, J s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_action_R8 =                                            &
  real(atomic_unit_of_action_R16, kind=R8) ! derived

!> Atomic unit of action, J s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_action_R4 =                                            &
  real(atomic_unit_of_action_R16, kind=R4) ! derived

!> Atomic unit of length, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_length_R16 =                                           &
  Bohr_radius_R16
  ! 5.29177210903e-11_R16

!> Atomic unit of length, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_length_R8 =                                            &
  real(atomic_unit_of_length_R16, kind=R8) ! derived

!> Atomic unit of length, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_length_R4 =                                            &
  real(atomic_unit_of_length_R16, kind=R4) ! derived

!> Atomic unit of energy, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_energy_R16 =                                           &
  Hartree_energy_R16 ! derived
  ! 4.3597447222071e-18_R16

!> Atomic unit of energy, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_energy_R8 =                                            &
  real(atomic_unit_of_energy_R16, kind=R8) ! derived

!> Atomic unit of energy, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_energy_R4 =                                            &
  real(atomic_unit_of_energy_R16, kind=R4) ! derived

!> Atomic unit of time, s
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_time_R16 =                                             &
  reduced_Planck_constant_R16 / Hartree_energy_R16
  ! 2.4188843265857e-17_R16

!> Atomic unit of time, s
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_time_R8 =                                              &
  real(atomic_unit_of_time_R16, kind=R8)

!> Atomic unit of time, s
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_time_R4 =                                              &
  real(atomic_unit_of_time_R16, kind=R4)

!> Atomic unit of force, N
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_force_R16 =                                            &
  Hartree_energy_R16 / Bohr_radius_R16
  ! 8.2387234983e-8_R16

!> Atomic unit of force, N
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_force_R8 =                                             &
  real(atomic_unit_of_force_R16, kind=R8)

!> Atomic unit of force, N
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_force_R4 =                                             &
  real(atomic_unit_of_force_R16, kind=R4)

!> Atomic unit of velocity, m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_velocity_R16 =                                         &
  Bohr_radius_R16 * Hartree_energy_R16 / reduced_Planck_constant_R16
  ! 2.18769126364e6_R16

!> Atomic unit of velocity, m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_velocity_R8 =                                          &
  real(atomic_unit_of_velocity_R16, kind=R8)

!> Atomic unit of velocity, m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_velocity_R4 =                                          &
  real(atomic_unit_of_velocity_R16, kind=R4)

!> Atomic unit of momentum, kg m s^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_momentum_R16 =                                         &
  reduced_Planck_constant_R16 / Bohr_radius_R16
  ! 1.99285191410e-24_R16

!> Atomic unit of momentum, kg m s^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_momentum_R8 =                                          &
  real(atomic_unit_of_momentum_R16, kind=R8)

!> Atomic unit of momentum, kg m s^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_momentum_R4 =                                          &
  real(atomic_unit_of_momentum_R16, kind=R4)

!> Atomic unit of current, A
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_current_R16 =                                          &
  elementary_charge_R16 * Hartree_energy_R16                            &
  / reduced_Planck_constant_R16
  ! 6.623618237510e-3_R16

!> Atomic unit of current, A
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_current_R8 =                                           &
  real(atomic_unit_of_current_R16, kind=R8)

!> Atomic unit of current, A
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_current_R4 =                                           &
  real(atomic_unit_of_current_R16, kind=R4)

!> Atomic unit of charge density, C m^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_charge_density_R16 =                                   &
  elementary_charge_R16                                                 &
  / (Bohr_radius_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 1.08120238457e12_R16

!> Atomic unit of charge density, C m^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_charge_density_R8 =                                    &
  real(atomic_unit_of_charge_density_R16, kind=R8)

!> Atomic unit of charge density, C m^-3
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_charge_density_R4 =                                    &
  real(atomic_unit_of_charge_density_R16, kind=R4)

!> Atomic unit of electric potential, V
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_potential_R16 =                               &
  Hartree_energy_R16 / elementary_charge_R16
  ! 27.211386245988_R16

!> Atomic unit of electric potential, V
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_potential_R8 =                                &
  real(atomic_unit_of_electric_potential_R16, kind=R8)

!> Atomic unit of electric potential, V
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_potential_R4 =                                &
  real(atomic_unit_of_electric_potential_R16, kind=R4)

!> Atomic unit of electric field, V m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_field_R16 =                                   &
  Hartree_energy_R16 / (elementary_charge_R16 * Bohr_radius_R16)
  ! 5.14220674763e11_R16

!> Atomic unit of electric field, V m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_field_R8 =                                    &
  real(atomic_unit_of_electric_field_R16, kind=R8)

!> Atomic unit of electric field, V m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_field_R4 =                                    &
  real(atomic_unit_of_electric_field_R16, kind=R4)

!> Atomic unit of electric field gradient, V m^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_field_gradient_R16 =                          &
  Hartree_energy_R16                                                    &
  / (elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 9.7173624292e21_R16

!> Atomic unit of electric field gradient, V m^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_field_gradient_R8 =                           &
  real(atomic_unit_of_electric_field_gradient_R16, kind=R8)

!> Atomic unit of electric field gradient, V m^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_field_gradient_R4 =                           &
  real(atomic_unit_of_electric_field_gradient_R16, kind=R4)

!> Atomic unit of electric dipole mom., C m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_dipole_moment_R16 =                           &
  elementary_charge_R16 * Bohr_radius_R16
  ! 8.4783536255e-30_R16

!> Atomic unit of electric dipole mom., C m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_dipole_moment_R8 =                            &
  real(atomic_unit_of_electric_dipole_moment_R16, kind=R8)

!> Atomic unit of electric dipole mom., C m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_dipole_moment_R4 =                            &
  real(atomic_unit_of_electric_dipole_moment_R16, kind=R4)

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_quadrupole_moment_R16 =                       &
  elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16
  ! 4.4865515246e-40_R16

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_quadrupole_moment_R8 =                        &
  real(atomic_unit_of_electric_quadrupole_moment_R16, kind=R8)

!> Atomic unit of electric quadrupole mom., C m^2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_quadrupole_moment_R4 =                        &
  real(atomic_unit_of_electric_quadrupole_moment_R16, kind=R4)

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_electric_polarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16                         &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / Hartree_energy_R16
  ! 1.64877727436e-41_R16

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_electric_polarizability_R8 =                           &
  real(atomic_unit_of_electric_polarizability_R16, kind=R8)

!> Atomic unit of electric polarizability, C^2 m^2 J^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_electric_polarizability_R4 =                           &
  real(atomic_unit_of_electric_polarizability_R16, kind=R4)

!> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_1st_hyperpolarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16 * elementary_charge_R16 &
  * Bohr_radius_R16 * Bohr_radius_R16 * Bohr_radius_R16                 &
  / (Hartree_energy_R16 * Hartree_energy_R16)
  ! 3.2063613061e-53_R16

!> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_1st_hyperpolarizability_R8 =                           &
  real(atomic_unit_of_1st_hyperpolarizability_R16, kind=R8)

! UNDERFLOW
! !> Atomic unit of 1st hyperpolarizability, C^3 m^3 J^-2
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   atomic_unit_of_1st_hyperpolarizability_R4 =                           &
!   real(atomic_unit_of_1st_hyperpolarizability_R16, kind=R4)

!> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_2nd_hyperpolarizability_R16 =                          &
  elementary_charge_R16 * elementary_charge_R16                         &
  * elementary_charge_R16 * elementary_charge_R16                       &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / (Hartree_energy_R16 * Hartree_energy_R16 * Hartree_energy_R16)
  ! 6.2353799905e-65_R16

!> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_2nd_hyperpolarizability_R8 =                           &
  real(atomic_unit_of_2nd_hyperpolarizability_R16, kind=R8)

! UNDERFLOW
! !> Atomic unit of 2nd hyperpolarizability, C^4 m^4 J^-3
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   atomic_unit_of_2nd_hyperpolarizability_R4 =                           &
!   real(atomic_unit_of_2nd_hyperpolarizability_R16, kind=R4)

!> Atomic unit of mag. flux density, T
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetic_flux_density_R16 =                            &
  reduced_Planck_constant_R16                                           &
  / (elementary_charge_R16 * Bohr_radius_R16 * Bohr_radius_R16)
  ! 2.35051756758e5_R16

!> Atomic unit of mag. flux density, T
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetic_flux_density_R8 =                             &
  real(atomic_unit_of_magnetic_flux_density_R16, kind=R8)

!> Atomic unit of mag. flux density, T
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetic_flux_density_R4 =                             &
  real(atomic_unit_of_magnetic_flux_density_R16, kind=R4)

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetic_dipole_moment_R16 =                           &
  reduced_Planck_constant_R16 * elementary_charge_R16                   &
  / electron_mass_R16
  ! 1.85480201566e-23_R16

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetic_dipole_moment_R8 =                            &
  real(atomic_unit_of_magnetic_dipole_moment_R16, kind=R8)

!> Atomic unit of mag. dipole mom., J T^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetic_dipole_moment_R4 =                            &
  real(atomic_unit_of_magnetic_dipole_moment_R16, kind=R4)

!> Atomic unit of magnetizability, J T^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_magnetizability_R16 =                                  &
  elementary_charge_R16 * elementary_charge_R16                         &
  * Bohr_radius_R16 * Bohr_radius_R16                                   &
  / electron_mass_R16
  ! 7.8910366008e-29_R16

!> Atomic unit of magnetizability, J T^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_magnetizability_R8 =                                   &
  real(atomic_unit_of_magnetizability_R16, kind=R8)

!> Atomic unit of magnetizability, J T^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_magnetizability_R4 =                                   &
  real(atomic_unit_of_magnetizability_R16, kind=R4)

!> Atomic unit of permittivity, F m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_unit_of_permittivity_R16 =                                     &
  elementary_charge_R16 * elementary_charge_R16                         &
  / (Bohr_radius_R16 * Hartree_energy_R16)
  ! 1.11265005545e-10_R16

!> Atomic unit of permittivity, F m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_unit_of_permittivity_R8 =                                      &
  real(atomic_unit_of_permittivity_R16, kind=R8) ! derived

!> Atomic unit of permittivity, F m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_unit_of_permittivity_R4 =                                      &
  real(atomic_unit_of_permittivity_R16, kind=R4) ! derived

!-----
! Adopted

!> Hyperfine transition frequency of Cs-133, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hyperfine_transition_frequency_of_Cs_133_R16 =                        &
  9192631770.0_R16

!> Hyperfine transition frequency of Cs-133, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hyperfine_transition_frequency_of_Cs_133_R8 =                         &
  real(hyperfine_transition_frequency_of_Cs_133_R16, kind=R8)

!> Hyperfine transition frequency of Cs-133, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hyperfine_transition_frequency_of_Cs_133_R4 =                         &
  real(hyperfine_transition_frequency_of_Cs_133_R16, kind=R4)

!> Molar mass constant, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_mass_constant_R16 =                                             &
  0.99999999965e-3_R16

!> Molar mass constant, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_mass_constant_R8 =                                              &
  real(molar_mass_constant_R16, kind=R8)

!> Molar mass constant, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_mass_constant_R4 =                                              &
  real(molar_mass_constant_R16, kind=R4)

!> Molar mass of carbon-12, kg mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_mass_of_carbon_12_R16 =                                         &
  11.9999999958e-3_R16

!> Molar mass of carbon-12, kg mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_mass_of_carbon_12_R8 =                                          &
  real(molar_mass_of_carbon_12_R16, kind=R8)

!> Molar mass of carbon-12, kg mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_mass_of_carbon_12_R4 =                                          &
  real(molar_mass_of_carbon_12_R16, kind=R4)

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_Josephson_constant_R16 =                        &
  483597.9e9_R16

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_Josephson_constant_R8 =                         &
  real(conventional_value_of_Josephson_constant_R16, kind=R8)

!> Conventional value of Josephson constant, Hz V^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_Josephson_constant_R4 =                         &
  real(conventional_value_of_Josephson_constant_R16, kind=R4)

!> Conventional value of von Klitzing constant, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_von_Klitzing_constant_R16 =                     &
  25812.807_R16

!> Conventional value of von Klitzing constant, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_von_Klitzing_constant_R8 =                      &
  real(conventional_value_of_von_Klitzing_constant_R16, kind=R8)

!> Conventional value of von Klitzing constant, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_von_Klitzing_constant_R4 =                      &
  real(conventional_value_of_von_Klitzing_constant_R16, kind=R4)

! !> Standard temperature, K
! !! (at most 33 significant digits)
! real(kind=R16), parameter, public ::                                    &
!   standard_temperature_R16 =                                            &
!   273.15_R16

!> Standard temperature, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_temperature_R8 =                                             &
  real(standard_temperature_R16, kind=R8)

!> Standard temperature, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_temperature_R4 =                                             &
  real(standard_temperature_R16, kind=R4)

! !> Standard atmosphere, Pa
! !! (at most 33 significant digits)
! real(kind=R16), parameter, public ::                                    &
!   standard_atmosphere_R16 =                                             &
!   101325.0_R16

!> Standard atmosphere, Pa
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_atmosphere_R8 =                                              &
  real(standard_atmosphere_R16, kind=R8)

!> Standard atmosphere, Pa
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_atmosphere_R4 =                                              &
  real(standard_atmosphere_R16, kind=R4)

!> Standard acceleration of gravity, m s^-2
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  standard_acceleration_of_gravity_R16 =                                &
  9.80665_R16

!> Standard acceleration of gravity, m s^-2
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_acceleration_of_gravity_R8 =                                 &
  real(standard_acceleration_of_gravity_R16, kind=R8)

!> Standard acceleration of gravity, m s^-2
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_acceleration_of_gravity_R4 =                                 &
  real(standard_acceleration_of_gravity_R16, kind=R4)

! !> Standard-state pressure, Pa
! !! (at most 33 significant digits)
! real(kind=R16), parameter, public ::                                    &
!   standard_state_pressure_R16 =                                         &
!   100000.0_R16

!> Standard-state pressure, Pa
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  standard_state_pressure_R8 =                                          &
  real(standard_state_pressure_R16, kind=R8)

!> Standard-state pressure, Pa
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  standard_state_pressure_R4 =                                          &
  real(standard_state_pressure_R16, kind=R4)

!> Luminous efficacy, lm W^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  luminous_efficacy_R16 =                                               &
  683.0_R16

!> Luminous efficacy, lm W^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  luminous_efficacy_R8 =                                                &
  real(luminous_efficacy_R16, kind=R8)

!> Luminous efficacy, lm W^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  luminous_efficacy_R4 =                                                &
  real(luminous_efficacy_R16, kind=R4)

!-----
! "As-maintained" electrical units

! BIPM maintained ohm-69BI -> ohm-BI85 = 0.999998437 ohm
! BIPM maintained volt-76BI = 0.99999241 V
! BIPM maintained ampere-BIPM = volt-76BI / ohm-69BI = 0.99999397 A


!> Conventional value of ampere-90, A
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_ampere_90_R16 =                                 &
  conventional_value_of_Josephson_constant_R16                          &
  * conventional_value_of_von_Klitzing_constant_R16                     &
  * elementary_charge_R16 / 2.0_R16
  ! (conventional_value_of_Josephson_constant_R16                         &
  ! / (2.0_R16 * elementary_charge_R16 / Planck_constant_R16))        &
  ! * (conventional_value_of_von_Klitzing_constant_R16                    &
  ! / (Planck_constant_R16                                                &
  ! / (elementary_charge_R16 * elementary_charge_R16)))
!   1.000 000 088 87..._R16 ! derived

!> Conventional value of ampere-90, A
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_ampere_90_R8 =                                  &
  real(conventional_value_of_ampere_90_R16, kind=R8) ! derived

!> Conventional value of ampere-90, A
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_ampere_90_R4 =                                  &
  real(conventional_value_of_ampere_90_R16, kind=R4) ! derived

!> Conventional value of coulomb-90, C
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_coulomb_90_R16 =                                &
  conventional_value_of_Josephson_constant_R16                          &
  * conventional_value_of_von_Klitzing_constant_R16                     &
  * elementary_charge_R16 / 2.0_R16
!   1.000 000 088 87..._R16 ! derived

!> Conventional value of coulomb-90, C
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_coulomb_90_R8 =                                 &
  real(conventional_value_of_coulomb_90_R16, kind=R8) ! derived

!> Conventional value of coulomb-90, C
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_coulomb_90_R4 =                                 &
  real(conventional_value_of_coulomb_90_R16, kind=R4) ! derived

!> Conventional value of farad-90, F
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_farad_90_R16 =                                  &
  conventional_value_of_von_Klitzing_constant_R16                       &
  * elementary_charge_R16 * elementary_charge_R16               &
  / Planck_constant_R16
!   0.999 999 982 20..._R16 ! derived

!> Conventional value of farad-90, F
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_farad_90_R8 =                                   &
  real(conventional_value_of_farad_90_R16, kind=R8) ! derived

!> Conventional value of farad-90, F
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_farad_90_R4 =                                   &
  real(conventional_value_of_farad_90_R16, kind=R4) ! derived

!> Conventional value of henry-90, H
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_henry_90_R16 =                                  &
  Planck_constant_R16                                                   &
  / (conventional_value_of_von_Klitzing_constant_R16                    &
  * elementary_charge_R16 * elementary_charge_R16)
!   1.000 000 017 79..._R16 ! derived

!> Conventional value of henry-90, H
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_henry_90_R8 =                                   &
  real(conventional_value_of_henry_90_R16, kind=R8) ! derived

!> Conventional value of henry-90, H
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_henry_90_R4 =                                   &
  real(conventional_value_of_henry_90_R16, kind=R4) ! derived

!> Conventional value of ohm-90, ohm
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  conventional_value_of_ohm_90_R16 =                                    &
  Planck_constant_R16                                                   &
  / (conventional_value_of_von_Klitzing_constant_R16                    &
  * elementary_charge_R16 * elementary_charge_R16)
  ! 1.000 000 017 79..._R16 ! derived

!> Conventional value of ohm-90, ohm
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  conventional_value_of_ohm_90_R8 =                                     &
  real(conventional_value_of_ohm_90_R16, kind=R8) ! derived

!> Conventional value of ohm-90, ohm
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  conventional_value_of_ohm_90_R4 =                                     &
  real(conventional_value_of_ohm_90_R16, kind=R4) ! derived

! !> Conventional value of volt-90, V
! !! (at most 33 significant digits)
! real(kind=R16), parameter, public ::                                    &
!   conventional_value_of_volt_90_R16 =                                   &
!   1.000 000 106 66..._R16 ! derived

! !> Conventional value of volt-90, V
! !! (at most 15 significant digits)
! real(kind=R8), parameter, public ::                                     &
!   conventional_value_of_volt_90_R8 =                                    &
!   real(conventional_value_of_volt_90_R16, kind=R8) ! derived

! !> Conventional value of volt-90, V
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   conventional_value_of_volt_90_R4 =                                    &
!   real(conventional_value_of_volt_90_R16, kind=R4) ! derived

! !> Conventional value of watt-90, W
! !! (at most 33 significant digits)
! real(kind=R16), parameter, public ::                                    &
!   conventional_value_of_watt_90_R16 =                                   &
!   1.000 000 195 53..._R16 ! derived

! !> Conventional value of watt-90, W
! !! (at most 15 significant digits)
! real(kind=R8), parameter, public ::                                     &
!   conventional_value_of_watt_90_R8 =                                    &
!   real(conventional_value_of_watt_90_R16, kind=R8) ! derived

! !> Conventional value of watt-90, W
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   conventional_value_of_watt_90_R4 =                                    &
!   real(conventional_value_of_watt_90_R16, kind=R4) ! derived

!-----
! X-ray standards

!> Copper x unit, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Copper_x_unit_R16 =                                                   &
  1.00207697e-13_R16

!> Copper x unit, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Copper_x_unit_R8 =                                                    &
  real(Copper_x_unit_R16, kind=R8)

!> Copper x unit, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Copper_x_unit_R4 =                                                    &
  real(Copper_x_unit_R16, kind=R4)

!> Molybdenum x unit, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Molybdenum_x_unit_R16 =                                               &
  1.00209952e-13_R16

!> Molybdenum x unit, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Molybdenum_x_unit_R8 =                                                &
  real(Molybdenum_x_unit_R16, kind=R8)

!> Molybdenum x unit, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Molybdenum_x_unit_R4 =                                                &
  real(Molybdenum_x_unit_R16, kind=R4)

!> Angstrom star, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  Angstrom_star_R16 =                                                   &
  1.00001495e-10_R16

!> Angstrom star, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  Angstrom_star_R8 =                                                    &
  real(Angstrom_star_R16, kind=R8)

!> Angstrom star, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  Angstrom_star_R4 =                                                    &
  real(Angstrom_star_R16, kind=R4)

!> Lattice parameter of silicon, m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  lattice_parameter_of_silicon_R16 =                                    &
  5.431020511e-10_R16

!> Lattice parameter of silicon, m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  lattice_parameter_of_silicon_R8 =                                     &
  real(lattice_parameter_of_silicon_R16, kind=R8)

!> Lattice parameter of silicon, m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  lattice_parameter_of_silicon_R4 =                                     &
  real(lattice_parameter_of_silicon_R16, kind=R4)

!> Lattice spacing of ideal Si (d220), m
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  lattice_spacing_of_ideal_silicon_d220_R16 =                           &
  1.920155716e-10_R16

!> Lattice spacing of ideal Si (d220), m
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  lattice_spacing_of_ideal_silicon_d220_R8 =                            &
  real(lattice_spacing_of_ideal_silicon_d220_R16, kind=R8)

!> Lattice spacing of ideal Si (d220), m
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  lattice_spacing_of_ideal_silicon_d220_R4 =                            &
  real(lattice_spacing_of_ideal_silicon_d220_R16, kind=R4)

!> Molar volume of silicon, m^3 mol^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  molar_volume_of_silicon_R16 =                                         &
  1.205883199e-5_R16

!> Molar volume of silicon, m^3 mol^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  molar_volume_of_silicon_R8 =                                          &
  real(molar_volume_of_silicon_R16, kind=R8)

!> Molar volume of silicon, m^3 mol^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  molar_volume_of_silicon_R4 =                                          &
  real(molar_volume_of_silicon_R16, kind=R4)

!-----
! Energy conversion factors

!> Joule-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_kilogram_relationship_R16 = 1.0_R16                             &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   1.112 650 056... e-17_R16 ! derived

!> Joule-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_kilogram_relationship_R8 =                                      &
  real(joule_kilogram_relationship_R16, kind=R8) ! derived

!> Joule-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_kilogram_relationship_R4 =                                      &
  real(joule_kilogram_relationship_R16, kind=R4) ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_inverse_meter_relationship_R16 =                                &
  1.0_R16 / (speed_of_light_in_vacuum_R16 * Planck_constant_R16)
  ! 5.034 116 567... e24_R16 ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_inverse_meter_relationship_R8 =                                 &
  real(joule_inverse_meter_relationship_R16, kind=R8) ! derived

!> Joule-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_inverse_meter_relationship_R4 =                                 &
  real(joule_inverse_meter_relationship_R16, kind=R4) ! derived

!> Joule-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_hertz_relationship_R16 =                                        &
  1.0_R16 / Planck_constant_R16
  ! 1.509 190 179... e33_R16 ! derived

!> Joule-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_hertz_relationship_R8 =                                         &
  real(joule_hertz_relationship_R16, kind=R8) ! derived

!> Joule-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_hertz_relationship_R4 =                                         &
  real(joule_hertz_relationship_R16, kind=R4) ! derived

!> Joule-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_kelvin_relationship_R16 =                                       &
  1.0_R16 / Boltzmann_constant_R16
!   7.242 970 516... e22_R16 ! derived

!> Joule-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_kelvin_relationship_R8 =                                        &
  real(joule_kelvin_relationship_R16, kind=R8) ! derived

!> Joule-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_kelvin_relationship_R4 =                                        &
  real(joule_kelvin_relationship_R16, kind=R4) ! derived

!> Joule-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_electron_volt_relationship_R16 =                                &
  1.0_R16 / elementary_charge_R16
!   6.241 509 074... e18_R16 ! derived

!> Joule-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_electron_volt_relationship_R8 =                                 &
  real(joule_electron_volt_relationship_R16, kind=R8) ! derived

!> Joule-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_electron_volt_relationship_R4 =                                 &
  real(joule_electron_volt_relationship_R16, kind=R4) ! derived

!> Joule-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_atomic_mass_unit_relationship_R16 =                             &
  1.0_R16 / atomic_mass_constant_energy_equivalent_R16
  ! 6.7005352565e9_R16

!> Joule-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_atomic_mass_unit_relationship_R8 =                              &
  real(joule_atomic_mass_unit_relationship_R16, kind=R8)

!> Joule-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_atomic_mass_unit_relationship_R4 =                              &
  real(joule_atomic_mass_unit_relationship_R16, kind=R4)

!> Joule-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  joule_hartree_relationship_R16 =                                      &
  1.0_R16 / Hartree_energy_R16
  ! 2.29371269...e17

!> Joule-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  joule_hartree_relationship_R8 =                                       &
  real(joule_hartree_relationship_R16, kind=R8)

!> Joule-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  joule_hartree_relationship_R4 =                                       &
  real(joule_hartree_relationship_R16, kind=R4)


!> Kilogram-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_joule_relationship_R16 =                                     &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16
!   8.987 551 787... e16_R16 ! derived

!> Kilogram-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_joule_relationship_R8 =                                      &
  real(kilogram_joule_relationship_R16, kind=R8) ! derived

!> Kilogram-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_joule_relationship_R4 =                                      &
  real(kilogram_joule_relationship_R16, kind=R4) ! derived

!> Kilogram-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_inverse_meter_relationship_R16 =                             &
  speed_of_light_in_vacuum_R16 / Planck_constant_R16
  ! 4.524 439 15... e41_R16 ! derived

!> Kilogram-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_inverse_meter_relationship_R8 =                              &
  real(kilogram_inverse_meter_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-inverse meter relationship, m^-1
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_inverse_meter_relationship_R4 =                              &
!   real(kilogram_inverse_meter_relationship_R16, kind=R4) ! derived

!> Kilogram-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_hertz_relationship_R16 =                                     &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Planck_constant_R16
  ! 1.356 392 489... e50_R16 ! derived

!> Kilogram-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_hertz_relationship_R8 =                                      &
  real(kilogram_hertz_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-hertz relationship, Hz
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_hertz_relationship_R4 =                                      &
!   real(kilogram_hertz_relationship_R16, kind=R4) ! derived

!> Kilogram-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_kelvin_relationship_R16 =                                    &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Boltzmann_constant_R16
  ! 6.509 657 260... e39_R16 ! derived

!> Kilogram-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_kelvin_relationship_R8 =                                     &
  real(kilogram_kelvin_relationship_R16, kind=R8) ! derived

! OVERFLOW
! !> Kilogram-kelvin relationship, K
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   kilogram_kelvin_relationship_R4 =                                     &
!   real(kilogram_kelvin_relationship_R16, kind=R4) ! derived

!> Kilogram-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_electron_volt_relationship_R16 =                             &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / electron_volt_R16
  ! 5.609 588 603... e35_R16 ! derived

!> Kilogram-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R8 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R8) ! derived

!> Kilogram-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_electron_volt_relationship_R4 =                              &
  real(kilogram_electron_volt_relationship_R16, kind=R4) ! derived

!> Kilogram-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_atomic_mass_unit_relationship_R16 =                          &
  1.0_R16 / unified_atomic_mass_unit_R16
  ! 6.0221407621e26_R16

!> Kilogram-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_atomic_mass_unit_relationship_R8 =                           &
  real(kilogram_atomic_mass_unit_relationship_R16, kind=R8)

!> Kilogram-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_atomic_mass_unit_relationship_R4 =                           &
  real(kilogram_atomic_mass_unit_relationship_R16, kind=R4)

!> Kilogram-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kilogram_hartree_relationship_R16 =                                   &
  speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16           &
  / Hartree_energy_R16
  ! 2.0614857887409e34_R16

!> Kilogram-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kilogram_hartree_relationship_R8 =                                    &
  real(kilogram_hartree_relationship_R16, kind=R8)

!> Kilogram-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kilogram_hartree_relationship_R4 =                                    &
  real(kilogram_hartree_relationship_R16, kind=R4)


!> Inverse meter-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_joule_relationship_R16 =                                &
  speed_of_light_in_vacuum_R16 * Planck_constant_R16
  ! 1.986 445 857... e-25_R16 ! derived

!> Inverse meter-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_joule_relationship_R8 =                                 &
  real(inverse_meter_joule_relationship_R16, kind=R8) ! derived

!> Inverse meter-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_joule_relationship_R4 =                                 &
  real(inverse_meter_joule_relationship_R16, kind=R4) ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_kilogram_relationship_R16 =                             &
  Planck_constant_R16 / speed_of_light_in_vacuum_R16
!   2.210 219 094... e-42_R16 ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_kilogram_relationship_R8 =                              &
  real(inverse_meter_kilogram_relationship_R16, kind=R8) ! derived

!> Inverse meter-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_kilogram_relationship_R4 =                              &
  real(inverse_meter_kilogram_relationship_R16, kind=R4) ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_hertz_relationship_R16 =                                &
  speed_of_light_in_vacuum_R16
  ! 299792458.0_R16 ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_hertz_relationship_R8 =                                 &
  real(inverse_meter_hertz_relationship_R16, kind=R8) ! derived

!> Inverse meter-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_hertz_relationship_R4 =                                 &
  real(inverse_meter_hertz_relationship_R16, kind=R4) ! derived

!> Inverse meter-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_kelvin_relationship_R16 =                               &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / Boltzmann_constant_R16
  ! 1.438 776 877... e-2_R16 ! derived

!> Inverse meter-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_kelvin_relationship_R8 =                                &
  real(inverse_meter_kelvin_relationship_R16, kind=R8) ! derived

!> Inverse meter-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_kelvin_relationship_R4 =                                &
  real(inverse_meter_kelvin_relationship_R16, kind=R4) ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_electron_volt_relationship_R16 =                        &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / elementary_charge_R16
  ! 1.239 841 984... e-6_R16 ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R8 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R8) ! derived

!> Inverse meter-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_electron_volt_relationship_R4 =                         &
  real(inverse_meter_electron_volt_relationship_R16, kind=R4) ! derived

!> Inverse meter-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_atomic_mass_unit_relationship_R16 =                     &
  Planck_constant_R16                                                   &
  / (speed_of_light_in_vacuum_R16 * unified_atomic_mass_unit_R16)
  ! 1.33102505010e-15_R16

!> Inverse meter-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_atomic_mass_unit_relationship_R8 =                      &
  real(inverse_meter_atomic_mass_unit_relationship_R16, kind=R8)

!> Inverse meter-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_atomic_mass_unit_relationship_R4 =                      &
  real(inverse_meter_atomic_mass_unit_relationship_R16, kind=R4)

!> Inverse meter-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  inverse_meter_hartree_relationship_R16 =                              &
  Planck_constant_R16 * speed_of_light_in_vacuum_R16                    &
  / Hartree_energy_R16
  ! 4.5563352529120e-8_R16

!> Inverse meter-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  inverse_meter_hartree_relationship_R8 =                               &
  real(inverse_meter_hartree_relationship_R16, kind=R8)

!> Inverse meter-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  inverse_meter_hartree_relationship_R4 =                               &
  real(inverse_meter_hartree_relationship_R16, kind=R4)


!> Hertz-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_joule_relationship_R16 =                                        &
  Planck_constant_R16
  ! 6.62607015e-34_R16 - derived

!> Hertz-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_joule_relationship_R8 =                                         &
  real(hertz_joule_relationship_R16, kind=R8)

!> Hertz-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_joule_relationship_R4 =                                         &
  real(hertz_joule_relationship_R16, kind=R4)

!> Hertz-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_kilogram_relationship_R16 =   Planck_constant_R16               &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
!   7.372 497 323... e-51_R16 ! derived

!> Hertz-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_kilogram_relationship_R8 =                                      &
  real(hertz_kilogram_relationship_R16, kind=R8) ! derived

! UNDERFLOW
! !> Hertz-kilogram relationship, kg
! !! (at most 6 significant digits)
! real(kind=R4), parameter, public ::                                     &
!   hertz_kilogram_relationship_R4 =                                      &
!   real(hertz_kilogram_relationship_R16, kind=R4) ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_inverse_meter_relationship_R16 =                                &
  1.0_R16 / speed_of_light_in_vacuum_R16
!   3.335 640 951... e-9_R16 ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_inverse_meter_relationship_R8 =                                 &
  real(hertz_inverse_meter_relationship_R16, kind=R8) ! derived

!> Hertz-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_inverse_meter_relationship_R4 =                                 &
  real(hertz_inverse_meter_relationship_R16, kind=R4) ! derived

!> Hertz-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_kelvin_relationship_R16 =                                       &
  Planck_constant_R16 / Boltzmann_constant_R16
!   4.799 243 073... e-11_R16 ! derived

!> Hertz-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_kelvin_relationship_R8 =                                        &
  real(hertz_kelvin_relationship_R16, kind=R8) ! derived

!> Hertz-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_kelvin_relationship_R4 =                                        &
  real(hertz_kelvin_relationship_R16, kind=R4) ! derived

!> Hertz-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_electron_volt_relationship_R16 =                                &
  Planck_constant_R16 / electron_volt_R16
!   4.135 667 696... e-15_R16 ! derived

!> Hertz-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_electron_volt_relationship_R8 =                                 &
  real(hertz_electron_volt_relationship_R16, kind=R8) ! derived

!> Hertz-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_electron_volt_relationship_R4 =                                 &
  real(hertz_electron_volt_relationship_R16, kind=R4) ! derived

!> Hertz-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_atomic_mass_unit_relationship_R16 =                             &
  Planck_constant_R16                                                   &
  / atomic_mass_constant_energy_equivalent_R16
  ! 4.4398216652e-24_R16

!> Hertz-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_atomic_mass_unit_relationship_R8 =                              &
  real(hertz_atomic_mass_unit_relationship_R16, kind=R8)

!> Hertz-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_atomic_mass_unit_relationship_R4 =                              &
  real(hertz_atomic_mass_unit_relationship_R16, kind=R4)

!> Hertz-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hertz_hartree_relationship_R16 =                                      &
  Planck_constant_R16 / Hartree_energy_R16
  !  1.5198298460570e-16_R16

!> Hertz-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hertz_hartree_relationship_R8 =                                       &
  real(hertz_hartree_relationship_R16, kind=R8)

!> Hertz-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hertz_hartree_relationship_R4 =                                       &
  real(hertz_hartree_relationship_R16, kind=R4)


!> Kelvin-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_joule_relationship_R16 =                                       &
  Boltzmann_constant_R16
!  1.380649e-23_R16

!> Kelvin-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_joule_relationship_R8 =                                        &
  real(kelvin_joule_relationship_R16, kind=R8)

!> Kelvin-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_joule_relationship_R4 =                                        &
  real(kelvin_joule_relationship_R16, kind=R4)

!> Kelvin-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_kilogram_relationship_R16 =                                    &
  Boltzmann_constant_R16                                                &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 1.536 179 187... e-40_R16 ! derived

!> Kelvin-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_kilogram_relationship_R8 =                                     &
  real(kelvin_kilogram_relationship_R16, kind=R8) ! derived

!> Kelvin-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_kilogram_relationship_R4 =                                     &
  real(kelvin_kilogram_relationship_R16, kind=R4) ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_inverse_meter_relationship_R16 =                               &
  Boltzmann_constant_R16                                                &
  / (Planck_constant_R16 * speed_of_light_in_vacuum_R16)
!   69.503 480 04..._R16 ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_inverse_meter_relationship_R8 =                                &
  real(kelvin_inverse_meter_relationship_R16, kind=R8) ! derived

!> Kelvin-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_inverse_meter_relationship_R4 =                                &
  real(kelvin_inverse_meter_relationship_R16, kind=R4) ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_hertz_relationship_R16 =                                       &
  Boltzmann_constant_R16 / Planck_constant_R16
!   2.083 661 912... e10_R16 ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_hertz_relationship_R8 =                                        &
  real(kelvin_hertz_relationship_R16, kind=R8) ! derived

!> Kelvin-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_hertz_relationship_R4 =                                        &
  real(kelvin_hertz_relationship_R16, kind=R4) ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_electron_volt_relationship_R16 =                               &
  Boltzmann_constant_R16 / electron_volt_R16
  ! 8.617 333 262... e-5_R16 ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_electron_volt_relationship_R8 =                                &
  real(kelvin_electron_volt_relationship_R16, kind=R8) ! derived

!> Kelvin-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_electron_volt_relationship_R4 =                                &
  real(kelvin_electron_volt_relationship_R16, kind=R4) ! derived

!> Kelvin-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_atomic_mass_unit_relationship_R16 =                            &
  Boltzmann_constant_R16                                                &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16        &
  * unified_atomic_mass_unit_R16)
  ! 9.2510873014e-14_R16

!> Kelvin-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_atomic_mass_unit_relationship_R8 =                             &
  real(kelvin_atomic_mass_unit_relationship_R16, kind=R8)

!> Kelvin-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_atomic_mass_unit_relationship_R4 =                             &
  real(kelvin_atomic_mass_unit_relationship_R16, kind=R4)

!> Kelvin-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  kelvin_hartree_relationship_R16 =                                     &
  Boltzmann_constant_R16 / Hartree_energy_R16
  ! 3.1668115634556e-6_R16

!> Kelvin-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  kelvin_hartree_relationship_R8 =                                      &
  real(kelvin_hartree_relationship_R16, kind=R8)

!> Kelvin-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  kelvin_hartree_relationship_R4 =                                      &
  real(kelvin_hartree_relationship_R16, kind=R4)


!> Electron volt-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_joule_relationship_R16 =                                &
  electron_volt_R16
  ! 1.602176634e-19_R16

!> Electron volt-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_joule_relationship_R8 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R8)

!> Electron volt-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_joule_relationship_R4 =                                 &
  real(electron_volt_joule_relationship_R16, kind=R4)

!> Electron volt-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_kilogram_relationship_R16 =                             &
  electron_volt_R16                                                     &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 1.782 661 921... e-36_R16 ! derived

!> Electron volt-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_kilogram_relationship_R8 =                              &
  real(electron_volt_kilogram_relationship_R16, kind=R8) ! derived

!> Electron volt-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_kilogram_relationship_R4 =                              &
  real(electron_volt_kilogram_relationship_R16, kind=R4) ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_inverse_meter_relationship_R16 =                        &
  electron_volt_R16                                                     &
  / (Planck_constant_R16 * speed_of_light_in_vacuum_R16)
  ! 8.065 543 937... e5_R16 ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R8 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R8) ! derived

!> Electron volt-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_inverse_meter_relationship_R4 =                         &
  real(electron_volt_inverse_meter_relationship_R16, kind=R4) ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_hertz_relationship_R16 =                                &
  electron_volt_R16 / Planck_constant_R16
  ! 2.417 989 242... e14_R16 ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_hertz_relationship_R8 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R8) ! derived

!> Electron volt-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_hertz_relationship_R4 =                                 &
  real(electron_volt_hertz_relationship_R16, kind=R4) ! derived

!> Electron volt-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_kelvin_relationship_R16 =                               &
  electron_volt_R16 / Boltzmann_constant_R16
  ! 1.160 451 812... e4_R16 ! derived

!> Electron volt-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R8 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R8) ! derived

!> Electron volt-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_kelvin_relationship_R4 =                                &
  real(electron_volt_kelvin_relationship_R16, kind=R4) ! derived

!> Electron volt-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_atomic_mass_unit_relationship_R16 =                     &
  electron_volt_R16                                                     &
  / (unified_atomic_mass_unit_R16                                       &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 1.07354410233e-9_R16

!> Electron volt-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_atomic_mass_unit_relationship_R8 =                      &
  real(electron_volt_atomic_mass_unit_relationship_R16, kind=R8)

!> Electron volt-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_atomic_mass_unit_relationship_R4 =                      &
  real(electron_volt_atomic_mass_unit_relationship_R16, kind=R4)

!> Electron volt-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  electron_volt_hartree_relationship_R16 =                              &
  electron_volt_R16 / Hartree_energy_R16
  ! 3.6749322175655e-2_R16

!> Electron volt-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  electron_volt_hartree_relationship_R8 =                               &
  real(electron_volt_hartree_relationship_R16, kind=R8)

!> Electron volt-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  electron_volt_hartree_relationship_R4 =                               &
  real(electron_volt_hartree_relationship_R16, kind=R4)


!> Atomic mass unit-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_joule_relationship_R16 =                             &
  atomic_mass_constant_energy_equivalent_R16
  ! 1.49241808560e-10_R16

!> Atomic mass unit-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_joule_relationship_R8 =                              &
  real(atomic_mass_unit_joule_relationship_R16, kind=R8)

!> Atomic mass unit-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_joule_relationship_R4 =                              &
  real(atomic_mass_unit_joule_relationship_R16, kind=R4)

!> Atomic mass unit-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_kilogram_relationship_R16 =                          &
  atomic_mass_constant_R16
  ! 1.66053906660e-27_R16

!> Atomic mass unit-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_kilogram_relationship_R8 =                           &
  real(atomic_mass_unit_kilogram_relationship_R16, kind=R8)

!> Atomic mass unit-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_kilogram_relationship_R4 =                           &
  real(atomic_mass_unit_kilogram_relationship_R16, kind=R4)

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_inverse_meter_relationship_R16 =                     &
  unified_atomic_mass_unit_R16 * speed_of_light_in_vacuum_R16           &
  / Planck_constant_R16
  ! 7.5130066104e14_R16

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_inverse_meter_relationship_R8 =                      &
  real(atomic_mass_unit_inverse_meter_relationship_R16, kind=R8)

!> Atomic mass unit-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_inverse_meter_relationship_R4 =                      &
  real(atomic_mass_unit_inverse_meter_relationship_R16, kind=R4)

!> Atomic mass unit-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_hertz_relationship_R16 =                             &
  atomic_mass_constant_energy_equivalent_R16                            &
  / Planck_constant_R16
  ! 2.25234271871e23_R16

!> Atomic mass unit-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_hertz_relationship_R8 =                              &
  real(atomic_mass_unit_hertz_relationship_R16, kind=R8)

!> Atomic mass unit-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_hertz_relationship_R4 =                              &
  real(atomic_mass_unit_hertz_relationship_R16, kind=R4)

!> Atomic mass unit-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_kelvin_relationship_R16 =                            &
  atomic_mass_constant_energy_equivalent_R16                            &
  / Boltzmann_constant_R16
  ! 1.08095401916e13_R16

!> Atomic mass unit-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_kelvin_relationship_R8 =                             &
  real(atomic_mass_unit_kelvin_relationship_R16, kind=R8)

!> Atomic mass unit-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_kelvin_relationship_R4 =                             &
  real(atomic_mass_unit_kelvin_relationship_R16, kind=R4)

!> Atomic mass unit-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_electron_volt_relationship_R16 =                     &
  1.0E6_R16 * atomic_mass_constant_energy_equivalent_in_MeV_R16
  ! 9.3149410242e8_R16

!> Atomic mass unit-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_electron_volt_relationship_R8 =                      &
  real(atomic_mass_unit_electron_volt_relationship_R16, kind=R8)

!> Atomic mass unit-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_electron_volt_relationship_R4 =                      &
  real(atomic_mass_unit_electron_volt_relationship_R16, kind=R4)

!> Atomic mass unit-hartree relationship, E_h
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  atomic_mass_unit_hartree_relationship_R16 =                           &
  atomic_mass_constant_energy_equivalent_R16                            &
  / Hartree_energy_R16
  ! 3.4231776874e7_R16

!> Atomic mass unit-hartree relationship, E_h
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  atomic_mass_unit_hartree_relationship_R8 =                            &
  real(atomic_mass_unit_hartree_relationship_R16, kind=R8)

!> Atomic mass unit-hartree relationship, E_h
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  atomic_mass_unit_hartree_relationship_R4 =                            &
  real(atomic_mass_unit_hartree_relationship_R16, kind=R4)


!> Hartree-joule relationship, J
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_joule_relationship_R16 =                                      &
  Hartree_energy_R16
  ! 4.3597447222071e-18_R16

!> Hartree-joule relationship, J
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_joule_relationship_R8 =                                       &
  real(hartree_joule_relationship_R16, kind=R8)

!> Hartree-joule relationship, J
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_joule_relationship_R4 =                                       &
  real(hartree_joule_relationship_R16, kind=R4)

!> Hartree-kilogram relationship, kg
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_kilogram_relationship_R16 =                                   &
  Hartree_energy_R16                                                    &
  / (speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 4.8508702095432e-35_R16

!> Hartree-kilogram relationship, kg
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_kilogram_relationship_R8 =                                    &
  real(hartree_kilogram_relationship_R16, kind=R8)

!> Hartree-kilogram relationship, kg
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_kilogram_relationship_R4 =                                    &
  real(hartree_kilogram_relationship_R16, kind=R4)

!> Hartree-inverse meter relationship, m^-1
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_inverse_meter_relationship_R16 =                              &
  Hartree_energy_R16                                                    &
  / (Planck_constant_R16 * speed_of_light_in_vacuum_R16)
  ! 2.1947463136320e7_R16

!> Hartree-inverse meter relationship, m^-1
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_inverse_meter_relationship_R8 =                               &
  real(hartree_inverse_meter_relationship_R16, kind=R8)

!> Hartree-inverse meter relationship, m^-1
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_inverse_meter_relationship_R4 =                               &
  real(hartree_inverse_meter_relationship_R16, kind=R4)

!> Hartree-hertz relationship, Hz
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_hertz_relationship_R16 =                                      &
  Hartree_energy_R16 / Planck_constant_R16
  ! 6.579683920502e15_R16

!> Hartree-hertz relationship, Hz
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_hertz_relationship_R8 =                                       &
  real(hartree_hertz_relationship_R16, kind=R8)

!> Hartree-hertz relationship, Hz
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_hertz_relationship_R4 =                                       &
  real(hartree_hertz_relationship_R16, kind=R4)

!> Hartree-kelvin relationship, K
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_kelvin_relationship_R16 =                                     &
  Hartree_energy_R16 / Boltzmann_constant_R16
  ! 3.1577502480407e5_R16

!> Hartree-kelvin relationship, K
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_kelvin_relationship_R8 =                                      &
  real(hartree_kelvin_relationship_R16, kind=R8)

!> Hartree-kelvin relationship, K
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_kelvin_relationship_R4 =                                      &
  real(hartree_kelvin_relationship_R16, kind=R4)

!> Hartree-electron volt relationship, eV
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_electron_volt_relationship_R16 =                              &
  Hartree_energy_R16 / electron_volt_R16
  ! 27.211386245988_R16

!> Hartree-electron volt relationship, eV
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_electron_volt_relationship_R8 =                               &
  real(hartree_electron_volt_relationship_R16, kind=R8)

!> Hartree-electron volt relationship, eV
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_electron_volt_relationship_R4 =                               &
  real(hartree_electron_volt_relationship_R16, kind=R4)

!> Hartree-atomic mass unit relationship, u
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  hartree_atomic_mass_unit_relationship_R16 =                           &
  Hartree_energy_R16                                                    &
  / (unified_atomic_mass_unit_R16                                       &
  * speed_of_light_in_vacuum_R16 * speed_of_light_in_vacuum_R16)
  ! 2.92126232205e-8_R16

!> Hartree-atomic mass unit relationship, u
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  hartree_atomic_mass_unit_relationship_R8 =                            &
  real(hartree_atomic_mass_unit_relationship_R16, kind=R8)

!> Hartree-atomic mass unit relationship, u
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  hartree_atomic_mass_unit_relationship_R4 =                            &
  real(hartree_atomic_mass_unit_relationship_R16, kind=R4)

!> Shielding difference of d and p in HD, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielding_difference_of_d_and_p_in_HD_R16 =                           &
  2.0200e-8_R16

!> Shielding difference of d and p in HD, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielding_difference_of_d_and_p_in_HD_R8 =                            &
  real(shielding_difference_of_d_and_p_in_HD_R16, kind=R8)

!> Shielding difference of d and p in HD, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielding_difference_of_d_and_p_in_HD_R4 =                            &
  real(shielding_difference_of_d_and_p_in_HD_R16, kind=R4)

!> Shielding difference of t and p in HT, dimensionless
!! (at most 33 significant digits)
real(kind=R16), parameter, public ::                                    &
  shielding_difference_of_t_and_p_in_HT_R16 =                           &
  2.4140e-8_R16

!> Shielding difference of t and p in HT, dimensionless
!! (at most 15 significant digits)
real(kind=R8), parameter, public ::                                     &
  shielding_difference_of_t_and_p_in_HT_R8 =                            &
  real(shielding_difference_of_t_and_p_in_HT_R16, kind=R8)

!> Shielding difference of t and p in HT, dimensionless
!! (at most 6 significant digits)
real(kind=R4), parameter, public ::                                     &
  shielding_difference_of_t_and_p_in_HT_R4 =                            &
  real(shielding_difference_of_t_and_p_in_HT_R16, kind=R4)

! File statistics:
! Maximum variable length =     55
! Literal parameters      =    292
! Derived parameters      =     62
! Total parameters        =    354

contains

!> Derived unit checks
subroutine check()
  use, intrinsic :: ISO_FORTRAN_ENV, only: stdout => OUTPUT_UNIT
  implicit none

1 format(A)
! 2 format(ES44.35E4) ! +1.000000000000000000000000000000000000E+0000
3 format(A, T62, ES44.35E4, ' =? ', A)

  continue

  write(unit=stdout, fmt=1)                                             &
    '! ---- STDLIB_CODATA_2018 Derived Unit Checks ----'

  write(unit=stdout, fmt=3)                                             &
    'Planck_constant_in_eV_Hz_R16',                                     &
    Planck_constant_in_eV_Hz_R16,                                       &
    '4.135 667 696... e-15'

  write(unit=stdout, fmt=3)                                             &
    'reduced_Planck_constant_R16',                                      &
    reduced_Planck_constant_R16,                                        &
    '1.054 571 817... e-34'

  write(unit=stdout, fmt=3)                                             &
    'reduced_Planck_constant_in_eV_s_R16',                              &
    reduced_Planck_constant_in_eV_s_R16,                                &
    '6.582 119 569... e-16'

  write(unit=stdout, fmt=3)                                             &
    'elementary_charge_over_h_bar_R16',                                 &
    elementary_charge_over_h_bar_R16,                                   &
    '1.519 267 447... e15'

  write(unit=stdout, fmt=3)                                             &
    'magnetic_flux_quantum_R16',                                            &
    magnetic_flux_quantum_R16,                                              &
    '2.067 833 848... e-15'

  write(unit=stdout, fmt=3)                                             &
    'conductance_quantum_R16',                                          &
    conductance_quantum_R16,                                            &
    '7.748 091 729... e-5'

  write(unit=stdout, fmt=3)                                             &
    'inverse_of_conductance_quantum_R16',                               &
    inverse_of_conductance_quantum_R16,                                 &
    '12 906.403 72...'

  write(unit=stdout, fmt=3)                                             &
    'Josephson_constant_R16',                                           &
    Josephson_constant_R16,                                             &
    '483 597.848 4... e9'

  write(unit=stdout, fmt=3)                                             &
    'von_Klitzing_constant_R16',                                        &
    von_Klitzing_constant_R16,                                          &
    '25 812.807 45...'

  write(unit=stdout, fmt=3)                                             &
    'Faraday_constant_R16',                                             &
    Faraday_constant_R16,                                               &
    '96 485.332 12...'

  write(unit=stdout, fmt=3)                                             &
    'molar_Planck_constant_R16',                                        &
    molar_Planck_constant_R16,                                          &
    '3.990 312 712... e-10'

  write(unit=stdout, fmt=3)                                             &
    'molar_gas_constant_R16',                                           &
    molar_gas_constant_R16,                                             &
    '8.314 462 618...'

  write(unit=stdout, fmt=3)                                             &
    'Boltzmann_constant_in_eV_K_R16',                                   &
    Boltzmann_constant_in_eV_K_R16,                                     &
    '8.617 333 262... e-5'

  write(unit=stdout, fmt=3)                                             &
   'Boltzmann_constant_in_Hz_K_R16',                                    &
    Boltzmann_constant_in_Hz_K_R16,                                     &
    '2.083 661 912... e10'

  write(unit=stdout, fmt=3)                                             &
    'Boltzmann_constant_in_inverse_meter_per_kelvin_R16',               &
    Boltzmann_constant_in_inverse_meter_per_kelvin_R16,                 &
    '69.503 480 04'

  write(unit=stdout, fmt=3)                                             &
    'molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16',               &
    molar_volume_of_ideal_gas_273_15_K_101_325_kPa_R16,                 &
    '22.413 969 54... e-3'

  write(unit=stdout, fmt=3)                                             &
    'Loschmidt_constant_273_15_K_101_325_kPa_R16',                      &
    Loschmidt_constant_273_15_K_101_325_kPa_R16,                        &
    '2.686 780 111... e25'

  write(unit=stdout, fmt=3)                                             &
    'molar_volume_of_ideal_gas_273_15_K_100_kPa_R16',                   &
    molar_volume_of_ideal_gas_273_15_K_100_kPa_R16,                     &
    '22.710 954 64... e-3'

  write(unit=stdout, fmt=3)                                             &
    'Loschmidt_constant_273_15_K_100_kPa_R16',                          &
    Loschmidt_constant_273_15_K_100_kPa_R16,                            &
    '2.651 645 804... e25'

  write(unit=stdout, fmt=3)                                             &
    'Stefan_Boltzmann_constant_R16',                                    &
    Stefan_Boltzmann_constant_R16,                                      &
    '5.670 374 419... e-8'

  write(unit=stdout, fmt=3)                                             &
    'first_radiation_constant_R16',                                     &
    first_radiation_constant_R16,                                       &
    '3.741 771 852... e-16'

  write(unit=stdout, fmt=3)                                             &
    'first_radiation_constant_for_spectral_radiance_R16',               &
    first_radiation_constant_for_spectral_radiance_R16,                 &
    '1.191 042 972... e-16'

  write(unit=stdout, fmt=3)                                             &
    'second_radiation_constant_R16',                                    &
    second_radiation_constant_R16,                                      &
    '1.438 776 877... e-2'

  write(unit=stdout, fmt=3)                                             &
    'Wien_wavelength_displacement_law_constant_R16',                    &
    Wien_wavelength_displacement_law_constant_R16,                      &
    '2.897 771 955... e-3'

  write(unit=stdout, fmt=3)                                             &
    'Wien_frequency_displacement_law_constant_R16',                     &
    Wien_frequency_displacement_law_constant_R16,                       &
    '5.878 925 757... e10'



  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_velocity_R16',                                     &
    natural_unit_of_velocity_R16,                                       &
    '299792458_R16'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_action_R16',                                       &
    natural_unit_of_action_R16,                                         &
    '1.054 571 817... e-34'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_action_in_eV_s_R16',                               &
    natural_unit_of_action_in_eV_s_R16,                                 &
    '6.582 119 569... e-16'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_mass_R16',                                         &
    natural_unit_of_mass_R16,                                           &
    '9.1093837015e-31'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_energy_R16',                                       &
    natural_unit_of_energy_R16,                                         &
    '8.1871057769e-14'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_energy_in_MeV_R16',                                &
    natural_unit_of_energy_in_MeV_R16,                                  &
    '0.51099895000'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_momentum_R16',                                     &
    natural_unit_of_momentum_R16,                                       &
    '2.73092453075e-22'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_momentum_in_MeV_c_R16',                            &
    natural_unit_of_momentum_in_MeV_c_R16,                              &
    '0.51099895000'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_length_R16',                                       &
    natural_unit_of_length_R16,                                         &
    '3.8615926796e-13'

  write(unit=stdout, fmt=3)                                             &
    'natural_unit_of_time_R16',                                         &
    natural_unit_of_time_R16,                                           &
    '1.28808866819e-21'


  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_charge_R16',                                        &
    atomic_unit_of_charge_R16,                                          &
    '1.602176634e-19'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_mass_R16',                                          &
    atomic_unit_of_mass_R16,                                            &
    '9.1093837015e-31'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_action_R16',                                        &
    atomic_unit_of_action_R16,                                          &
    '1.054 571 817... e-34'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_length_R16',                                        &
    atomic_unit_of_length_R16,                                          &
    '5.29177210903e-11'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_energy_R16',                                        &
    atomic_unit_of_energy_R16,                                          &
    '4.3597447222071e-18'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_time_R16',                                          &
    atomic_unit_of_time_R16,                                            &
    '2.4188843265857e-17'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_force_R16',                                         &
    atomic_unit_of_force_R16,                                           &
    '8.2387234983e-8'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_velocity_R16',                                      &
    atomic_unit_of_velocity_R16,                                        &
    '2.18769126364e6'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_momentum_R16',                                      &
    atomic_unit_of_momentum_R16,                                        &
    '1.99285191410e-24'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_current_R16',                                       &
    atomic_unit_of_current_R16,                                         &
    '6.623618237510e-3'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_charge_density_R16',                                &
    atomic_unit_of_charge_density_R16,                                  &
    '1.08120238457e12'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_electric_potential_R16',                            &
    atomic_unit_of_electric_potential_R16,                              &
    '27.211386245988'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_electric_field_R16',                                &
    atomic_unit_of_electric_field_R16,                                  &
    '5.14220674763e11'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_electric_field_gradient_R16',                       &
    atomic_unit_of_electric_field_gradient_R16,                         &
    '9.7173624292e21'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_electric_dipole_moment_R16',                        &
    atomic_unit_of_electric_dipole_moment_R16,                          &
    '8.4783536255e-30'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_electric_quadrupole_moment_R16',                    &
    atomic_unit_of_electric_quadrupole_moment_R16,                      &
    '4.4865515246e-40'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_electric_polarizability_R16',                       &
    atomic_unit_of_electric_polarizability_R16,                         &
    '1.64877727436e-41'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_1st_hyperpolarizability_R16',                       &
    atomic_unit_of_1st_hyperpolarizability_R16,                         &
    '3.2063613061e-53'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_2nd_hyperpolarizability_R16',                       &
    atomic_unit_of_2nd_hyperpolarizability_R16,                         &
    '6.2353799905e-65'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_magnetic_flux_density_R16',                         &
    atomic_unit_of_magnetic_flux_density_R16,                           &
    '2.35051756758e5'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_magnetic_dipole_moment_R16',                        &
    atomic_unit_of_magnetic_dipole_moment_R16,                          &
    '1.85480201566e-23'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_magnetizability_R16',                               &
    atomic_unit_of_magnetizability_R16,                                 &
    '7.8910366008e-29'

  write(unit=stdout, fmt=3)                                             &
    'atomic_unit_of_permittivity_R16',                                  &
    atomic_unit_of_permittivity_R16,                                    &
    '1.11265005545e-10'


  write(unit=stdout, fmt=3)                                             &
    'conventional_value_of_ampere_90_R16',                              &
    conventional_value_of_ampere_90_R16,                                &
    '1.000 000 088 87...'

  write(unit=stdout, fmt=3)                                             &
    'conventional_value_of_coulomb_90_R16',                             &
    conventional_value_of_coulomb_90_R16,                               &
    '1.000 000 088 87...'

  write(unit=stdout, fmt=3)                                             &
    'conventional_value_of_farad_90_R16',                               &
    conventional_value_of_farad_90_R16,                                 &
    '0.999 999 982 20...'

  write(unit=stdout, fmt=3)                                             &
    'conventional_value_of_henry_90_R16',                               &
    conventional_value_of_henry_90_R16,                                 &
    '1.000 000 017 79..._R16'

  write(unit=stdout, fmt=3)                                             &
    'conventional_value_of_ohm_90_R16',                                 &
    conventional_value_of_ohm_90_R16,                                   &
    '1.000 000 017 79...'


  write(unit=stdout, fmt=3)                                             &
    'hertz_inverse_meter_relationship_R16',                             &
    hertz_inverse_meter_relationship_R16,                               &
    '3.335 640 951... e-9'

  write(unit=stdout, fmt=3)                                             &
    'hertz_kelvin_relationship_R16',                                    &
    hertz_kelvin_relationship_R16,                                      &
    '4.799 243 073... e-11'

  write(unit=stdout, fmt=3)                                             &
    'hertz_kilogram_relationship_R16',                                  &
    hertz_kilogram_relationship_R16,                                    &
    '7.372 497 323... e-51'


  write(unit=stdout, fmt=3)                                             &
    'joule_kilogram_relationship_R16',                                  &
    joule_kilogram_relationship_R16,                                    &
    '1.112 650 056... e-17'

  write(unit=stdout, fmt=3)                                             &
    'joule_inverse_meter_relationship_R16',                             &
    joule_inverse_meter_relationship_R16,                               &
    '5.034 116 567... e24'

  write(unit=stdout, fmt=3)                                             &
    'joule_hertz_relationship_R16',                                     &
    joule_hertz_relationship_R16,                                       &
    '1.509 190 179... e33'

  write(unit=stdout, fmt=3)                                             &
    'joule_kelvin_relationship_R16',                                    &
    joule_kelvin_relationship_R16,                                      &
    '7.242 970 516... e22'

  write(unit=stdout, fmt=3)                                             &
    'joule_electron_volt_relationship_R16',                             &
    joule_electron_volt_relationship_R16,                               &
    '6.241 509 074... e18'

  write(unit=stdout, fmt=3)                                             &
    'joule_atomic_mass_unit_relationship_R16',                          &
    joule_atomic_mass_unit_relationship_R16,                            &
    '6.700 535 2565... e9'

  write(unit=stdout, fmt=3)                                             &
    'joule_hartree_relationship_R16',                                   &
    joule_hartree_relationship_R16,                                     &
    '2.293 712 278 3963... e17'


  write(unit=stdout, fmt=3)                                             &
    'kilogram_joule_relationship_R16',                                  &
    kilogram_joule_relationship_R16,                                    &
    '8.987 551 787... e16'

  write(unit=stdout, fmt=3)                                             &
    'kilogram_inverse_meter_relationship_R16',                          &
    kilogram_inverse_meter_relationship_R16,                            &
    '4.524 438 335... e41'

  write(unit=stdout, fmt=3)                                             &
    'kilogram_hertz_relationship_R16',                                  &
    kilogram_hertz_relationship_R16,                                    &
    '1.356 392 489... e50'

  write(unit=stdout, fmt=3)                                             &
    'kilogram_kelvin_relationship_R16',                                 &
    kilogram_kelvin_relationship_R16,                                   &
    '6.509 657 260... e39'

  write(unit=stdout, fmt=3)                                             &
    'kilogram_electron_volt_relationship_R16',                          &
    kilogram_electron_volt_relationship_R16,                            &
    '5.609 588 603... e35'

  write(unit=stdout, fmt=3)                                             &
    'kilogram_atomic_mass_unit_relationship_R16',                       &
    kilogram_atomic_mass_unit_relationship_R16,                         &
    '6.022 140 7621... e26'

  write(unit=stdout, fmt=3)                                             &
    'kilogram_hartree_relationship_R16',                                &
    kilogram_hartree_relationship_R16,                                  &
    '2.061 485 788 7409... e34'


  write(unit=stdout, fmt=3)                                             &
    'inverse_meter_joule_relationship_R16',                             &
    inverse_meter_joule_relationship_R16,                               &
    '1.986 445 857... e-25'

  write(unit=stdout, fmt=3)                                             &
    'inverse_meter_kilogram_relationship_R16',                          &
    inverse_meter_kilogram_relationship_R16,                            &
    '2.210 219 094... e-42'

  write(unit=stdout, fmt=3)                                             &
    'inverse_meter_hertz_relationship_R16',                             &
    inverse_meter_hertz_relationship_R16,                               &
    '299 792 458'

  write(unit=stdout, fmt=3)                                             &
    'inverse_meter_kelvin_relationship_R16',                            &
    inverse_meter_kelvin_relationship_R16,                              &
    '1.438 776 877... e-2'

  write(unit=stdout, fmt=3)                                             &
    'inverse_meter_electron_volt_relationship_R16',                     &
    inverse_meter_electron_volt_relationship_R16,                       &
    '1.239 841 984... e-6'

  write(unit=stdout, fmt=3)                                             &
    'inverse_meter_atomic_mass_unit_relationship_R16',                  &
    inverse_meter_atomic_mass_unit_relationship_R16,                    &
    '1.331 025 050 10... e-15'

  write(unit=stdout, fmt=3)                                             &
    'inverse_meter_hartree_relationship_R16',                           &
    inverse_meter_hartree_relationship_R16,                             &
    '4.556 335 252 9120... e-8'


  write(unit=stdout, fmt=3)                                             &
    'hertz_joule_relationship_R16',                                     &
    hertz_joule_relationship_R16,                                       &
    '6.626 070 15... e−34'

  write(unit=stdout, fmt=3)                                             &
    'hertz_kilogram_relationship_R16',                                  &
    hertz_kilogram_relationship_R16,                                    &
    '7.372 497 323... e-51'

  write(unit=stdout, fmt=3)                                             &
    'hertz_inverse_meter_relationship_R16',                             &
    hertz_inverse_meter_relationship_R16,                               &
    '3.335 640 951 ... e-9'

  write(unit=stdout, fmt=3)                                             &
    'hertz_kelvin_relationship_R16',                                    &
    hertz_kelvin_relationship_R16,                                      &
    '4.799 243 073... e-11'

  write(unit=stdout, fmt=3)                                             &
    'hertz_electron_volt_relationship_R16',                             &
    hertz_electron_volt_relationship_R16,                               &
    '4.135 667 696... e-15'

  write(unit=stdout, fmt=3)                                             &
    'hertz_atomic_mass_unit_relationship_R16',                          &
    hertz_atomic_mass_unit_relationship_R16,                            &
    '4.439 821 6652... e-24'

  write(unit=stdout, fmt=3)                                             &
    'hertz_hartree_relationship_R16',                                   &
    hertz_hartree_relationship_R16,                                     &
    '1.519 829 846 0570... e-16'


  write(unit=stdout, fmt=3)                                             &
    'kelvin_joule_relationship_R16',                                    &
    kelvin_joule_relationship_R16,                                      &
    '1.380 649... e−23'

  write(unit=stdout, fmt=3)                                             &
    'kelvin_kilogram_relationship_R16',                                 &
    kelvin_kilogram_relationship_R16,                                   &
    '1.536 179 187... e-40'

  write(unit=stdout, fmt=3)                                             &
    'kelvin_inverse_meter_relationship_R16',                            &
    kelvin_inverse_meter_relationship_R16,                              &
    '69.503 480 04 ...'

  write(unit=stdout, fmt=3)                                             &
    'kelvin_hertz_relationship_R16',                                    &
    kelvin_hertz_relationship_R16,                                      &
    '2.083 661 912... e10'

  write(unit=stdout, fmt=3)                                             &
    'kelvin_electron_volt_relationship_R16',                            &
    kelvin_electron_volt_relationship_R16,                              &
    '8.617 333 262... e-5'

  write(unit=stdout, fmt=3)                                             &
    'kelvin_atomic_mass_unit_relationship_R16',                         &
    kelvin_atomic_mass_unit_relationship_R16,                           &
    '9.251 087 3014... e-14'

  write(unit=stdout, fmt=3)                                             &
    'kelvin_hartree_relationship_R16',                                  &
    kelvin_hartree_relationship_R16,                                    &
    '3.166 811 563 4556... e-6'


  write(unit=stdout, fmt=3)                                             &
    'electron_volt_joule_relationship_R16',                             &
    electron_volt_joule_relationship_R16,                               &
    '1.602 176 634... e-19'

  write(unit=stdout, fmt=3)                                             &
    'electron_volt_kilogram_relationship_R16',                          &
    electron_volt_kilogram_relationship_R16,                            &
    '1.782 661 921... e-36'

  write(unit=stdout, fmt=3)                                             &
    'electron_volt_inverse_meter_relationship_R16',                     &
    electron_volt_inverse_meter_relationship_R16,                       &
    '8.065 543 937... e5'

  write(unit=stdout, fmt=3)                                             &
    'electron_volt_hertz_relationship_R16',                             &
    electron_volt_hertz_relationship_R16,                               &
    '2.417 989 242... e14'

  write(unit=stdout, fmt=3)                                             &
    'electron_volt_kelvin_relationship_R16',                            &
    electron_volt_kelvin_relationship_R16,                              &
    '1.160 451 812... e4'

  write(unit=stdout, fmt=3)                                             &
    'electron_volt_atomic_mass_unit_relationship_R16',                  &
    electron_volt_atomic_mass_unit_relationship_R16,                    &
    '1.073 544 102 33... e-9'

  write(unit=stdout, fmt=3)                                             &
    'electron_volt_hartree_relationship_R16',                           &
    electron_volt_hartree_relationship_R16,                             &
    '3.674 932 217 5655... e-2'


  write(unit=stdout, fmt=3)                                             &
    'atomic_mass_unit_joule_relationship_R16',                          &
    atomic_mass_unit_joule_relationship_R16,                            &
    '1.492 418 085 60... e-10'

  write(unit=stdout, fmt=3)                                             &
    'atomic_mass_unit_kilogram_relationship_R16',                       &
    atomic_mass_unit_kilogram_relationship_R16,                         &
    '1.660 539 066 60... e-27'

  write(unit=stdout, fmt=3)                                             &
    'atomic_mass_unit_inverse_meter_relationship_R16',                  &
    atomic_mass_unit_inverse_meter_relationship_R16,                    &
    '7.513 006 6104... e14'

  write(unit=stdout, fmt=3)                                             &
    'atomic_mass_unit_hertz_relationship_R16',                          &
    atomic_mass_unit_hertz_relationship_R16,                            &
    '2.252 342 718 71... e14'

  write(unit=stdout, fmt=3)                                             &
    'atomic_mass_unit_kelvin_relationship_R16',                         &
    atomic_mass_unit_kelvin_relationship_R16,                           &
    '1.080 954 019 16... e13'

  write(unit=stdout, fmt=3)                                             &
    'atomic_mass_unit_electron_volt_relationship_R16',                  &
    atomic_mass_unit_electron_volt_relationship_R16,                    &
    '9.314 941 0242... e8'

  write(unit=stdout, fmt=3)                                             &
    'atomic_mass_unit_hartree_relationship_R16',                        &
    atomic_mass_unit_hartree_relationship_R16,                          &
    '3.423 177 6874... e7'


  write(unit=stdout, fmt=3)                                             &
    'hartree_joule_relationship_R16',                                   &
    hartree_joule_relationship_R16,                                     &
    '4.359 744 722 2071... e-18'

  write(unit=stdout, fmt=3)                                             &
    'hartree_kilogram_relationship_R16',                                &
    hartree_kilogram_relationship_R16,                                  &
    '4.850 870 209 5432... e-35'

  write(unit=stdout, fmt=3)                                             &
    'hartree_inverse_meter_relationship_R16',                           &
    hartree_inverse_meter_relationship_R16,                             &
    '2.194 746 313 6320... e7'

  write(unit=stdout, fmt=3)                                             &
    'hartree_hertz_relationship_R16',                                   &
    hartree_hertz_relationship_R16,                                     &
    '6.579 683 920 502... e15'

  write(unit=stdout, fmt=3)                                             &
    'hartree_kelvin_relationship_R16',                                  &
    hartree_kelvin_relationship_R16,                                    &
    '3.157 750 248 0407... e5'

  write(unit=stdout, fmt=3)                                             &
    'hartree_electron_volt_relationship_R16',                           &
    hartree_electron_volt_relationship_R16,                             &
    '27.211 386 245 988...'

  write(unit=stdout, fmt=3)                                             &
    'hartree_atomic_mass_unit_relationship_R16',                        &
    hartree_atomic_mass_unit_relationship_R16,                          &
    '2.921 262 322 05... e-8'

    return
end subroutine check

end module STDLIB_CODATA_2018