# stdlib-codata

A multi-precision versioned archive of fundamental physical constants from CODATA for use with modern Fortran

## Status

This module is under active development and many sections still require documentation and testing.
`stdlib_codata_2018` is believed complete, tested, and documented. Other datasets are being reviewed for correctness and consistency with the naming convention set by `stdlib_codata_2018`

## Design Goals

The most recent standard set of fundamental physical constants should be available in one or more Fortran modules.

The data should be documented and reviewed to a degree suitable for use in safety-significant engineering applications. Note that no warranty is expressed or implied, however best-effort software engineering practice will be applied in the library's construction and review.

The modules should incur no run-time overhead; all calculations or derivations should be resolvable at compile-time.

Constants should be provided in single, double, and quad precision.

For historical and legacy software support, as many datasets should be supported as practical.

The module for each dataset should be a faithful reproduction of that dataset, complete, and with minimal additional constants provided.

Constants should be named sensibly and consistently.

Constants should be stored in maximal precision and cast to less precise types to preserve precision.

Constants are listed in an order similar to that given in the original human-readable source documents to simplify review.

Constant names are derived from CODATA 2018. Constant names in previous datasets may be adjusted to conform with CODATA 2018 terminology.

All derived constants are calculated from base constants, possibly including non-CODATA numerical constants or well-defined convenience constants. No effort is made to ensure every permutation of physical unit is supported for all constants.

Consistency among modules is preferred to minimize effort and risk upgrading from one dataset to a later one. New constants are added in each new CODATA release and there is no guarantee that constants in a later dataset are available in an earlier set. With limited exceptions, the previous dataset is a subset of the current dataset. For example, CODATA 1986 can be expected to contain most or all of CODATA 1973 however there are constants in CODATA 1986 which are not listed in CODATA 1973.

In some cases, casting higher precision to lower precision types may result in overflow or underflow. If the lower precision version of a constant will overflow or underflow, it is removed from the dataset, typically commented out with an explanation of the numerical exception caused. Precision loss resulting from down-casting is acceptable provided underflow does not occur.

At present, the precision of a constant is indicated by the suffix `_R16`, `_R8`, and `_R4`, shorthand for standard floating point kinds `REAL128`, `REAL64` and `REAL32` respectively. This should probably change before public release.

Ambiguity is considered a greater risk to code correctness than verbosity. In usage, it is expected that the user will create their own constants which take the values of long-named constants in the CODATA modules or will rename long-valued constants on import (**aliasing**). See the example application for several possible implementations of this concept.

CODATA 2018 names are preferred to historical names. It is not expected that constants in past datasets will be renamed based on later releases of CODATA. Constants in datasets earlier than CODATA 2018 were modified for consistency for initial release. Going forward there is no expectation to rename constants in historical datasets; the initial goal was to avoid having too many aliases for constants in historical datasets and provide baseline consistency with CODATA 2018. Aliases and synonyms may be necessary in the future to allow backward compatibility; they are not considered necessary for the initial release since backward compatibility is not a concern.

Uncertainty in values was not tracked or used in any way. This decision may be revisited.

CMake is used as the principal build automation tool and the library should build, test, and package on recent Windows, Linux, and MacOS systems.

Doxygen is used as the principal documentation tool since it can produce formal documentation in PDF format via LaTeX. Formal documentation in PDF format is required for engineering review and verification. Before suggesting the project change to an alternate documentation system, ensure the alternate system can produce publication-quality PDF output. At present this is not considered negotiable.

## Example Usage

A typical use case is illustrated in `./example` by the main program code `migration_example.f90` and the module files `m_constants_*.f90`. The main program code contains four routines which only differ in what modules and constants are imported - the variable declarations and executable body of each subroutine should be identical. This example is an idealized situation where constants and precision of legacy code are gradually upgraded with time.

`m_constants_1973_sp.f90` defines the local application's name of each constant by renaming variables imported from `stdlib_codata_1973`, the first set of adjusted constants released by CODATA. The declaration of `GRAV` shows how constants missing from a dataset may be defined locally within `m_constants_1973_sp.f90` and exported to the main application. This allows the main application to rely on a common set of sensibly named constants without needing to be concerned whether the data is locally defined or imported. Note that `m_constants_1973_sp` is default public, meaning all imported elements are visible as exports. Here all constants are imported as single precision values and `WP` (working precision) is set to `REAL32` and available for export.

`m_constants_2006_dp.f90` differs from `m_constants_1973_sp.f90` by defining `WP` as `REAL64` (doube precision) and importing double precision constants from `stdlib_codata_2006`. Like the previous module, it is default public and can export all variables it imports.

`m_constants_2006_dp_private.f90` also defines `WP` as `REAL64` (doube precision) and imports double precision constants from `stdlib_codata_2006`. However this module is set to `private` and exported elements must be specifically set to `public`. Here a number of local constants are defined and take their values from constants imported from `stdlib_codata_2006`; long-named CODATA 2006 constants and the working precision `WP` are not avai or lable for export. This illustrates how access control can effectively guard against unintential exports and provide a controlled, consistent interface to the application.

Finally, `m_constants_2018_dp_private.f90` provides updated double precision constants from `stdlib_codata_2018`. Note that `standard_acceleration_of_gravity_R8` is now provided by CODATA 2018; the value of `GRAV` exported by `m_constants_2018_dp_private` uses this constant value rather than a local literal as in the other `m_constants_*` modules.

This shows how a local module may act as a wrapper around `stdlib_codata` modules, how datasets and constants of specific precision can be imported, and that there are multiple ways to control what is exported from the local module. It also shows that the long names used in the `stdlib_codata` modules can be effectively masked by a local module to give application programmers a more familiar notation or conform to legacy usage.

Looking at the results of the example application, we can see very minor changes in results as expected. The effect of changing from single to double precision is seen in the entry for fission energy, calculated by `dE = dM * c**2` were `dE` is energy release in J, `dM` is mass difference in kg, and `c` is the speed of light in vacuum in m/s. The speed of light is identical in each case (review `speed_of_light_in_vacuum_R16` in each of the CODATA modules). The only difference is that `chem1973` is single precision while all other implementing routines are in double precision. Similar results are seen for coolant launch weight. Coolant mass and acceleration due to gravity are identical in all four cases but `chem1973` calculates results in single precision while the other routines calculate in double precision. The presentation is somewhat misleading since 12 digis of precision are displayed even though single precision can only provide 7 digits of precision; the low order digits are effectively noise in the `chem1973` results. This is consistent with the results seen for fission energy and coolant launch weight.

Note that results from `chem2006a` and `chem2006b` are identical. That is expected since the only difference between `m_constants_2006_dp.f90` and `m_constants_2006_dp_private.f90`is internal structure and access control. The values assigned to each constant remain the same.

Minor differences between expansion volume and frequency are seen between `chem2006b` and `chem2018` due to differences in the molar gas constant and the reduced Planck constant from CODATA 2006 to CODATA 2018. In the redefinition of SI in CODATA 2018, the molar gas constant is a derived constant defined as the product of Boltzmann's constant and Avogadro's number. In previous CODATA releases, the molar gas constant is given as a literal measured value with some uncertainty. The results for RGAS check show the ratio of molar gas constant to the product of Boltzmann's constant and Avogadro's number as provided by CODATA. A ratio of 1 in `chem2018` clearly shows the 2018 value of R to be derived from k and Na. The check ratio for 1973 and 2006 show the effect of the limited precision of measured quantities, although the 1973 results may be partly attributable to single vs double precision calculation. This shows the limitations inherent in the CODATA recommended values. Measured constants are often not known to more than 6-12 digits of precision, far less than the resolution of `REAL128` types. This is simply a reminder to recall the computational distinction between accuracy and precision. This lack of exactness is often not relevant in engineering calculations; in high precision scientific applications, other techniques may be applicable. As usual, it is the application developer's responsibility to ensure their results are of the expected or required accuracy; part of that involves understanding the limitations and intent of the underlying CODATA datasets.

Finally, for simplicity `migration_example.f90` is compiled against `stdlib_codata_*.f90` source files. Results are not expected to differ if `stdlib_codata` is built as a static or shared library and then linked to the application. See the target `${MIGRATION_NAME}` in `CMakeLists.txt` for details on compilation.

## Availability

The project is currently hosted at https://gitlab.com/apthorpe/stdlib-codata

## Contibutor Guidance

Potential contributors are expected to treat each other with respect and dignity; see `CODE_OF_CONDUCT.md` for details.

With that out of the way, I would appreciate issue reports, test results, and suggestions for improvement at https://gitlab.com/apthorpe/stdlib-codata/-/issues

## License

Copyright (c) 2021 Bob Apthorpe, released under the MIT License. Some utility and `contrib` code is derived from code licensed under LGPLv3, specifically code under `./utility/lambert_w_128` and `./contrib/lambert_w_128` used for calculating Wien Displacement Law constants.
