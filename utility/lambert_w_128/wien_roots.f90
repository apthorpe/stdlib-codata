!> Find roots of Lambert W function for calculating Wien Displacement
!! Law constants
program wien_roots
  use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL128, IP => INT32, &
    stdout => OUTPUT_UNIT
  use m_lambert_w, only: wapr, bisect!, crude,

  implicit none

  real(kind=WP) :: x
  real(kind=WP) :: w
  integer(kind=IP) :: ibranch
  integer(kind=IP) :: ierror
  integer(kind=IP) :: ioffset

  integer(kind=IP) :: ict

  real(kind=WP) :: xprev
  real(kind=WP) :: xlo
  real(kind=WP) :: xhi
  real(kind=WP) :: dx

  real(kind=WP) :: wprev
  real(kind=WP) :: wlo
  real(kind=WP) :: whi
  real(kind=WP) :: dw

  real(kind=WP) :: tol

! 1 format(A, ES18.11)
1 format(A, ES44.35)

  continue

  ibranch = 0
  ioffset = 0

  ! Wavelength root

  ! Find by direct W function method

  x = -5.0_WP * exp(-5.0_WP)

  w = 0.0_WP
  ierror = 0

  write(unit=stdout, fmt=1) 'X                                  = ', x
  w = wapr(x, ibranch, ierror, ioffset)

  if (ierror == 0) then
    write(unit=stdout, fmt=1) 'W(X) {direct, upper, no offset}    = ', w
    write(unit=stdout, fmt=1) 'Solution: 5 + W(X)                 = ',  &
      5.0_WP + w
  else
    write(unit=stdout, fmt=1)                                           &
    'W(X) {upper, no offset} did not converge for X = ', x
  end if

  ! Find by alternate W function method

  w = bisect(x, ibranch, ierror, ioffset)

  if (ierror == 0) then
    write(unit=stdout, fmt=1) 'W(X) {bisection, upper, no offset} = ', w
    write(unit=stdout, fmt=1) 'Solution: 5 + W(X)                 = ',  &
      5.0_WP + w
  else
    write(unit=stdout, fmt=1)                                           &
    'W(X) {upper, no offset} did not converge for X = ', x
  end if

  ! Find by bisection

  xlo = 4.965_WP
  xhi = 4.966_WP

  wlo = f_wavelength(xlo)
  whi = f_wavelength(xhi)

  ict = 0_IP
  dx = xhi - xlo
  dw = whi - wlo
  tol = 1000.0_WP * tiny(1.0_WP)

  xprev = xlo
  wprev = wlo
  do while (dx > tol .and. dw > tol)
    x = 0.5_WP * (xhi + xlo)
    w = f_wavelength(x)
    if (w < 0.0_WP) then
      xlo = x
      wlo = w
    else if (w > 0.0_WP) then
      xhi = x
      whi = w
    else
      write(unit=stdout, fmt=1)                                     &
        'f_wavelength(X) exactly converged at X = ', x
      exit
    end if

    if(xprev == x .or. wprev == w) then
      write(unit=stdout, fmt=1)                                     &
        'Quitting; making no progress at X  = ', x
      exit
    end if

    dx = xhi - xlo
    dw = abs(whi - wlo)
    xprev = x
    wprev = w

    ! ict = ict + 1_IP
    ! if (mod(ict, 10_IP) == 0_IP) then
    !   write(unit=stdout, fmt=1) '. X = ', x
    !   write(unit=stdout, fmt=1) '. f_wavelength(X) = ', w
    !   write(unit=stdout, fmt=1) '. dX = ', dx
    !   write(unit=stdout, fmt=1) '. df = ', dw
    ! end if
  end do
  write(unit=stdout, fmt=1)                                         &
    'Root of f_wavelength(X) found at X = ', x

  write(unit=stdout, fmt=*)


  ! Frequency root

  ! Find by direct W function method

  x = -3.0_WP * exp(-3.0_WP)

  w = 0.0_WP
  ierror = 0

  write(unit=stdout, fmt=1) 'X                                  = ', x
  w = wapr(x, ibranch, ierror, ioffset)

  if (ierror == 0) then
    write(unit=stdout, fmt=1) 'W(X) {direct, upper, no offset}    = ', w
    write(unit=stdout, fmt=1) 'Solution: 3 + W(X)                 = ',  &
      3.0_WP + w
  else
    write(unit=stdout, fmt=1)                                           &
      'W(X) {upper, no offset} did not converge for X = ', x
  end if

  ! Find by alternate W function method

  w = bisect(x, ibranch, ierror, ioffset)

  if (ierror == 0) then
    write(unit=stdout, fmt=1) 'W(X) {bisection, upper, no offset} = ', w
    write(unit=stdout, fmt=1) 'Solution: 3 + W(X)                 = ',  &
      3.0_WP + w
  else
    write(unit=stdout, fmt=1)                                           &
      'W(X) {upper, no offset} did not converge for X = ', x
  end if

  ! Find by bisection

  xlo = 2.821_WP
  xhi = 2.822_WP

  wlo = f_frequency(xlo)
  whi = f_frequency(xhi)

  ict = 0_IP
  dx = xhi - xlo
  dw = whi - wlo
  tol = 1000.0_WP * tiny(1.0_WP)

  xprev = xlo
  wprev = wlo
  do while (dx > tol .and. dw > tol)
    x = 0.5_WP * (xhi + xlo)
    w = f_frequency(x)
    if (w < 0.0_WP) then
      xlo = x
      wlo = w
    else if (w > 0.0_WP) then
      xhi = x
      whi = w
    else
      write(unit=stdout, fmt=1)                                     &
        'f_frequency(X) exactly converged at X = ', x
      exit
    end if

    if(xprev == x .or. wprev == w) then
      write(unit=stdout, fmt=1)                                     &
        'Quitting; making no progress at X  = ', x
      exit
    end if

    dx = xhi - xlo
    dw = abs(whi - wlo)
    xprev = x
    wprev = w

    ! ict = ict + 1_IP
    ! if (mod(ict, 10_IP) == 0_IP) then
    !   write(unit=stdout, fmt=1) '. X = ', x
    !   write(unit=stdout, fmt=1) '. f_frequency(X) = ', w
    !   write(unit=stdout, fmt=1) '. dX = ', dx
    !   write(unit=stdout, fmt=1) '. df = ', dw
    ! end if
  end do
  write(unit=stdout, fmt=1)                                         &
    'Root of f_frequency(X) found at X  = ', x

contains

pure real(kind=WP) function f_wavelength(x) result(f)
  implicit none
  real(kind=WP), intent(in) :: x
  continue
  f = (x - 5.0_WP) * exp(x) + 5.0_WP
  return
end function f_wavelength

real(kind=WP) function f_frequency(x) result(f)
  implicit none
  real(kind=WP), intent(in) :: x
  continue
  f = (x - 3.0_WP) * exp(x) + 3.0_WP
  return
end function f_frequency

end program wien_roots