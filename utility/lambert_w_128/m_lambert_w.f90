!> Modularization and quad-precision extension of John Burkardt's F90
!! version of the Lambert W function calculator originally in F77 by
!! Andrew Barry, S. J. Barry, Patricia Culligan-Hensley.
!!
!! WP represents 'working precision'; set WP => REAL64 for 64-bit
!! (double precision) or WP => REAL64 for 128-bit (quad precision)
!!
!! This code is distributed under the GNU LGPL license.
module m_lambert_w
use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL128, IP => INT32

private

public :: bisect
public :: crude
public :: wapr

!
!  Various mathematical constants.
!

!> Numerical constant 0
real(kind=WP), parameter :: ZERO = 0.0_WP

!> Numerical constant 0.001
real(kind=WP), parameter :: TINY1M3 = 1.0E-3_WP

!> Numerical constant 0.002
real(kind=WP), parameter :: TINY2M3 = 2.0E-3_WP

!> Numerical constant 1/2
real(kind=WP), parameter :: HALF = 0.5_WP

!> Numerical constant 1
real(kind=WP), parameter :: ONE = 1.0_WP

!> Numerical constant 2
real(kind=WP), parameter :: TWO = 2.0_WP

!> Numerical constant 3
real(kind=WP), parameter :: THREE = 3.0_WP

!> Numerical constant 4
real(kind=WP), parameter :: FOUR = 4.0_WP

!> Numerical constant 6
real(kind=WP), parameter :: SIX = 6.0_WP

!> Numerical constant 7
real(kind=WP), parameter :: SEVEN = 7.0_WP

!> Numerical constant 8
real(kind=WP), parameter :: EIGHT = 8.0_WP

!> Numerical constant 9
real(kind=WP), parameter :: NINE = 9.0_WP

!> Numerical constant -1 / e
real(kind=WP), parameter :: em = -exp ( -ONE )

!> Numerical constant -e**-9
real(kind=WP), parameter :: em9 = -exp ( -NINE )

!> Numerical constant 1/3
real(kind=WP), parameter :: c13 = ONE / THREE

!> Numerical constant 2/3
real(kind=WP), parameter :: c23 = TWO / THREE

!> Numerical constant -2e
real(kind=WP), parameter :: em2 = TWO / em

!> Numerical constant 2e
real(kind=WP), parameter :: d12 = -em2

!> Numerical constant 2/3
real(kind=WP), parameter :: an3 = EIGHT / THREE

!> Numerical constant 135/83
real(kind=WP), parameter :: an4 = 135.0_WP / 83.0_WP

!> Numerical constant 166/39
real(kind=WP), parameter :: an5 = 166.0_WP / 39.0_WP

!> Numerical constant 3167/3549
real(kind=WP), parameter :: an6 = 3167.0_WP / 3549.0_WP

!> Numerical constant sqrt(2)
real(kind=WP), parameter :: s2 = sqrt ( TWO )

!> Numerical constant 2 sqrt(2) - 3
real(kind=WP), parameter :: s21 = TWO * s2 - THREE

!> Numerical constant 4 - 3 sqrt(2)
real(kind=WP), parameter :: s22 = FOUR - THREE * s2

!> Numerical constant sqrt(2) - 2
real(kind=WP), parameter :: s23 = s2 - TWO

!> Derives precision information needed by wapr()
type :: precision_monitor
  !> Number of bits in significand minus one
  integer(kind=IP) :: nbits = 0

  !> Number of iterations to perform in wapr
  integer(kind=IP) :: niter = 1

  !> Mystery parameter tb
  real(kind=WP) :: tb = ZERO

  !> Mystery parameter tb2
  real(kind=WP) :: tb2 = ZERO

  !> Mystery parameter x0
  real(kind=WP) :: x0 = ZERO

  !> Mystery parameter x1
  real(kind=WP) :: x1 = ZERO
contains
  procedure :: compute_nbits => precision_monitor_compute_nbits
  procedure :: init => precision_monitor_init
end type precision_monitor

!> Module precision monitor object
type(precision_monitor) :: pm

contains

!> Object wrapper experimentally determines (number of bits) - 1 in
!! the significand of type WP
subroutine precision_monitor_compute_nbits(this)
  implicit none

  !> Object reference
  class(precision_monitor), intent(inout) :: this

  real(kind=WP) :: b
  real(kind=WP) :: v

  continue

  this%nbits = 0

  b = ONE

  do

    b = b / TWO
    v = b + ONE

    if ( v == ONE ) then
      exit
    end if

    this%nbits = this%nbits + 1
  end do

  return
end subroutine precision_monitor_compute_nbits

!> Initialize precision parameters
subroutine precision_monitor_init(this)
  implicit none
  !> Object reference
  class(precision_monitor), intent(inout) :: this
  continue

  call this%compute_nbits()

  if ( 56_IP <= pm%nbits ) then
    pm%niter = 2_IP
  end if

  this%tb = HALF ** this%nbits

  this%tb2 = sqrt ( this%tb )

  this%x0 = this%tb ** ( ONE / SIX ) * HALF

  this%x1 = ( ONE - 17.0_WP * this%tb ** ( TWO / SEVEN ) ) * em

  return
end subroutine precision_monitor_init

!> Approximates the Lambert W function using bisection
function bisect ( xx, nb, ner, l )
!*****************************************************************************80
!
!  BISECT approximates the W function using bisection.
!
!  Discussion:
!
!    The parameter TOL, which determines the accuracy of the bisection
!    method, is calculated using NBITS (assuming the final bit is lost
!    due to rounding error).
!
!    N0 is the maximum number of iterations used in the bisection
!    method.
!
!    For XX close to 0 for Wp, the exponential approximation is used.
!    The approximation is exact to O(XX^8) so, depending on the value
!    of NBITS, the range of application of this formula varies. Outside
!    this range, the usual bisection method is used.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    15 June 2014
!
!  Author:
!
!    Original FORTRAN77 version by Andrew Barry, S. J. Barry,
!    Patricia Culligan-Hensley.
!    This FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Andrew Barry, S. J. Barry, Patricia Culligan-Hensley,
!    Algorithm 743: WAPR - A Fortran routine for calculating real
!    values of the W-function,
!    ACM Transactions on Mathematical Software,
!    Volume 21, Number 2, June 1995, pages 172-181.
!
!  Parameters:
!
!    Input, real(kind=WP) :: XX, the argument.
!
!    Input, integer(kind=IP) :: NB, indicates the branch of the W function.
!    0, the upper branch;
!    nonzero, the lower branch.
!
!    Output, integer(kind=IP) :: NER, the error flag.
!    0, success;
!    1, the routine did not converge.  Perhaps reduce NBITS and try again.
!
!    Input, integer(kind=IP) :: L, the offset indicator.
!    1, XX represents the offset of the argument from -exp(-1).
!    not 1, XX is the actual argument.
!
!    Output, real(kind=WP) :: BISECT, the value of W(X), as determined
!
  implicit none

  integer(kind=IP), parameter :: n0 = 500_IP

  real(kind=WP) :: bisect

  !> Argument of Lambert W function
  real(kind=WP), intent(in) :: xx

  !> Indicates solution should be taken from the desired branch:
  !!
  !!    * 0, the upper branch;
  !!    * nonzero, the lower branch.
  integer(kind=IP), intent(in) :: nb

  !> Error flag. Zero indicates success, non-zero indicates an error
  integer(kind=IP), intent(out) :: ner

  !> L indicates the interpretation of X:
  !!
  !!    * 1, X is actually the offset from -(exp-1), so compute W(X-exp(-1)).
  !!    * not 1, X is the argument; compute W(X);
  integer(kind=IP), intent(in) :: l

  real(kind=WP) :: d
  real(kind=WP) :: f
  real(kind=WP) :: fd
  integer(kind=IP) :: i
  real(kind=WP) :: r
  real(kind=WP) :: test
  real(kind=WP) :: tol
  real(kind=WP) :: u
  real(kind=WP) :: x

  bisect = ZERO
  ner = 0

  if ( pm%nbits == 0 ) then
    call pm%init()
  end if

  if ( l == 1 ) then
    x = xx - exp ( -ONE )
  else
    x = xx
  end if

  if ( nb == 0 ) then

    test = ONE / ( TWO ** pm%nbits ) ** ( ONE / SEVEN )

    if ( abs ( x ) < test ) then

      bisect = x &
        * exp ( - x &
        * exp ( - x &
        * exp ( - x &
        * exp ( - x &
        * exp ( - x &
        * exp ( - x ))))))

      return

    else

      u = crude ( x, nb ) + TINY1M3
      tol = abs ( u ) / TWO ** pm%nbits
      d = max ( u - TINY2M3, -ONE )

      do i = 1, n0

        r = HALF * ( u - d )
        bisect = d + r
!
!  Find root using w*exp(w)-x to avoid ln(0) error.
!
        if ( x < exp ( ONE ) ) then

          f = bisect * exp ( bisect ) - x
          fd = d * exp ( d ) - x
!
!  Find root using ln(w/x)+w to avoid overflow error.
!
        else

          f = log ( bisect / x ) + bisect
          fd = log ( d / x ) + d

        end if

        if ( f == ZERO ) then
          return
        end if

        if ( abs ( r ) <= tol ) then
          return
        end if

        if ( ZERO < fd * f ) then
          d = bisect
        else
          u = bisect
        end if

      end do

    end if

  else

    d = crude ( x, nb ) - TINY1M3
    u = min ( d + TINY2M3, -ONE )
    tol = abs ( u ) / TWO ** pm%nbits

    do i = 1, n0

      r = HALF * ( u - d )
      bisect = d + r
      f = bisect * exp ( bisect ) - x

      if ( f == ZERO ) then
        return
      end if

      if ( abs ( r ) <= tol ) then
        return
      end if

      fd = d * exp ( d ) - x

      if ( ZERO < fd * f ) then
        d = bisect
      else
        u = bisect
      end if

    end do

  end if
!
!  The iteration did not converge.
!
  ner = 1

  return
end function bisect

!> A crude approximation for the Lambert W function
function crude ( xx, nb )
!*****************************************************************************80
!
!  CRUDE returns a crude approximation for the W function.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    15 June 2014
!
!  Author:
!
!    Original FORTRAN77 version by Andrew Barry, S. J. Barry,
!    Patricia Culligan-Hensley.
!    This FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Andrew Barry, S. J. Barry, Patricia Culligan-Hensley,
!    Algorithm 743: WAPR - A Fortran routine for calculating real
!    values of the W-function,
!    ACM Transactions on Mathematical Software,
!    Volume 21, Number 2, June 1995, pages 172-181.
!
!  Parameters:
!
!    Input, real(kind=WP) :: XX, the argument.
!
!    Input, integer(kind=IP) :: NB, indicates the desired branch.
!    * 0, the upper branch;
!    * nonzero, the lower branch.
!
!    Output, real(kind=WP) :: CRUDE, the crude approximation to W at XX.
!
  implicit none

  real(kind=WP) :: crude

  !> Argument of Lambert W function
  real(kind=WP), intent(in) :: xx

  !> Indicates solution should be taken from the desired branch:
  !!
  !!    * 0, the upper branch;
  !!    * nonzero, the lower branch.
  integer(kind=IP), intent(in) :: nb

  real(kind=WP) :: an2
  real(kind=WP) :: eta
  real(kind=WP) :: reta
  real(kind=WP) :: t
  real(kind=WP) :: ts
  real(kind=WP) :: zl

  continue

  crude = ZERO
!
!  Crude Wp.
!
  if ( nb == 0 ) then

    if ( xx <= 20.0_WP ) then
      reta = s2 * sqrt ( ONE - xx / em )
      an2 = 4.612634277343749_WP * sqrt ( sqrt ( reta + &
        1.09556884765625_WP ) )
      crude = reta / ( ONE + reta / ( THREE &
        + ( s21 * an2 + s22 ) * reta / ( s23 * ( an2 + reta )))) - ONE
    else
      zl = log ( xx )
      crude = log ( xx / log ( xx &
        / zl ** exp ( -1.124491989777808_WP / &
        ( 0.4225028202459761_WP + zl ))))
    end if

  else
!
!  Crude Wm.
!
    if ( xx <= em9 ) then
      zl = log ( -xx )
      t = -ONE - zl
      ts = sqrt ( t )
      crude = zl - ( TWO * ts ) / ( s2 + ( c13 - t &
        / ( 270.0_WP + ts * 127.0471381349219_WP ) ) * ts )
    else
      zl = log ( -xx )
      eta = TWO - em2 * xx
      crude = log ( xx / log ( - xx / ( ( ONE &
        - 0.5043921323068457_WP * ( zl + ONE ) ) &
        * ( sqrt ( eta ) + eta / THREE ) + ONE ) ) )
     end if

  end if

  return
end function crude

!> Approximate solution to the Lambert W function
function wapr ( x, nb, nerror, l )

!*****************************************************************************80
!
!! WAPR approximates the W function.
!
!  Discussion:
!
!    The call will fail if the input value X is out of range.
!    The range requirement for the upper branch is:
!      -exp(-1) <= X.
!    The range requirement for the lower branch is:
!      -exp(-1) < X < 0.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    15 June 2014
!
!  Author:
!
!    Original FORTRAN77 version by Andrew Barry, S. J. Barry,
!    Patricia Culligan-Hensley.
!    This FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Andrew Barry, S. J. Barry, Patricia Culligan-Hensley,
!    Algorithm 743: WAPR - A Fortran routine for calculating real
!    values of the W-function,
!    ACM Transactions on Mathematical Software,
!    Volume 21, Number 2, June 1995, pages 172-181.
!
!  Parameters:
!
!    Input, real(kind=WP) :: X, the argument.
!
!    Input, integer(kind=IP) :: NB, indicates the desired branch.
!    * 0, the upper branch;
!    * nonzero, the lower branch.
!
!    Output, integer(kind=IP) :: NERROR, the error flag.
!    * 0, successful call.
!    * 1, failure, the input X is out of range.
!
!    Input, integer(kind=IP) :: L, indicates the interpretation of X.
!    * 1, X is actually the offset from -(exp-1), so compute W(X-exp(-1)).
!    * not 1, X is the argument; compute W(X);
!
!    Output, real(kind=WP) :: WAPR, the approximate value of W(X).
!
  use, intrinsic :: ISO_FORTRAN_ENV, only: stderr => ERROR_UNIT
  implicit none

  real(kind=WP) :: wapr

  !> Argument of Lambert W function
  real(kind=WP), intent(in) :: x

  !> Indicates solution should be taken from the desired branch:
  !!
  !!    * 0, the upper branch;
  !!    * nonzero, the lower branch.
  integer(kind=IP), intent(in) :: nb

  !> Error flag. Zero indicates success, non-zero indicates an error
  integer(kind=IP), intent(out) :: nerror

  !> L indicates the interpretation of X:
  !!
  !!    * 1, X is actually the offset from -(exp-1), so compute W(X-exp(-1)).
  !!    * not 1, X is the argument; compute W(X);
  integer(kind=IP), intent(in) :: l

  real(kind=WP) :: an2
  real(kind=WP) :: delx
  real(kind=WP) :: eta
  integer(kind=IP) :: i
  real(kind=WP) :: reta
  real(kind=WP) :: t
  real(kind=WP) :: temp
  real(kind=WP) :: temp2
  real(kind=WP) :: ts
  real(kind=WP) :: xx
  real(kind=WP) :: zl
  real(kind=WP) :: zn

1 format(A)

  continue

  wapr = ZERO
  nerror = 0

  if ( pm%nbits == 0 ) then
    call pm%init()
  end if

  if ( l == 1 ) then

    delx = x

    if ( delx < ZERO ) then
      nerror = 1
      write ( unit=stderr, fmt=1 ) ''
      write ( unit=stderr, fmt=1 ) 'WAPR - Fatal error!'
      write ( unit=stderr, fmt=1 ) '  The offset X is negative.'
      write ( unit=stderr, fmt=1 ) '  It must be nonnegative.'
      stop 1
    end if

    xx = x + em

  else

    if ( x < em ) then
      nerror = 1
      return
    else if ( x == em ) then
      wapr = -ONE
      return
    end if

    xx = x
    delx = xx - em

  end if

  if ( nb == 0 ) then
!
!  Calculations for Wp.
!
    if ( abs ( xx ) <= pm%x0 ) then
      wapr = xx / ( ONE + xx / ( ONE + xx &
        / ( TWO + xx / ( 0.6_WP + 0.34_WP * xx ))))
      return
    else if ( xx <= pm%x1 ) then
      reta = sqrt ( d12 * delx )
      wapr = reta / ( ONE + reta / ( THREE + reta / ( reta &
        / ( an4 + reta / ( reta * an6 + an5 ) ) + an3 ) ) ) &
        - ONE
      return
    else if ( xx <= 20.0_WP ) then
      reta = s2 * sqrt ( ONE - xx / em )
      an2 = 4.612634277343749_WP * sqrt ( sqrt ( reta + &
        1.09556884765625_WP ))
      wapr = reta / ( ONE + reta / ( THREE + ( s21 * an2 &
        + s22 ) * reta / ( s23 * ( an2 + reta )))) - ONE
    else
      zl = log ( xx )
      wapr = log ( xx / log ( xx &
        / zl ** exp ( -1.124491989777808_WP / &
        ( 0.4225028202459761_WP + zl ))))
    end if
!
!  Calculations for Wm.
!
  else

    if ( ZERO <= xx ) then
      nerror = 1
      return
    else if ( xx <= pm%x1 ) then
      reta = sqrt ( d12 * delx )
      wapr = reta / ( reta / ( THREE + reta / ( reta / ( an4 &
        + reta / ( reta * an6 - an5 ) ) - an3 ) ) - ONE ) - ONE
      return
    else if ( xx <= em9 ) then
      zl = log ( -xx )
      t = -ONE - zl
      ts = sqrt ( t )
      wapr = zl - ( TWO * ts ) / ( s2 + ( c13 - t &
        / ( 270.0_WP + ts * 127.0471381349219_WP )) * ts )
    else
      zl = log ( -xx )
      eta = TWO - em2 * xx
      wapr = log ( xx / log ( -xx / ( ( ONE &
        - 0.5043921323068457_WP * ( zl + ONE ) ) &
        * ( sqrt ( eta ) + eta / THREE ) + ONE )))
    end if

  end if

  do i = 1, pm%niter
    zn = log ( xx / wapr ) - wapr
    temp = ONE + wapr
    temp2 = temp + c23 * zn
    temp2 = TWO * temp * temp2
    wapr = wapr * ( ONE + ( zn / temp ) * ( temp2 - zn ) &
      / ( temp2 - TWO * zn ) )
  end do

  return
end function wapr

end module m_lambert_w