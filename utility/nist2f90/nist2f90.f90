module m_textio

  public

contains

!> @brief Return an upper case letter if input is lower case letter
!!
!! The input character is examined. If has an ASCII character in the
!! range from 97 through 122, it is a lower case letter. If it is a
!! lower case letter NN is set equal to the value minus 32. This is the
!! upper case equivalent to the letter. UC is set equal to char(NN)
character(len=1) function UC(A)
  implicit none

  !> Input character
  character(len=1), intent(in) :: A

  integer :: NN

  continue

  NN = ichar(A)

  if ((NN >= 97) .AND. (NN <= 122)) then
    NN = NN - 32
    UC = char(NN)
  else
    UC = A
  end if

  return
end function UC

!> Removes leading and trailing whitespace from a string, i.e. what
!! one expects trim() to do instead of behaving like ltrim()
pure function munch(str)
  implicit none

  character(len=:), allocatable :: munch

  !> String to modify
  character(len=*), intent(in) :: str

  continue

  munch = trim(adjustl(str))

  return
end function munch

!> Removes space from a string
pure function despace(str)
  implicit none

  character(len=:), allocatable :: despace

  !> String to modify
  character(len=*), intent(in) :: str

  character(len=:), allocatable :: tmpstr
  character(len=1) :: ch
  integer :: nchars
  integer :: ipos
  integer :: opos

  continue

  tmpstr = munch(str)
  nchars = len_trim(tmpstr)
  if (nchars == 0) then
    despace = tmpstr
  else
    despace = repeat(' ', nchars)
    opos = 0
    do ipos = 1, nchars
      ch = tmpstr(ipos:ipos)
      if (ch /= ' ') then
        opos = opos + 1
        despace(opos:opos) = ch
      end if
    end do
    despace = trim(despace)
  end if

  return
end function despace
end module m_textio

!> Convert NIST physical constant data text files to Fortran source code
program nist2f90
  use, intrinsic :: ISO_FORTRAN_ENV, only: stdin => INPUT_UNIT,         &
    stdout => OUTPUT_UNIT, REAL128
  use m_textio
  implicit none

  character(len=*), parameter :: VARCHARS =                             &
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890_'
  character(len=*), parameter :: HRULE    = '----------'
  character(len=*), parameter :: ELLIPSES = '...'

  integer :: ioerr
  character(len=80) :: errmsg

  character(len=125) :: card

  character(len=60) :: label
  character(len=25) :: tvalue
  character(len=25) :: teps
  character(len=15) :: units

  character(len=80) :: desc
  character(len=:), allocatable :: rdesc
  character(len=:), allocatable :: runits
  character(len=:), allocatable :: rvalue
  character(len=60) :: varname
  character(len=132), dimension(:), allocatable :: dcard

  logical :: data_found

  integer :: labellen
  integer :: varlen
  integer :: maxvarlen
  integer :: ilpos
  integer :: nderived
  integer :: nvars
  integer :: dctr
  integer :: doff

  real(kind=REAL128) :: fvalue

  logical :: defer_derived

1 format(A)
2 format(ES44.35E4) ! +1.000000000000000000000000000000000000E+0000
31 format('real(kind=R16), parameter, public :: ', T73, '&')
32 format('  ', A, '_R16 =', T73, '&')
33 format('  ', A, '_R16')
61 format('real(kind=', A, '), parameter, public :: ', T73, '&')
62 format('  ', A, '_', A, ' =', T73, '&')
63 format('  real(', A, '_R16, kind=', A, ')')
41 format('! real(kind=R16), parameter, public :: ', T75, '&')
42 format('!   ', A, '_R16 =', T75, '&')
43 format('!   ', A, '_R16 ! derived')
51 format('! real(kind=', A, '), parameter, public :: ', T75, '&')
52 format('!   ', A, '_', A, ' =', T75, '&')
53 format('!   real(', A, '_R16, kind=', A, ') ! derived')
6 format(A, I6)

  continue

  defer_derived = .true.
  if (defer_derived) then
    allocate(dcard(62 * 18))
    dcard = ''
  end if
  doff = 0
  nvars = 0
  maxvarlen = 0
  nderived = 0
  data_found = .false.
  ioerr = 0
  do while (ioerr == 0)
    read(unit=stdin, fmt=1, iostat=ioerr, iomsg=errmsg) card
    if (ioerr /= 0) then
      exit
    end if

    if (data_found) then
      nvars = nvars + 1

      label  = card(1:60)
      tvalue = card(61:85)
      teps   = card(86:110)
      units  = card(111:125)

      if (index(string=tvalue, substring=ELLIPSES) > 0) then
        ! This is a derived value
        fvalue = 0.0_REAL128
        rvalue = munch(tvalue)
      else
        ! This is a literal value
        read(tvalue, fmt=2) fvalue
        rvalue = despace(tvalue)
      end if

      runits = trim(adjustl(units))
      if (len(runits) <= 0) then
        runits = 'dimensionless'
      end if

      labellen = len_trim(label)
      desc = '!> ' // UC(label(1:1)) // label(2:labellen) // ', '       &
        // runits

      varname = trim(adjustl(label))
      varlen = len_trim(varname)
      maxvarlen = max(varlen, maxvarlen)

      ilpos = verify(string=varname(1:varlen), set=VARCHARS)
      do while (ilpos > 0)
        varname(ilpos:ilpos) = '_'
        ilpos = verify(string=varname(1:varlen), set=VARCHARS)
      end do

      rdesc = trim(adjustl(desc))
      if (fvalue == 0.0_REAL128) then
        nderived = nderived + 1

        if (defer_derived) then
          ! doff = (nderived - 1) * 18
          write(unit=dcard(doff+1), fmt=1) '! ' // rdesc
          write(unit=dcard(doff+2), fmt=1) '! !! (at most 33 significant digits)'
          write(unit=dcard(doff+3), fmt=41)
          write(unit=dcard(doff+4), fmt=42) varname(1:varlen)
          write(unit=dcard(doff+5), fmt=43) rvalue
          write(unit=dcard(doff+6), fmt=*)

          doff = doff + 6

          write(unit=dcard(doff+1), fmt=1) '! ' // rdesc
          write(unit=dcard(doff+2), fmt=1) '! !! (at most 15 significant digits)'
          write(unit=dcard(doff+3), fmt=51) 'R8'
          write(unit=dcard(doff+4), fmt=52) varname(1:varlen), 'R8'
          write(unit=dcard(doff+5), fmt=53) varname(1:varlen), 'R8'
          write(unit=dcard(doff+6), fmt=*)

          doff = doff + 6

          write(unit=dcard(doff+1), fmt=1) '! ' // rdesc
          write(unit=dcard(doff+2), fmt=1) '! !! (at most 6 significant digits)'
          write(unit=dcard(doff+3), fmt=51) 'R4'
          write(unit=dcard(doff+4), fmt=52) varname(1:varlen), 'R4'
          write(unit=dcard(doff+5), fmt=53) varname(1:varlen), 'R4'
          write(unit=dcard(doff+6), fmt=*)

          doff = doff + 6
        else
          write(unit=stdout, fmt=1) '! ' // rdesc
          write(unit=stdout, fmt=1) '! !! (at most 33 significant digits)'
          write(unit=stdout, fmt=41)
          write(unit=stdout, fmt=42) varname(1:varlen)
          write(unit=stdout, fmt=43) rvalue
          write(unit=stdout, fmt=*)

          write(unit=stdout, fmt=1) '! ' // rdesc
          write(unit=stdout, fmt=1) '! !! (at most 15 significant digits)'
          write(unit=stdout, fmt=51) 'R8'
          write(unit=stdout, fmt=52) varname(1:varlen), 'R8'
          write(unit=stdout, fmt=53) varname(1:varlen), 'R8'
          write(unit=stdout, fmt=*)

          write(unit=stdout, fmt=1) '! ' // rdesc
          write(unit=stdout, fmt=1) '! !! (at most 6 significant digits)'
          write(unit=stdout, fmt=51) 'R4'
          write(unit=stdout, fmt=52) varname(1:varlen), 'R4'
          write(unit=stdout, fmt=53) varname(1:varlen), 'R4'
          write(unit=stdout, fmt=*)
        end if
      else
        write(unit=stdout, fmt=1) rdesc
        write(unit=stdout, fmt=1) '!! (at most 33 significant digits)'
        write(unit=stdout, fmt=31)
        write(unit=stdout, fmt=32) varname(1:varlen)
        write(unit=stdout, fmt=33) rvalue
        write(unit=stdout, fmt=*)

        write(unit=stdout, fmt=1) rdesc
        write(unit=stdout, fmt=1) '!! (at most 15 significant digits)'
        write(unit=stdout, fmt=61) 'R8'
        write(unit=stdout, fmt=62) varname(1:varlen), 'R8'
        write(unit=stdout, fmt=63) varname(1:varlen), 'R8'
        write(unit=stdout, fmt=*)

        write(unit=stdout, fmt=1) rdesc
        write(unit=stdout, fmt=1) '!! (at most 6 significant digits)'
        write(unit=stdout, fmt=61) 'R4'
        write(unit=stdout, fmt=62) varname(1:varlen), 'R4'
        write(unit=stdout, fmt=63) varname(1:varlen), 'R4'
        write(unit=stdout, fmt=*)
      end if

    else
      data_found = (card(1:10) == HRULE)
    end if
  end do

  if (defer_derived) then
    ! write(unit=stdout, fmt=*)
    write(unit=stdout, fmt=1) '! Derived constants'
    ! write(unit=stdout, fmt=6) '!* Writing dcards from 1 to ', doff
    write(unit=stdout, fmt=*)

    do dctr = 1, doff
      write(unit=stdout, fmt=1) trim(dcard(dctr))
    end do

    if (allocated(dcard)) then
      deallocate(dcard)
    end if
  end if

  write(unit=stdout, fmt=1) '! File statistics:'
  write(unit=stdout, fmt=6) '! Maximum variable length = ', maxvarlen
  write(unit=stdout, fmt=6) '! Literal parameters      = ', nvars - nderived
  write(unit=stdout, fmt=6) '! Derived parameters      = ', nderived
  write(unit=stdout, fmt=6) '! Total parameters        = ', nvars

end program nist2f90