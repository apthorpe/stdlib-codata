!> Check CODATA derived constants
program check_codata
  use STDLIB_CODATA_2018, only: check_2018 => check
  use STDLIB_CODATA_2014, only: check_2014 => check
  use STDLIB_CODATA_2010, only: check_2010 => check
  use STDLIB_CODATA_2006, only: check_2006 => check
  use STDLIB_CODATA_2002, only: check_2002 => check
  use STDLIB_CODATA_1998, only: check_1998 => check
  use STDLIB_CODATA_1986, only: check_1986 => check
  use STDLIB_CODATA_1973, only: check_1973 => check
  use STDLIB_CODATA_1969, only: check_1969 => check
  implicit none

  continue

  call check_2018()
  call check_2014()
  call check_2010()
  call check_2006()
  call check_2002()
  call check_1998()
  call check_1986()
  call check_1973()
  call check_1969()

end program check_codata