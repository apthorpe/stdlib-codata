#!/usr/bin/zsh
foreach yn (1969 1973 1986 1998 2002 2006 2010 2014 2018) {
    echo $yn ;
    grep '^[^!]*_R\(4\|8\|16\)' STDLIB_CODATA_$yn.f90  | \
    sed 's/^.*[^A-Za-z0-9_]\([a-zA-Z_][A-Za-z0-9_]*_R\(4\|8\|16\)\).*/\1/' | \
    grep -v '\.' | \
    grep '[a-zA-QS-Z]' | \
    sort -u  | \
    grep _R16 >| $yn.txt
}

