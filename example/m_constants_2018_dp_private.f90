!>
module m_constants_2018_dp_private
    use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL64
    use stdlib_codata_2018, only:                                           &
      speed_of_light_in_vacuum_R8,                                          &
      Boltzmann_constant_R8,                                                &
      reduced_Planck_constant_R8,                                           &
      molar_gas_constant_R8,                                                &
      Avogadro_constant_R8,                                                 &
      standard_atmosphere_R8,                                               &
      standard_acceleration_of_gravity_R8

      ! Set permissions to default private; does not expose imports (WP)
      private

      ! standard_acceleration_of_gravity_R8 does not exist in CODATA 2006
      ! but does exist in CODATA 2018. Import it from CODATA 2018 module

      !> (Earth) gravitational acceleration, m / s**2
      real(kind=WP), parameter, public :: GRAV =                            &
        standard_acceleration_of_gravity_R8

      !> Speed of light in vacuum, m / s
      real(kind=WP), parameter, public :: C    = speed_of_light_in_vacuum_R8

      !> Boltzmann constant, J / k
      real(kind=WP), parameter, public :: K    = Boltzmann_constant_R8

      !> Planck constant divided by two pi, J / Hz
      real(kind=WP), parameter, public :: HBAR = reduced_Planck_constant_R8

      !> Universal (molar) gas constant, J / mol K
      real(kind=WP), parameter, public :: RGAS = molar_gas_constant_R8

      !> Avogadro's number, elements / mol
      real(kind=WP), parameter, public :: NA   = Avogadro_constant_R8

      !> Standard atmospheric pressure, 101325 Pa
      real(kind=WP), parameter, public :: PATM = standard_atmosphere_R8
    ! contains

    end module m_constants_2018_dp_private