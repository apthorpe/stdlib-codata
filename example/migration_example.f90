!> Single precision routine using the 1973 CODATA constants
subroutine chem73()
  use, intrinsic :: ISO_FORTRAN_ENV, only: stdout => OUTPUT_UNIT
  use m_constants_1973_sp
  implicit none
  ! C    => speed_of_light_in_vacuum_R8,                                  &
  ! K    => Boltzmann_constant_R8,                                        &
  ! HBAR => reduced_Planck_constant_R8,                                   &
  ! RGAS => molar_gas_constant_R8,                                        &
  ! NA   => Avogadro_constant_R8,                                         &
  ! PATM => standard_atmosphere_R8
  ! GRAV => standard_acceleration_of_gravity_R8

  real(kind=WP) :: m_fuel
  real(kind=WP) :: m_fp
  real(kind=WP) :: dm
  real(kind=WP) :: e_fission
  real(kind=WP) :: dv_coolant
  real(kind=WP) :: m_coolant
  real(kind=WP) :: mw_coolant
  real(kind=WP) :: cp_coolant
  real(kind=WP) :: p_coolant
  real(kind=WP) :: dt
  real(kind=WP) :: t_in
  real(kind=WP) :: t_out

  continue

  m_fuel = 5.0E-9_WP
  m_fp = 4.997E-9_WP

  mw_coolant  = 22.98976928_WP
  p_coolant = 5.0_WP * PATM
  cp_coolant = 60.0_WP
  t_in = 1400.0_WP
  m_coolant = 850.0_WP

  dm = m_fuel - m_fp
  e_fission = dm * C**2

  dt = (e_fission * mw_coolant) / (m_coolant * cp_coolant)
  t_out = dt + t_in
  dv_coolant = m_coolant / mw_coolant * RGAS * dt / p_coolant

  write(unit=stdout, fmt='("   PROJECT TUDBALL: REACTOR PARAMETERS")')
  write(unit=stdout, fmt='("Fission energy, J        ", ES18.11)') e_fission
  write(unit=stdout, fmt='("Inlet pressure, Pa       ", ES18.11)') p_coolant
  write(unit=stdout, fmt='("Temperature rise, K      ", ES18.11)') dt
  write(unit=stdout, fmt='("Hot leg temperature, K   ", ES18.11)') t_out
  write(unit=stdout, fmt='("Expansion, m**3          ", ES18.11)') dv_coolant
  write(unit=stdout, fmt='("Coolant launch weight, N ", ES18.11)') m_coolant * GRAV
  write(unit=stdout, fmt='("Inlet frequency, Hz      ", ES18.11)') K * t_in / HBAR
  write(unit=stdout, fmt='("Outlet frequency, Hz     ", ES18.11)') K * t_out / HBAR

  write(unit=stdout, fmt='("RGAS check          1 =? ", ES18.11)') RGAS / (K * NA)

  return
end subroutine chem73

!> Double precision routine using the 2006 CODATA constants
subroutine chem06a()
  use, intrinsic :: ISO_FORTRAN_ENV, only: stdout => OUTPUT_UNIT
  use m_constants_2006_dp

  implicit none

  real(kind=WP) :: m_fuel
  real(kind=WP) :: m_fp
  real(kind=WP) :: dm
  real(kind=WP) :: e_fission
  real(kind=WP) :: dv_coolant
  real(kind=WP) :: m_coolant
  real(kind=WP) :: mw_coolant
  real(kind=WP) :: cp_coolant
  real(kind=WP) :: p_coolant
  real(kind=WP) :: dt
  real(kind=WP) :: t_in
  real(kind=WP) :: t_out

  continue

  m_fuel = 5.0E-9_WP
  m_fp = 4.997E-9_WP

  mw_coolant  = 22.98976928_WP
  p_coolant = 5.0_WP * PATM
  cp_coolant = 60.0_WP
  t_in = 1400.0_WP
  m_coolant = 850.0_WP

  dm = m_fuel - m_fp
  e_fission = dm * C**2

  dt = (e_fission * mw_coolant) / (m_coolant * cp_coolant)
  t_out = dt + t_in
  dv_coolant = m_coolant / mw_coolant * RGAS * dt / p_coolant

  write(unit=stdout, fmt='("    PROJECT TUDBALL: REACTOR PARAMETERS")')
  write(unit=stdout, fmt='("Fission energy, J        ", ES18.11)') e_fission
  write(unit=stdout, fmt='("Inlet pressure, Pa       ", ES18.11)') p_coolant
  write(unit=stdout, fmt='("Temperature rise, K      ", ES18.11)') dt
  write(unit=stdout, fmt='("Hot leg temperature, K   ", ES18.11)') t_out
  write(unit=stdout, fmt='("Expansion, m**3          ", ES18.11)') dv_coolant
  write(unit=stdout, fmt='("Coolant launch weight, N ", ES18.11)') m_coolant * GRAV
  write(unit=stdout, fmt='("Inlet frequency, Hz      ", ES18.11)') K * t_in / HBAR
  write(unit=stdout, fmt='("Outlet frequency, Hz     ", ES18.11)') K * t_out / HBAR

  write(unit=stdout, fmt='("RGAS check          1 =? ", ES18.11)') RGAS / (K * NA)

  return
end subroutine chem06a

!> Double precision routine using the 2006 CODATA constants
!! (no import leakage)
subroutine chem06b()
  use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL64,                &
    stdout => OUTPUT_UNIT
  use m_constants_2006_dp_private

  implicit none

  real(kind=WP) :: m_fuel
  real(kind=WP) :: m_fp
  real(kind=WP) :: dm
  real(kind=WP) :: e_fission
  real(kind=WP) :: dv_coolant
  real(kind=WP) :: m_coolant
  real(kind=WP) :: mw_coolant
  real(kind=WP) :: cp_coolant
  real(kind=WP) :: p_coolant
  real(kind=WP) :: dt
  real(kind=WP) :: t_in
  real(kind=WP) :: t_out

  continue

  m_fuel = 5.0E-9_WP
  m_fp = 4.997E-9_WP

  mw_coolant  = 22.98976928_WP
  p_coolant = 5.0_WP * PATM
  cp_coolant = 60.0_WP
  t_in = 1400.0_WP
  m_coolant = 850.0_WP

  dm = m_fuel - m_fp
  e_fission = dm * C**2

  dt = (e_fission * mw_coolant) / (m_coolant * cp_coolant)
  t_out = dt + t_in
  dv_coolant = m_coolant / mw_coolant * RGAS * dt / p_coolant

  write(unit=stdout, fmt='("    PROJECT TUDBALL: REACTOR PARAMETERS")')
  write(unit=stdout, fmt='("Fission energy, J        ", ES18.11)') e_fission
  write(unit=stdout, fmt='("Inlet pressure, Pa       ", ES18.11)') p_coolant
  write(unit=stdout, fmt='("Temperature rise, K      ", ES18.11)') dt
  write(unit=stdout, fmt='("Hot leg temperature, K   ", ES18.11)') t_out
  write(unit=stdout, fmt='("Expansion, m**3          ", ES18.11)') dv_coolant
  write(unit=stdout, fmt='("Coolant launch weight, N ", ES18.11)') m_coolant * GRAV
  write(unit=stdout, fmt='("Inlet frequency, Hz      ", ES18.11)') K * t_in / HBAR
  write(unit=stdout, fmt='("Outlet frequency, Hz     ", ES18.11)') K * t_out / HBAR

  write(unit=stdout, fmt='("RGAS check          1 =? ", ES18.11)') RGAS / (K * NA)

  return
end subroutine chem06b

!> Double precision routine using the 2018 CODATA constants
!! (no import leakage)
subroutine chem18()
  use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL64,                &
    stdout => OUTPUT_UNIT
  use m_constants_2018_dp_private

  implicit none

  real(kind=WP) :: m_fuel
  real(kind=WP) :: m_fp
  real(kind=WP) :: dm
  real(kind=WP) :: e_fission
  real(kind=WP) :: dv_coolant
  real(kind=WP) :: m_coolant
  real(kind=WP) :: mw_coolant
  real(kind=WP) :: cp_coolant
  real(kind=WP) :: p_coolant
  real(kind=WP) :: dt
  real(kind=WP) :: t_in
  real(kind=WP) :: t_out

  continue

  m_fuel = 5.0E-9_WP
  m_fp = 4.997E-9_WP

  mw_coolant  = 22.98976928_WP
  p_coolant = 5.0_WP * PATM
  cp_coolant = 60.0_WP
  t_in = 1400.0_WP
  m_coolant = 850.0_WP

  dm = m_fuel - m_fp
  e_fission = dm * C**2

  dt = (e_fission * mw_coolant) / (m_coolant * cp_coolant)
  t_out = dt + t_in
  dv_coolant = m_coolant / mw_coolant * RGAS * dt / p_coolant

  write(unit=stdout, fmt='("    PROJECT TUDBALL: REACTOR PARAMETERS")')
  write(unit=stdout, fmt='("Fission energy, J        ", ES18.11)') e_fission
  write(unit=stdout, fmt='("Inlet pressure, Pa       ", ES18.11)') p_coolant
  write(unit=stdout, fmt='("Temperature rise, K      ", ES18.11)') dt
  write(unit=stdout, fmt='("Hot leg temperature, K   ", ES18.11)') t_out
  write(unit=stdout, fmt='("Expansion, m**3          ", ES18.11)') dv_coolant
  write(unit=stdout, fmt='("Coolant launch weight, N ", ES18.11)') m_coolant * GRAV
  write(unit=stdout, fmt='("Inlet frequency, Hz      ", ES18.11)') K * t_in / HBAR
  write(unit=stdout, fmt='("Outlet frequency, Hz     ", ES18.11)') K * t_out / HBAR

  write(unit=stdout, fmt='("RGAS check          1 =? ", ES18.11)') RGAS / (K * NA)

  return
end subroutine chem18

!>
program migration
  use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL64,                &
    stdout => OUTPUT_UNIT
  implicit none
  continue

  write(unit=stdout, fmt='("       *** TUREPA, Version 1973 ***")')
  call chem73()
  write(unit=stdout, fmt=*)

  write(unit=stdout, fmt='("       *** TUREPA, Version 2006a ***")')
  call chem06a()
  write(unit=stdout, fmt=*)

  write(unit=stdout, fmt='("       *** TUREPA, Version 2006b ***")')
  call chem06b()
  write(unit=stdout, fmt=*)

  write(unit=stdout, fmt='("  *** Visual TUREPA Pro, Version 2018 ***")')
  call chem18()

end program migration