!>
module m_constants_2006_dp
use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL64
use stdlib_codata_2006, only:                                           &
  C    => speed_of_light_in_vacuum_R8,                                  &
  K    => Boltzmann_constant_R8,                                        &
  HBAR => reduced_Planck_constant_R8,                                   &
  RGAS => molar_gas_constant_R8,                                        &
  NA   => Avogadro_constant_R8,                                         &
  PATM => standard_atmosphere_R8

  ! Define (and export) aliases for actual physical constants in CODATA
  ! module

  ! standard_acceleration_of_gravity_R8 does not exist in CODATA 2006
  ! We define it locally

  !> (Earth) gravitational acceleration, m/s**2
  real(kind=WP), parameter, public :: GRAV = 9.80665_WP

! contains

end module m_constants_2006_dp