!>
module m_constants_1973_sp
use, intrinsic :: ISO_FORTRAN_ENV, only: WP => REAL32
use stdlib_codata_1973, only:                                           &
  C    => speed_of_light_in_vacuum_R4,                                  &
  K    => Boltzmann_constant_R4,                                        &
  HBAR => reduced_Planck_constant_R4,                                   &
  RGAS => molar_gas_constant_R4,                                        &
  NA   => Avogadro_constant_R4

  ! Define (and export) aliases for actual physical constants in CODATA
  ! module

  ! Neither standard_atmosphere_R8 nor
  ! standard_acceleration_of_gravity_R8 are defined in CODATA 1973
  ! We define them locally.

  !> (Earth) gravitational acceleration, m/s**2
  real(kind=WP), parameter, public :: GRAV = 9.80665_WP

  !> Standard atmospheric pressure, 101325 Pa
  real(kind=WP), parameter, public :: PATM = 101325.0_WP

! contains

end module m_constants_1973_sp